<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'paypal' => [
        'client_id' => 'AWDUPF6_dfeafw-hU9ywfifKaZwYd9AQUinUOo4l9fKIsEV0H2rQ-wCach8XE76-mJIcLXoz3ytFSBJV',
        'secret' => 'EC8tfnx_yH3PrhVMQqDl5MvHxGV4U-4bus-Y0O_xAArQ-s738MFHeNxPl6a54vzuGOfPLiMnYXMPWoag'
    ],

    'nexmo' => [
      'key' => env('NEXMO_KEY'),
      'secret' => env('NEXMO_SECRET'),
      'sms_from' => '+393313567032',
    ],

];
