<?php

return [
    "vendorId"=> env('MYALLOCATOR_VENDOR_ID'),
    "vendorPassword"=> env('MYALLOCATOR_VENDOR_PASSWORD'),
    "userToken"=> env('MYALLOCATOR_USER_TOKEN')
];