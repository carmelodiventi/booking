<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationPayments extends Model
{
    //
    //protected $connection = 'property';
    public static function getSum($reservation_id)
    {
        ReservationPayments::where('reservation_id', $reservation_id)->sum('total');
    }
}
