<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;

class ReservationsHistory extends Model
{
    protected $table = 'reservations_history';
    /**
    * Scope a query to only include property.
    *
    * @param \Illuminate\Database\Eloquent\Builder $query
    * @return \Illuminate\Database\Eloquent\Builder
    */
    public function scopeProperty($query)
    {
        return $query->where('reservations.property_id', '=', Helper::get_property());
    }
}
