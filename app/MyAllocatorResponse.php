<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyAllocatorResponse extends Model
{
    //
    protected $table = "myallocator_response";
}
