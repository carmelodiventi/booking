<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Booking;
use Omnipay\Omnipay;
use Illuminate\Http\Request;

class PaymentsController extends Controller
{
    //

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->property = $request->property;
    }

    public function paypal()
    {
        $params = array(
            'cancelUrl' => url($this->property . '/payment-cancel'),
            'returnUrl' => url($this->property . '/payment-complete'),
            'description' => 'Reservation PNR:' . $this->request->session()->get('pnr') . '',
            'amount' => $this->request->session()->get('grandtotal'),
        );

        session()->put('paypal_params', $params); // here you save the params to the session so you can use them later.
        session()->save();

        $gateway = Omnipay::create('PayPal_Express');
        $gateway->setUsername(Helper::get_setting('paypal')->api_username);
        $gateway->setPassword(Helper::get_setting('paypal')->api_password);
        $gateway->setSignature(Helper::get_setting('paypal')->api_signature);
        $gateway->setTestMode(Helper::get_setting('paypal')->test);
        $gateway->setCurrency(Helper::get_setting('currency')->code);
        $response = $gateway->purchase($params)->send();

        if ($response->isRedirect()) {
            // redirect to offsite payment gateway
            $response->redirect();
        } else {
            return back()->with('message_error', $response->getMessage());
        }
    }

    public function paymentComplete()
    {
        $gateway = Omnipay::create('PayPal_Express');
        $gateway->setUsername(Helper::get_setting('paypal')->api_username);
        $gateway->setPassword(Helper::get_setting('paypal')->api_password);
        $gateway->setSignature(Helper::get_setting('paypal')->api_signature);
        $gateway->setTestMode(Helper::get_setting('paypal')->test);
        $gateway->setCurrency(Helper::get_setting('currency')->code);
        $params = session()->get('paypal_params');
        $response = $gateway->completePurchase($params)->send();
        $paypalResponse = $response->getData(); // this is the raw response object

        if (isset($paypalResponse['PAYMENTINFO_0_ACK']) && $paypalResponse['PAYMENTINFO_0_ACK'] === 'Success') {
            $data = [
                'property_id' => $this->property,
                'hotel' => Helper::get_setting('hotel_data'),
                'pnr' => $this->request->session()->get('pnr'),
                'date' => date('Y-m-d H:i:s'),
                'check_in' => $this->request->session()->get('check_in'),
                'check_out' => $this->request->session()->get('check_out'),
                'nights' => $this->request->session()->get('nights'),
                'customer' => $this->request->session()->get('customer'),
                'rooms' => $this->request->session()->get('rooms'),
                'offert' =>  $this->request->session()->get('offert'),
                'fees' => $this->request->session()->get('fees'),
                'note' => $this->request->session()->get('note'),
                'payment_selected' => $this->request->session()->get('payment_selected'),
                'payment' => array(
                    'transaction_id' => $paypalResponse['PAYMENTINFO_0_TRANSACTIONID'],
                    'casual' => Helper::get_setting('hotel_data')->name . ' - Reservation PNR :' . $this->request->session()->get('pnr'),
                    'method' => $paypalResponse['PAYMENTINFO_0_TRANSACTIONTYPE'],
                    'payment_type' => 'payment',
                    'payment_date' => $paypalResponse['PAYMENTINFO_0_ORDERTIME'],
                    'fee' => $paypalResponse['PAYMENTINFO_0_FEEAMT'],
                    'total' => $paypalResponse['PAYMENTINFO_0_AMT'],
                    'status' => 'Welded',
                ),
                'discount' => $this->request->session()->get('discount'),
                'totaltax' => $this->request->session()->get('totaltax'),
                'subtotal' => $this->request->session()->get('subtotal'),
                'grandtotal' => $this->request->session()->get('grandtotal'),
                'status' => 'SLD'
            ];


            $res_booking = Booking::putReservation($data);

            if ($res_booking->Success) {
                // show ticket
                return redirect($this->property . '/ticket');
            } else {
                // show errors
                return back()->withErrors($res_booking->Errors);
            }
        } else {
            $this->request->session()->flush();
            return view('payment_cancel');
        }
    }

    public function paymentCancel()
    {
        $this->request->session()->flush();
        return view('payment_cancel')->with(['property_id' => $this->property]);
    }


    public function stripe()
    {
        if (!empty($this->request->session()->get('stripeToken'))) {
            $stripeToken = $this->request->session()->get('stripeToken');
            $gateway = Omnipay::create('Stripe');
            $gateway->setApiKey(Helper::get_setting('stripe')->secret_key);

            if (Helper::get_refundable($this->request->session()->get('rooms'))) {
                $customer = $gateway->createCustomer([
                    'email' => $this->request->session()->get('customer')['email'],
                    'description' => 'Reservation PNR:' . $this->request->session()->get('pnr')
                ])->send();

                if ($customer->isRedirect()) {
                    // redirect to offsite payment gateway
                    return response()->json($response);
                    //$response->redirect();
                } elseif ($customer->isSuccessful()) {
                    // payment was successful: update database
                    $customer_id = $customer->getData()['id'];
                }

                if (isset($customer_id)) {
                    $response = $gateway->createCard([
                        'amount' => $this->request->session()->get('grandtotal'),
                        'currency' => Helper::get_setting('currency')->code,
                        'token' => $stripeToken,
                        'description' => 'Reservation PNR:' . $this->request->session()->get('pnr'),
                        'customerReference' => $customer_id,
                        'metadata' => ['customerName' => $this->request->session()->get('customer')->name, 'customerSurname' => $this->request->session()->get('customer')->surname],
                    ])->send();
                }
            } else {

                $response = $gateway->authorize([
                    'amount' => $this->request->session()->get('grandtotal'),
                    'currency' => Helper::get_setting('currency')->code,
                    'token' => $stripeToken,
                    'description' => 'Reservation PNR:' . $this->request->session()->get('pnr'),
                    'metadata' => ['customerName' => $this->request->session()->get('customer')->name, 'customerSurname' => $this->request->session()->get('customer')->surname],
                ])->send();
            }


            if ($response->isRedirect()) {
                // redirect to offsite payment gateway
                return response()->json($response);
                //$response->redirect();
            } elseif ($response->isSuccessful()) {
                // payment was successful: make data array

                $data = [
                    'property_id' => $this->property,
                    'hotel' => Helper::get_setting('hotel_data'),
                    'pnr' => $this->request->session()->get('pnr'),
                    'date' => date('Y-m-d H:i:s'),
                    'check_in' => $this->request->session()->get('check_in'),
                    'check_out' => $this->request->session()->get('check_out'),
                    'nights' => $this->request->session()->get('nights'),
                    'customer' => $this->request->session()->get('customer'),
                    'rooms' => $this->request->session()->get('rooms'),
                    'offert' =>  $this->request->session()->get('offert'),
                    'fees' => $this->request->session()->get('fees'),
                    'note' => $this->request->session()->get('note'),
                    'payment_selected' => $this->request->session()->get('payment_selected'),
                    'discount' => $this->request->session()->get('discount'),
                    'totaltax' => $this->request->session()->get('totaltax'),
                    'subtotal' => $this->request->session()->get('subtotal'),
                    'grandtotal' => $this->request->session()->get('grandtotal'),
                    'status' => 'CNF'
                ];

                if ($data_response = $response->getData()) {

                    if ($data_response['object'] == 'card') {

                        $data['credit_card'] = [
                            'id' => $data_response['id'],
                            'name' => $data_response['name'],
                            'number' => $data_response['last4'],
                            'expiryMonth' => $data_response['exp_month'],
                            'expiryYear' => $data_response['exp_year'],
                            'brand' => $data_response['brand'],
                            'country' => $data_response['country']
                        ];
                    } elseif ($data_response['object'] == 'charge') {

                        $data['payment'] = [
                            'transaction_id' => $data_response['id'],
                            'casual' => $data_response['description'],
                            'method' => 'credit_card',
                            'payment_type' => 'payment',
                            'payment_date' => date('Y-m-d H:s:i', $data_response['created']),
                            'fee' =>  $data_response['application_fee'],
                            'total' =>  $data_response['amount'],
                            'status' => 'On Hold'
                        ];

                        $data['credit_card'] = [
                            'id' => $data_response['source']['id'],
                            'name' => $data_response['source']['name'],
                            'number' => $data_response['source']['last4'],
                            'expiryMonth' => $data_response['source']['exp_month'],
                            'expiryYear' => $data_response['source']['exp_year'],
                            'brand' => $data_response['source']['brand'],
                            'country' => $data_response['source']['country']
                        ];
                    }
                }

                $res_booking = Booking::putReservation($data);

                if ($res_booking->Success) {
                    // show ticket
                    return redirect($this->property . '/ticket');
                } else {
                    // show errors
                    return back()->withErrors($res_booking->Errors);
                }
            } else {
                // payment failed: display message to customer
                return back()->withErrors($response->getMessage());
            }
        } else {
            return redirect($this->property . '/rooms-details')->with('message_error', 'stripeToken required');
        }
    }
}
