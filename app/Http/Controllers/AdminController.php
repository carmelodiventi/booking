<?php

namespace App\Http\Controllers;

use Auth;
use Redirect;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    //

    public function dashboard()
    {
        return view('admin.dashboard');
    }

    public function planning(Request $request)
    {
    
        $rooms = \App\Rooms::leftJoin('rooms_type', 'rooms_type.id', '=', 'rooms.rooms_type_id')
        ->get(['rooms.id','rooms.rooms_type_id','name','type','ordinal'])->sortBy("ordinal");
        $rooms_type = \App\RoomsType::where('isDisabled', '<>', '1')
        ->orderBy('capacity', 'ASC')->get(['id','ma_room_id','type','capacity','units']);

        $master_rate = \App\RatesType::where('master_rate', 1)->first();
        $master_rate = $master_rate ? $master_rate->id : null;

        if ($request->get('interval')) {
            $interval = strtotime($request->get('interval'));
            $start = date('Y-m-1', strtotime($request->get('interval')));
            $end = date('Y-m-d', strtotime('+45 days', strtotime($request->get('interval'))));
        } else {
            $interval = strtotime('now');
            $start = date('Y-m-d', $interval);
            $end = date('Y-m-d', strtotime('+45 days', $interval));
        }
        return view('admin.planning', [ 'rooms_type' => $rooms_type, 'rooms' => $rooms, 'master_rate' => $master_rate, 'interval' => $interval]);
    }

    public function profile()
    {
        $user = \App\User::findOrFail(Auth::user()->id);
        return view('admin.profile', ['user'=>$user]);
    }
}
