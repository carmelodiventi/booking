<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservations;
use App\ReservationPayments;
use App\ReservationAdjustments;

class ReservationPaymentsController extends Controller
{
    //
    public function delete($id, $reservation_id)
    {
        $payment = ReservationPayments::findOrFail($id);
        $payment->delete();
        
        $reservation = Reservations::findOrFail($reservation_id);
        $reservation->status = 'CNF';
        $reservation->save();
        return back()->with('message_success', trans('admin.payment_cancelled'));
    }

    public function confirm($id, $reservation_id)
    {
        $payment = ReservationPayments::findOrFail($id);
        $payment->status= 'Welded';
        $payment->save();

        $reservation = Reservations::findOrFail($reservation_id);
        $payments = ReservationPayments::groupBy('reservation_id')->selectRaw('sum(total) as sum')
        ->where('reservation_id', $reservation_id)
        ->where('status', 'Welded')
        ->first();
        $payments_sum = 0.00;
        if ($payments) {
            $payments_sum = $payments->sum;
        }
        if ($payments_sum == $reservation->total) {
            $reservation->status = 'SLD';
            $reservation->save();
        }
        return back()->with('message_success', trans('admin.payment_confirmed'));
    }
}
