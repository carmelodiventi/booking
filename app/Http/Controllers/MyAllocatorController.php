<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customers;
use App\Reservations;
use App\MyAllocatorResponse;
use App\Rooms;
use App\RoomsType;
use App\RoomsBooked;
use App\Portals;
use App\PortalsCommission;
use App\Availability;
use App\Property;
use App\Helpers\Helper;
use App\Helpers\MyAllocator;
use App\Notifications\Notifications;
use Curl;
use Validator;

class MyAllocatorController extends Controller
{
    //



    public function Callback(Request $request)
    {
        $password = $request->input('password');

        if ($password == env('MYALLOCATOR_CALLBACK_PASSWORD')) {
            $myallocator_response = new MyAllocatorResponse();
            $myallocator_response->response = json_encode($request->all());
            $myallocator_response->save();
            $booking = json_decode($request->input('booking'));

            if (isset($booking)) {
                $property = Property::find($booking->PropertyId);
                if ($property) {
                    if (Helper::save_reservation($booking, $property->id)) {
                        return response()->json(['success'=>true]);
                    }
                } else {
                    return response()->json([
                      'success'=>false,
                      'error' => [  "code"=> 31, "msg" => "Could not find property on this system for myallocator PropertyId:" . $booking->PropertyId]
                    ]);
                }
            } else {
                return response()->json([
                  'success'=>false,
                  'error' => [  "code"=> 29, "msg" => "Booking object is empty!"]
                ]);
            }
        } else {
            return response()->json(
              [
                'success'=> false,
                'error'=>[
                  'code'=>30,
                  'msg' => 'the password not match or is missing'
                  ]
              ]
            );
        }
    }

    public function roomAvailabilityList(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'startDate' => 'required',
          'endDate' => 'required'
        ], [
          'startDate.required' => trans('required.startDate'),
          'endDate.required' => trans('required.endDate')
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages());
        } else {
            $response = MyAllocator::RoomAvailabilityList($request->input('startDate'), $request->input('endDate'));
            if ($response && $response->Success == true) {
                $includePrice = $request->input('includePrice');
                return \App\Availability::synchronize($response, $includePrice);
            } else {
                return response()->json($response);
            }
        }
    }
}
