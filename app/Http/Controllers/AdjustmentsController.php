<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservations;
use App\ReservationAdjustments;

class AdjustmentsController extends Controller
{
    //
    

    public function delete($id, $reservation_id)
    {
        $adjustment = ReservationAdjustments::findOrFail($id);
        $adjustment->delete();

        $total = Reservations::getTotal($reservation_id);
        $reservation = Reservations::findOrFail($reservation_id);
        $reservation->total = $total;
        $reservation->save();

        return back()->with('message_success', trans('admin.adjustments_deleted'));
    }
}
