<?php
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Offerts;
use App\Helpers\Helper;
use Auth;
use Validator;

class OffertsController extends Controller
{
 
    protected $property;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->property = Helper::get_property();
            return $next($request);
        });
    }

    public function index()
    {
        $offerts = Offerts::all();
        return view('admin.offerts.show', ['offerts' => $offerts]);
    }

    public function create()
    {
        return view('admin.offerts.create');
    }

    public function edit($id)
    {
        $offert = Offerts::find($id);
        return view('admin.offerts.edit', ['offert' => $offert]);
    }

    public function update(Request $request, $id)
    {
        $offert = Offerts::findOrFail($id);
        $this->validate(
          $request,
          [
          'title' => 'required',
          'code' => 'required',
          'condition' => 'required',
          'value' => 'required|numeric',
          'status' => 'required'
      ],
          [
              'title.required' => trans('request.title_required'),
              'code.required' => trans('request.code_required'),
              'condition.required' => trans('request.condition_required'),
              'value.required' => trans('request.value_required'),
              'value.number' => trans('request.value_required_number')
          ]
      );

        $offert->title = $request->input('title');
        $offert->code = $request->input('code');
        $offert->condition = $request->input('condition');
        $offert->value = $request->input('value');
        if ($request->input('start_date') and $request->input('end_date')) {
            $offert->start_date = $request->input('start_date');
            $offert->end_date = $request->input('end_date');
        }
        if ($request->input('description')) {
            $offert->description = $request->input('description');
        }
        $offert->status = $request->input('status');
        $offert->save();
        return redirect('admin/offerts')->with('message_success', 'La tua offerta è stata aggiornata');
    }

    public function store(Request $request)
    {
        $this->validate(
          $request,
          [
          'title' => 'required',
          'code' => 'required',
          'condition' => 'required',
          'value' => 'required|numeric',
          'status' => 'required'
      ],
          [
              'title.required' => trans('request.title_required'),
              'code.required' => trans('request.code_required'),
              'condition.required' => trans('request.condition_required'),
              'value.required' => trans('request.value_required'),
              'value.number' => trans('request.value_required_number')
          ]
      );

        $offert = new Offerts();
        $offert->title = $request->input('title');
        $offert->code = $request->input('code');
        $offert->condition = $request->input('condition');
        $offert->value = $request->input('value');
        if ($request->input('start_date') and $request->input('end_date')) {
            $offert->start_date = $request->input('start_date');
            $offert->end_date = $request->input('end_date');
        }
        if ($request->input('description')) {
            $offert->description = $request->input('description');
        }
        $offert->status = $request->input('status');
        $offert->property_id = $this->property;
        $offert->save();
        return redirect('admin/offerts')->with('message_success', 'La tua offerta è stata aggiornata');
    }

    public function destroy($id)
    {
        $fee = Offerts::findOrFail($id);
        $fee->delete();
        return redirect('admin/offerts')->with('message_success', 'La offerta è stata eliminata');
    }

    public function validateCode(Request $request)
    {
        $validator = Validator::make($request->all(), 
            [ 'code' => 'required'], 
            ['code.required' => trans('required.code_required')]
        );
    
        if ($validator->fails()) {
            return response()->json($validator->messages());
        } else {
            $code = rtrim($request->input('code'));
            $today = date('Y-m-d', strtotime('now'));        
            $offert = Offerts::select('code', 'description', 'condition', 'value')
            ->where('start_date', '<=', $today)
            ->where('end_date', '>=',  $today)
            ->where('code', $code)
            ->first();
            session()->put('offert', $offert);
            return response()->json($offert);
        }

       
    }
}
