<?php

namespace App\Http\Controllers;

use App\Customers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Helper;

class CustomersController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->property = $request->property;
    }

    public function index(Request $request)
    {
        if ($request->get('keyword')) {
            $keyword = $request->get('keyword');
            $customer = Customers::where('name', 'like', "%$keyword%")
                ->orWhere('surname', 'like', "%$keyword%")
                ->orWhere('email', 'like', "%$keyword%")
                ->orWhere("phone", "LIKE", "%$keyword%")
                ->orWhere("country", "LIKE", "%$keyword%")
                ->paginate(100);
        } else {
            $customer = Customers::paginate(100);
        }

        return view('admin.customers.show', ['customers' => $customer]);
    }

    public function create()
    {
        return view('admin.customers.create');
    }

    public function edit($id)
    {
        $customers = Customers::findOrFail($id);
        return view('admin.customers.edit', ['customers' => $customers]);
    }

    public function update(Request $request, $id)
    {
        $customer = Customers::findOrFail($id);
        $this->validate(
            $request,
            [
                'surname' => 'required',
            ],
            [
                'name.required' => 'Il nome è richiesto',
                'surname.required' => 'Anche il cognome è richiesto',
                'country.required' => 'Anche la Nazione è richiesta',
                'phone.required' => 'Anche il telefono è richiesto',
                'email.email' => 'Inserisci un indirizzo email valido'
            ]
        );
        $customer->name = $request->input('name');
        $customer->surname = $request->input('surname');
        $customer->phone = $request->input('phone');
        $customer->phone_2 = $request->input('phone_2');
        $customer->phone_3 = $request->input('phone_3');
        $customer->email = $request->input('email');
        $customer->address = $request->input('address');
        $customer->city = $request->input('city');
        $customer->province = $request->input('province');
        $customer->country = $request->input('country');
        $customer->save();
        return redirect('admin/customers')->with('message_success', 'Il Cliente è stata aggiornato');
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'email' => 'unique:customers,email,' . $this->property . ',property_id',
                'name' => 'required',
                'surname' => 'required',
                'phone' => 'required',
            ],
            [
                'name.required' => 'Il nome è richiesto',
                'surname.required' => 'Anche il cognome è richiesto',
                'country.required' => 'Anche la Nazione è richiesta',
                'phone.required' => 'Anche il Telefono è richiesto',
                'email.email' => 'Inserisci un indirizzo email valido',
                'email.unique' => 'Indirizzo email già registrato'
            ]
        );

        $customer = new Customers();
        $customer->name = $request->input('name');
        $customer->surname = $request->input('surname');
        $customer->phone = $request->input('phone');
        $customer->phone_2 = $request->input('phone_2');
        $customer->phone_3 = $request->input('phone_3');
        $customer->email = $request->input('email');
        $customer->address = $request->input('address');
        $customer->city = $request->input('city');
        $customer->province = $request->input('province');
        $customer->country = $request->input('country');
        $customer->property_id = Helper::get_property();
        $customer->save();
        return redirect('admin/customers/')->with('message_success', 'Il Cliente è stata aggiunto');
    }

    public function destroy($id)
    {
        $customer = Customers::findOrFail($id)->delete();
        return redirect('admin/customers')->with('message_success', 'Cliente eliminato');
    }

    public function login()
    {
        $validator = Validator::make(
            $this->request->all(),
            [
                'email' => 'required|email',
                'password' => 'required|min:8'
            ],
            [
                'email.required' =>  trans('required.email_required'),
                'email.email' =>  trans('required.email_wrong_format'),
                'password.required' => trans('required.password_required'),
            ]
        );

        if ($validator->fails()) {
            $response = array('title' => trans('booking.title_login_msg'), 'status' => 'error', 'msg' => $validator->messages());
        } else {
            $customer = Customers::where('property_id', $this->property)->where('email', $this->request->email)->first();
            if ($customer) {
                if (Hash::check($this->request->input('password'), $customer->password)) {
                    $response = array(
                        'title' => trans('booking.title_login_msg'),
                        'status' => 'success',
                        'msg' => trans('booking.login_success_msg', ['name' => $customer->name])
                    );
                    $this->request->session()->put('customer', $customer);
                } else {
                    $response = array(
                        'title' => trans('booking.title_login_msg'),
                        'status' => 'error',
                        'msg' => ['password' => [0 => trans('booking.login_error_msg')]],
                    );
                }
            } else {
                $response = array(
                    'title' => trans('booking.title_login_msg'),
                    'status' => 'error',
                    'msg' => ['password' => [0 => trans('booking.login_error_msg')]],
                );
            }
        }

        return response()->json($response);
    }

    public function register()
    {
        $validator = Validator::make(
            $this->request->all(),
            [
                'email' => 'required|email|unique:customers,email,null,null,property_id,' . $this->property,
                'password' => 'required|min:8',
                'name' => 'required',
                'surname' => 'required',
                'phone' => 'required',
                'country' => 'required',
            ],
            [
                'email.required' => trans('required.email_required'),
                'email.email' => trans('required.email_wrong_format'),
                'email.unique' => trans('required.email_exists'),
                'password.required' => trans('required.password_required'),
                'password.min' => trans('required.password_lenght'),
                'name.required' => trans('required.name_required'),
                'surname.required' => trans('required.surname_required'),
                'phone.required' => trans('required.phone_required'),
                'country.required' => trans('required.country_required'),
            ]
        );

        if ($validator->fails()) {
            $response = array(
                'title' => trans('booking.title_register_msg'),
                'status' => 'error',
                'msg' => $validator->errors(),
            );
        } else {
            $customer = new Customers();
            $customer->email = $this->request->input('email');
            $customer->password = bcrypt($this->request->input('password'));
            $customer->name = $this->request->input('name');
            $customer->surname = $this->request->input('surname');
            $customer->phone = $this->request->input('phone');
            $customer->country = $this->request->input('country');
            $customer->property_id = $this->property;
            $customer->save();

            $this->request->session()->put('customer', $customer);

            Mail::send(
                'emails.customer_registration',
                ['name' => $customer->name, 'email' => $customer->email, 'password' => $this->request->input('password'), 'hotel' => Helper::get_setting('hotel_data')],
                function ($message) use ($customer) {
                    $message->to($customer->email)->subject(trans('emails.registration_confirm'));
                }
            );

            $response = array(
                'title' => trans('booking.title_register_msg'),
                'status' => 'success',
                'msg' =>  trans('booking.register_success_msg', ['name' => $customer->name])
            );
        }

        return response()->json($response);
    }

    public function resetPassword()
    {
        $validator = Validator::make(
            $this->request->all(),
            [
                'email' => 'required|email',
            ],
            [
                'email.required' =>  trans('required.email_required'),
                'email.email' =>  trans('required.email_wrong_format')
            ]
        );

        if ($validator->fails()) {
            $response = array('title' => trans('booking.title_login_msg'), 'status' => 'error', 'msg' => $validator->messages());
        } else {
            $customer = Customers::where('property_id', $this->property)->where('email', $this->request->email)->first();
            if ($customer) {
                $token = csrf_token();
                $customer = Customers::findOrFail($customer->id);
                $customer->reset_token = $token;
                $customer->save();

                Mail::send(
                    'emails.customer_password_reset',
                    ['name' => $customer->name, 'reset_url' => $token, 'hotel' => Helper::get_setting('hotel_data')],
                    function ($message) use ($customer) {
                        $message->to($customer->email)->subject(trans('emails.reset_password_confirmation'));
                    }
                );

                $response = array(
                    'title' => trans('booking.title_login_data_sent_msg'),
                    'status' => 'success',
                    'msg' =>  trans('booking.login_data_sent_success_msg', ['name' => $customer->name])
                );
            } else {
                $response = array(
                    'status' => 'error',
                    'msg' => ['reset-password' => [0 => trans('booking.email_not_exist')]],
                );
            }
        }

        return response()->json($response);
    }

    public function renewPassword()
    {
        $validator = Validator::make(
            $this->request->all(),
            [
                'email' => 'required|email',
            ],
            [
                'email.required' =>  trans('required.email_required'),
                'email.email' =>  trans('required.email_wrong_format')
            ]
        );

        if ($validator->fails()) {
            $response = ['title' => trans('booking.title_login_msg'), 'status' => 'error', 'msg' => $validator->messages()];
        } else {
            $customer = Customers::where('property_id', $this->property)->where('email', $this->request->email)->first();
            if ($customer) {
                $password = uniqid(8);
                $customer->password = bcrypt($password);
                $customer->save();

                Mail::send(
                    'emails.customer_password_renew',
                    ['name' => $customer->name, 'email' => $customer->email, 'password' => $password, 'hotel' => Helper::get_setting('hotel_data')],
                    function ($message) use ($customer) {
                        $message->to($customer->email)->subject(trans('emails.reset_password_confirmation'));
                    }
                );

                $response = array(
                    'title' => trans('booking.title_login_data_sent_msg'),
                    'status' => 'success',
                    'msg' =>  trans('booking.login_data_sent_success_msg', ['name' => $customer->name])
                );
            } else {
                $response = array(
                    'status' => 'error',
                    'msg' => ['reset-password' => [0 => trans('booking.email_not_exist')]],
                );
            }
        }

        return response()->json($response);
    }

    public function updatePassword($token)
    {
        $customer = Customers::where('reset_token', $token)->first();

        if ($this->request->all()) {
            $validator = Validator::make(
                $this->request->all(),
                [
                    'password' => 'required|min:8|confirmed',
                    'password_confirmation' => 'required|min:8',
                ],
                [
                    'password.required' =>  trans('required.password'),
                    'password.min' => trans('required.password_lenght'),
                    'password_confirmation.required' =>  trans('required.password_conf'),
                ]
            );
            if ($validator->fails()) {
                return view('customer.password.update', ['customer' => $customer, 'token' => $token])->withErrors($validator->messages());
            } else {
                $customer->password = bcrypt($this->request->input('password'));
                $customer->reset_token = '';
                $customer->save();
                return redirect('/')->with('message_success', trans('booking.password_updated_successfully'));
            }
        } else {
            return view('customer.password.update', ['customer' => $customer, 'token' => $token]);
        }
    }
}
