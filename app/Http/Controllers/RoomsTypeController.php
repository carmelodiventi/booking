<?php

namespace App\Http\Controllers;

use App\RoomsPhoto;
use App\RoomsType;
// use App\Helpers\MyAllocator;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Validator;
use Redirect;
use MyAllocator\phpsdk\src\Api\MaApi;
use MyAllocator\phpsdk\src\Api\RoomList;
use MyAllocator\phpsdk\src\Api\RoomCreate;
use MyAllocator\phpsdk\src\Api\RoomRemove;

class RoomsTypeController extends Controller
{
    protected $property;
    public $auth_api;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->property = Helper::get_property();
            $this->auth_api = config('myallocator');
            $this->auth_api['propertyId'] = Helper::get_property();
        
            return $next($request);
        });
        
    }

    public function index()
    {

        $rooms_type = RoomsType::orderBy('capacity')->get();

        if (Helper::get_setting('my_allocator')->active) {
            $myAllocator = new RoomList([ 'auth' => $this->auth_api]);
            $myAllocator_response = $myAllocator->callApi();
            return view('admin.rooms-type.show', ['rooms_type' => $rooms_type, 'ma_RoomsType' => $myAllocator_response]);
        } else {
            return view('admin.rooms-type.show', ['rooms_type' => $rooms_type]);
        }
    }

    public function create()
    {
        return view('admin.rooms-type.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
        'type.*' => 'required',
        'description.*' => 'required',
        'language.*' => 'required',
        'capacity' => 'required|numeric',
        'gender' => 'required',
        'units' => 'required|numeric',
        ], [
          'type.*.required' => trans('required.type_required'),
          'description.*.required' => trans('required.description_required'),
          'capacity.required' => trans('required.capacity_required'),
          'gender.required' => trans('required.gender_required'),
          'units.required' => trans('required.units_required'),
          'language.*.required' => trans('required.language_extra_required'),
        ]);

        if ($validator->fails()) {
            return redirect('admin/rooms-type/create')->withErrors($validator);
        } else {
            $room_type_name = [];
            $room_description = [];
            foreach ($request->input('type') as $key => $value) {
                $room_type_name[$request->input('language')[$key]] = $request->input('type')[$key];
            }
            foreach ($request->input('description') as $key => $value) {
                $room_description[$request->input('language')[$key]] = $request->input('description')[$key];
            }

            $room_type = new RoomsType();
            $room_type->type = json_encode($room_type_name);
            $room_type->description = json_encode($room_description);
            $room_type->units = $request->input('units');
            $room_type->capacity = $request->input('capacity');
            $room_type->gender = $request->input('gender');
            $room_type->private = 'true';
            $room_type->isDisabled = $request->input('isDisabled');
            $room_type->property_id = $this->property;
            $room_type->save();

            if (Helper::get_setting('my_allocator')->active) {

                $params = [
                    "ValidateOnly" => true,
                    "AuthorizeBilling" => true,
                    "isDisabled" => (bool) $room_type->isDisabled,
                    "Rooms"=>[
                        0 => [
                            "PMSRoomId"=> $room_type->id,
                            "Label"=> Helper::get_traslated_string($room_type->type),
                            "Units"=> $room_type->units,
                            "Occupancy"=> $room_type->capacity,
                            "PrivateRoom"=> $room_type->private,
                            "Gender"=> $room_type->gender
                        ]
                    ]
                ];

                $myAllocator = new RoomCreate([ 'auth' => $this->auth_api]);
                $response = $myAllocator->callApiWithParams($params);
          
                if ($response['response']['body']['Success'] === true) {
                    $room_type = RoomsType::find($room_type->id);
                    $room_type->ma_room_id = $response['response']['body']['Rooms'][0]['RoomId'];
                    $room_type->save();
                    return redirect('admin/rooms-type')->with('message_success', 'La tua lista è stata aggiornata con una nuovo tipo ed è stato aggiunto sul servizio di Channel Manager');
                } else {
                    return redirect('admin/rooms-type')->with('message_warning', trans('admin.error_reporting', ['error'=> MyAllocator::GetMyAllocatorResponse($response)['errors']]));
                }
            } else {
                return redirect('admin/rooms-type')->with('message_success', 'La tua lista è stata aggiornata con una nuovo tipo');
            }
        }
    }


    public function edit($id)
    {
        $room_type = RoomsType::find($id);
        return view('admin.rooms-type.edit', ['room_type' => $room_type]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
          'type.*' => 'required',
          'description.*' => 'required',
          'language.*' => 'required',
          'units' => 'required|numeric',
          'gender' => 'required',
          'capacity' => 'required|numeric',
        ], [
          'type.*.required' => trans('required.type_required'),
          'description.*.required' => trans('required.description_required'),
          'units.required' => trans('required.units_required'),
          'gender.required' => trans('required.gender_required'),
          'capacity.required' => trans('required.capacity_required'),
          'language.*.required' => trans('required.language_extra_required'),
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator);
        } else {
            $room_type_name = [];
            $room_description = [];
            foreach ($request->input('type') as $key => $value) {
                $room_type_name[$request->input('language')[$key]] = $request->input('type')[$key];
            }
            foreach ($request->input('description') as $key => $value) {
                $room_description[$request->input('language')[$key]] = $request->input('description')[$key];
            }

            $room_type = RoomsType::findOrFail($id);
            $room_type->type = json_encode($room_type_name);
            $room_type->description = json_encode($room_description);
            $room_type->capacity = $request->input('capacity');
            $room_type->units = $request->input('units');
            $room_type->gender = $request->input('gender');
            $room_type->isDisabled = $request->input('isDisabled');
            $room_type->save();

            if (Helper::get_setting('my_allocator')->active) {
                $response = MyAllocator::GetMyAllocatorResponse(MyAllocator::RoomUpdate($room_type));
                if ($response && $response->Success == true) {
                    return redirect('admin/rooms-type')->with('message_success', 'La tua lista è stata aggiornata con una nuovo tipo ed è stato aggiunto sul servizio di Channel Manager');
                } else {
                    return redirect('admin/rooms-type')->withErrors($response->Errors);
                }
            } else {
                return redirect('admin/rooms-type')->with('message_success', 'Il tuo Tipo camera è stato aggiornato');
            }
        }
    }

    public function destroy($id)
    {
        $room_type = RoomsType::findOrFail($id);

        if (Helper::get_setting('my_allocator')->active) {

            $params = [ "Rooms" =>[  0 => [ "RoomId" => $room_type->ma_room_id ] ]];
            $myAllocator = new RoomRemove([ 'auth' => $this->auth_api]);
            $response = $myAllocator->callApiWithParams($params);

            if ($response['response']['body']['Success'] === true) {
                $room_type->delete();
                return redirect('admin/rooms-type')->with('message_success', 'Il tuo Tipo Camera è stato Eliminato ed è stato avvisato il servizio di Channel Manager');
            } else {
                return redirect('admin/rooms-type')->with('message_warning', trans('admin.error_reporting', ['error'=> MyAllocator::GetMyAllocatorResponse($MyAllocator_response)['errors']]));
            }
        } else {
            $room_type->delete();
            return redirect('admin/rooms-type')->with('message_success', 'Il tuo Tipo Camera è stato Eliminato');
        }
    }

    public function photo($id)
    {
        $room_type = RoomsType::find($id);
        $photo_list = RoomsPhoto::where('rooms_type_id', '=', $id)->get();
        return view('admin.rooms-type.photo-upload', ['room_type' => $room_type, 'photo_list' => $photo_list]);
    }

    public function photoupload(Request $request, $id)
    {
        $room_type = RoomsType::findOrFail($id);

        $validator = Validator::make(
            $request->all(),
        ['file' => 'required|max:10240','dimensions' => 'min_width=643|min_height=643'],
        ['file.required' => trans('required.file_required')]
        );

        if ($validator->fails()) {
            return response()->json([
              'error' => true,
              'message' => $validator->messages(),
              'code' => 400
          ], 400);
        }

        $name = time() . '.' . $request->file->getClientOriginalExtension();
        $path = public_path('images/' . $room_type->id);
        $thumbnails_path = public_path('images/' . $room_type->id . '/thumbnails');
        $imagePath = public_path('images/' . $room_type->id . '/' . $name);
        $request->file->move($path, $name);
        // Image Resize library
        $img = Image::make($imagePath);
        $img->fit(653, 653);
        $img->save($path.'/'.$name);
        if (!File::exists($thumbnails_path)) {
            File::makeDirectory($thumbnails_path, 0775, true);
        }
        $img = Image::make($imagePath);
        $img->fit(207, 197);
        $img->save($thumbnails_path.'/'.$name);
        // Image Resize library
        $rooms_photo = new RoomsPhoto();
        $rooms_photo->rooms_type_id = $room_type->id;
        $rooms_photo->url = 'public/images/' . $room_type->id . '/' . $name;
        $rooms_photo->save();

        return response()->json([
            'error' => false,
            'code' => trans('admin.photo_uploaded')
        ]);
    }

    public function photodelete(Request $request, $id)
    {
        $rooms_photo = RoomsPhoto::findOrFail($id);
        $image = base_path($rooms_photo->url);
        $rooms_photo->delete();
        unlink($image);
        return back()->with('message_success', trans('admin.photo_deleted'));
    }
}
