<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RatesType;
use App\Availability;
use App\Helpers\Helper;
use Redirect;
use Auth;

class RatesTypeController extends Controller
{
    //

    protected $property;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->property = Helper::get_property();
            return $next($request);
        });
    }

    public function index()
    {
        $rates_type = RatesType::all();
        return view('admin.rate-type.show', ['rates_type' => $rates_type]);
    }

    public function show($id)
    {
        $rates_type = RatesType::findOrFail($id);
        return view('admin.rate-type.show', ['rates_type' => $rates_type]);
    }

    public function create()
    {
        return view('admin.rate-type.create');
    }

    public function edit($id)
    {
        $rates_type = RatesType::findOrFail($id);
        return view('admin.rate-type.edit', ['rates_type' => $rates_type]);
    }

    public function update(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'name.*' => 'required',
                'description.*' => 'required',
            ],
            [
                'name.*.required' => 'Il nome è richiesto',
                'description.*.required' => 'La descrizione è richiesto'
            ]
        );

        $rates_type = RatesType::findOrFail($id);
        $rates_type_name = [];
        $rates_type_description = [];
        foreach ($request->input('name') as $key => $value) {
            $rates_type_name[$request->input('language')[$key]] = $request->input('name')[$key];
        }
        foreach ($request->input('description') as $key => $value) {
            $rates_type_description[$request->input('language')[$key]] = $request->input('description')[$key];
        }
        $rates_type->name = json_encode($rates_type_name);
        $rates_type->description = json_encode($rates_type_description);
        if ($request->input('refundable')) {
            $rates_type->refundable = 1;
            $rates_type->rules = null;
        } else {
            $rates_type->refundable = 0;
            $rates_type->rules = json_encode([$request->input('increment_decrement'),$request->input('total') ,$request->input('operator')]);
        }
        $rates_type->save();
        return back()->with('message_success', 'Il tuo Tipo Tariffa è stata aggiornato');
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
          [
              'name.*' => 'required',
              'description.*' => 'required',
          ],
          [
              'name.*.required' => 'Il nome è richiesto',
              'description.*.required' => 'La descrizione è richiesto'
          ]
      );

        $rates_type = new RatesType;
        $rates_type_name = [];
        $rates_type_description = [];
        foreach ($request->input('name') as $key => $value) {
            $rates_type_name[$request->input('language')[$key]] = $request->input('name')[$key];
        }
        foreach ($request->input('description') as $key => $value) {
            $rates_type_description[$request->input('language')[$key]] = $request->input('description')[$key];
        }
        $rates_type->name = json_encode($rates_type_name);
        $rates_type->description = json_encode($rates_type_description);
        if ($request->input('refundable')) {
            $rates_type->refundable = 1;
            $rates_type->rules = null;
        } else {
            $rates_type->refundable = 0;
            $rates_type->rules = json_encode([$request->input('increment_decrement'),$request->input('total') ,$request->input('operator')]);
        }
        $rates_type->property_id = $this->property;
        $rates_type->save();
        return back()->with('message_success', 'Il tuo Tipo Tariffa è stata aggiunto');
    }

    public function destroy($id)
    {
        $rates_type = RatesType::findOrFail($id);
        $rates_type->delete();
        Availability::where('rooms_type_id', $id)->delete();
        return redirect('admin/rate-type')->with('message_success', 'Tipo tariffa eliminato');
    }
}
