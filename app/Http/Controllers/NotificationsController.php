<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;

class NotificationsController extends Controller
{
    //

    public function MarkAsRead(Request $request)
    {
        $user = Auth::user();
        $user->unreadNotifications()->where('notifications.id', $request->notif_id)->update(['read_at' => Carbon::now()]);
        return response()->json(['status'=>'success']);
    }
}
