<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Customers;
use App\Api;
use App\Rooms;
use App\Helpers\Helper;
use App\Helpers\MyAllocator;
use App\RoomsBooked;
use App\Availability;
use App\RoomsType;

class ApiController extends Controller
{
    public function loadReservations(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'start' => 'required|date',
                'end' => 'required|date',
                'room_id' => 'required',
            ],
            [
                'check_in.required' => trans('required.check_in_required'),
                'check_out.required' => trans('required.check_out_required'),
                'room_id.required' => trans('required.room_id_required'),
            ]
        );
        if ($validator->fails()) {
            return response()->json($validator->messages());
        } else {
            $reservations = RoomsBooked::loadReservations($request->input('start'), $request->input('end'), $request->input('room_id'));
            return $reservations;
        }
    }

    public function getRate(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'check_in' => 'required|date',
                'check_out' => 'required|date',
            ],
            [
                'check_in.required' => trans('required.check_in_required'),
                'check_out.required' => trans('required.check_out_required'),
            ]
        );

        if ($validator->fails()) {
            return response()->json($validator->messages());
        } else {
            $rates = Availability::getRate($request->input('check_in'), $request->input('check_out'), $request->input('id'));
            if ($rates) {
                return response()->json(['status' => 'success', 'data' => $rates]);
            } else {
                return response()->json(['status' => 'error', 'data' => []]);
            }
        }
    }

    /*
        public function checkAvailability(Request $request)
        {
            if ($request->input('check_in')
                and $request->input('check_out')
                and $request->input('rooms')
            ) {
                $result = DB::table('rooms_booked')
                    ->select(DB::raw('COUNT(*) as count'))
                    ->where('check_in', '<', $request->input('check_out'))
                    ->where('check_out', '>', $request->input('check_in'))
                    ->where('status', '=', 'CNF')
                    ->whereIn('room_id', [$request->input('rooms')])
                    ->get();
                return response()->json($result[0]->count);
            }
        }

        public function checkAvailabilityRoom(Request $request)
        {
            if ($request->input('check_in')
                and $request->input('check_out')
                and $request->input('room_id')
            ) {
                $result = DB::table('rooms_booked')
                    ->select(DB::raw('COUNT(rooms_booked.room_id) as count'), 'rooms.name')
                    ->rightjoin('rooms', 'rooms.id', '=', 'rooms_booked.room_id')
                    ->where('check_in', '<', $request->input('check_out'))
                    ->where('check_out', '>', $request->input('check_in'))
                    ->where('rooms_booked.status', '=', 'CNF')
                    ->where('room_id', [$request->input('room_id')])
                    //->where('reservation_id','<>', $request->input('reservation_id'))
                    ->first();
                return response()->json($result);
            }
        }

        */

    public function getAvailability(Request $request)
    {
        if (
            $request->input('check_in') and $request->input('check_out')
        ) {
            $availability = Availability::get_rooms_type_availability($request->check_in, $request->check_out);
            return response()->json([
                'status' => 'success',
                'data' => $availability
            ]);
        } else {
            return response()->json(['status' => 'error']);
        }
    }

    public function getAccomodationsAvailability(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'check_in' => 'required|date',
            'check_out' => 'required|date',
            'room_id' => 'required|integer'
        ], [
            'check_in.required' => trans('required.check_in_required'),
            'check_out.required' => trans('required.check_out_required'),
            'room_id.required' => trans('required.reservation_id_required')
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages());
        } else {
            $rooms = Rooms::get_accomodations_availability($request->check_in, $request->check_out, $request->room_id);
            return response()->json(['status' => 'success', 'data' => $rooms]);
        }
    }

    public function getDetails(Request $request)
    {
        if ($request->input('pnr')) {
            $result = DB::table('reservations as r')
                ->select('r.id', 'r.pnr', 'r.check_in', 'r.check_out', 'r.created_at', 'r.updated_at', 'r.note', 'r.total', 'r.deposit', 'r.advance', 'r.discount', 'r.topay', 'c.name', 'c.surname', 'c.email', 'c.phone', 'c.country', 'r.status')
                ->join('customers as c', 'r.customer_id', '=', 'c.id')
                ->where('r.pnr', $request->input('pnr'))
                ->first();

            return response()->json($result);
        } else {
            $result = array('status' => 'error', 'msg' => trans('admin.no_details_founds'));
            return response()->json($result[0]->count);
        }
    }

    public function getCustomers()
    {
        $customers = Customers::select('id', DB::raw('CONCAT(COALESCE(`name`,\'\'), " ", COALESCE(`surname`,\'\')) AS full_name'))->pluck('full_name', 'id');
        return response()->json($customers);
    }

    public function getRoomStatus(Request $request)
    {
        if ($request->input('id') and $request->input('start') and $request->input('end')) {
            $period = Api::getRoomStatus($request->input('start'), $request->input('end'), $request->input('id'));
            return response()->json($period);
        }
    }

    public function getRoomsTypeRate(Request $request)
    {
        if (
            $request->input('start')
            and $request->input('end')
            and $request->input('id')
        ) {
            $rate = Api::getRoomsTypeRate($request->input('id'), $request->input('start'), $request->input('end'));
            return $rate;
        }
    }

    public function getDashboardData()
    {
        $revenue = Api::getRevenue();
        $statistics = Api::getStatistics();
        return array('revenue' => $revenue, 'statistics' => $statistics);
    }

    public function validateOffertCode(Request $request)
    {
        if ($request->input('code')) {
            $offert = Api::validateOffertCode($request->input('code'));
            session()->put('offert', $offert);
            return response()->json($offert);
        }
    }

    public function selectRoom(Request $request)
    {
        if ($request->input('rooms')) {
            $rooms = $request->input('rooms');
            $rooms = Api::resoures($rooms);
            session()->put('reservation_rooms', $rooms);
            return response()->json($rooms);
        }
    }

    public function moveReservation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pnr' => 'required|min:8',
            'room_type_id' => 'required|integer',
            'room_booked_type_id' => 'required|integer',
            'room_booked_id' => 'required|integer',
            'room_id' => 'required|integer',
            'date' => 'required|date'
        ], [
            'pnr.required' => trans('required.pnr'),
            'room_type_id.required' => trans('required.room_type_id'),
            'room_booked_type_id.required' => trans('required.room_booked_type_id'),
            'room_booked_id.required' => trans('required.room_booked_id'),
            'room_id.required' => trans('required.room_id'),
            'date.required' => trans('required.date')
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages());
        } else {
            $rooms_booked = RoomsBooked::findOrFail($request->room_booked_id);
            $new_stay_date = Helper::get_new_stay($rooms_booked->check_in, $rooms_booked->check_out, $request->date);

            $room_available = RoomsBooked::getRoomAvaibility($new_stay_date->check_in, $new_stay_date->check_out, $request->room_id, $request->room_booked_id);
            if ($room_available > 0) {
                $response = ['status' => 'error', 'msg' => trans('admin.reservation_not_moved')];
            } else {
                //update availability
                Availability::add($rooms_booked->check_in, $rooms_booked->check_out, $request->room_booked_type_id, 1);
                Availability::scale($new_stay_date->check_in, $new_stay_date->check_out, $request->room_type_id, 1);

                $rooms_booked->check_in = $new_stay_date->check_in;
                $rooms_booked->check_out = $new_stay_date->check_out;
                $rooms_booked->room_id = $request->room_id;
                $rooms_booked->rooms_type_id = $request->room_type_id;
                $rooms_booked->save();



                if (Helper::get_setting('my_allocator')->active) {
                    $rooms_type_ids = [$request->room_type_id, $request->room_booked_type_id];
                    $arr_date = [$rooms_booked->check_in, $rooms_booked->check_out, $new_stay_date->check_in, $new_stay_date->check_out];
                    $min_date = min(array_map('strtotime', $arr_date));
                    $max_date = max(array_map('strtotime', $arr_date));
                    $allocations = \App\Availability::get_availability_status(date('Y-m-d', $min_date), date('Y-m-d', $max_date), $rooms_type_ids);

                    $response = MyAllocator::GetMyAllocatorResponse(MyAllocator::ARIUpdate($allocations));
                    if ($response->Success == true) {
                        $message = [
                            'Success' => true,
                            'Text' => 'La tua prenotazione è stata modificata ed è stata inviata una richiesta al servizio Channel Manager'

                        ];
                    } else {
                        $message = [
                            'Success' => false,
                            'Text' => 'La tua prenotazione è stata modificata ma si è verificato un errore nel servizio Channel Manager<br>' . $response->Errors
                        ];
                    }
                } else {
                    $message = [
                        'Success' => true,
                        'Text' => trans('admin.reservation_moved')
                    ];
                }
            }
            return response()->json($message);
        }
    }

    public function getAvailabilityFromDate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'startDate' => 'required|date',
            'endDate' => 'required|date'
        ], [
            'start_date.required' => trans('required.date'),
            'end_date.required' => trans('required.date')
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages());
        } else {
            $rooms_type_ids = RoomsType::pluck('id')->toArray();
            $availability = Availability::whereIn('rooms_type_id', $rooms_type_ids)->whereBetween('date', [$request->startDate, $request->endDate])
                ->get();
            return response()->json(['Success' => true, 'Availability' => $availability]);
        }
    }

    public function storeAvailability(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'changes' => 'required'
        ], [
            'changes.required' => trans('required.changes')
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages());
        } else {
            $changes = $request->input('changes');
            $changes = json_decode($changes);
            $response = Availability::store_availability($changes);
            return response()->json($response);
        }
    }
}
