<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Rate;
use App\RatesType;
use App\RoomsType;
use Illuminate\Http\Request;
use Redirect;
use App\Helpers\MyAllocator;

class RateController extends Controller
{
    //

    public function index(Request $request)
    {
        if ($request->get('interval')) {
            $interval = strtotime($request->get('interval'));
            $start = date('Y-m-1', strtotime($request->get('interval')));
            $end = date('Y-m-d', strtotime('+45 days', strtotime($request->get('interval'))));
        } else {
            $interval = strtotime('now');
            $start = date('Y-m-d', $interval);
            $end = date('Y-m-d', strtotime('+45 days', $interval));
        }

        $rates = RatesType::all();
        $rooms_type = RoomsType::all();
        $rooms_type_ids = RoomsType::pluck('id');
        $prices = Rate::getPrices($start, $end, $rooms_type_ids);
        return view('admin.rate.show', ['rooms_type' => $rooms_type,'rates' => $rates, 'prices'=> $prices, 'interval' => $interval]);
    }

    public function create()
    {
        $rooms_type = RoomsType::pluck('type', 'id');
        $rates_type = RatesType::pluck('name', 'id');
        return view('admin.rate.create', ['rates_type'=> $rates_type,'rooms_type' => $rooms_type]);
    }


    public function edit($id)
    {
        $rooms_type = RoomsType::pluck('type', 'id');
        $rates_type = RatesType::pluck('name', 'id');
        $rate = Rate::findOrFail($id);
        return view('admin.rate.edit', ['rate' => $rate, 'rates_type'=> $rates_type, 'rooms_type' => $rooms_type]);
    }

    public function update(Request $request)
    {
        $this->validate($request,
            [
                'rates' => 'required',
                'units' => 'required',
            ],
            [
                'rates.required' => 'Rate request',
            ]
        );

        $rates = $request->input('rates');
        $units = $request->input('units');

        //dd($rates,$units);

        foreach ($rates as $rate) {
            foreach ($rate as $id => $price) {
                $rate = Rate::find($id);
                $rate->rate = $price;
                $rate->save();
            }
        }



        return back()->with('message_success', 'I prezzi sono stati aggiornati');
    }

    public function store(Request $request)
    {
        $this->validate($request,
            [
                'type' => 'required',
                'rates_type' => 'required',
                'price' => 'required',
                'start' => 'required',
                'end' => 'required',
                'status' => 'required'
            ],
            [
                'type.required' => 'Il tipo è richiesto',
                'rates_type.required' => 'Il tipo tariffa è richiesto',
                'name.required' => 'Il nome è richiesto',
                'price.required' => 'Anche il valore è richiesto',
                'start.required' => 'Anche la data di inizio è richiesta',
                'end.required' => 'Anche la data di fine è richiesta',
                'status.required' => 'Anche lo status è richiesto'
            ]
        );


        $start = strtotime($request->input('start'));
        $end = strtotime($request->input('end'));

        while ($start <= $end) {
            $rate = Rate::updateOrCreate([
              'date' => date("Y-m-d", $start),
              'rooms_type_id' => $request->type,
              'rates_type_id' => $request->rates_type
              ], ['rate' => $request->price]
            );
            $start = strtotime("+1 day", $start);
        }
        return back()->with('message_success', 'I prezzi per il Periodo selezionato sono stati aggiunti');
    }

    public function destroy($id)
    {
        $rate = Rate::findOrFail($id);
        $rate->delete();
        return redirect('admin/rate')->with('message_success', 'La tassa è stata eliminata');
    }
}
