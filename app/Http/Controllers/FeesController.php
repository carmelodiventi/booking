<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Fees;
use Redirect;
use Auth;

class FeesController extends Controller
{
    protected $property;
    
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->property = Helper::get_property();
            return $next($request);
        });
    }

    public function index()
    {
        $fees = Fees::all();
        return view('admin.fees.show', ['fees' => $fees]);
    }

    public function create()
    {
        return view('admin.fees.create');
    }

    public function edit($id)
    {
        $fee = Fees::find($id);
        return view('admin.fees.edit', ['fee' => $fee]);
    }

    public function update(Request $request, $id)
    {
        $fee = Fees::findOrFail($id);
        $this->validate(
            $request,
            [
            'name' => 'required',
            'value' => 'required|numeric',
            'status' => 'required'
        ],
            [
                'name.required' => 'Il nome è richiesto',
                'value.required' => 'Anche il valore è richiesto',
                'status.required' => 'Anche lo status è richiesto'
            ]
        );
        $fee->name = $request->input('name');
        $fee->value = $request->input('value');
        if ($request->input('percentage')) {
            $fee->percentage = $request->input('percentage');
        }
        $fee->status = $request->input('status');
        $fee->save();
        return redirect('admin/fees')->with('message_success', 'La tua tassa è stata aggiornata');
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
            'name' => 'required',
            'value' => 'required|numeric',
            'status' => 'required'
        ],
            [
                'name.required' => 'Il nome è richiesto',
                'value.required' => 'Anche il valore è richiesto',
                'status.required' => 'Anche lo status è richiesto'
            ]
        );
        $fee = new Fees();
        $fee->name = $request->input('name');
        $fee->value = $request->input('value');
        if ($request->input('percentage')) {
            $fee->percentage = $request->input('percentage');
        }
        $fee->status = $request->input('status');
        $fee->property_id = $this->property;
        $fee->save();
        return redirect('admin/fees')->with('message_success', 'La tua tassa è stata aggiornata');
    }

    public function destroy($id)
    {
        $fee = Fees::findOrFail($id);
        $fee->delete();
        return redirect('admin/fees')->with('message_success', 'La tassa è stata eliminata');
    }
}
