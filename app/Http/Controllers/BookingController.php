<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Hashids\Hashids;
use Cart;
use App\Booking;
use App\Availability;
use App\Fees;
use App\Helpers\Helper;
use App\Notifications\Notifications;

class BookingController extends Controller
{
    //

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->property = $request->property;
    }

    public function index()
    {

        if (!empty($this->property)) {
            return view('home')->with(['css_class' => 'home', 'property_id' => $this->property]);
        } else {
            return view('index')->with(['css_class' => 'home']);
        }
    }


    public function roomsAvailability()
    {
        $validator = Validator::make(
            $this->request->all(),
            [
                'check_in' => 'date|required|after_or_equal:' . date('Y-m-d') . '',
                'check_out' => 'date|required|after:check_in',
            ],
            [
                'check_in.required' => trans('required.check_in_date'),
                'check_in.after_or_equal' => trans('required.check_in_date_after_or_equal'),
                'check_out.required' => trans('required.check_out_date'),
                'check_out.after' => trans('required.check_out_date_after_check_in'),
            ]
        );

        if ($validator->fails()) {
            return redirect('/' . $this->property)->withErrors($validator);
        } else {
            $check_in = $this->request->input('check_in');
            $check_out = $this->request->input('check_out');
            $guests = $this->request->input('guests');
            $offert_code = $this->request->input('offert_code');

            $rooms_types = Availability::get_rooms_type_availability($check_in, $check_out, $guests);
            $rates = Availability::get_availability($check_in, $check_out, $guests);

            // Map Rooms Types Collection adding rates for each rooms that match with the room_type_id
            $rooms_types->map(function ($room_type, $key) use ($rates) {
                $rates_list = [];
                $room_type_id = $room_type['rooms_type_id'];
                $room_type->rates = $rates->filter(function ($rate) use ($room_type_id) {
                    return $rate->rooms_type_id === $room_type_id;
                });
                return $room_type;
            });

            $nights_to_stay = Helper::get_nights_to_stay($check_in, $check_out);
            $offert = $this->request->session()->get('offert');

            // Putting all items in the session in order to retrive them in the future step

            $this->request->session()->put('property_id', $this->property);
            $this->request->session()->put('check_in', $check_in);
            $this->request->session()->put('check_out', $check_out);
            $this->request->session()->put('nights', $nights_to_stay);
            $this->request->session()->put('guests', $guests);
            $this->request->session()->put('offert_code', $offert_code);
            $this->request->session()->put('rooms_types', $rooms_types);

            /* Destroy Cart */
            Cart::destroy();

            return view('rooms_availability')->with([
                'property_id' => $this->property,
                'check_in' => $check_in,
                'check_out' => $check_out,
                'nights' => $nights_to_stay,
                'guests' => $guests,
                'offert_code' => $offert_code,
                'offert' =>  $offert,
                'rooms_types' => $rooms_types
            ]);
        }
    }

    public function roomsDetails()
    {
        if (
            $this->request->session()->has('check_in')
            && $this->request->session()->has('check_out')
            && $this->request->session()->has('nights')
            && !Cart::isEmpty()
        ) {

            $check_in = $this->request->session()->get('check_in');
            $check_out = $this->request->session()->get('check_out');
            $guests = $this->request->session()->get('guests');
            $nights_to_stay = $this->request->session()->get('nights');
            $offert = $this->request->session()->get('offert');
            $rooms_types = $this->request->session()->get('rooms_types');

            if ($rooms = Cart::all()) {
                $hashids = new Hashids(env('APP_KEY'));
                foreach ($rooms as $key => $room) {
                    $room_type_id = $hashids->decode($room->rooms_type_id)[0];
                    $rate_id = $hashids->decode($room->rate_id)[0];

                    $room_type = $rooms_types->filter(function ($room_type) use ($room_type_id) {
                        return $room_type->rooms_type_id === $room_type_id;
                    })->first();

                    $rate = $room_type->rates->filter(function ($rate) use ($rate_id) {
                        return $rate->rate_id === $rate_id;
                    })->first();

                    $rooms_details[] = (object)[
                        'type' => $room_type->type,
                        'rooms_type_id' => $room_type->rooms_type_id,
                        'ma_room_id' => $room_type->ma_room_id,
                        'photo' => $room_type->photo,
                        'guest' => $room_type->capacity,
                        'rate_id' => $rate->rate_id,
                        'rate_name' => $rate->rate_name,
                        'refundable' => $rate->rate_refundable,
                        'rate_amount' => $guests == 1 && ($rate->single_rate_amount > 0 || $rate->single_rate_amount !== null) ? $rate->single_rate_amount : $rate->rate_amount
                    ];
                }
            } else {
                return back()->with('message_error', trans('booking.session_error'));
            }

            $fees_list = Fees::where('status', 'CNF')->select('id', 'name', 'value', 'percentage')->get();
            $total_room = count((array)$rooms_details);
            $subtotal = Helper::get_subtotal($rooms_details);
            $fees_list = Helper::get_fees_list($fees_list, $subtotal);
            $totaltax = Booking::getFeesCost($subtotal);
            $paypal = Helper::get_setting('paypal');
            $stripe = Helper::get_setting('stripe');
            $credit_card = Helper::get_setting('credit_card');
            $payment_method = ['paypal' => $paypal, 'stripe' => $stripe, 'credit_card' => $credit_card];
            $grandtotal = Helper::get_total($totaltax, $subtotal);
            $discount = Helper::get_discount($offert, $grandtotal);
            $grandtotal = $grandtotal - $discount;

            $this->request->session()->put('fees', $fees_list);
            $this->request->session()->put('rooms', $rooms_details);
            $this->request->session()->put('totaltax', $totaltax);
            $this->request->session()->put('subtotal', $subtotal);
            $this->request->session()->put('grandtotal', $grandtotal);
            $this->request->session()->put('discount', $discount);

            return view('rooms_details')->with([
                'property_id' => $this->property,
                'check_in' => $check_in,
                'check_out' => $check_out,
                'nights' => $nights_to_stay,
                'guests' => $guests,
                'rooms' => $rooms_details,
                'total_room' => $total_room,
                'payment_method' => $payment_method,
                'offert' =>  $offert,
                'fees' => $fees_list,
                'totaltax' => $totaltax,
                'discount' => $discount,
                'subtotal' => $subtotal,
                'grandtotal' => $grandtotal
            ]);
        } else {
            return back()->with('message_error', trans('booking.session_error'));
        }
    }

    public function payment()
    {
        if (
            !$this->request->session()->has('check_in')
            and !$this->request->session()->has('check_out')
            and !$this->request->session()->has('nights')
            and !$this->request->session()->has('subtotal')
            and !$this->request->session()->has('grandtotal')
        ) {
            return redirect('/' . $this->property)->with('message_error', trans('booking.session_error'));
        } elseif (empty($this->request->payment)) {
            return back()->with('message_error', trans('booking.payment_not_selected'));
        } elseif (!$this->request->session()->has('customer')) {
            return back()->with('message_error', trans('booking.customer_not_login'));
        } else {

            $check_in = $this->request->session()->get('check_in');
            $check_out = $this->request->session()->get('check_out');
            $guests = $this->request->session()->get('guests');

            $pnr = Helper::get_PNR();
            $payment = $this->request->input('payment');
            $note = $this->request->input('note');

            $this->request->session()->put('pnr', $pnr);
            $this->request->session()->put('note', $note);
            $this->request->session()->put('payment_selected', $payment);


            switch ($this->request->payment) {
                case 1:
                    $stripeToken = $this->request->input('stripeToken');
                    $this->request->session()->put('stripeToken', $stripeToken);
                    return redirect($this->property . '/stripe');
                    break;
                case 2:
                    return redirect($this->property . '/paypal');
                    break;
            }
        }
    }

    public function ticket()
    {
        if (
            $this->request->session()->has('check_in')
            and $this->request->session()->has('check_out')
            and $this->request->session()->has('nights')
            and $this->request->session()->has('subtotal')
            and $this->request->session()->has('grandtotal')
            and $this->request->session()->has('customer')
            and $this->request->session()->has('rooms')
            and $this->request->session()->has('fees')
        ) {
            $data = [
                'property_id' => $this->property,
                'hotel' => Helper::get_setting('hotel_data'),
                'pnr' => $this->request->session()->get('pnr'),
                'date' => date('Y-m-d H:i:s'),
                'check_in' => $this->request->session()->get('check_in'),
                'check_out' => $this->request->session()->get('check_out'),
                'nights' => $this->request->session()->get('nights'),
                'guests' => $this->request->session()->get('guests'),
                'customer' => $this->request->session()->get('customer'),
                'rooms' => $this->request->session()->get('rooms'),
                'fees' => $this->request->session()->get('fees'),
                'note' => $this->request->session()->get('note'),
                'payment_selected' => $this->request->session()->get('payment_selected'),
                'offert' =>  $this->request->session()->get('offert'),
                'discount' => $this->request->session()->get('discount'),
                'totaltax' => $this->request->session()->get('totaltax'),
                'subtotal' => $this->request->session()->get('subtotal'),
                'grandtotal' => $this->request->session()->get('grandtotal')
            ];
            // send notifications to administrator

            $users = \App\User::where('property_id', $this->property)
                ->join('properties', 'users.property_id', '=', 'properties.id')->get(['users.id']);

            foreach ($users as $user) {
                $user->notify(new Notifications(
                    [
                        'title' => trans('admin.new_reservations_received'),
                        'message' => trans('admin.new_reservations_message', ['pnr' => $data['pnr']])
                    ]
                ));
            }

            $hotel_email = Helper::get_setting('hotel_data')->email;
            $customer_email = $this->request->session()->get('customer')->email;

            if ($hotel_email) {
                // send mail ticket to administrator
                Mail::send(
                    'emails.new_reservation',
                    $data,
                    function ($message) use ($hotel_email) {
                        $message->to($hotel_email)->subject(trans('emails.new_reservation'));
                    }
                );
            }

            if ($customer_email) {
                // send mail ticket to customer
                Mail::send(
                    'emails.ticket',
                    $data,
                    function ($message) use ($customer_email) {
                        $message->to($customer_email)->subject(trans('emails.reservation_confirm'));
                    }
                );
            }
            // destroy session and show ticket
            $this->request->session()->flush();

            return view('ticket')->with($data);
        } else {
            return redirect('/')->with('message_error', trans('booking.session_error'));
        }
    }
}
