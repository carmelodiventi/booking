<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Extra;
use LaravelLocalization;
use Validator;

class ExtraController extends Controller
{
    protected $property;
    
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->property = Helper::get_property();
            return $next($request);
        });
    }

    //
    public function index()
    {
        $extras = Extra::all();
        return view('admin.extra.show', ['extras' => $extras]);
    }

    public function create()
    {
        return view('admin.extra.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'name.*' => 'required',
          'price' => 'required',
          'status.*'=> 'required'
        ], [
          'name.required' => trans('required.name_extra_required'),
          'price.required' => trans('required.price_extra_required'),
          'status.*.required' => trans('required.status_extra_required'),
        ]);

        if ($validator->fails()) {
            return redirect('admin/extra/create')->withErrors($validator);
        } else {
            $extra_name = [];
            foreach ($request->input('name') as $key => $value) {
                $extra_name[$request->input('language')[$key]] = $request->input('name')[$key];
            }
            $extra = new Extra();
            $extra->name = json_encode($extra_name);
            $extra->price = $request->input('price');
            $extra->property_id = $this->property;
            $extra->save();
            return redirect('admin/extra')->with('message_success', 'Extra aggiunto con successo');
        }
    }

    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
          'name.*' => 'required',
          'price' => 'required',
          'status.*'=> 'required'
        ], [
          'name.required' => trans('required.name_extra_required'),
          'price.required' => trans('required.price_extra_required'),
          'status.*.required' => trans('required.status_extra_required'),
        ]);

        if ($validator->fails()) {
            return redirect('admin/extra/create')->withErrors($validator);
        } else {
            $extra_name = [];
            foreach ($request->input('name') as $key => $value) {
                $extra_name[$request->input('language')[$key]] = $request->input('name')[$key];
            }
            $extra = Extra::findOrFail($id);
            $extra->name = json_encode($extra_name);
            $extra->price = $request->input('price');
            $extra->save();
            return redirect('admin/extra')->with('message_success', 'Extra aggiornato con successo');
        }
    }

    public function edit($id)
    {
        $extra = Extra::findOrFail($id);
        return view('admin.extra.edit', ['extra' => $extra]);
    }

    public function destroy($id)
    {
        $extra = Extra::findOrFail($id);
        $extra->delete();
        return redirect('admin/extra')->with('message_success', 'Extra eliminato con successo');
    }
}
