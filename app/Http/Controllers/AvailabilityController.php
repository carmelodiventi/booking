<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RatesType;
use App\Availability;

class AvailabilityController extends Controller
{
    //

    public function index(Request $request)
    {
        if ($request->get('start_date') && $request->get('end_date')) {
            $start = date('Y-m-d', strtotime($request->get('start_date')));
            $end = date('Y-m-d', strtotime($request->get('end_date')));
        } else {
            $now = strtotime('now');
            $start = date('Y-m-d', $now);
            $end = date('Y-m-d', strtotime('+15 days', $now));
        }

        if ($request->get('rooms_type')) {
            $rooms_type = \App\RoomsType::
             whereIn('id', $request->get('rooms_type'))
             ->where('isDisabled', 0)
             ->get();
        } else {
            $rooms_type = \App\RoomsType::where('isDisabled', 0)->get();
        }

        $rates_type = \App\RatesType::all();
        $rooms_type_ids = \App\RoomsType::pluck('id');
        $availability = \App\Availability::whereBetween('date', [$start,$end])
        ->whereIn('rooms_type_id',$rooms_type_ids)->get();

        return view('admin.availability', ['rooms_type' => $rooms_type,'rates_type' => $rates_type,'availability'=> $availability,'start' => $start, 'end'=> $end]);
    }
}
