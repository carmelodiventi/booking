<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Lang;
use App\Fragment;
use App\Translations;
use Validator;
use Illuminate\Support\Facades\Cache;

class FragmentController extends Controller
{
    //

    public function index()
    {
        $groups = Fragment::groupBy('group')->pluck('group', 'group');
        return view('admin.fragments.show', ['groups'=>$groups]);
    }

    public function list($group)
    {
        $fragments = Translations::where('group', $group)->get();
        return view('admin.fragments.list', ['fragments'=> $fragments]);
    }

    public function create(Request $request)
    {
        $groups = Fragment::groupBy('group')->pluck('group', 'group');
        return view('admin.fragments.create', ['groups'=>$groups]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'group' => 'required',
            'key' => 'required',
            'text.*' => 'required'
          ], [
            'group.required' => trans('required.group'),
            'key.required' => trans('required.key'),
            'text.*.required' => trans('required.text')
          ]);


        if ($validator->fails()) {
            return redirect('admin/fragments/create')->withErrors($validator);
        } else {
            $fragment = new Fragment();
            $fragment->key = $request->group . '.' .$request->key;
            $fragment->group = $request->group;
            foreach ($request->text as $key => $value) {
                $fragment->setTranslation('text', $request->language[$key], $value);
            }
            $fragment->save();
            return redirect('admin/fragments/create')->with('message_success', trans('admin.fragments_addedd_successfully'));
        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'key' => 'required',
            'text.*' => 'required'
          ], [
            'key.required' => trans('required.key'),
            'text.*.required' => trans('required.text')
          ]);

        if ($validator->fails()) {
            return redirect('admin/fragments')->withErrors($validator);
        } else {
            $fragment = Fragment::findOrFail($id);
            $fragment->key = $request->key;
            foreach ($request->text as $key => $value) {
                $fragment->setTranslation('text', $request->language[$key], $value);
            }
            $fragment->save();

            Cache::forget('locale');

            return response()->json([
              'status' => 'success',
              'msg' => trans('admin.fragments_updated_successfully')]
            );
        }
    }

    public function delete($id)
    {
        $fragment = Fragment::findOrFail($id);
        $fragment->delete();

        Cache::forget('locale');

        return response()->json([
          'status' => 'success',
          'msg' => trans('admin.fragments_deleted_successfully')]
        );
    }

    public function put()
    {
        /*$fragments_it = Lang::get('required', array(), 'it');
        dd($fragments_it);

        foreach ($fragments_it as $key => $value) {
            $fragment = new Fragment();
            $fragment->key = 'required.'.$key;
            $fragment->setTranslation('text', 'it', $value);
            $fragment->setTranslation('text', 'en', $value);
            $fragment->setTranslation('text', 'fr', $value);
            $fragment->group = 'required';
            $fragment->save();
        }*/
    }
}
