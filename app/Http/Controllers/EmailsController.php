<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Emails;
use App\Helpers\Helper;
use Auth;

class EmailsController extends Controller
{
    //
    protected $property;
    
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->property = Helper::get_property();
            return $next($request);
        });
    }

    public function index()
    {
        $emails = Emails::all();
        return view('admin.emails.show', ['emails' => $emails]);
    }

    public function create()
    {
        return view('admin.emails.create');
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
        'code' => 'required',
        'subject' => 'required',
        'body' => 'required',
        'language' => 'required',
        'status' => 'required'
        ],
        [
        'code.required' => 'Il Code è richiesto',
        'subject.required' => 'Anche l\'oggetto è richiesto',
        'body.required' => 'Anche il corpo è richiesto',
        'language.required' => 'Anche la Lingua è richiesta',
        'status.required' => 'Anche lo status è richiesto'
        ]
        );

        $email = new Emails();
        $email->code = $request->input('code');
        $email->subject = $request->input('subject');
        $email->body = $request->input('body');
        $email->lang = $request->input('language');
        $email->status = $request->input('status');
        $email->property_id = $this->property;
        $email->save();
        return redirect('admin/emails')->with('message_success', 'Il tuo Template Email è stato inserito');
    }

    public function edit($id)
    {
        $email = Emails::findOrFail($id);
        return view('admin.emails.edit', ['email' => $email]);
    }

    public function update(Request $request, $id)
    {
        $this->validate(
            $request,
            [
        'code' => 'required',
        'subject' => 'required',
        'body' => 'required',
        'language' => 'required',
        'status' => 'required'
        ],
        [
        'code.required' => 'Il Code è richiesto',
        'subject.required' => 'Anche l\'oggetto è richiesto',
        'body.required' => 'Anche il corpo è richiesto',
        'language.required' => 'Anche la Lingua è richiesta',
        'status.required' => 'Anche lo status è richiesto'
        ]
        );

        $email = Emails::findOrFail($id);
        $email->code = $request->input('code');
        $email->subject = $request->input('subject');
        $email->body = $request->input('body');
        $email->lang = $request->input('language');
        $email->status = $request->input('status');
        $email->property_id = $this->property;
        $email->save();
        return redirect('admin/emails')->with('message_success', 'Il tuo Template Email è stato aggiornato');
    }

    public function destroy($id)
    {
        $email = Emails::findOrFail($id);
        $email->delete();
        return redirect('admin/emails')->with('message_success', 'Il tuo Template Email è stato eliminato');
    }
}
