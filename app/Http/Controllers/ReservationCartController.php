<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;

class ReservationCartController extends Controller
{
    //

   public function get()
   {
       $cart = Cart::all();
       return response()->json($cart);
   }

    public function add(Request $request)
    {
        if (!$this->findItem($request->room_id)) {
            $item = Cart::add(
            $request->room_id,
            $request->room_type,
            1,
            $request->room_price,
            [
              'rooms_type_id' => $request->room_type_id,
              'rate_id' => $request->rate_id,
              'rate_name'=> $request->rate_name
            ]
            );
            $result = ['status'=> 'success','item' => $item];
        } else {
            $result = ['status'=> 'item_exits'];
        }
        return response()->json($result);
    }

    public function remove(Request $request)
    {
        $cart = Cart::remove($request->rowId);
        return response()->json($cart);
    }

    public function destroy()
    {
        $cart = Cart::destroy();
        return response()->json($cart);
    }

    private function findItem($id)
    {
        if ($cart = Cart::all()) {
            foreach ($cart as $item) {
                if ($item->id == $id) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
}
