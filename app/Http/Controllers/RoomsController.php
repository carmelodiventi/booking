<?php

namespace App\Http\Controllers;

use App\Rooms;
use App\RoomsType;
use App\RoomsBlocked;
use App\RoomsBooked;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Redirect;
use Auth;

class RoomsController extends Controller
{
    protected $property;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->property = Helper::get_property();
            return $next($request);
        });
    }

    public function index()
    {
        $rooms = Rooms::property()->join('rooms_type', 'rooms.rooms_type_id', '=', 'rooms_type.id')->get(
            ['rooms.id','rooms.name','rooms_type.type','rooms.status','rooms_type.capacity']);

      return view('admin.rooms.show', ['rooms' => $rooms]);
    }

    public function create()
    {
        $rooms_type = RoomsType::pluck('type', 'id');
        return view('admin.rooms.create', ['rooms_type' => $rooms_type]);
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
            'name' => 'required',
            'type' => 'required|numeric',
            'status' => 'required'
        ],
            [
                'name.required' => 'Il nome è richiesto',
                'type.required' => 'Anche il tipo è richiesto',
                'status.required' => 'Anche lo status è richiesto'
            ]
        );

        $room = new Rooms;
        $room->name = $request->input('name');
        $room->rooms_type_id = $request->input('type');
        $room->ordinal = $request->input('ordinal');
        $room->property_id = $this->property;
        $room->save();

        return redirect('admin/rooms')->with('message_success', 'La tua lista è stata aggiornata con una nuova camera');
    }


    public function edit($id)
    {
        $room = Rooms::find($id);
        $rooms_type = RoomsType::pluck('type', 'id');
        return view('admin.rooms.edit', ['room' => $room, 'rooms_type' => $rooms_type]);
    }

    public function update(Request $request, $id)
    {
        $room = Rooms::findOrFail($id);
        $this->validate(
            $request,
            [
            'name' => 'required',
            'type' => 'required|numeric',
            'status' => 'required'
        ],
            [
                'name.required' => 'Il nome è richiesto',
                'type.required' => 'Anche il tipo è richiesto',
                'status.required' => 'Anche lo status è richiesto'
            ]
        );
        $room->name = $request->input('name');
        $room->rooms_type_id = $request->input('type');
        $room->status = $request->input('status');
        $room->ordinal = $request->input('ordinal');
        $room->save();
        return redirect('admin/rooms')->with('message_success', 'La tua camera è stata aggiornata');
    }

    public function destroy($id)
    {
        $room = Rooms::findOrFail($id);
        $room->delete();
        return redirect('admin/rooms')->with('message_success', 'La camera è stata eliminata');
    }
}
