<?php

namespace App\Http\Controllers;


use App\Payments;
use App\Customers;
use App\RoomsType;
use App\ReservationAdjustments;
use App\Portals;
use App\PortalsCommission;
use App\Reservations;
use App\Rooms;
use App\RatesType;
use App\RoomsBooked;
use App\ReservationPayments;
use App\ReservationsHistory;
use App\Availability;
use App\Notifications\Notifications;
use App\Helpers\Helper;
use App\Helpers\MyAllocator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;


class ReservationsController extends Controller
{

    protected $property;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->property = Helper::get_property();
            return $next($request);
        });
    }

    //
    public function index(Request $request)
    {
        if ($request->input('from') && $request->input('to')) {
            $keyword = $request->input('keyword');
            $status = $request->input('status');
            $from = date('Y-m-d', strtotime($request->input('from')));
            $to = date('Y-m-d', strtotime($request->input('to')));
            $reservations = Reservations::getReservations($keyword, $from, $to, $status);
        } elseif ($request->input('keyword')) {
            $keyword = $request->input('keyword');
            $reservations = Reservations::getReservations($keyword);
        } elseif ($request->input('status')) {
            $status = $request->input('status');
            $reservations = Reservations::getReservations(null, null, null, $status);
        } else {
            $reservations = Reservations::getReservations();
        }


        return view('admin.reservations.show', ['reservations' => $reservations]);
    }

    public function create(Request $request)
    {
        if ($request->input('check_in') and $request->input('check_out')) {
            $rooms = Rooms::get_availability($request->input('check_in'), $request->input('check_out'))->pluck('name', 'id');
        } else {
            $rooms = Rooms::pluck('name', 'id');
        }
        //$rooms_type = RoomsType::pluck('type', 'id');
        $customers = Customers::select('id', DB::raw('CONCAT(COALESCE(`name`,\'\'), " ", COALESCE(`surname`,\'\')) AS full_name'))->pluck('full_name', 'id');
        $portals = Portals::pluck('name', 'id');
        $payments_list = Payments::pluck('name', 'initials');
        $stay = array('check_in' => $request->input('check_in'), 'check_out' => $request->input('check_out'));
        $lodgingID = $request->input('lodgingID');
        return view('admin.reservations.create', ['stay' => $stay, 'lodgingID' => $lodgingID, 'customers' => $customers, 'portals' => $portals, 'payments_list' => $payments_list]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer' => 'required',
            'check_in' => 'required|date',
            'check_out' => 'required|date',
            'rooms.*' => 'required',
            'rooms_type.*' => 'required',
            'guests.*' => 'required',
            'price.*' => 'required',
        ], [
            'customer.required' => trans('required.customer_required'),
            'check_in.required' => trans('required.check_in_required'),
            'check_out.required' => trans('required.check_out_required'),
            'rooms.*.required' => trans('required.rooms_required'),
            'rooms_type.*.required' => trans('required.rate_required'),
            'guests.*.required' => trans('required.guests_required'),
            'price.*.required' => trans('required.price_required'),
        ]);

        if ($validator->fails()) {
            return redirect('admin/reservations/create')->withErrors($validator);
        } else {
            if ($request->input('adjustments')) {
                $validator = Validator::make($request->all(), [
                    'adjustments[name].*' => 'required',
                    'adjustments[amount].*' => 'required',
                ], [
                    'adjustments[name].*.required' => 'Il campo Supplemento è richiesto',
                    'adjustments[amount].*.required' => 'Anche il campo Prezzo Supplemento'
                ]);
                if ($validator->fails()) {
                    return redirect('admin/reservations/create')->withErrors($validator);
                }
            }
            if ($request->input('origin') <> 1) {
                $validator = Validator::make($request->all(), [
                    'commission_percentage' => 'required',
                    'transaction_code' => 'required',
                ], [
                    'commission_percentage.required' => 'Il campo Percentuale è richiesto',
                    'transaction_code.required' => 'Anche il campo Prezzo Costo'
                ]);

                if ($validator->fails()) {
                    return redirect('admin/reservations/create')->withErrors($validator);
                }
            }
            if ($request->register_deposit) {
                $validator = Validator::make($request->all(), [
                    'payment_method' => 'required',
                    'amount' => 'required|regex:/^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$/',
                ], [
                    'commission_percentage.required' => 'Metodo di Pagamento richiesto',
                    'amount.required' => 'Importo pagamento richiesto'
                ]);

                if ($validator->fails()) {
                    return redirect('admin/reservations/create')->withErrors($validator);
                }

                if ($request->payment_method === 'credit_card') {
                    $card_data = [
                        'firstName'   => $request->firstName_cc,
                        'lastName'   => $request->lastName_cc,
                        'number'      => $request->number_cc,
                        'expiryMonth' => $request->month_exp_cc,
                        'expiryYear'  => $request->year_exp_cc,
                        'type' => $request->type_cc
                    ];

                    $response = Helper::set_card($card_data);

                    if ($response !== true) {
                        return redirect('admin/reservations/create')->withErrors($response);
                    }
                }
            }


            $data = Helper::standardize_data_to_reservation($request->all());
            $period = date('Y-m', strtotime($request->input('check_in')));

            // Reservations
            $reservations = new Reservations();
            $reservations->property_id = $this->property;
            $reservations->customer_id = $data->customer;
            $reservations->pnr = $data->pnr;
            $reservations->reservation_note = $data->note;
            $reservations->origin = $data->origin;
            $reservations->discount = $data->discount;
            $reservations->subtotal = $data->subtotal;
            $reservations->total = $data->total;

            if ($request->mark_to_review) {
                $reservations->review = 1;
            }
            $reservations->save();
            // set End date with 1 night in minus
            $data->check_out = date('Y-m-d', strtotime('-1 day', strtotime($data->check_out)));

            $rooms_type_ids = [];

            foreach ($data->rooms as $key => $room) {
                $rooms_type_ids[$key] = $room->rooms_type_id;
                $roomsfound = collect(\App\Rooms::get_rooms_available_by_type($this->property, $data->check_in, $data->check_out, 1, $room->guest))->first();

                if ($roomsfound) {
                    $roomsbooked = new \App\RoomsBooked();
                    $roomsbooked->property_id = $this->property;
                    $roomsbooked->room_id = $roomsfound->id;
                    $roomsbooked->rooms_type_id = $room->rooms_type_id;
                    $roomsbooked->reservation_id = $reservations->id;
                    $roomsbooked->check_in = $data->check_in;
                    $roomsbooked->check_out = $data->check_out;
                    $roomsbooked->guests = $room->guest;
                    $roomsbooked->price = $room->rate_amount;
                    $roomsbooked->rate_id = $room->rate_id;
                    $roomsbooked->status = 'CNF';
                    $roomsbooked->save();

                    /*
                    Scale Availability
                    */
                    \App\Availability::scale($data->check_in, $data->check_out, $room->rooms_type_id);
                } else {
                    $reservations = \App\Reservations::find($reservations->id);
                    $reservations->status = 'CAN';
                    $reservations->save();

                    $rooms = \App\RoomsBooked::where('reservation_id', $reservations->id)->get();
                    foreach ($rooms as $key => $room) {
                        \App\Availability::add($room->check_in, $room->check_out, $room->rooms_type_id);
                    }
                    \App\RoomsBooked::where('reservation_id', '=', $reservations->id)->update(['status' => 'CAN']);

                    $message = [
                        'type' => 'message_warning',
                        'text' => 'La tua prenotazione non è stata creata perchè non ci sono tutte o alcune camere disponibili per le date indicate'
                    ];
                    return redirect('/admin/reservation/details/' . $reservations->id)->with($message['type'], $message['text']);
                }
            }

            if ($request->input('adjustments')) {
                for ($i = 0; $i < count($request->input('adjustments')['name']); $i++) {
                    $adjustments = new ReservationAdjustments;
                    $adjustments->reservation_id = $reservations->id;
                    $adjustments->name = $request->input('adjustments')['name'][$i];
                    if ($request->input('adjustments')['type'][$i] == 1) {
                        $adjustments_amount = ($request->input('adjustments')['amount'][$i] / 100) * $reservations->total;
                    } else {
                        $adjustments_amount = $request->input('adjustments')['amount'][$i];
                    }
                    $adjustments->price = $adjustments_amount;
                    $adjustments->save();
                }
            }

            if ($request->input('origin') <> 1) {
                $portals_commission = new PortalsCommission();
                $portals_commission->reservation_id = $reservations->id;
                $portals_commission->portal_id = $request->input('origin');
                $portals_commission->percentage = $request->input('commission_percentage');
                $portals_commission->cost =  ($request->input('commission_percentage') / 100) * $request->input('subtotal');
                $portals_commission->transaction_code = $request->input('transaction_code');
                $portals_commission->save();
            }

            if ($request->input('register_deposit')) {
                $reservation_payments = new ReservationPayments;
                $reservation_payments->reservation_id = $reservations->id;
                $reservation_payments->method = $request->input('payment_method');
                $reservation_payments->payment_date = date('Y-m-d H.s:i');
                if ($request->input('amount') == $reservations->total) {
                    $reservation_payments->payment_type = 'payment';
                    $reservations = Reservations::findOrFail($reservations->id);
                    $reservations->status = 'SLD';
                    $reservations->save();
                } else {
                    $reservations = Reservations::findOrFail($reservations->id);
                    $reservations->status = 'CNF';
                    $reservations->save();
                }
                $reservation_payments->total = $request->amount;
                if ($request->received) {
                    $reservation_payments->status = 'Welded';
                } else {
                    $reservation_payments->status = 'On Hold';
                }
                $reservation_payments->save();
                if ($request->input('payment_method') == 'credit_card') {
                    $reservation_credit_card = Reservations::storeCreditCard($reservations->customer_id, $reservations->id, $card_data);
                }
            }

            $mail_data = [
                'address' => Helper::get_setting('email')->address,
                'subject' => trans('emails.new_reservation_added'),
                'name' => Helper::get_setting('email')->name,
                'view' => 'emails.new_reservation_added',
                'with' => array(
                    'hotel' => Helper::get_setting('hotel_data'),
                    'reservation_id' => $reservations->id
                )
            ];

            $users = \App\User::where('property_id', $this->property)
                ->join('properties', 'users.property_id', '=', 'properties.id')->get(['users.id']);

            foreach ($users as $user) {
                $user->notify(new Notifications(
                    [
                        'title' => trans('admin.new_reservations_added'),
                        'message' => trans('admin.new_reservations_message', ['pnr' => $reservations->pnr])
                    ]
                ));
            }

            // Call Myallocator Function

            if (Helper::get_setting('my_allocator')->active) {
                $allocations = \App\Availability::get_availability_status($data->check_in, $data->check_out, $rooms_type_ids);
                $response = MyAllocator::GetMyAllocatorResponse(MyAllocator::ARIUpdate($allocations));

                if ($response->Success == true) {
                    $message = [
                        'type' => 'message_success',
                        'text' => 'La tua prenotazione è stata aggiunta ed è stata inviata una richiesta al servizio Channel Manager'
                    ];
                } else {
                    $message = [
                        'type' => 'message_warning',
                        'text' => 'La tua prenotazione è stata aggiunta ma si è verificato un errore nel servizio Channel Manager<br>' . $response->Errors
                    ];
                }
            } else {
                $message = [
                    'type' => 'message_warning',
                    'text' => 'La tua prenotazione è stata aggiunta ma non è stato aggiornato il servizio Channel Manager è possibile che non sia attivato'
                ];
            }


            return redirect('admin/planning?interval=' . $period)->with($message['type'], $message['text']);
        }
    }

    public function cancel(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            ['id' => 'required'],
            ['id.required' => 'reservation id is required']
        );

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()]);
        } else {
            $id = $request->input('id');
            $data = (object)[];
            $data->rooms = RoomsBooked::where('reservation_id', '=', $id)->get();
            $rooms_type_ids = [];

            foreach ($data->rooms as $key => $room) {
                $rooms_type_ids[$key] = $room->rooms_type_id;
                $data->check_in = $room->check_in;
                $data->check_out = $room->check_out;
                Availability::add($data->check_in, $data->check_out, $room->rooms_type_id);
            }

            $reservation = Reservations::findOrFail($id);
            $reservation->status = 'CAN';
            $reservation->save();
            RoomsBooked::where('reservation_id', '=', $id)->update(['status' => 'CAN']);

            if (Helper::get_setting('my_allocator')->active) {
                if (isset($reservation->myallocatorId) && !empty($reservation->myallocatorId)) {
                    $response = MyAllocator::GetMyAllocatorResponse(MyAllocator::BookingCancel($reservation->myallocatorId, $request->input('cancellationReason')));
                } else {
                    $allocations = Availability::get_availability_status($data->check_in, $data->check_out, $rooms_type_ids);
                    $response = MyAllocator::GetMyAllocatorResponse(MyAllocator::ARIUpdate($allocations));
                }
                if ($response->Success == true) {
                    $message = [
                        'success' => true,
                        'type' => 'message_success',
                        'text' => 'La tua prenotazione è stata eliminata ed è stata inviata una richiesta al servizio Channel Manager' . $reservation->MyAllocatorId
                    ];
                } else {
                    $message = [
                        'success' => true,
                        'type' => 'message_warning',
                        'text' => 'La tua prenotazione è stata eliminata ma si è verificato un errore nel servizio Channel Manager<br>' . $response->Errors
                    ];
                }
            } else {
                $message = [
                    'success' => true,
                    'type' => 'message_warning',
                    'text' => 'La tua prenotazione è stata eliminata ma non è stato aggiornato il servizio Channel Manager è possibile che non sia attivato'
                ];
            }

            return response()->json($message);
        }
    }

    public function destroy(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            ['id' => 'required'],
            ['id.required' => 'reservation id is required']
        );

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()]);
        } else {
            $id = $request->input('id');

            $reservation = Reservations::findOrFail($id);
            if ($reservation->status !== 'CAN') {
                return response()->json(['type' => 'warning', 'text' => trans('admin.you_must_first_cancel_the_reservation')]);
            }
            $reservation->delete();
            RoomsBooked::where('reservation_id', '=', $id)->delete();

            $message = [
                'success' => true,
                'type' => 'message_success',
                'text' => 'La tua prenotazione è stata eliminata ed è stata inviata una richiesta al servizio Channel Manager'
            ];

            return response()->json($message);
        }
    }

    public function details($id)
    {


        $reservation = Reservations::getReservation($id);

        if ($reservation) {
            $topay = Reservations::getTopay($id);
            $reservation_adjustments = ReservationAdjustments::where('reservation_id', $reservation->id)->get();
            $reservation_payments =  ReservationPayments::where('reservation_id', $reservation->id)->get();
            $reservation_history =  ReservationsHistory::where('reservation_id', $reservation->id)->get();
            $rooms_booked = RoomsBooked::getRoomsBookedByReservationID($id);
            $hotel = Helper::get_setting('hotel_data');


            return view('admin.reservations.details', [
                'reservation' => $reservation,
                'topay' => $topay,
                'reservation_history' => $reservation_history,
                'reservation_adjustments' => $reservation_adjustments,
                'reservation_payments' => $reservation_payments,
                'hotel' => $hotel,
                'rooms_booked' => $rooms_booked
            ]);
        } else {
            return redirect('admin/reservations/')->with('message_error', trans('admin.reservation_not_found'));
        }
    }

    public function adjustments($id, Request $request)
    {
        $reservation = Reservations::getReservation($id);

        if ($request->all()) {
            $validator = Validator::make($request->all(), [
                'description' => 'required',
                'amount' => 'required',
            ], [
                'description.required' => trans('required.description_required'),
                'amount.required' => trans('required.amount_required'),
            ]);
            if ($validator->fails()) {
                return view('admin.reservations.adjustments', ['reservation' => $reservation])->withErrors($validator);
            } else {
                $adjustments = new ReservationAdjustments;
                $adjustments->reservation_id = $reservation->id;
                $adjustments->name = $request->description;
                if ($request->adjustaments['type'] == 1) {
                    $adjustments_amount = ($request->amount / 100) * $reservation->total;
                } else {
                    $adjustments_amount = $request->amount;
                }
                $adjustments->price = $adjustments_amount;
                $adjustments->save();
                // Update Reservation To Pay and Total
                $total = Reservations::getTotal($reservation->id);
                $reservation = Reservations::findOrFail($reservation->id);
                if ($reservation->status == 'SLD') {
                    $reservation->status = 'CNF';
                }
                $reservation->total = $total;
                $reservation->save();

                return redirect('admin/reservation/details' . '/' . $reservation->id)->with('message_success', trans('admin.reservation_updated'));
            }
        } else {
            return view('admin.reservations.adjustments', ['reservation' => $reservation]);
        }
    }

    public function customer($id, Request $request)
    {

        $reservation = Reservations::findOrFail($id);
        $customer = Customers::select(DB::raw("CONCAT(name,' ',surname) as name"), "id")->pluck('name', 'id');
        if ($request->all()) {
            $validator = Validator::make($request->all(), [
                'customer' => 'required',
            ], [
                'name.required' => 'Il nome è richiesto'
            ]);
            if ($validator->fails()) {
                return view('admin.reservations.customer', ['customer' => $customer, 'reservation' => $reservation])->withErrors($validator);
            } else {
                $reservation->customer_id = $request->customer;
                $reservation->save();

                return redirect('admin/reservation/details' . '/' . $reservation->id)->with('message_success', 'Ospite aggiornato');
            }
        } else {
            return view('admin.reservations.customer', ['customer' => $customer, 'reservation' => $reservation]);
        }
    }

    public function agency($id, Request $request)
    {
        $reservation = Reservations::getReservation($id);
        $agency = Portals::all()->pluck('name', 'id');

        if ($request->all()) {
            $validator = Validator::make($request->all(), [
                'origin' => 'required',
                'transaction_code' => 'required',
                'commission_percentage' => 'required',
            ], [
                'origin.required' => 'Origine richiesta',
                'transaction_code.required' => 'Codice transazione richiesto',
                'commission_percentage.required' => 'Percentuale richiesta'
            ]);
            if ($validator->fails()) {
                return view('admin.reservations.agency', ['agency' => $agency, 'reservation' => $reservation])->withErrors($validator);
            } else {
                $reservation = Reservations::findOrFail($id);
                $reservation->origin = $request->origin;
                $reservation->save();
                //
                $portals_commission = PortalsCommission::where('reservation_id', $reservation->id)->first();
                if (!$portals_commission) {
                    $portals_commission = new PortalsCommission;
                }
                $portals_commission->reservation_id = $reservation->id;
                $portals_commission->portal_id = $request->origin;
                $portals_commission->cost = ($reservation->total / 100) * $request->commission_percentage;
                $portals_commission->transaction_code = $request->transaction_code;
                $portals_commission->percentage = $request->commission_percentage;
                $portals_commission->save();

                return redirect('admin/reservation/details' . '/' . $reservation->id)->with('message_success', 'Agency aggiornato');
            }
        } else {
            return view('admin.reservations.agency', ['agency' => $agency, 'reservation' => $reservation]);
        }
    }

    public function accomodations($reservation_id, Request $request)
    {
        $reservation = Reservations::getReservation($reservation_id);
        $rooms_booked = RoomsBooked::getRoomsBookedByReservationID($reservation_id);
        $rooms_type = RoomsType::pluck('type', 'id');
        $rates_type = RatesType::get(['name', 'id'])->toArray();
        $rooms = Rooms::property()->pluck('name', 'id');

        if ($request->has('add_rooms')) {

            $validator = Validator::make($request->all(), [
                'check_in' => 'required|date',
                'check_out' => 'required|date'
            ], [
                'check_in.required' => trans('required.check_in_required'),
                'check_out.required' => trans('required.check_out_required')
            ]);
            if ($validator->fails()) {
                return view('admin.reservations.accomodations', [
                    'rooms_booked' => $rooms_booked,
                    'rooms' => $rooms,
                    'rooms_type' => $rooms_type,
                    'rates_type' => $rates_type,
                    'reservation' => $reservation
                ])->withErrors($validator);
            } else {

                $data = Helper::standardize_data_to_reservation($request->all());
                $period = date('Y-m', strtotime($request->input('check_in')));


                // Reservations
                $reservation = Reservations::findOrFail($reservation_id);
                $reservation->subtotal = $reservation->subtotal + $data->total;
                $reservation->total = $reservation->total + $data->total;
                // set End date with 1 night in minus
                $data->check_out = date('Y-m-d', strtotime('-1 day', strtotime($data->check_out)));

                $rooms_type_ids = [];

                foreach ($data->rooms as $key => $room) {
                    $rooms_type_ids[$key] = $room->rooms_type_id;
                    $roomsfound = collect(\App\Rooms::get_rooms_available_by_type($this->property, $data->check_in, $data->check_out, 1, $room->guest))->first();

                    if ($roomsfound) {
                        $roomsbooked = new \App\RoomsBooked();
                        $roomsbooked->property_id = $this->property;
                        $roomsbooked->room_id = $roomsfound->id;
                        $roomsbooked->rooms_type_id = $room->rooms_type_id;
                        $roomsbooked->reservation_id = $reservation->id;
                        $roomsbooked->check_in = $data->check_in;
                        $roomsbooked->check_out = $data->check_out;
                        $roomsbooked->guests = $room->guest;
                        $roomsbooked->price = $room->rate_amount;
                        $roomsbooked->rate_id = $room->rate_id;
                        $roomsbooked->status = 'CNF';
                        $roomsbooked->save();

                        /*
                    Scale Availability
                    */
                        \App\Availability::scale($data->check_in, $data->check_out, $room->rooms_type_id);
                    } else {
                        $reservation = \App\Reservations::find($reservation->id);
                        $reservation->status = 'CAN';
                        $reservation->save();

                        $rooms = \App\RoomsBooked::where('reservation_id', $reservation->id)->get();
                        foreach ($rooms as $key => $room) {
                            \App\Availability::add($room->check_in, $room->check_out, $room->rooms_type_id);
                        }
                        \App\RoomsBooked::where('reservation_id', '=', $reservation->id)->update(['status' => 'CAN']);

                        $message = [
                            'type' => 'message_warning',
                            'text' => 'La tua prenotazione non è stata creata perchè non ci sono tutte o alcune camere disponibili per le date indicate'
                        ];
                        return redirect('/admin/reservation/details/' . $reservation->id)->with($message['type'], $message['text']);
                    }
                }

                $reservation->status = 'CNF';
                $reservation->save();

                $users = \App\User::where('property_id', $this->property)
                    ->join('properties', 'users.property_id', '=', 'properties.id')->get(['users.id']);

                foreach ($users as $user) {
                    $user->notify(new \App\Notifications\Notifications(
                        [
                            'title' => trans('admin.reservations_modified'),
                            'message' => trans('admin.reservations_modified_message', ['pnr' => $reservation->pnr])
                        ]
                    ));
                }

                // Call Myallocator Function

                if (Helper::get_setting('my_allocator')->active) {
                    $allocations = \App\Availability::get_availability_status($data->check_in, $data->check_out, $rooms_type_ids);
                    $response = MyAllocator::GetMyAllocatorResponse(MyAllocator::ARIUpdate($allocations));

                    if ($response->Success == true) {
                        $message = [
                            'type' => 'message_success',
                            'text' => 'La tua prenotazione è stata aggiunta ed è stata inviata una richiesta al servizio Channel Manager'
                        ];
                    } else {
                        $message = [
                            'type' => 'message_warning',
                            'text' => 'La tua prenotazione è stata aggiunta ma si è verificato un errore nel servizio Channel Manager<br>' . $response->Errors
                        ];
                    }
                } else {
                    $message = [
                        'type' => 'message_warning',
                        'text' => 'La tua prenotazione è stata aggiunta ma non è stato aggiornato il servizio Channel Manager è possibile che non sia attivato'
                    ];
                }


                return redirect('admin/planning?interval=' . $period)->with($message['type'], $message['text']);
            }
        } elseif ($request->has('edit_rooms')) {

            $validator = Validator::make($request->all(), [
                'check_in.*' => 'date|required',
                'check_out.*' => 'date|required',
                'rooms.*' => 'required',
                'rooms_booked.*' => 'required',
                'rates_type.*' => 'required',
                'guests.*' => 'required',
                'prices.*' => 'required'
            ], [
                'check_in.*.required' => trans('required.check_in_required'),
                'check_out.*.required' => trans('required.check_out_required'),
                'rooms_booked.*.required' => trans('required.check_out_required'),
                'rooms.*.required' => trans('required.rooms_required'),
                'rates_type.*.required' => trans('required.rates_type_required'),
                'guests.*.required' => trans('required.guests_required'),
                'price.*.required' => trans('required.price_required'),
            ]);
            if ($validator->fails()) {

                return view(
                    'admin.reservations.accomodations',
                    [
                        'rooms_booked' => $rooms_booked,
                        'rooms' => $rooms,
                        'rooms_type' => $rooms_type,
                        'rates_type' => $rates_type,
                        'reservation' => $reservation
                    ]
                )->withErrors($validator);
            } else {
                // Reservations
                $reservation = Reservations::findOrFail($reservation_id);
                $total = 0.00;
                foreach ($request->rooms_booked as $key => $value) {
                    $rooms_booked = RoomsBooked::findOrFail($request->rooms_booked[$key]);
                    $rooms_booked->room_id = $request->rooms[$key];
                    $rooms_booked->check_in = $request->check_in[$key];
                    $rooms_booked->check_out = date('Y-m-d', strtotime('-1 day', strtotime($request->check_out[$key])));
                    $rooms_booked->guests = $request->guests[$key];
                    $rooms_booked->price = $request->prices[$key];
                    $rooms_booked->rate_id = $request->rates_type[$key];
                    $rooms_booked->save();
                    $total = $total + $rooms_booked->price;
                }
                $reservation->subtotal = $total;
                $reservation->total = $total;
                $reservation->save();

                $message = ['type' => 'message_success', 'text' => 'The rooms have been updated'];
                return redirect('/admin/reservation/details/' . $reservation->id)->with($message['type'], $message['text']);
            }
        } else {

            return view(
                'admin.reservations.accomodations',
                [
                    'rooms_booked' => $rooms_booked,
                    'rooms' => $rooms,
                    'rooms_type' => $rooms_type,
                    'rates_type' => $rates_type,
                    'reservation' => $reservation
                ]
            );
        }
    }

    public function accomodations_remove($reservation_id, $id)
    {
        $room_booked = RoomsBooked::where('reservation_id', '=', $reservation_id)
            ->where('rooms_booked.id', '=', $id)
            ->first();
        $room_booked->delete();

        Availability::add($room_booked->check_in, $room_booked->check_out, $room_booked->rooms_type_id);

        if (Helper::get_setting('my_allocator')->active) {
            $allocations = Availability::get_availability_status($room_booked->check_in, $room_booked->check_out, [$room_booked->rooms_type_id]);
            $response = MyAllocator::GetMyAllocatorResponse(MyAllocator::ARIUpdate($allocations));

            if ($response->Success == true) {
                $message = [
                    'type' => 'message_success',
                    'text' => 'La camera è stata eliminata ed è stata inviata una richiesta al servizio Channel Manager'
                ];
            } else {
                $message = [
                    'type' => 'message_warning',
                    'text' => 'La camera è stata eliminata ma si è verificato un errore nel servizio Channel Manager<br>' . $response->Errors
                ];
            }
        } else {
            $message = [
                'type' => 'message_warning',
                'text' => 'La camera è stata eliminata ma non è stato aggiornato il servizio Channel Manager è possibile che non sia attivato'
            ];
        }

        return back()->with($message['type'], $message['text']);
    }

    public function register_deposit($id, Request $request)
    {
        $reservation = Reservations::getReservation($id);
        $payments_list = Payments::pluck('name', 'initials');
        $topay = Reservations::getTopay($id);

        if ($request->all()) {
            $validator = Validator::make($request->all(), [
                'payment_method' => 'required',
                'topay' => 'required',
                'amount' => 'required|regex:/^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$/',
            ], [
                'payment_method.required' => trans('required.deposit_method_required'),
                'topay.required' => trans('required.topay_required'),
                'amount.required' => trans('required.amount_required')
            ]);

            if ($validator->fails()) {
                return view('admin.reservations.register-deposit', ['reservation' => $reservation, 'topay' => $topay, 'payments_list' => $payments_list])->withErrors($validator);
            } else {
                if ($request->amount <= $topay) {
                    $reservation_payments = new ReservationPayments;
                    $reservation_payments->reservation_id = $reservation->id;
                    $reservation_payments->payment_type = 'deposit';
                    $reservation_payments->method = $request->payment_method;
                    $reservation_payments->payment_date = date('Y-m-d H.s:i');
                    $reservation_payments->total = $request->amount;
                    if ($request->received) {
                        $reservation_payments->status = 'Welded';
                    } else {
                        $reservation_payments->status = 'On Hold';
                    }
                    $reservation_payments->save();

                    $reservation = Reservations::findOrFail($id);
                    if ($topay == '0.00') {
                        $reservation->status = 'SLD';
                    } else {
                        $reservation->status = 'CNF';
                    }
                    $reservation->save();
                    return redirect('admin/reservation/details' . '/' . $reservation->id)->with('message_success', trans('admin.reservation_updated'));
                }
            }
        } else {
            return view('admin.reservations.register-deposit', ['reservation' => $reservation, 'topay' => $topay, 'payments_list' => $payments_list]);
        }


        /*
        Registrare un pagamento, sulla tabella reservation_payments, ci saranno due tipi di pagamento (deposito e pagamento)
        il deposito è un acconto sul totale
        il pagamento è il saldo totale
        la differanza da pagare (Importo rimanente da saldare) è la differenza tra il totale dei pagamenti e il totale della prenotazione, per le prenotazioni vecchie
        si deve creare un pagaento di tipo (carta di credito) con la differenza tra il totale della prenotazione meno il topay della tabella reservations, cosi da allineare con le nuove modalità di insertimento dei pagamenti.
        */
    }

    public function register_payment($id, Request $request)
    {
        $reservation = Reservations::getReservation($id);
        $topay = Reservations::getTopay($id);
        $payments_list = Payments::pluck('name', 'initials');

        if ($request->all()) {
            $validator = Validator::make($request->all(), [
                'payment_method' => 'required',
                'topay' => 'required',
                'amount' => 'required|regex:/^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$/',
            ], [
                'payment_method.required' => trans('required.deposit_method_required'),
                'topay.required' => trans('required.topay_required'),
                'amount.required' => trans('required.amount_required')
            ]);

            if ($validator->fails()) {
                return view('admin.reservations.register-payment', ['reservation' => $reservation, 'topay' => $topay, 'payments_list' => $payments_list])->withErrors($validator);
            } else {
                $reservation_payments = new ReservationPayments;
                $reservation_payments->reservation_id = $reservation->id;
                $reservation_payments->payment_type = 'payment';
                $reservation_payments->method = $request->payment_method;
                $reservation_payments->payment_date = date('Y-m-d H.s:i');
                $reservation_payments->total = $request->amount;
                if ($request->received) {
                    $reservation_payments->status = 'Welded';
                    $status = 'SLD';
                } else {
                    $reservation_payments->status = 'On Hold';
                    $status = 'CNF';
                }
                $reservation_payments->save();

                $reservation = Reservations::findOrFail($id);
                $reservation->status = $status;
                $reservation->save();

                return redirect('admin/reservation/details' . '/' . $reservation->id)->with('message_success', trans('admin.reservation_updated'));
            }
        } else {
            return view('admin.reservations.register-payment', ['reservation' => $reservation, 'topay' => $topay, 'payments_list' => $payments_list]);
        }
    }

    public function mark_to_review($id)
    {
        $reservation = Reservations::findOrFail($id);
        $reservation->review = 1;
        $reservation->save();
        return redirect('admin/reservation/details' . '/' . $reservation->id)->with('message_success', trans('admin.reservation_updated'));
    }

    public function mark_to_complete($id)
    {
        $reservation = Reservations::findOrFail($id);
        $reservation->review = 0;
        $reservation->save();
        return redirect('admin/reservation/details' . '/' . $reservation->id)->with('message_success', trans('admin.reservation_updated'));
    }

    public function note($id, Request $request)
    {
        $reservation = Reservations::getReservation($id);

        if ($request->all()) {
            // Update Reservation To Pay and Total
            $reservation = Reservations::findOrFail($reservation->id);
            $reservation->reservation_note = $request->note;
            $reservation->save();

            return redirect('admin/reservation/details' . '/' . $reservation->id)->with('message_success', trans('admin.reservation_updated'));
        } else {
            return view('admin.reservations.note', ['reservation' => $reservation]);
        }
    }

    public function discount($id, Request $request)
    {
        $reservation = Reservations::getReservation($id);
        $topay = Reservations::getTopay($id);
        if ($request->all()) {
            $validator = Validator::make($request->all(), [
                'amount' => 'required',
            ], [
                'amount.required' => trans('required.amount'),
            ]);

            if ($validator->fails()) {
                return view('admin.reservations.discount', ['reservation' => $reservation, 'topay' => $topay])->withErrors($validator);
            } else {
                $reservation = Reservations::findOrFail($id);
                if ($request->amount > $topay) {
                    return view('admin.reservations.discount', ['reservation' => $reservation, 'topay' => $topay])->withErrors(trans('admin.amount_exceeds'));
                }
                $reservation->discount = $request->amount;
                $reservation->save();

                $total = Reservations::getTotal($id);
                $reservation->total = $total;
                $reservation->save();

                return redirect('admin/reservation/details' . '/' . $reservation->id)->with('message_success', trans('admin.reservation_updated'));
            }
        } else {
            return view('admin.reservations.discount', ['reservation' => $reservation, 'topay' => $topay]);
        }
    }

    public function guests($reservation_id, $id, Request $request)
    {
        $reservation = Reservations::getReservation($reservation_id);
        $room_booked = RoomsBooked::join('rooms', 'rooms_booked.room_id', '=', 'rooms.id')
            ->join('rooms_type', 'rooms_type.id', '=', 'rooms.rooms_type_id')
            ->where('rooms_booked.id', '=', $id)
            ->first(['rooms_booked.id', 'rooms_booked.room_id', 'rooms.name', 'rooms_booked.check_in', 'rooms_booked.check_out', 'rooms_type.type', 'rooms_booked.price', 'rooms_booked.guests']);

        if ($request->add_rooms) {
            $validator = Validator::make(
                $request->all(),
                [
                    'guests' => 'required',
                ],
                [
                    'guests.required' => trans('required.guests'),
                ]
            );

            if ($validator->fails()) {
                return view('admin.reservations.guests', ['reservation' => $reservation])->withErrors($validator);
            } else {
                $room_booked = RoomsBooked::findOrFail($id);
                $room_booked->guests = $request->input('guests');
                $room_booked->save();
                return back()->with('message_success', trans('admin.reservation_updated'));
            }
        } else {
            return view('admin.reservations.guests', ['reservation' => $reservation, 'room_booked' => $room_booked]);
        }
    }

    public function fix()
    {
        $reservations = Reservations::get();
        foreach ($reservations as $reservation) {
            $reservation_to_edit = Reservations::find($reservation->id);

            $res_data = $reservation_to_edit->data;
            if ($res_data) {
                $res_data = @unserialize($res_data);
                if (empty($res_data)) {
                    $res_data = @json_decode($res_data);
                }
                $reservation_data = [];
                (!empty($res_data->OrderId)) ? $reservation_data['OrderId'] = $res_data->OrderId : '';
                (!empty($res_data->Acknowledged)) ? $reservation_data['Acknowledged'] = $res_data->Acknowledged : '';
                (!empty($res_data->IsCancellation)) ? $reservation_data['IsCancellation'] = $res_data->IsCancellation : '';
                (!empty($res_data->IsModification)) ? $reservation_data['IsModification'] = $res_data->IsModification : '';
                (!empty($reservation_data)) ? $reservation_to_edit->data = json_encode($reservation_data) : $reservation_to_edit->data = null;
            }
            $reservation_to_edit->save();
            $rooms_booked = RoomsBooked::where('reservation_id', $reservation->id)->get();

            foreach ($rooms_booked as $key => $room) {
                $data = $room->reservation_data;
                if ($data) {
                    $data = @unserialize($data);
                    is_array($data) ? (object)$data : $data;
                    //is_array($data) ? dd($data) : dd($data);
                    $room_to_edit = RoomsBooked::find($room->id);

                    $reservation_data = [];
                    (!empty($data->OccupantLName)) ? $reservation_data['OccupantLName'] = $data->OccupantLName : '';
                    (!empty($data->OccupantSmoker)) ? $reservation_data['OccupantSmoker'] = $data->OccupantSmoker : '';
                    (!empty($data->OccupantNote)) ? $reservation_data['OccupantNote'] = $data->OccupantNote : '';
                    (!empty($reservation_data)) ? $room_to_edit->reservation_data = json_encode($reservation_data) : $room_to_edit->reservation_data = null;
                    $room_to_edit->rate_id = (!empty($data->RateId)) ? $data->RateId : $room->rate_id;
                    $room_to_edit->rate_desc = (!empty($data->RateDesc)) ? $data->RateDesc : '';
                    $room_to_edit->save();
                }
            }
        }
    }

    public function email($id)
    {

        $reservation = Reservations::getReservation($id);
        if (isset($reservation->id)) {
            $topay = Reservations::getTopay($id);
            $reservation_adjustments = ReservationAdjustments::where('reservation_id', $reservation->id)->get();
            $reservation_payments =  ReservationPayments::where('reservation_id', $reservation->id)->get();
            $reservation_history =  ReservationsHistory::where('reservation_id', $reservation->id)->get();
            $rooms_booked = RoomsBooked::getRoomsBookedByReservationID($id);
            $customer = Customers::findOrFail($reservation->customer_id);
            $hotel = Helper::get_setting('hotel_data');
            $reservation_dates = Helper::get_checkin_checkout_dates($rooms_booked);
            $reservation_dates = [
                'check-in' => $reservation_dates[0],
                'check-out' => date('Y-m-d', strtotime('+1 day', strtotime($reservation_dates[1])))
            ];

            $data = [
                'property_id' => $this->property,
                'hotel' => Helper::get_setting('hotel_data'),
                'pnr' => $reservation->pnr,
                'date' => date('Y-m-d H:i:s'),
                'check_in' => $reservation_dates['check-in'],
                'check_out' => $reservation_dates['check-out'],
                'nights' => Helper::get_nights_to_stay($reservation_dates['check-in'], $reservation_dates['check-out']),
                'guests' => 0,
                'customer' => $customer,
                'rooms' => $rooms_booked,
                'fees' => '',
                'note' => $reservation->note,
                'payment_selected' => $reservation->payment_method,
                'offert' =>  $reservation->offert_code,
                'discount' => $reservation->discount,
                'totaltax' => 0.00,
                'subtotal' => $reservation->subtotal,
                'grandtotal' => $reservation->total
            ];


            // send mail ticket to customer

            Mail::send(
                'emails.ticket',
                $data,
                function ($message) use ($customer) {
                    $message->to($customer->email)->subject(trans('emails.reservation_confirm'));
                }
            );

            return redirect("admin/reservation/details/$id")->with('message_success', trans('admin.reservation_confirm'));
        } else {
            return redirect("admin/reservation/details/$id")->with('message_error', trans('admin.reservation_not_found'));
        }
    }
}
