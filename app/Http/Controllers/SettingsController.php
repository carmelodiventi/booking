<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Settings;
use App\Helpers\Helper;
use Cache;

class SettingsController extends Controller
{
    //
    public function index()
    {
        $settings = config()->get('settings.' . Helper::get_property());
        return view('admin.settings', ['settings'=> $settings]);
    }


    public function create()
    {
    }

    public function update(Request $request)
    {
        $values = $request->all();
        unset($values['_token']);


        $settings = Settings::where('key', $request->key)->first();
        if($settings == null){
            $settings = new Settings();
            $settings->property_id = Helper::get_property();
            $settings->key = $request->key;
        }
        $settings->value = json_encode($values);
        $settings->save();

        Cache::forget('settings');

        return redirect('admin/settings')->with('message_success', 'Le Tue impostazioni sono state aggiornate');
    }


    public function hotel(Request $request)
    {
        $this->validate(
            $request,
            [
              'name' => 'required',
              'email' => 'required|email',
              'phone' => 'required',
              'address' => 'required',
              'city' => 'required',
              'country' => 'required'
          ],
              [
                  'name.required' => 'Il nome è richiesto',
                  'email.required' => 'Anche un indirizzo email è richiesto',
                  'email.email' => 'Un indirizzo email valido è richiesto',
                  'phone.required' => 'Anche il telefono è richiesto',
                  'city.required' => 'Anche la città è richiesta',
                  'country.required' => 'Anche la nazione è richiesta',
              ]
        );

        if (Settings::update_settings_hotel($request)) {
            return redirect('admin/settings')->with('message_success', 'Le Tue impostazioni sono state aggiornate');
        }
    }

    public function paypal(Request $request)
    {
        if ($request->input('active') == true) {
            $this->validate(
                $request,
                [
              'name' => 'required',
              'api_username' => 'required',
              'api_password' => 'required',
            ],
            [
                'name.required' => 'Il nome è richiesto',
                'api_username.required' => 'Api Username richiesto',
                'api_password.required' => 'Api Password richiesto',
            ]
            );
        }


        if (Settings::update_settings_paypal($request)) {
            return redirect('admin/settings')->with('message_success', 'Le Tue impostazioni sono state aggiornate');
        }
    }


    public function stripe(Request $request)
    {
        if ($request->input('active') == true) {
            $this->validate(
                $request,
                ['publishable_key' => 'required','secret_key' => 'required'],
            ['publishable_key.required' => 'Publishable key è richiesto','secret_key.required' => 'Secret key richiesto']
            );
        }


        if (Settings::update_settings_stripe($request)) {
            return redirect('admin/settings')->with('message_success', 'Le Tue impostazioni sono state aggiornate');
        }
    }

    public function credit_card(Request $request)
    {
        if (Settings::update_settings_credit_card($request)) {
            return redirect('admin/settings')->with('message_success', 'Le Tue impostazioni sono state aggiornate');
        }
    }

    public function currency(Request $request)
    {
        $this->validate(
            $request,
            [
            'code' => 'required',
            'symbol' => 'required'
            ],
            [
                'code.required' => 'Il Codice è richiesto',
                'symbol.required' => 'Anche il simbolo è richiesto',
            ]
        );


        if (Settings::update_settings_currency($request)) {
            return redirect('admin/settings')->with('message_success', 'Le Tue impostazioni sono state aggiornate');
        }
    }

    public function my_allocator(Request $request)
    {
        $this->validate(
            $request,
            [
            'api_url' => 'required',
            'vendor_id' => 'required',
            'vendor_password' => 'required',
            'user_id' => 'required',
            'user_password' => 'required',
            'user_token' => 'required',
            'callback_url' => 'required',
            'property_id' => 'required',
            ],
            [
            'api_url.required' => 'Api URL è richiesto',
            'vendor_id.required' => 'Vendor ID è richiesto',
            'vendor_password.required' => 'Vendor Password è richiesto',
            'user_id.required' => 'User ID è richiesto',
            'user_password.required' => 'User Password è richiesto',
            'user_token.required' => 'User Token è richiesto',
            'callback_url.required' => 'Calback URL è richiesto',
            'property_id.required' => 'Property ID è richiesto',
            ]
        );

        if (Settings::update_settings_my_allocator($request)) {
            if (\App\Helpers\Helper::get_setting('my_allocator')->active) {
                $response = \App\Helpers\MyAllocator::GetMyAllocatorResponse(\App\Helpers\MyAllocator::VendorSet());
                if ($response->Success) {
                    return redirect('admin/settings')->with('message_success', 'Le Tue impostazioni sono state aggiornate ed è stato comunicato l\'indirizzo di callback');
                } else {
                    return redirect('admin/settings')->with('message_warning', 'Le Tue impostazioni sono state aggiornate ma non è stato comunicato l\'indirizzo di callback');
                }
            } else {
                return redirect('admin/settings')->with('message_success', 'Le Tue impostazioni sono state aggiornate');
            }
        }
    }
}
