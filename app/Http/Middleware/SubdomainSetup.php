<?php

namespace App\Http\Middleware;

use Closure;
use App\Property;

class SubdomainSetup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $subdomain = $request->route()->subdomain;
        $database = Property::where('subdomain', $subdomain)->where('isDisabled', 0)->first();

        if (!$database) {
            // account not found, do something else
            \App::abort(404, 'message');
        }
        // Account found and is active, Continue processing...

        // Get the database config details from wherever and however you want,
        // Could be stored in a database (as JSON), separate file, or stored in an array here
        \DB::purge("mysql");
        // And then do something like this
        \Config::set('database.connections.property.driver', 'mysql');
        \Config::set('database.connections.property.host', $database->db_host);
        \Config::set('database.connections.property.port', $database->db_port);
        \Config::set('database.connections.property.database', $database->db_name);
        \Config::set('database.connections.property.username', $database->db_user);
        \Config::set('database.connections.property.password', $database->db_passwrod);

        return $next($request);
    }
}
