<?php

namespace App\Http\Middleware;

use App\Property;
use Closure;

class PropertyWeb
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $property = Property::where('id', $request->property)->first();
        if (!$property) {
            \App::abort(404, 'message');
        } elseif (session()->has('property_id')) {
            if ($request->property !== session()->get('property_id')) {
                session()->forget('property_id');
                return redirect($request->property .'/')->withErrors(trans('admin.property_has_changed'));
            }
        }

        return $next($request);
    }
}
