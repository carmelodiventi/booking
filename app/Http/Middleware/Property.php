<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class Property
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->property_id) {
            Auth::logout();
            return redirect('/login')->withErrors('Property not found');
        }
        return $next($request);
    }
}
