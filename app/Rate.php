<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Scopes\PropertyScope;

class Rate extends Model
{
    protected $table = 'rate_availability';
    protected $fillable = ['rates_type_id','rooms_type_id','date','rate','status'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new PropertyScope);
    }

    public static function getPrices($start, $end, $ids)
    {
        return Rate::whereBetween('date', [$start,$end])
        ->whereIn('rate.rooms_type_id', $ids)
        ->join('rates_type', 'rates_type.id', '=', 'rate.rates_type_id')
        ->get(['rate.id','rates_type_id', 'rate.rooms_type_id', 'rate.date', 'rate.rate','units']);
    }

    public static function getRate($check_in, $check_out, $rooms_type_id)
    {
        return DB::select('SELECT rates_type_id,
           ra.rooms_type_id,
           rates_type.name,
           rooms_type.capacity,
           SUM(ra.rate) as rate_amount,
           GROUP_CONCAT(ra.date,"|",ra.rate) AS rate_day,
           ra.units
           FROM rate_availability ra
           LEFT JOIN rooms_type ON rooms_type.id = ra.rooms_type_id
           LEFT JOIN rates_type ON rates_type.id = ra.rates_type_id
           WHERE date BETWEEN "'.$check_in.'" AND DATE_SUB("'.$check_out.'", INTERVAL 1 DAY) AND ra.rooms_type_id = '.$rooms_type_id.'
           GROUP BY rates_type_id');
    }
}
