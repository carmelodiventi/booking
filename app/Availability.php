<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\RoomsType;
use App\Helpers\Helper;
use App\Helpers\MyAllocator;
use Auth;
use DateTime;
use DateInterval;
use Request;

class Availability extends Model
{
    //
    protected $table = 'rate_availability';
    protected $fillable = ['rates_type_id', 'rooms_type_id', 'date', 'units', 'rate', 'single_rate', 'min_stay', 'max_stay', 'closed'];

    /**
     * Scope a query to only include property.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeProperty($query)
    {
        return $query->where('rooms_type.property_id', '=', Helper::get_property());
    }

    public function rates_type()
    {
        return $this->belongsTo('App\RatesType');
    }

    public function rooms_type()
    {
        return $this->belongsTo('App\RoomsType');
    }

    public static function get_availability($checkin, $checkout, $guest)
    {
        $date = new DateTime($checkout);
        $date->sub(new DateInterval('P1D'));
        $date = date_format($date, 'Y-m-d');

        return Availability::property()->select([
            'rate_availability.rooms_type_id AS rooms_type_id',
            'rate_availability.rates_type_id AS rate_id',
            'rates_type.name AS rate_name',
            'rates_type.description AS rate_description',
            'rates_type.refundable AS rate_refundable',
            DB::raw("MIN(rate_availability.units) as units"),
            DB::raw("SUM(rate_availability.rate) as rate_amount"),
            DB::raw("SUM(rate_availability.single_rate) AS single_rate_amount"),
            DB::raw("GROUP_CONCAT(rate_availability.rate,'|', rate_availability.date) as day_rate"),
            DB::raw("DATEDIFF('$checkout','$checkin') AS nights_stay"),
            DB::raw("IFNULL(rate_availability.closed, 0) as closed"),
            DB::raw("IFNULL(rate_availability.min_stay,0) as min_stay"),
            DB::raw("IFNULL(rate_availability.max_stay,14) as max_stay"),
        ])
            ->leftJoin('rooms_type', 'rate_availability.rooms_type_id', '=', 'rooms_type.id')
            ->leftJoin('rates_type', 'rate_availability.rates_type_id', '=', 'rates_type.id')
            ->whereBetween('rate_availability.date', [$checkin, $date])
            ->where('rooms_type.isDisabled', 0)
            ->groupBy(['rate_availability.rooms_type_id', 'rate_availability.rates_type_id'])
            ->having('nights_stay', '>=', 'min_stay')
            ->having('closed', '=', 0)
            ->having('units', '>', 0)
            ->having('rate_amount', '>', 0)
            ->orderBy('rate_amount')
            ->get();
    }

    public static function get_rooms_type_availability($checkin, $checkout)
    {
        $date = new DateTime($checkout);
        $date->sub(new DateInterval('P1D'));
        $date = date_format($date, 'Y-m-d');

        return Availability::property()->select([
            'rate_availability.rooms_type_id AS rooms_type_id',
            'rooms_type.type',
            'rooms_type.description',
            'rooms_type.capacity AS capacity',
            'rooms_type.ma_room_id',
            DB::raw("MIN(rate_availability.units) as units"),
            DB::raw("(SELECT GROUP_CONCAT(url SEPARATOR ',') FROM rooms_photo WHERE rooms_photo.rooms_type_id = rooms_type.id) AS photo"),
            DB::raw("IFNULL(rate_availability.closed,0) as closed"),
        ])
            ->leftJoin('rooms_type', 'rate_availability.rooms_type_id', '=', 'rooms_type.id')
            ->whereBetween('rate_availability.date', [$checkin, $date])
            ->where('rooms_type.isDisabled', 0)
            ->groupBy('rooms_type_id')
            ->having('closed', '=', 0)
            ->having('units', '>', 0)
            ->orderBy('capacity')->get();
    }

    // public static function get_rooms_type_availability($checkin, $checkout, $guest)
    // {
    //     $date = new DateTime($checkout);
    //     $date->sub(new DateInterval('P1D'));
    //     $date = date_format($date, 'Y-m-d');

    //     return Availability::property()->select([
    //       'rate_availability.rooms_type_id AS rooms_type_id',
    //       'rate_availability.units AS units',
    //       'rooms_type.type',
    //       'rooms_type.description',
    //       'rooms_type.capacity AS capacity',
    //       DB::raw("(SELECT group_concat(url SEPARATOR ',') FROM rooms_photo WHERE rooms_photo.rooms_type_id = rooms_type.id) AS photo"),
    //       DB::raw("IFNULL(rate_availability.closed, 0) as closed"),
    //       'rate_availability.rate',
    //     ])
    //     ->leftJoin('rooms_type', 'rate_availability.rooms_type_id', '=', 'rooms_type.id')
    //     ->whereBetween('rate_availability.date', [ $checkin, $date])
    //     ->where('rooms_type.isDisabled', 0)
    //     ->groupBy('rooms_type_id')
    //     ->having('rate_availability.rate', '<>', 0)
    //     ->having('closed', '=', 0)
    //     ->having('units', '>' , 0)

    //     ->having('rooms_type.capacity', '>=', $guest)
    //     ->orderBy('capacity')
    //     ->get();
    // }

    public static function getRate($checkin, $checkout, $rooms_type_id)
    {
        $date = new DateTime($checkout);
        $date->sub(new DateInterval('P1D'));
        $date = date_format($date, 'Y-m-d');

        return Availability::property()->select([
            'rates_type_id',
            'rate_availability.rooms_type_id',
            'rates_type.name',
            'rooms_type.capacity',
            'rate_availability.units',
            DB::raw('SUM(rate_availability.rate) as rate_amount'),
            DB::raw('GROUP_CONCAT(rate_availability.date,"|",rate_availability.rate) AS rate_day')
        ])
            ->leftJoin('rooms_type', 'rate_availability.rooms_type_id', '=', 'rooms_type.id')
            ->leftJoin('rates_type', 'rate_availability.rates_type_id', '=', 'rates_type.id')
            ->whereBetween('rate_availability.date', [$checkin, $date])
            ->where('rate_availability.rooms_type_id', $rooms_type_id)
            ->groupBy('rate_availability.rates_type_id')
            ->get();
    }

    public static function getPrices($start, $end, $ids)
    {
        return Availability::whereBetween('date', [$start, $end])
            ->whereIn('rate.rooms_type_id', $ids)
            ->join('rates_type', 'rates_type.id', '=', 'rate.rates_type_id')
            ->get(['rate.id', 'rates_type_id', 'rate.rooms_type_id', 'rate.date', 'rate.rate', 'units']);
    }

    public static function scale($start_date, $end_date, $rooms_type_id, $qty = null)
    {
        //$end_date = date('Y-m-d', strtotime('-1 day', strtotime($end_date)));
        $qty = ($qty == null) ? 1 : $qty;
        $availability = Availability::where('rooms_type_id', $rooms_type_id)
            ->whereBetween('date', [$start_date, $end_date])->get(['id', 'units']);

        foreach ($availability as $avail) {
            if ($avail->units > 0) {
                Availability::where('id', $avail->id)->decrement('units', $qty);
            } elseif ($avail->units <= 0) {
                Availability::where('id', $avail->id)->update(['units' => 0]);
            }
        }
    }

    public static function add($start_date, $end_date, $rooms_type_id, $qty = null)
    {
        //$end_date = date('Y-m-d', strtotime('-1 day', strtotime($end_date)));
        $qty = ($qty == null) ? 1 : $qty;
        $availability = Availability::where('rooms_type_id', $rooms_type_id)
            ->whereBetween('date', [$start_date, $end_date])->get(['id', 'units']);
        $max_units = RoomsType::where('id', $rooms_type_id)->first()->units;


        foreach ($availability as $avail) {
            if ($avail->units < $max_units) {
                Availability::where('id', $avail->id)->increment('units', $qty);
            } elseif ($avail->units <= $max_units) {
                Availability::where('id', $avail->id)->update(['units' => $max_units]);
            }
        }
    }

    public static function synchronize()
    {
        $response = func_get_arg(0);
        $include_price = func_get_arg(1);
        $rates = RatesType::all();

        foreach ($rates as $rate) {
            if ($rate->rules) {
                $rules = json_decode($rate->rules);
                $operator = $rules[0];
                $amount = $rules[1];
                $type = $rules[2];
            }

            $refundable = $rate->refundable;

            foreach ($response->Rooms as $key => $value) {
                $roomId = RoomsType::where('ma_room_id', $value->RoomId)->get();
                $rooms_type_id = RoomsType::where('ma_room_id', $value->RoomId)->get();

                if (!$roomId->isEmpty()) {
                    $roomId = $roomId[0]->ma_room_id;
                } else {
                    $roomId = null;
                }

                if (!$rooms_type_id->isEmpty()) {
                    $rooms_type_id = $rooms_type_id[0]->id;
                } else {
                    $rooms_type_id = null;
                }

                if ($roomId == $value->RoomId) {
                    foreach ($value->Dates as $key => $value) {
                        if ($value->MaxStay == 0) {
                            $value->MaxStay = null;
                        }

                        if ($refundable == 0 && $value->Price != 0.00) {
                            $price = Helper::calculate_rate_price($value->Price, $amount, $operator, $type);
                        } elseif ($refundable == 1) {
                            $price = $value->Price;
                        }

                        if (isset($include_price) && $include_price == 'true') {
                            Availability::updateOrCreate(
                                [
                                    'date' => $value->Date,
                                    'rooms_type_id' => $rooms_type_id,
                                    'rates_type_id' => $rate->id
                                ],
                                [
                                    'units' => $value->Units,
                                    'max_stay' => $value->MaxStay,
                                    'min_stay' => $value->MinStay,
                                    'closed' => $value->Closed,
                                    'rate' => $price
                                ]
                            );
                        } else {
                            Availability::updateOrCreate(
                                [
                                    'date' => $value->Date,
                                    'rooms_type_id' => $rooms_type_id,
                                    'rates_type_id' => $rate->id
                                ],
                                [
                                    'units' => $value->Units,
                                    'max_stay' => $value->MaxStay,
                                    'min_stay' => $value->MinStay,
                                    'closed' => $value->Closed
                                ]
                            );
                        }
                    }
                }
            }
        }
        return response()->json(['Success' => true, 'msg' => trans('admin.availability_synchronized_successfully')]);
    }

    public static function get_availability_status($start_date, $end_date, $rooms_type_ids)
    {
        return Availability::leftJoin('rooms_type as rt', 'rt.id', '=', 'rate_availability.rooms_type_id')
            ->whereBetween('date', [$start_date, $end_date])
            ->whereIn('rooms_type_id', $rooms_type_ids)
            ->select(
                'rt.ma_room_id as RoomId',
                'date as StartDate',
                'date as EndDate',
                'rate_availability.units as Units',
                DB::raw('ifnull(rate_availability.min_stay, 0) as MinStay'),
                DB::raw('ifnull(rate_availability.max_stay, 0) as MaxStay'),
                'closed as Closed'
            )
            ->get()->toArray();
    }

    public static function store_availability($changes)
    {
        function updateOrCreate($start, $end, $rooms_type_id, $ma_rooms_type_id, $rates_type_id = null, $type, $val, $weekdays)
        {
            if ($rates_type_id != null) {
                while ($start <= $end) {
                    if ($weekdays) {
                        foreach ($weekdays as $day) {
                            $day_n = intval(date('w', strtotime($start)));
                            if ($day === $day_n) {
                                Availability::updateOrCreate(['date' => $start, 'rooms_type_id' => $rooms_type_id, 'rates_type_id' => $rates_type_id], [$type => $val]);
                            }
                        }
                    } else {
                        Availability::updateOrCreate(['date' => $start, 'rooms_type_id' => $rooms_type_id, 'rates_type_id' => $rates_type_id], [$type => $val]);
                    }
                    $start = date('Y-m-d', strtotime('+1 days', strtotime($start)));
                }
            } else {
                $rates_type = RatesType::all();
                while ($start <= $end) {
                    if ($weekdays) {
                        foreach ($weekdays as $day) {
                            $day_n = intval(date('w', strtotime($start)));
                            if ($day === $day_n) {
                                foreach ($rates_type as $rate_type) {
                                    Availability::updateOrCreate(['date' => $start, 'rooms_type_id' => $rooms_type_id, 'rates_type_id' => $rate_type->id], [$type => $val]);
                                }
                            }
                        }
                    } else {
                        foreach ($rates_type as $rate_type) {
                            Availability::updateOrCreate(['date' => $start, 'rooms_type_id' => $rooms_type_id, 'rates_type_id' => $rate_type->id], [$type => $val]);
                        }
                    }
                    $start = date('Y-m-d', strtotime('+1 days', strtotime($start)));
                }
            }
        }

        if ($changes) {
            $allocations = [];
            foreach ($changes as $rtid => $value) {
                foreach ($value as $k => $v) {
                    if ($k === 'rate') {
                        foreach ($v as $attr) {
                            updateOrCreate($attr->s, $attr->e, $rtid, $attr->mrtid, $attr->r, $k, $attr->v, $attr->wd);

                            if (Helper::get_setting('my_allocator')->esclude_price) {
                                $attr->v = "";
                            }
                            if ((isset($attr->mr) ? $attr->mr : null) == 1) {
                                array_push($allocations, ["RoomId" => $attr->mrtid,  "StartDate" => $attr->s, "EndDate" => $attr->e, "Price" => $attr->v]);
                            }
                        }
                    }
                    if ($k === 'single_rate') {
                        foreach ($v as $attr) {
                            updateOrCreate($attr->s, $attr->e, $rtid, $attr->mrtid, $attr->r, $k, $attr->v, $attr->wd);
                            if ((isset($attr->mr) ? $attr->mr : null) == 1) {
                                array_push($allocations, ["RoomId" => $attr->mrtid, "StartDate" => $attr->s,  "EndDate" => $attr->e, "SingleRate" => $attr->v]);
                            }
                        }
                    }
                    if ($k === 'units') {
                        // update all with rooms_type_id
                        foreach ($v as $attr) {
                            updateOrCreate($attr->s, $attr->e, $rtid, $attr->mrtid, null, $k, $attr->v, $attr->wd);
                            if ((isset($attr->mr) ? $attr->mr : null) == 1) {
                                array_push($allocations, ["RoomId" => $attr->mrtid, "StartDate" => $attr->s, "EndDate" => $attr->e,  "Units" => $attr->v]);
                            }
                        }
                    }
                    if ($k === 'min_stay') {
                        foreach ($v as $attr) {
                            updateOrCreate($attr->s, $attr->e, $rtid, $attr->mrtid, $attr->r, $k, $attr->v, $attr->wd);
                            if ((isset($attr->mr) ? $attr->mr : null) == 1) {
                                array_push($allocations, ["RoomId" => $attr->mrtid, "StartDate" => $attr->s, "EndDate" => $attr->e, "MinStay" => $attr->v]);
                            }
                        }
                    }
                    if ($k === 'max_stay') {
                        foreach ($v as $attr) {
                            updateOrCreate($attr->s, $attr->e, $rtid, $attr->mrtid, $attr->r, $k, $attr->v, $attr->wd);
                            if ((isset($attr->mr) ? $attr->mr : null) == 1) {
                                array_push($allocations, ["RoomId" => $attr->mrtid, "StartDate" => $attr->s, "EndDate" => $attr->e, "MaxStay" => $attr->v]);
                            }
                        }
                    }
                    if ($k === 'closed') {
                        // update all with rooms_type_id
                        foreach ($v as $attr) {
                            updateOrCreate($attr->s, $attr->e, $rtid, $attr->mrtid, $attr->r, $k, $attr->v, $attr->wd);
                            if ((isset($attr->mr) ? $attr->mr : null) == 1) {
                                array_push($allocations, ["RoomId" => $attr->mrtid, "StartDate" => $attr->s, "EndDate" => $attr->e, "Closed" => $attr->v]);
                            }
                        }
                    }
                }
            }

            if (Helper::get_setting('my_allocator')->active) {
                // Allocations array for myAllocator
                if (!empty($allocations)) {
                    $response = Myallocator::GetMyAllocatorResponse(Myallocator::ARIUpdate($allocations));
                    if ($response && $response->Success == true) {
                        $response = array('status' => 'success', 'msg' => 'La disponibilità è stata aggiornata ed è stata inviata una richiesta al servizio Channel Manager', 'allocations' => $allocations);
                    } else {
                        $response = array('status' => 'error', 'msg' => 'La disponibilità è stata aggiornata ma si è verificato un errore nel servizio Channel Manager', 'allocations' => $allocations);
                    }
                } else {
                    $response = array('status' => 'success', 'msg' => 'La disponibilità è stata aggiornata');
                }
            } else {
                $response = array('status' => 'success', 'msg' => 'La disponibilità è stata aggiornata ma non è stato aggiornato il servizio Channel Manager è possibile che non sia attivato', 'allocations' => $allocations);
            }
        } else {
            $response = array('status' => 'error');
        }

        return $response;
    }
}
