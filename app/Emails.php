<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\PropertyScope;

class Emails extends Model
{
    //
    protected $table = 'emails_template';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new PropertyScope);
    }
}
