<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\PropertyScope;

class RatesType extends Model
{
    //
    protected $table = 'rates_type';


    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new PropertyScope);
    }

    public function rate()
    {
        return $this->hasMany('App\Rate');
    }
}
