<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Scopes\PropertyScope;

class Fees extends Model
{

  /**
   * The "booting" method of the model.
   *
   * @return void
   */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new PropertyScope);
    }
}
