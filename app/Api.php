<?php

namespace App;

use App\Offerts;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Scopes\PropertyScope;
use App\Helpers\Helper;

class Api extends Model
{
    //
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new PropertyScope);
    }

    /**
    * Scope a query to only include property.
    *
    * @param \Illuminate\Database\Eloquent\Builder $query
    * @return \Illuminate\Database\Eloquent\Builder
    */
    public function scopeProperty($query)
    {
        return $query->where('reservations.property_id', '=', Helper::get_property());
    }

    public static function getRevenue()
    {
        return Reservations::select('portals.name', DB::raw('count(*) as count'))
        ->leftjoin('portals', 'reservations.origin', '=', 'portals.id')
        ->whereRaw('DATE_FORMAT(reservations.created_at, "%Y") = YEAR(CURDATE())')
        ->groupBy('portals.id')->get();
    }

    public static function getStatistics()
    {
        $statistics = [];

        $statistics['total'] = Reservations::property()->select(DB::raw('count(*) as count'))->first()->count;

        $statistics['cancelled'] = Reservations::property()->select(DB::raw('count(*) as count'))
        ->where('status', 'CAN')->first()->count;

        $statistics['confirmed'] = Reservations::property()->select(DB::raw('count(*) as count'))
        ->where('status', 'CNF')->first()->count;

        $statistics['paid'] = Reservations::property()->select(DB::raw('count(*) as count'))
        ->where('status', 'SLD')->first()->count;

        $statistics['incoming'] = Reservations::property()->select(DB::raw('count(*) as incoming'))
        ->leftjoin('rooms_booked', 'reservation_id', '=', 'reservations.id')
        ->where('rooms_booked.check_in', '>', DB::raw('CURDATE()'))
        ->first()->incoming;

        $statistics['outgoing'] = Reservations::property()->select(DB::raw('count(*) as outgoing'))
        ->leftjoin('rooms_booked', 'reservation_id', '=', 'reservations.id')
        ->where('rooms_booked.check_in', '<', DB::raw('CURDATE()'))
        ->first()->outgoing;

        $statistics['incoming_today'] = Reservations::property()->select(DB::raw('count(*) as incoming_today'))
        ->leftjoin('rooms_booked', 'reservation_id', '=', 'reservations.id')
        ->where('rooms_booked.check_in', '=', DB::raw('CURDATE()'))
        ->first()->incoming_today;

        $statistics['incoming_tomorrow'] = Reservations::property()->select(DB::raw('count(*) as incoming_tomorrow'))
        ->leftjoin('rooms_booked', 'reservation_id', '=', 'reservations.id')
        ->where('rooms_booked.check_in', '=', DB::raw('DATE_ADD(CURDATE(), INTERVAL -1 DAY)'))
        ->first()->incoming_tomorrow;

        $statistics['outgoing_today'] = Reservations::property()->select(DB::raw('count(*) as outgoing_today'))
        ->leftjoin('rooms_booked', 'reservation_id', '=', 'reservations.id')
        ->where('rooms_booked.check_out', '=', DB::raw('CURDATE()'))
        ->first()->outgoing_today;

        $statistics['outgoing_tomorrow'] = Reservations::property()->select(DB::raw('count(*) as outgoing_tomorrow'))
        ->leftjoin('rooms_booked', 'reservation_id', '=', 'reservations.id')
        ->where('rooms_booked.check_out', '=', DB::raw('DATE_ADD(CURDATE(), INTERVAL -1 DAY)'))
        ->first()->outgoing_tomorrow;

        return $statistics;
    }
  
}
