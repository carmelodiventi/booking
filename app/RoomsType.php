<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\PropertyScope;

class RoomsType extends Model
{
    protected $table = 'rooms_type';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new PropertyScope);
    }

    public function rooms()
    {
        return $this->hasMany('App\Rooms');
    }
}
