<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;

class Rooms extends Model
{
   
    protected $table = 'rooms';

    /**
        * Scope a query to only include property.
        *
        * @param \Illuminate\Database\Eloquent\Builder $query
        * @return \Illuminate\Database\Eloquent\Builder
        */
    public function scopeProperty($query)
    {
        return $query->where('rooms.property_id', '=', Helper::get_property());
    }

    /*
      * Get the rooms types.
     */
    public function roomsType()
    {
        return $this->hasMany('App\Room');
    }

    public static function get_rooms()
    {
        return DB::table('rooms_type as rtp')
            ->select('r.id', 'r.rooms_type_id', 'rtp.type', 'rtp.capacity', 'rtp.description', 'r.name', 'r.status')
            ->join('rooms as r', 'r.rooms_type_id', '=', 'rtp.id')->get();
    }

    public static function get_rooms_type()
    {
        return DB::table('rooms_type as rtp')
            ->select('rtp.id', 'rtp.type')
            ->where('status', '=', 'CNF')
            ->get();
    }

    public static function get_availability($check_in, $check_out)
    {
        return Rooms::whereNotIn('id', function ($query) use ($check_in, $check_out) {
            $query->select('room_id')
                      ->from('rooms_booked')
                      ->where('check_in', '<', $check_out)
                      ->where('check_out', '>', $check_in)
                      ->where('status', '=', 'CNF');
        })->get();
    }


    public static function get_accomodations_availability($check_in, $check_out, $room_id)
    {
        return Rooms::whereNotIn('id', function ($query) use ($check_in, $check_out, $room_id) {
            $query->select('room_id')
                      ->from('rooms_booked')
                      ->where('check_in', '<', $check_out)
                      ->where('check_out', '>', $check_in)
                      ->whereNotIn('room_id', [$room_id])
                      ->where('status', 'CNF');
        })->get();
    }

    public static function get_rooms_available_by_type($property_id, $starDate, $endDate, $roomsQty, $guests)
    {
        $rooms_type = \App\RoomsType::where('capacity', '>=', $guests)
        ->where('isDisabled', '<>', 1)
        ->where('property_id', $property_id)
        ->orderBy('capacity', 'ASC')
        ->get(['id']);
        $ids = collect($rooms_type)->implode('id', ',');

        return Rooms::where('property_id', $property_id)
        ->whereIn('rooms_type_id', $rooms_type)
        ->whereNotIn('rooms.id', function ($q) use ($starDate,$endDate) {
            $q->select('room_id')->from('rooms_booked')
            ->where('status', 'CNF')
            ->where('check_in', '<=', $endDate)
            ->where('check_out', '>=', $starDate);
        })
        ->orderByRaw("FIND_IN_SET(rooms_type_id,'$ids') ASC")
        ->limit($roomsQty)->get();
    }

    public static function get_rooms_available_by_roomtype($property_id, $starDate, $endDate, $roomsQty, $rooms_type)
    {
        

        $rooms =  Rooms::where('property_id', $property_id)
        ->where('rooms_type_id', $rooms_type)
        ->whereNotIn('rooms.id', function ($q) use ($starDate,$endDate) {
            $q->select('room_id')->from('rooms_booked')
            ->where('status', 'CNF')
            ->where('check_in', '<=', $endDate)
            ->where('check_out', '>=', $starDate);
        })
        ->limit($roomsQty)
        ->get();

        return $rooms;
    }
}
