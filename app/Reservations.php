<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;

class Reservations extends Model
{


    /**
     * Scope a query to only include property.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeProperty($query)
    {
        return $query->where('reservations.property_id', '=', Helper::get_property());
    }

    /**
     * getReservations
     * get data of all reservation
     *
     * Parameters:
     *     (keyword) - filter for reservation result list
     *     (from) - date start search
     *     (to) - date to search
     */


    public static function getReservations($keyword = null, $from = null, $to = null, $status = null)
    {
        $query = Reservations::property()->select(
            'reservations.id',
            'reservations.myallocatorId',
            'reservations.pnr',
            DB::raw('min(rb.check_in) as check_in'),
            DB::raw('min(rb.check_out) as check_out'),
            'reservations.created_at',
            'reservations.updated_at',
            'reservations.note',
            'reservations.reservation_note',
            'reservations.total',
            'reservations.subtotal',
            'reservations.discount',
            DB::raw('(SELECT GROUP_CONCAT(JSON_OBJECT(\'check_in\', t1.check_in ,
                    \'check_out\' ,t1.check_out,
                    \'guests\' ,t1.guests,
                    \'name\' , t2.name,
                    \'room_type\' , t3.type)) 
                            FROM rooms_booked t1
                            JOIN rooms t2 ON t1.room_id = t2.id
                            JOIN rooms_type t3 ON t1.rooms_type_id = t3.id
                            WHERE t1.reservation_id = reservations.id
                            ) AS rooms_booked'),
            DB::raw('(select COALESCE(sum(price),0) as rooms_booked_sum from rooms_booked as rb
                where rb.reservation_id = reservations.id) as rooms_booked_sum'),
            DB::raw('(select COALESCE(sum(price),0) as adjustments_sum from reservation_adjustments as ra
                where ra.reservation_id = reservations.id) as adjustments_sum'),
            DB::raw('(select COALESCE(sum(total),0) as payments_sum from reservation_payments as rp
                where rp.reservation_id = reservations.id) as payments_sum'),
            'reservations.review',
            'reservations.status as status',
            'p.name as origin',
            'c.name',
            'c.surname',
            'c.email',
            'c.phone'
        )
            ->leftJoin('customers as c', 'reservations.customer_id', '=', 'c.id')
            ->leftJoin('portals_commission as pc', 'reservations.id', '=', 'pc.reservation_id')
            ->leftJoin('portals as p', 'p.id', '=', 'reservations.origin')
            ->leftJoin('rooms_booked as rb', 'rb.reservation_id', '=', 'reservations.id');


        if ($from != null and $to != null) {
            $query->where(function ($query) use ($from, $to) {
                $query->whereBetween('check_in', [$from, $to]);
            });
            if ($status != null) {
                $query->where(function ($query) use ($status) {
                    foreach ($status as $key => $value) {
                        $query->orWhere("reservations.status", $value);
                    }
                });
            }
        } elseif ($keyword != null) {
            $query->where(function ($query) use ($keyword) {
                $query->where("reservations.pnr", "LIKE", "%$keyword%")
                    ->orWhere("p.name", "LIKE", "%$keyword%")
                    ->orWhere("c.surname", "LIKE", "%$keyword%")
                    ->orWhere("c.name", "LIKE", "%$keyword%")
                    ->orWhere("c.email", "LIKE", "%$keyword%");
            });
        } elseif ($status != null) {
            $query->where(function ($query) use ($status) {
                $query->where("reservations.status", $status);
            });
        }

        //$query->groupBy('reservations.id')->orderBy('check_in','asc');

        $query->where('check_in', '<>', null);

        $query->groupBy('reservations.id');

        $query->orderBy(DB::raw('ABS(DATEDIFF(check_in, NOW()) )'));

        //dd($query->toSql());

        if ($from != null and $to != null) {
            $reservations = $query->paginate(100)->appends('from', $from)->appends('to', $to);
        } elseif ($keyword != null) {
            $reservations = $query->paginate(100)->appends('keyword', $keyword);
        } elseif ($status != null) {
            $reservations = $query->paginate(100)->appends('status', $status);
        } else {
            $reservations = $query->paginate(100);
        }

        return $reservations;
    }

    /**
     * getReservationsFilter
     * get data of all reservations filter
     *
     * Parameters:
     *     (filter) - filter for reservation result list
     *     (from) - date start search
     *     (to) - date to search
     */


    public static function getReservationsFilter($keyword = null, $from = null, $to = null, $status = null)
    {
        $query = Reservations::property()->select(
            'reservations.id',
            'reservations.myallocatorId',
            'reservations.pnr',
            DB::raw('min(rb.check_in) as check_in'),
            DB::raw('min(rb.check_out) as check_out'),
            'reservations.created_at',
            'reservations.updated_at',
            'reservations.note',
            'reservations.reservation_note',
            'reservations.total',
            'reservations.subtotal',
            'reservations.discount',
            DB::raw('(select COALESCE(sum(price),0) as rooms_booked_sum from rooms_booked as rb
                where rb.reservation_id = reservations.id) as rooms_booked_sum'),
            DB::raw('(select COALESCE(sum(price),0) as adjustments_sum from reservation_adjustments as ra
                where ra.reservation_id = reservations.id) as adjustments_sum'),
            DB::raw('(select COALESCE(sum(total),0) as payments_sum from reservation_payments as rp
                where rp.reservation_id = reservations.id) as payments_sum'),
            'reservations.review',
            'reservations.status as status',
            'p.name as origin',
            'c.name',
            'c.surname',
            'c.email',
            'c.phone'
        )
            ->leftJoin('customers as c', 'reservations.customer_id', '=', 'c.id')
            ->leftJoin('portals_commission as pc', 'reservations.id', '=', 'pc.reservation_id')
            ->leftJoin('portals as p', 'p.id', '=', 'reservations.origin')
            ->leftJoin('rooms_booked as rb', 'rb.reservation_id', '=', 'reservations.id')
            ->where('check_in', '<=', DB::raw('CURDATE()'));


        if ($from != null and $to != null) {
            $query->where(function ($query) use ($from, $to) {
                $query->whereBetween('check_in', [$from, $to]);
            });
        } elseif ($keyword != null) {
            $query->where(function ($query) use ($keyword) {
                $query->where("reservations.pnr", "LIKE", "%$keyword%")
                    ->orWhere("p.name", "LIKE", "%$keyword%")
                    ->orWhere("c.surname", "LIKE", "%$keyword%")
                    ->orWhere("c.name", "LIKE", "%$keyword%")
                    ->orWhere("c.email", "LIKE", "%$keyword%");
            });
        } elseif ($status != null) {
            $query->where(function ($query) use ($status) {
                $query->where("reservations.status", "LIKE", "%$status%");
            });
        }

        $query->groupBy('reservations.id')->orderBy('check_in');

        if ($from != null and $to != null) {
            $reservations = $query->paginate(100)->appends('from', $from)->appends('to', $to);
        } elseif ($keyword != null) {
            $reservations = $query->paginate(100)->appends('keyword', $keyword);
        } elseif ($status != null) {
            $reservations = $query->paginate(100)->appends('status', $status);
        } else {
            $reservations = $query->paginate(100);
        }

        return $reservations;
    }


    /**
     * getReservation
     * get data of single reservation
     *
     * Parameters:
     *     (id) - id of reservation
     */

    public static function getReservation($id = null)
    {
        $reservations = Reservations::property()->select(
            'reservations.id',
            'reservations.myallocatorId',
            'reservations.pnr',
            'reservations.created_at',
            'reservations.updated_at',
            'reservations.note',
            'reservations.reservation_note',
            'reservations.subtotal',
            'reservations.total',
            'reservations.discount',
            'reservations.customer_id',
            'reservations.origin',
            'reservations.review',
            'c.name',
            'c.surname',
            'c.email',
            'c.phone',
            'c.country',
            'p.name as portal_name',
            'pc.cost as commission_cost',
            'pc.transaction_code',
            'ro.code as offert_code',
            'rp.method as payment_method',
            'rp.transaction_id as transaction_id',
            'rc.data as credit_card_data',
            'reservations.status as status'
        )
            ->leftjoin('customers as c', 'reservations.customer_id', '=', 'c.id')
            ->leftjoin('portals_commission as pc', 'pc.reservation_id', '=', 'reservations.id')
            ->leftjoin('portals as p', 'p.id', '=', 'reservations.origin')
            ->leftjoin('reservation_payments as rp', 'rp.reservation_id', '=', 'reservations.id')
            ->leftjoin('reservation_offert as ro', 'ro.reservation_id', '=', 'reservations.id')
            ->leftjoin('reservation_credit_card as rc', 'rc.reservation_id', '=', 'reservations.id')
            ->where('reservations.id', $id)
            ->first();

        return $reservations;
    }


    /**
     * getTopay
     * get topay amount
     *
     * Parameters:
     *     (id) - id of reservation
     */

    public static function getTopay($id)
    {
        return Reservations::select(DB::raw('((select COALESCE(sum(price),0) as rooms_booked_sum from rooms_booked as rb
          where rb.reservation_id = reservations.id) +
          (select COALESCE(sum(price),0) as adjustments_sum from reservation_adjustments as ra
          where ra.reservation_id = reservations.id) -
          (select COALESCE(sum(total),0) as payments_sum from reservation_payments as rp
          where rp.reservation_id = reservations.id) -
          reservations.discount) AS topay'))->where('reservations.id', $id)->first()->topay;
    }

    /**
     * getTotal
     * get total amount of reservation
     *
     * Parameters:
     *     (id) - id of reservation
     */

    public static function getTotal($id)
    {
        return Reservations::select(DB::raw('((select COALESCE(sum(price),0) as rooms_booked_sum from rooms_booked as rb
        where rb.reservation_id = reservations.id) +
        (select COALESCE(sum(price),0) as adjustments_sum from reservation_adjustments as ra
        where ra.reservation_id = reservations.id) -
        reservations.discount) AS total'))->where('reservations.id', $id)->first()->total;
    }

    /**
     * checkRoomAvailable
     * check if rooms is present in rooms_booked table
     *
     * Parameters:
     *     (check_in) - date of check in
     *     (check_out) - date of check out
     *     (rooms) - array with rooms_ids
     */

    public static function checkRoomAvailable($check_in, $check_out, $rooms)
    {
        $result = RoomsBooked::select(DB::raw('COUNT(*) as count'))
            ->where('check_in', '<', $check_out)
            ->where('check_out', '>', $check_in)
            ->where('status', '=', 'CNF')
            ->whereIn('room_id', [$rooms])
            ->get();
        return $result[0]->count;
    }

    /**
     * storeCreditCard
     * Store credit card of customer, and encrypt data by hash256
     *
     * Parameters:
     *     (customer_id) - id of customer
     *     (reservation_id) - reservation id for join with reservation table
     *     (card_data) - array with data of credit card
     */

    public static function storeCreditCard($customer_id, $reservation_id, $card_data)
    {
        DB::connection('property')
            ->table('reservation_credit_card')
            ->insert(['customer_id' => $customer_id, 'reservation_id' => $reservation_id, 'data' => encrypt($card_data)]);
    }
}
