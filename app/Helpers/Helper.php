<?php

namespace App\Helpers;

use DateTime;
use Lang;
use LaravelLocalization;
use Auth;
use Request;
use Omnipay\Common\CreditCard;
use Hashids\Hashids;
use Carbon\Carbon;

class Helper
{


  /**
  * get_property
  * get property id
  */

    public static function get_property()
    {
        if (isset(request()->property)) {
            $property_id = request()->property;
        } elseif (isset(Auth::user()->property_id)) {
            $property_id = Auth::user()->property_id;
        }

        return $property_id;
    }

    /**
    * validate_date
    * get data of all reservation
    *
    * Parameters:
    *     (date) - date to Validate
    */

    public static function validate_date($date)
    {
        $d = DateTime::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') === $date;
    }

    /**
    * get_nights_to_stay
    * get count of nights to stay
    *
    * Parameters:
    *     (check_in_date) - check in date
    *     (check_out_date) - check out date
    */

    public static function get_nights_to_stay($check_in_date, $check_out_date)
    {
        $datetime1 = new DateTime($check_in_date);
        $datetime2 = new DateTime($check_out_date);
        $nights = date_diff($datetime1, $datetime2);
        return intval($nights->format('%R%a'));
    }

    public static function get_traslated_string($data, $lang = null)
    {
        $current_locale = LaravelLocalization::getCurrentLocale();

        if (is_array($data)) {
            foreach ($data as $key => $value) {
                if ($lang == null) {
                    if ($current_locale == $value) {
                        return json_decode($data, true)[$current_locale];
                    }
                } else {
                    if ($lang == $value) {
                        return json_decode($data, true)[$lang];
                    }
                }
            }
        } else {
            if ($lang == null) {
                return json_decode($data, true)[$current_locale];
            } else {
                return json_decode($data, true)[$lang];
            }
        }
    }

    public static function get_photo_gallery($urls = null, $index = 0)
    {
        if (!empty($urls)) {
            $thumbs = explode(',', $urls);
            $thumbs_gallery = [];
            foreach ($thumbs as $thumb) {
                $thumbs_gallery[$index] = url($thumb);
                $index++;
            }
            $thumbs_gallery = implode(',', $thumbs_gallery);
            echo '<button type="button" onclick="app.showGallery(\'' . $thumbs_gallery . '\')"> <i class="ion-images"></i> </button>';
        }
    }

    public static function get_single_photo($urls = null)
    {
        if (!empty($urls)) {
            $thumbs = explode(',', $urls);
            $thumbs = $thumbs[0];
        } else {
            $thumbs = '';
        }

        if (!empty($urls)) {
            echo '<img alt="" src="' . url($thumbs) . '">';
        } else {
            echo '<img src="' . url('resources/assets/front/img/no-room-foto.jpg') . '">';
        }
    }

    public static function get_src_photo($urls = null)
    {
        if (!empty($urls)) {
            $thumbs = explode(',', $urls);
            $thumbs = $thumbs[0];
        } else {
            $thumbs = '';
        }

        if (!empty($urls)) {
            echo url($thumbs);
        } else {
            echo url('resources/assets/front/img/no-room-foto.jpg');
        }
    }

 
    public static function get_book_now_drop($num_room, $type, $type_id, $rate_id, $rate_name, $amount, $nights, $guests)
    {
        if (!empty($amount)) {
            $drop = '';
            $drop .= '<button class="btn btn-primary drop-target drop-target-example-drop-theme-arrows-bounce-dark drop-theme-arrows-bounce-dark" type="button" drop-target="'.$type_id.'">' . trans('booking.book_now') . '</button>';
            $drop .= '<div class="drop-element">';
            $drop .= '<div class="drop-content-inner" drop-content="'.$type_id.'">';
            $drop .= '<div class="drop-title"><span>'.trans('booking.available_rooms'). ' (' . $num_room . ')</span></div>';
            $num = 1;
            $i = 0;
            $hashids = new Hashids(env('APP_KEY'));
            $type_id = $hashids->encode($type_id);
            $rate_id = $hashids->encode($rate_id);
            while ($num <= $num_room) {
                $drop .= '<div class="room-line">';
                $drop .= '<div class="room-price">';
                $drop .= '<span>' . trans('booking.room') . '&nbsp;' . $num . '&nbsp;' . ' </span>
                <span class="price text-uc"><sup class="currency">' . Helper::get_setting('currency')->symbol . '</sup>' . $amount . '<br>' . Lang::choice('booking.for_nights', $nights, ['nights' => $nights]) .'</span>
                </div>
                <div class="room-book text-right text-uc">
                  <button type="button"
                    data-room-price="'. $amount .'"
                    data-room-type="'.$type.'"
                    data-room-type-id="'.$type_id.'"
                    data-room-max-qty="'.$num_room.'"
                    data-rate-id="'.$rate_id.'"
                    data-rate-name="'.$rate_name.'"
                    class="book-now text-uc"
                    onclick="app.reserve(this)">' . trans('booking.book') .'</button>';
                $drop .= '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 406.8 406.8" width="512" height="512"><polygon points="385.6 62.5 146.2 301.9 21.2 176.9 0 198.1 146.2 344.3 406.8 83.7 " fill="#FFF"/></svg>';
                $drop .= '</div>';
                $drop .= '<div class="clearfix"></div>';
                $drop .= '</div>';
                $num++;
                $i++;
            }
            $drop .= '</div>';
            $drop .= '</div>';
            return $drop;
        } else {
            return trans('booking.room_not_available');
        }
    }

    public static function get_combo_price($amount = null, $num_room = null, $id_type = null, $id_rate = null, $type = null, $nights = null)
    {
        if (!empty($amount)) {
            $hashids = new \Hashids\Hashids(env('APP_KEY'));
            $id_type = $hashids->encode($id_type);
            $id_rate = $hashids->encode($id_rate);

            $combo_price = '<select class="sel_room_result p_room_' . $id_type . ' form-field form-select-primary"
                               name="'. $id_type . '_'.$id_rate.'"
                               room-type="' . $type . '"
                               room-price="' . $amount . '"
                               room-type-id="' . $id_type . '"
                               room-max="' . $num_room . '">';
            $combo_price .= '<option value="0">' . trans('booking.select_sooms') . '</option>';
            for ($i = 1; $i <= $num_room; $i++) {
                $combo_price .= '<option value="' . $i . '">' . $i . '&nbsp;' . Lang::choice('booking.room', $i) . ' (' . $amount * $i . ' ' . Helper::get_setting('currency')->symbol . ')</option>';
            }
            $combo_price .= '</select>';
            $combo_price .= '<input type="hidden" name="">';
            return $combo_price;
        } else {
            return trans('booking.room_not_available');
        }
    }

    public static function get_book_now($num_room, $type, $type_id, $rate_id, $rate_name, $amount, $nights, $guests)
    {
        if (!empty($amount)) {
            $num = 1;
            $i = 0;
            $hashids = new Hashids(env('APP_KEY'));
            $type_id = $hashids->encode($type_id);
            $rate_id = $hashids->encode($rate_id);

            $rooms_list = '';
            while ($num <= $num_room) {
                $rooms_list .= '<div class="room-line">';
                $rooms_list .= '<button type="button"
                    data-id="'.$type_id.$num.'"
                    data-room-price="'. $amount .'"
                    data-room-type="'.$type.'"
                    data-room-type-id="'.$type_id.'"
                    data-room-max-qty="'.$num_room.'"
                    data-rate-id="'.$rate_id.'"
                    data-rate-name="'.$rate_name.'"
                    data-guests="'.$guests.'"
                    class="btn btn-book-now"
                    onclick="app.reserve(this)">';
                if ($num_room > 1) {
                    $rooms_list .= '<span class="text-uc room-count">' . trans('booking.room') . '&nbsp;' . trans('booking.rooms_count_of', ['count'=> $num, 'totalcount' => $num_room]) . '</span>';
                }
                $rooms_list .= trans('booking.book');
                $rooms_list .= '</button>';
                $rooms_list .= '</div>';
                $num++;
                $i++;
            }

            return $rooms_list;
        } else {
            return trans('booking.room_not_available');
        }
    }

    public static function get_subtotal($rooms)
    {
        $subtotal = 0.00;

        if ($rooms) {
            foreach ($rooms as $room) {
                $subtotal = $subtotal + $room->rate_amount;
            }
            return $subtotal;
        }

        return $subtotal;
    }

    public static function get_total($totaltax, $subtotal, $discount = null)
    {
        $total = $subtotal + $totaltax;
        return number_format($total, 2, '.', '');
    }

    public static function get_discount($offert, $grandtotal)
    {
        
        $discount = 0.00;

        if($offert){
            $condition = $offert->condition;
            $value = $offert->value;
            switch($condition){
                case "total_value" : 
                    $discount = number_format($value, 2, '.', '');
                    break;
                case "percentage_value" : 
                    $discount = number_format(($value / 100) * $grandtotal, 2, '.', '');
                    break;
            }
        }

        return $discount;

    }

    public static function get_fees_list($fees, $subtotal)
    {
        foreach ($fees as $fee) {
            if ($fee->percentage == 1) {
                $fee->value = ($fee->value / 100) * $subtotal;
            } else {
                $fee->value = $fee->value;
            }
        }
        return $fees;
    }

    public static function gen_ID()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 20; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return strtoupper($randomString);
    }


    public static function get_PNR()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 8; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return strtoupper($randomString);
    }

    public static function getstatus($status, $review = null, $total = null, $topay = null)
    {
        switch ($status) {
            case 'ALT':
                $status = '<span class="label label-danger">' . trans('admin.alert') . '</span>';
                break;
            case 'CAN':
                $status = '<span class="label label-danger">' . trans('admin.canceled') . '</span>';
                break;
            case 'CNF':
                $status = '<span class="label label-warning">' . trans('admin.confirm') . '</span>';
                  break;
            case 'SLD':
                $status = '<span class="label label-success">' . trans('admin.paid') . '</span>';
                break;
            case 'CKO':
                $status = '<span class="label label-info">' . trans('admin.checkout') . '</span>';
                break;
            case 'OPN':
                $status = '<span class="label label-info">' . trans('admin.option') . '</span>';
                break;
        }

        switch ($review) {
            case 0:
                $status = $status;
                break;
            case 1:
                $status = '<span class="label label-danger">' . trans('admin.alert') . '</span>';
                break;
        }

        return $status;
    }

    public static function get_payments_method($status)
    {
        switch ($status) {
            case 'bank_transfer':
            return trans('admin.bank_transfer');
            break;
            case 'cash':
            return trans('admin.cash');
            break;
            case 'bank_check':
            return trans('admin.bank_check');
            break;
            case 'credit_card':
            return trans('admin.credit_card');
            break;
            case 'credit_card_customer':
            return trans('admin.credit_card_customer');
            break;
            case 'bancomat':
            return trans('admin.bancomat');
            break;
            case 'voucher':
            return trans('admin.voucher');
            break;
        }
    }


    public static function get_topay_difference($payments_sum, $total)
    {
        return number_format(($total - $payments_sum), 2, ',', ' ');
    }

    public static function availability_calendar($rooms_type, $rates, $start, $end)
    {
        $start_date = date("Y-m-d", strtotime($start));
        $end_date = date("Y-m-d", strtotime($end));
        $start_date = strtotime($start_date);
        $end_date = strtotime($end_date);
        $date = $start_date;

        $calendar = '<table data-toggle="table" class="calendar-rates table" id="table-rates" data-loading-text="<i class=\'fa fa-spinner fa-spin\'></i> Sto caricando la Disponibilità">';
        $calendar .= '<tbody>';

        // begin calendar row
        $calendar .= '<tr class="calendar-row">';
        $calendar .= '<th></th>';
        while ($date <= $end_date) {
            if (date('w', $date) == 6 || date('w', $date) == 0) {
                $weekend =  ' weekend';
            } else {
                $weekend =  '';
            }
            $calendar.= '<th class="month-day calendar-day'.$weekend.'" data-field="'.date('Y-m-d', $date).'">';
            $calendar.= '<div class="day"><strong>'.date('D', $date).'</strong></div>';
            $calendar.= '<div class="day-number">'.date('d', $date).'</div>';
            $calendar.= '<div class="month-name"><strong>'. date('M', $date) .'</strong></div>';
            $calendar .= '</div>';
            $calendar.= '</th>';
            $date = strtotime("+1 day", $date);
        }
        $calendar .= '</tr>';
        // end calendar row

        foreach ($rooms_type as $r_type) {
            // begin rooms type row
            $calendar .= '<tr>';
            $calendar .= '<td><span class="calendar-title">'.self::get_traslated_string($r_type->type).'</span></td>';
            $date = $start_date;
            while ($date <= $end_date) {
                $calendar.= '<td>';
                $calendar.= '</td>';
                $date = strtotime("+1 day", $date);
            }
            $calendar .= '</tr>';
            // end rooms type row
            foreach ($rates as $rate) {
                $calendar .= '<tr>';
                $calendar .= '<td><span class="calendar-title-rates">'.self::get_traslated_string($rate->name).'</span></td>';
                $date = $start_date;
                $i = 0;
                while ($date <= $end_date) {
                    $calendar.= '<td>';
                    $current_day = date('Y-m-d', $date);
                    $is_disable = ($date < strtotime('-1 days', strtotime('now')) ? 'disabled' : '');


                    if ($rate->master_rate == 1) {
                        $calendar .="<div class='input-group'><span class='input-group-addon' title='".trans('admin.availability')."'><i class=\"ti-envelope\"></i></span>
                             <input type='number' class='input-rates form-control units' value='0' min='0' max='$r_type->units' data-type='units' data-master-rate='$rate->master_rate'  data-rtid='$r_type->id' data-mrtid='$r_type->ma_room_id' data-idx='$i' data-rid='$rate->id' data-date='$current_day' $is_disable/>
                             </div>";
                    }
                    // price room

                    $calendar.= "<div class='input-group'>
                                <span class='input-group-addon' title='EUR'>€</span>
                                <input type='number' class='input-rates form-control' value='0.00' min='0' data-type='rate' data-master-rate='$rate->master_rate' data-rtid='$r_type->id' data-mrtid='$r_type->ma_room_id' data-idx='$i' data-rid='$rate->id'  data-date='$current_day' ".$is_disable. "
                                        data-toggle='tooltip' data-html='true' title=''></div>";
                    // price room single use
                    if ($r_type->capacity == 2) {
                        $calendar.= "<div class='input-group'>
                                  <span class='input-group-addon' title='EUR'>€</span>
                                  <input type='number' class='input-rates form-control' value='0.00' min='0' data-type='single_rate' data-master-rate='$rate->master_rate' data-rtid='$r_type->id' data-mrtid='$r_type->ma_room_id' data-idx='$i' data-rid='$rate->id'  data-date='$current_day' ".$is_disable. "
                                          data-toggle='tooltip' data-html='true' title=''></div>";
                    }
                    // Minium room
                    $calendar.= "<div class='input-group'>
                                <span class='input-group-addon' title='".trans('admin.min_stay')."'><i class=\"ti-split-h\"></i></span>
                                <input type='number' class='input-rates form-control min_stay' value='1' min='1' data-type='min_stay'data-master-rate='$rate->master_rate'  data-rtid='$r_type->id' data-mrtid='$r_type->ma_room_id' data-idx='$i' data-rid='$rate->id' data-date='$current_day' $is_disable>
                                </div>";
                    // close room

                    $calendar .= "<div class='input-group'>
                                <span class='input-group-addon' title='". trans('admin.close') ."'> <i class=\"ti-lock\"></i></span>
                                <input type='number' class='input-rates closed form-control' value='0' min='0' max='1' data-type='closed' data-master-rate='$rate->master_rate' data-rtid='$r_type->id' data-mrtid='$r_type->ma_room_id' data-idx='$i' data-rid='$rate->id' data-date='$current_day' $is_disable>
                                </div>";



                    $calendar.= '</td>';
                    $date = strtotime("+1 day", $date);
                    $i++;
                }
                $calendar .= '</tr>';
            }
        }

        $calendar .= '</tbody>';
        $calendar .= '</table>';
        // end calendar

        return $calendar;
    }

    public static function planning_calendar($rooms_type, $rooms, $master_rate, $interval)
    {
        $date = date("Y-m-d", $interval);
        $start_date = date("Y-m-d", $interval);
        $end_date = date("Y-m-d", strtotime('+45 days', strtotime($start_date)));
        $next_month = date('Y-m', strtotime(' +1 month', $interval));
        $prev_month = date('Y-m', strtotime(' -1 month', $interval));
        $start_date = strtotime($start_date);
        $end_date = strtotime($end_date);
        $date = $start_date;

        $calendar = '<table class="table planning" id="table-scheduler">';
        $calendar .= '<thead>';

        $calendar .= '<tr class="calendar-row">';
        $calendar .= '<th></th>';
        while ($date <= $end_date) {
            if (date('w', $date) == 6 || date('w', $date) == 0) {
                $day =  ' weekend';
            } elseif (date('d', $date) == 1) {
                $day =  ' first-day';
            } else {
                $day =  '';
            }
            $calendar.= '<th class="month-day calendar-day '. $day . ($end_date == $date ? 'last-day' : '' ) .'">';
            $calendar.= '<div class="day"><strong>'.date('D', $date).'</strong></div>';
            $calendar.= '<div class="day-number">'.date('d', $date).'</div>';
            $calendar.= '<div class="month-name"><strong>'. date('M', $date) .'</strong></div>';
            $calendar .= '</div>';
            $calendar.= '</th>';
            $date = strtotime("+1 day", $date);
        }
        /* End Day Name */
        $calendar.= '</tr>';
        $calendar .= '</thead>';
        $calendar .= '<tbody>';
        $calendar .= '<tr>';

        foreach ($rooms_type as $r_type) {
            $calendar .= '<tr class="rooms-type">';
            $calendar .= '<td><h4><span class="calendar-title">'.self::get_traslated_string($r_type->type).'</span></h4></td>';
            $date = $start_date;
            while ($date <= $end_date) {
                $calendar.= '<td data-date="'.date('Y-m-d', $date).'" data-room-type-id="'.$r_type->id.'" data-rates-type-id="'.$master_rate.'">';
                $calendar.= '</td>';
                $date = strtotime("+1 day", $date);
            }
            foreach ($rooms as $r) {
                if ($r_type->id == $r->rooms_type_id) {
                    $calendar .= '<tr data-room-id="'.$r->id.'" class="calendar-prices">';
                    $calendar .= '<td><h6><span class="calendar-title">' . $r->name .'</span></h6></td>';
                    $date = $start_date;
                    while ($date <= $end_date) {
                        if ($date === strtotime(date('Y-m-d'))) {
                            $today = 'today';
                        } else {
                            $today = null;
                        }
                        $calendar.= '<td class="calendar-day '. $today . ($end_date == $date ? 'last-day' : '' ) . '" data-room-id="'.$r->id.'" data-room-type-id="'.$r->rooms_type_id.'" data-room-type="'.self::get_traslated_string($r_type->type).'" data-room-name="'.$r->name.'" data-date="'.date('Y-m-d', $date).'"></td>';
                        $date = strtotime("+1 day", $date);
                    }

                    $calendar .= '</tr>';
                }
            }
        }

        $calendar .= '</tr>';
        $calendar .= '</tbody>';
        $calendar .= '</table>';
        return $calendar;
    }

    public static function planning_short_link()
    {
        $short_link = '<ul id="collapse1" class="list-unstyled collapse">';
        for ($i=1;$i<=12;$i++) {
            $short_link .= '<li>';
            $short_link .= '<a href="'.url('admin/planning?interval='.date('Y').'-'.$i.'').'">'.trans('admin.'.date('F', mktime(0, 0, 0, $i, 10))).'</a>';
            $short_link .= '</li>';
        }
        $short_link .= '</ul>';
        return $short_link;
    }

    public static function get_setting($key = null)
    {
        if (isset(request()->property)) {
            $property_id = request()->property;
        } elseif (isset(Auth::user()->property_id)) {
            $property_id = Auth::user()->property_id;
        }

        if (!empty($key) && !empty($property_id)) {
            if (!empty(config()->get('settings.'.$property_id)[$key])) {
                return json_decode(config()->get('settings.'.$property_id)[$key]);
            }
        }
    }

    /**
    * Get Money
    *
    * Parameters:
    *     (price) - number
    */

    public static function get_money($price = null)
    {
        if (!empty($price)) {
            $price = number_format($price, 2, '.', ',');
        } else {
            $price = '0.00';
        }

        return '<sup>' . self::get_setting('currency')->symbol . '</sup>' . $price;
    }

    /**
    * set_card
    * Set a credit card
    *
    * Parameters:
    *     (data) - Data of Credit Card
    */

    public static function set_card($data)
    {
        try {
            $card = [
              'firstName' => $data['firstName'],
              'lastName' => $data['lastName'],
              'number' => $data['number'],
              'expiryMonth' => $data['expiryMonth'],
              'expiryYear' => $data['expiryYear'],
              'type' => $data['type']
            ];
            $ccard = new CreditCard($card);
            $ccard->validate();
            return true;
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }


    public static function get_card($data)
    {
        try {
            $data = decrypt($data);

            $card = "";

            if (isset($data['id'])) {
                $card .= trans('admin.id_cc') . ': <strong>' . $data['id'] . '</strong></br>';
            }

            if (isset($data['firstName'])) {
                $card .= trans('admin.firstName_cc') . ': <strong>' . $data['firstName'] . '</strong>' . '<br>';
            }
            if (isset($data['lastName'])) {
                $card .= trans('admin.lastName_cc') . ': <strong>' . $data['lastName'] . '</strong>' . '<br>';
            }
            if (isset($data['number'])) {
                $card .= trans('admin.number_cc') . ': <strong>' . $data['number'] . '</strong>' . '<br>';
            }
            if (isset($data['expiryMonth'])) {
                $card .= trans('admin.expiryMonth_cc') . ': <strong>' . $data['expiryMonth'] . '</strong>' . '<br>';
            }
            if (isset($data['expiryYear'])) {
                $card .= trans('admin.expiryYear_cc') . ': <strong>' . $data['expiryYear'] . '</strong>' . '<br>';
            }
            if (isset($data['type'])) {
                $card .= trans('admin.type_cc') . ': <strong>' . $data['type'] . '</strong>';
            }
            if (isset($data['brand'])) {
                $card .= trans('admin.type_cc') . ': <strong>' . $data['brand'] . '</strong>';
            }

            return $card;
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }

    /**
    * get_new_stay
    * get new stya of reservation moved
    *
    * Parameters:
    *     (checkin) - date of check-in
    *     (checkout) - date of check-out
    *     (date) - new date of check-in
    */

    public static function get_new_stay($checkin, $checkout, $date)
    {
        $count_day = self::get_nights_to_stay($checkin, $checkout);
        $checkin = Carbon::parse($date)->format('Y-m-d');
        $checkout = Carbon::parse($date)->addDays($count_day)->format('Y-m-d');
        return (object)['check_in' => $checkin,'check_out' => $checkout];
    }

    /**
    * validate_date
    * get data of all reservation
    *
    * Parameters:
    *     (rooms_booked_amount) - rooms booked amount
    *     (adjustaments_amount) - adjustaments amount
    *     (discount_amount) - discount amount
    */

    public static function get_topay($rooms_booked_amount = null, $adjustaments_amount= null, $discount_amount= null, $payments_sum_amount = null)
    {
        $get_to_pay = 0.00;
        $get_to_pay = ($rooms_booked_amount + $adjustaments_amount) - $discount_amount - $payments_sum_amount;
        return $get_to_pay;
    }


    /**
    * standardize_data_to_reservation
    * standardize data to an object to a reservation
    *
    * Parameters:
    *     (data) - data of reservation
    */

    public static function standardize_data_to_reservation($data)
    {
        $rooms = [];
        foreach ($data['rooms'] as $key => $value) {
            $i = 0;
            while ($i<$value) {
                $rates = explode('|', $data['rates'][$key]);
                $rooms[] = (object)[
                  'rooms_type_id' => $data['rooms_type'][$key],
                  'guest' => $data['guests'][$data['rooms_type'][$key]][$i],
                  'rate_id' => $rates[0],
                  'rate_name' => $rates[1],
                  'rate_amount' => $data['price'][$data['rooms_type'][$key]][$i]
                ];
                $i++;
            }
        }
        $data['rooms'] = (object)$rooms;
        $data['pnr'] = Helper::get_PNR();
        // remove element unused from request
        unset($data['rooms_type']);
        unset($data['price']);
        unset($data['guests']);
        unset($data['rates']);

        return (object)$data;
    }


    /**
    * calculate_rate_price
    * calculate rate price
    *
    * Parameters:
    *     (rate_price) - price
    *     (amount) - amount
    *     (operator) - operator + or -
    *     (type) - percentage or addition and substraction
    */

    public static function calculate_rate_price($rate_price, $amount, $operator, $type)
    {
        $price = 0.00;
        if ($operator == '-' && $type == '%') {
            $price =  $rate_price - ($rate_price * ($amount/100));
        } elseif ($operator == '+' && $type == '%') {
            $price =  $rate_price + ($rate_price * ($amount/100));
        } elseif ($operator == '-' && $type == '€') {
            $price =  $rate_price - $amount;
        } elseif ($operator == '+' && $type == '€') {
            $price =  $rate_price + $amount;
        }
        return $price;
    }

    /**
    * get_Refundable
    * get true or false if is refundable
    *
    * Parameters:
    *     (rooms) - object
    */

    public static function get_refundable($rooms)
    {
        $result = false;
        foreach ($rooms as $room) {
            if ($room->refundable === 1) {
                $result = true;
            }
        }
        return $result;
    }


    /**
    * save_reservation
    * Save reservation
    *
    * Parameters:
    *     (booking) - object
    */

     public static function save_reservation($booking, $property_id)
    {
        if (isset($booking) && isset($property_id)) {
            $reservation = \App\Reservations::where('myallocatorId', $booking->MyallocatorId)->first();
            if (!$reservation) {

                if (isset($booking->Customers)) {
                    foreach ($booking->Customers as $customer) {
                        
                        $new_customer = new \App\Customers;
                         
                        if (isset($customer->CustomerLName)) {
                            $new_customer->name = $customer->CustomerLName;
                        }
                        if (isset($customer->CustomerFName)) {
                            $new_customer->surname = $customer->CustomerFName;
                        }
                        if (isset($customer->CustomerEmail)) {
                            $new_customer->email = $customer->CustomerEmail;
                        }
                        if (isset($customer->CustomerPhone)) {
                            $new_customer->phone = $customer->CustomerPhone;
                        }
                        if (isset($customer->CustomerPhoneMobile)) {
                            $new_customer->phone_2 = $customer->CustomerPhoneMobile;
                        }
                        if (isset($customer->CustomerCity)) {
                            $new_customer->city = $customer->CustomerCity;
                        }
                        if (isset($customer->CustomerCountry)) {
                            $new_customer->country = $customer->CustomerCountry;
                        }
                        if (isset($customer->CustomerAddress)) {
                            $new_customer->address = $customer->CustomerAddress;
                        }
                        $new_customer->property_id = $property_id;
                        $new_customer->save();

                        if (isset($customer->CustomerNote)) {
                            $customer_note = $customer->CustomerNote;
                        } else {
                            $customer_note = '';
                        }
                    }
                }
                if (isset($booking->OrderSource)) {
                    $portal = \App\Portals::where('name', $booking->OrderSource)->orWhere('name', 'like', '%' . $booking->OrderSource . '%')->first();
                } else {
                    $portal = \App\Portals::where('initial', $booking->Channel)->first();
                }
                if (!$portal) {
                    $portal = new \App\Portals;
                    $portal->name = $booking->OrderSource;
                    $portal->initial = $booking->Channel;
                    $portal->status = 'CNF';
                    $portal->save();
                }

                $reservation = new \App\Reservations;
                $reservation->customer_id = $new_customer->id;
                $reservation->myallocatorId = $booking->MyallocatorId;
                $reservation->pnr = self::get_PNR();
                $reservation->note = $customer_note;
                $reservation->origin = $portal->id;

                if (isset($booking->TotalPrice)) {
                    $booking->TotalPrice = $booking->TotalPrice;
                } else {
                    $booking->TotalPrice = 0.00;
                }

                $reservation->total = $booking->TotalPrice;
                $reservation->data = json_encode([
                    'OrderId' => $booking->OrderId,
                    'IsCancellation' => $booking->IsCancellation,
                    'IsModification' => $booking->IsModification,
                    'Acknowledged' => $booking->Acknowledged
                ]);

                if ($booking->IsCancellation === true) {
                    $reservation->status = 'CAN';
                }

                $reservation->property_id = $property_id;
                $reservation->save();

                $portals_commission = new \App\PortalsCommission();
                $portals_commission->reservation_id = $reservation->id;
                $portals_commission->portal_id = $portal->id;

                if (isset($booking->Commission)) {
                    $portals_commission->cost =  $booking->Commission;
                }
                if (isset($booking->OrderId)) {
                    $portals_commission->transaction_code = $booking->OrderId;
                }

                $portals_commission->save();

                if (isset($booking->Rooms)) {
                    foreach ($booking->Rooms as $key => $room) {
                        if ($room->Units >= 1) {
                            
                            $room->RoomTypeId = $room->RoomTypeIds[0];

                            if (isset($room->Occupancy)) {
                                $room->Occupancy = $room->Occupancy;
                            } elseif(isset($room->Adults)) {
                                $room->Occupancy = $room->Adults;
                            }

                            $room->RoomTypeId = \App\RoomsType::where('ma_room_id', $room->RoomTypeId)->first()->id;

                            $start_date = $room->StartDate;
                            $end_date = $room->EndDate; // date('Y-m-d', strtotime('-1 day', strtotime($room->EndDate)));
                            $units = $room->Units;
                            $occupancy = $room->Occupancy;
                            $rooms_type = $room->RoomTypeId;

                            $roomsfound = \App\Rooms::get_rooms_available_by_roomtype($property_id, $start_date, $end_date, $units, $rooms_type);
                            // Booking Found

                            foreach ($roomsfound as $roomfound) {
                                // Rooms Found
                                $roomsbooked = new \App\RoomsBooked();

                                if (isset($roomfound->id)) {
                                    $roomsbooked->room_id = $roomfound->id;
                                }
                                if (isset($reservation->id)) {
                                    $roomsbooked->reservation_id = $reservation->id;
                                }
                                if (isset($room->RoomTypeIds)) {
                                    $roomsbooked->rooms_type_id = $room->RoomTypeId;
                                }
                                if (isset($room->StartDate)) {
                                    //$roomsbooked->check_in = $room->StartDate;
                                    $roomsbooked->check_in = $start_date;
                                }
                                if (isset($room->EndDate)) {
                                    //$roomsbooked->check_out =  $room->EndDate;
                                    $roomsbooked->check_out = $end_date;
                                }
                                if (isset($room->Occupancy)) {
                                    // $roomsbooked->guests = $room->Occupancy;
                                    $roomsbooked->guests = $occupancy;
                                }
                                if (isset($room->Price)) {
                                    $roomsbooked->price = $room->Price;
                                }
                                if (isset($room->RateId)) {
                                    $roomsbooked->rate_id = $room->RateId;
                                }
                                if (isset($room->RateDesc)) {
                                    $roomsbooked->rate_desc = $room->RateDesc;
                                }
                                if ($booking->IsCancellation === true) {
                                    $roomsbooked->status = 'CAN';
                                }

                                $reservation_data = [];

                                (!empty($room->OccupantLName)) ? $reservation_data['OccupantLName'] = $room->OccupantLName : '';
                                (!empty($room->OccupantSmoker)) ? $reservation_data['OccupantSmoker'] = $room->OccupantSmoker : '';
                                (!empty($room->OccupantNote)) ? $reservation_data['OccupantNote'] = $room->OccupantNote : '';
                                (!empty($reservation_data)) ? $roomsbooked->reservation_data = json_encode($reservation_data) : $roomsbooked->reservation_data = null;
                                
                                $roomsbooked->property_id = $property_id;
                                $roomsbooked->save();

                                /*
                                Scale Availability
                                */

                                if ($booking->IsCancellation !== true) {
                                    \App\Availability::scale($room->StartDate, $room->EndDate, $room->RoomTypeId);
                                }
                            }
                        }
                    }
                }

                $users = \App\User::where('property_id', 'property.id')->join('properties', 'users.property_id', '=', 'properties.id');
                foreach ($users as $user) {
                    $user->notify(new \App\Notifications\Notifications(
                        [
                          'title'=> trans('admin.new_reservations_received'),
                          'message'=> trans('admin.new_reservations_message', ['pnr'=> $reservation->pnr])
                        ]
                      ));
                }
            } else {
                if ($booking->IsCancellation == true) {
                    $reservation = \App\Reservations::where('myallocatorId', $booking->MyallocatorId)->first();
                    $reservation->status = 'CAN';
                    $reservation->save();

                    $rooms = \App\RoomsBooked::where('reservation_id', $reservation->id)->get();
                    foreach ($rooms as $key => $room) {
                        \App\Availability::add($room->check_in, $room->check_out, $room->rooms_type_id);
                    }
                    \App\RoomsBooked::where('reservation_id', '=', $reservation->id)->update(['status' => 'CAN']);

                    $users = \App\User::where('property_id', $property_id)
                    ->join('properties', 'users.property_id', '=', 'properties.id')->get(['users.id']);

                    foreach ($users as $user) {
                        $user->notify(new \App\Notifications\Notifications(
                            [
                              'title'=> trans('admin.reservations_cancelled'),
                              'message'=> trans('admin.reservations_cancelled_message', ['pnr'=> $reservation->pnr])
                            ]
                          ));
                    }
                } elseif ($booking->IsModification == true) {
                    //get Reservations from MyallocatorId
                    $reservation = \App\Reservations::where('myallocatorId', $booking->MyallocatorId)->first();
                    //get RoomsBooked from reservation_id
                    
                    $rooms = \App\RoomsBooked::join('rooms', 'rooms.id', '=', 'rooms_booked.room_id')
                    ->where('reservation_id', $reservation->id)->get([
                      'name',
                      'room_id',
                      'rooms_booked.rooms_type_id',
                      'reservation_data',
                      'check_in',
                      'check_out',
                      'guests',
                      'price'
                    ]);
                    //store data in reservations history
                    $data = [
                      'reservations' => collect($reservation)->toArray(),
                      'rooms' => collect($rooms)->toArray()
                    ];
                    $res_history = new \App\ReservationsHistory();
                    $res_history->reservation_id = $reservation->id;
                    $res_history->data = serialize(json_encode($data));
                    $res_history->save();
                    
               

                    // Delete rooms_booked
                    
                    \App\RoomsBooked::where('reservation_id', $reservation->id)->delete();
                    
                     foreach ($rooms as $key => $room) {
                        \App\Availability::add($room->check_in, $room->check_out, $room->rooms_type_id);
                    }
                    
                    
                    if (isset($booking->Customers)) {
                        foreach ($booking->Customers as $res_customer) {
                            
                            $customer = \App\Customers::where('id', $reservation->customer_id)->first();
                            
                            if (isset($res_customer->CustomerLName)) {
                                $customer->name = $res_customer->CustomerLName;
                            }
                            if (isset($res_customer->CustomerFName)) {
                                $customer->surname = $res_customer->CustomerFName;
                            }
                            if (isset($res_customer->CustomerEmail)) {
                                $customer->email = $res_customer->CustomerEmail;
                            }
                            if (isset($res_customer->CustomerPhone)) {
                                $customer->phone = $res_customer->CustomerPhone;
                            }
                            if (isset($res_customer->CustomerPhoneMobile)) {
                                $customer->phone_2 = $res_customer->CustomerPhoneMobile;
                            }
                            if (isset($res_customer->CustomerCity)) {
                                $customer->city = $res_customer->CustomerCity;
                            }
                            if (isset($res_customer->CustomerCountry)) {
                                $customer->country = $res_customer->CustomerCountry;
                            }
                            if (isset($res_customer->CustomerAddress)) {
                                $customer->address = $res_customer->CustomerAddress;
                            }
                            
                            $customer->property_id = $property_id;
                            $customer->save();

                        }
                    }
                    

                    $reservation->note = isset($res_customer->CustomerNote) ? $res_customer->CustomerNote : null;
                    $reservation->total = isset($booking->TotalPrice) ? $booking->TotalPrice : 0.00;
                    $reservation->data = json_encode([
                        'OrderId' => $booking->OrderId,
                        'IsCancellation' => $booking->IsCancellation,
                        'IsModification' => $booking->IsModification,
                        'Acknowledged' => $booking->Acknowledged
                    ]);
                    $reservation->review = 1;
                    $reservation->save();

                    $portals_commission = \App\PortalsCommission::where('reservation_id', $reservation->id)->first();
                    $portals_commission->cost =  isset($booking->Commission) ? $booking->Commission : 0.00;
                    $portals_commission->transaction_code = isset($booking->OrderId) ? $booking->OrderId : null;
                    
                    /* 
                    if (isset($booking->Commission)) {
                        $portals_commission->cost =  isset($booking->Commission) ? $booking->Commission : 0.00;
                    }
                    if (isset($booking->OrderId)) {
                        $portals_commission->transaction_code = isset($booking->OrderId) ? $booking->OrderId : null;
                    }
                    */
                    
                    $portals_commission->save();

                    
                    if (isset($booking->Rooms)) {
                        
                        foreach ($booking->Rooms as $key => $room) {
                            
                            if ($room->Units >= 1) {
                                
                                $room->RoomTypeId = $room->RoomTypeIds[0];

                                if (isset($room->Occupancy)) {
                                    $room->Occupancy = $room->Occupancy;
                                } elseif(isset($room->Adults)) {
                                    $room->Occupancy = $room->Adults;
                                }
    
                                $room->RoomTypeId = \App\RoomsType::where('ma_room_id', $room->RoomTypeId)->first()->id;
    
                                $start_date = $room->StartDate;
                                $end_date = $room->EndDate;
                                $units = $room->Units;
                                $occupancy = $room->Occupancy;
                                $rooms_type = $room->RoomTypeId;
    
                                //$roomsfound = \App\Rooms::get_rooms_available_by_roomtype($property_id, $room->StartDate, $room->EndDate, $room->Units, $room->Occupancy);
                                $roomsfound = \App\Rooms::get_rooms_available_by_roomtype($property_id, $start_date, $end_date, $units, $rooms_type);
                                // Booking Found

                                foreach ($roomsfound as $roomfound) {
                                    // Rooms Found
                                    $roomsbooked = new \App\RoomsBooked();

                                    if (isset($roomfound->id)) {
                                        $roomsbooked->room_id = $roomfound->id;
                                    }
                                    if (isset($reservation->id)) {
                                        $roomsbooked->reservation_id = $reservation->id;
                                    }
                                    if (isset($room->RoomTypeIds)) {
                                        $roomsbooked->rooms_type_id = $room->RoomTypeId;
                                    }
                                    if (isset($room->StartDate)) {
                                        $roomsbooked->check_in = $room->StartDate;
                                    }
                                    if (isset($room->EndDate)) {
                                        $roomsbooked->check_out =  $room->EndDate;
                                    }
                                    if (isset($room->Occupancy)) {
                                        $roomsbooked->guests = $room->Occupancy;
                                    }
                                    if (isset($room->Price)) {
                                        $roomsbooked->price = $room->Price;
                                    }
                                    if (isset($room->RateId)) {
                                        $roomsbooked->rate_id = $room->RateId;
                                    }
                                    if (isset($room->RateDesc)) {
                                        $roomsbooked->rate_desc = $room->RateDesc;
                                    }
                                    if ($booking->IsCancellation === true) {
                                        $roomsbooked->status = 'CAN';
                                    }
                                    $reservation_data = [];
                                    (!empty($room->OccupantLName)) ? $reservation_data['OccupantLName'] = $room->OccupantLName : '';
                                    (!empty($room->OccupantSmoker)) ? $reservation_data['OccupantSmoker'] = $room->OccupantSmoker : '';
                                    (!empty($room->OccupantNote)) ? $reservation_data['OccupantNote'] = $room->OccupantNote : '';
                                    (!empty($reservation_data)) ? $roomsbooked->reservation_data = json_encode($reservation_data) : $roomsbooked->reservation_data = null;
                                    $roomsbooked->property_id = $property_id;
                                    $roomsbooked->save();
                                    
                                    //Scale Availability
                                    
                                    if ($booking->IsCancellation !== true) {
                                        \App\Availability::scale($room->StartDate, $room->EndDate, $room->RoomTypeId);
                                    }
                                }
                            }
                        }
                    }
                    
                    
            
                    
                    $users = \App\User::where('property_id', $property_id)
                    ->join('properties', 'users.property_id', '=', 'properties.id')->get(['users.id']);

                    foreach ($users as $user) {
                        $user->notify(new \App\Notifications\Notifications(
                          [
                            'title'=> trans('admin.reservations_modified'),
                            'message'=> trans('admin.reservations_modified_message', ['pnr'=> $reservation->pnr])
                          ]
                        ));
                    }
                }
            }

            return true;
        }
    }

    /**
    * Get Check in and Check out dates from booked rooms list
    *
    * Parameters:
    *     (rooms) - object
    */

    public static function get_checkin_checkout_dates($rooms){
        if(!empty($rooms)){
            $dates_check_in = [];
            $dates_check_out = [];
            foreach($rooms as $room){
                array_push($dates_check_in, $room->check_in);
                array_push($dates_check_out, $room->check_out);
            }
            return [min($dates_check_in), max($dates_check_out)];
        }
    }


    /**
    * Get Cheepest Price
    *
    * Parameters:
    *     (rates) - object
    */

    public static function get_cheepest_price($rates, $guests = 1){
        if(!empty($rates)){
            $prices = [];
            foreach($rates as $rate){
               if($rate->single_rate_amount !== null && $rate->single_rate_amount > 0  && $guests == 1){
                    array_push($prices, $rate->single_rate_amount);
               }else{
                    array_push($prices, $rate->rate_amount);
               }
            }
            return self::get_money(min($prices));
        }
    }
    

}
