<?php

namespace App\Helpers;

use App\Customers;
use App\Reservations;
use App\Helpers\Helper;
use App\Mail\MailNotifications;
use Illuminate\Support\Facades\Mail;
use Curl;
use Auth;
use Request;

class MyAllocator
{


    /**
    * LoopBookingCreate
    * Create a new booking in the Loopback Channel. Parameters are optional and if not specified then booking will be populated with DWIW (Do What I Want) style data. See the "BookingList" api call for a complete discussion of Booking related fields.
    * Booking callback notifications are disabled ** for bookings created via LoopBookingCreate. To test booking callback notifications, you can setup BookNow in the web interface for a test property and generate test bookings.
    * Bookings will not automatically adjust on myallocator or the channels ** for bookings created via LoopBookingCreate. To test booking adjustments, you can setup BookNow in the web interface for a test property and generate test bookings.
    *
    * Parameters:
    *     ($booking) - array with booking data
    */

    public static function LoopBookingCreate($ma_data = null)
    {
        try {
            if ($ma_data == null) {
                throw new \Exception(trans('admin.param_is_missing'));
            } else {
                $json = [
                "Auth/VendorId"=> env('MYALLOCATOR_VENDOR_ID'),
                "Auth/VendorPassword"=> env('MYALLOCATOR_VENDOR_PASSWORD'),
                "Auth/UserToken"=> env('MYALLOCATOR_USER_TOKEN'),
                "Auth/PropertyId"=> Helper::get_property(),
                "Booking"=> $ma_data,
              ];

                $response = Curl::to(env('MYALLOCATOR_API_URL')."LoopBookingCreate")
                ->withData($json)
                ->asJson()
                ->returnResponseObject()
                ->post();

                return $response->content;
            }
        } catch (\Exception $e) {
            print $e->getMessage();
        }
    }

    /*
        This method allows you to query for bookings made to a specific property by booking date, modification date or arrival date.

        It is important to know that not every booking that is returned through the API neccessarily resulted in an adjustment of the other channels. If the booking is not mapped to any rooms on our system, or if the channel has just been setup, the adjustment will not be carried out. The requests consists of search criteria by date. Only specify the StartDate/EndDate of one criteria. note: formerly BookingList (v1).

        ** Data Formatting ** Different channels return a different amount of information about a booking, therefore many fields are optional. (Sorry this isn't more useful, we're working on addressing this)

        ** Best Pratices **

        Callbacks are the fastest, best, and preferred way of receiving Booking data.
        Every effort will be made to synchronize the callback data format and this format. (You can/should use the same data parsing code)
        BookingList method should only be used as a backup to correct errors or lost callbacks. Or to periodically verify integrity of data.
        Always use ModificationStartDate ModificationEndDate in production.
        If a change/cancellation is received then the modification date will also be different from the creation date)
        Do not frequently poll this API, one call, per property, every 30 minutes is considered "Acceptable Usage".
        It is acceptable use for a PMS to burst on this API when initially pulling down data for an existing MA client.
      */

    public static function BookingList()
    {
        $arg_list = func_get_args();
        if (isset($arg_list[0]['CreationStartDate'])
            && isset($arg_list[0]['CreationEndDate'])) {
            $creationStartDate = $arg_list[0]['CreationStartDate'];
            $creationEndDate = $arg_list[0]['CreationEndDate'];
        }
        $json = [
          "Auth/VendorId"=> env('MYALLOCATOR_VENDOR_ID'),
          "Auth/VendorPassword"=>env('MYALLOCATOR_VENDOR_PASSWORD'),
          "Auth/UserToken"=> env('MYALLOCATOR_USER_TOKEN'),
          "Auth/PropertyId"=>Helper::get_property(),
          "ArrivalStartDate" => "",//Query for date of arrival (first day of staying).
          "ArrivalEndDate" => "",//Required if ArrivalStartDate is used.
          "ModificationStartDate"=>"",//Query by booking modification date. If the booking has not been modified the creation date is used. Timezone used is UTC. The date corresponds to the date the booking was modified/created on myallocator and not neccessarily on the channel, although it should be equal for most cases.
          "ModificationEndDate"=>"",//Required if ModificationStartDate is used. We recommend to add one day to the end date for regular booking imports, to make up for time differences between your and our server.
          "CreationStartDate"=>$creationStartDate,//Query by booking creation date. Timezone used is UTC. The date corresponds to the date the booking was created on myallocator and not neccessarily on the channel, although it should be equal for most cases. Important: We strongly recommend to use ModificationStartDate / ModificationEndDate instead of CreationStartDate/CreationEndDate to regular booking retrievals!
          "CreationEndDate"=>$creationEndDate,//Required if CreationStartDate is used.
          "CreationStartDateTime"=>"", //Same as CreationStartDate but uses YYYY-MM-DD hh:mm:ss format. Timezone is UTC.
          "CreationEndDateTime"=>"", //Same as CreationEndDate but uses YYYY-MM-DD hh:mm:ss format. Timezone is UTC.
          "ModificationStartDateTime"=>"", //Same as ModificationStartDate but uses YYYY-MM-DD hh:mm:ss format. Timezone is UTC.
          "ModificationEndDateTime"=>"", //Same as ModificationEndDate but uses YYYY-MM-DD hh:mm:ss format. Timezone is UTC. We recommend adding 5 minutes to the end date to make up for time differences between your and our server.
          "NotificationQueuedStartDateTime"=>"", //for querying QueuedNotifications (will always return latest version of booking)
          "NotificationQueuedEndDateTime"=>"", //see NotificationQueuedStartDate
          "OrderId"=>"", //Query for a specific order ID (assigned by channel). Be aware that order IDs are not guaranteed to be unique across different channels! Using BookingId below is strongly recommended for individual booking retrievals.
          "BookingId"=>"", // Query for a specific booking id (also known as "MyAllocatorId")
          "CreditCardPassword"=>"", //Password to decrypt booking credit card details. Also requires Options/IncludePaymentData option to be true.
          "Options"=>[
            "NormalizeToCurrency" => "", // CurrencyCode
            "IncludeArchived" => "" // Bolean
          ],
        ];


        $response = Curl::to(env('MYALLOCATOR_API_URL')."/BookingList")
        ->withData($json)
        ->asJson()
        ->returnResponseObject()
        ->post();

        return $response->content;
    }


    public static function BookingCancel($my_AllocatorId, $cancellation_Reason)
    {
        $json = [
          "Auth/VendorId"=> env('MYALLOCATOR_VENDOR_ID'),
          "Auth/VendorPassword"=>env('MYALLOCATOR_VENDOR_PASSWORD'),
          "Auth/UserToken"=> env('MYALLOCATOR_USER_TOKEN'),
          "Auth/PropertyId"=>Helper::get_property(),
          "MyAllocatorId"=>$my_AllocatorId,
          "CancellationReason"=>$cancellation_Reason
        ];

        $response = Curl::to(env('MYALLOCATOR_API_URL')."BookingCancel")
        ->withData($json)
        ->asJson()
        ->returnResponseObject()
        ->post();

        return $response->content;
    }


    public static function RatePlanList()
    {
        $json = [
        "Auth/VendorId"=> env('MYALLOCATOR_VENDOR_ID'),
        "Auth/VendorPassword"=>env('MYALLOCATOR_VENDOR_PASSWORD'),
        "Auth/UserToken"=> env('MYALLOCATOR_USER_TOKEN'),
        "Auth/PropertyId"=>Helper::get_property()
        ];

        $response = Curl::to(env('MYALLOCATOR_API_URL')."RatePlanList")
      ->withData($json)
      ->asJson()
      ->returnResponseObject()
      ->post();

        return $response->content;
    }

    public static function RoomCreate($room_type = null)
    {
        try {
            if ($room_type == null) {
                throw new \Exception(trans('admin.param_is_missing'));
            } else {
                $json = [
                "Auth/VendorId"=> env('MYALLOCATOR_VENDOR_ID'),
                "Auth/VendorPassword"=>env('MYALLOCATOR_VENDOR_PASSWORD'),
                "Auth/UserToken"=> env('MYALLOCATOR_USER_TOKEN'),
                "Auth/PropertyId"=>Helper::get_property(),
                "ValidateOnly"=>true,
                "AuthorizeBilling"=>true,
                "Rooms"=>[
                    0 =>[
                    "PMSRoomId"=>$room_type->id,
                    "Label"=>Helper::get_traslated_string($room_type->type),
                    "Units"=>$room_type->units,
                    "Occupancy"=>$room_type->capacity,
                    "PrivateRoom"=>$room_type->private,
                    "Gender"=>$room_type->gender
                    ]
                ]
               ];


                $response = Curl::to(env('MYALLOCATOR_API_URL')."RoomCreate")
            ->withData($json)
            ->asJson()
            ->returnResponseObject()
            ->post();

                return $response->content;
            }
        } catch (\Exception $e) {
            print $e->getMessage();
        }
    }

    public static function RoomUpdate($room_type = null)
    {
        try {
            if ($room_type == null) {
                throw new \Exception(trans('admin.param_is_missing'));
            } else {
                $json = [
                "Auth/VendorId"=> env('MYALLOCATOR_VENDOR_ID'),
                "Auth/VendorPassword"=>env('MYALLOCATOR_VENDOR_PASSWORD'),
                "Auth/UserToken"=> env('MYALLOCATOR_USER_TOKEN'),
                "Auth/PropertyId"=>Helper::get_property(),
                "ValidateOnly"=>true,
                "AuthorizeBilling"=>true,
                "Rooms"=>[
                    0 =>[
                      "RoomId" => $room_type->ma_room_id,
                      "PMSRoomId"=>$room_type->id,
                      "Label"=>Helper::get_traslated_string($room_type->type),
                      "Description" => Helper::get_traslated_string($room_type->description),
                      "Units"=>$room_type->units,
                      "Occupancy"=>$room_type->capacity,
                      "PrivateRoom"=>$room_type->private,
                      "Gender"=>$room_type->gender
                    ]
                ]
               ];

                $response = Curl::to(env('MYALLOCATOR_API_URL')."RoomUpdate")
                ->withData($json)
                ->asJson()
                ->returnResponseObject()
                ->post();

                return $response->content;
            }
        } catch (\Exception $e) {
            print $e->getMessage();
        }
    }

    public static function RoomRemove($room_id = null)
    {
        try {
            if ($room_id == null) {
                throw new \Exception(trans('admin.param_is_missing'));
            } else {
                $json = [
                "Auth/VendorId"=> env('MYALLOCATOR_VENDOR_ID'),
                "Auth/VendorPassword"=>env('MYALLOCATOR_VENDOR_PASSWORD'),
                "Auth/UserToken"=> env('MYALLOCATOR_USER_TOKEN'),
                "Auth/PropertyId"=>Helper::get_property(),
                "Rooms"=>[ 
                    0 => [
                        "RoomId" => $room_id
                    ]
                ]
               ];


                $response = Curl::to(env('MYALLOCATOR_API_URL')."RoomRemove")
            ->withData($json)
            ->asJson()
            ->returnResponseObject()
            ->post();


                return $response->content;
            }
        } catch (\Exception $e) {
            print $e->getMessage();
        }
    }


    public static function RatePlanCreate($ma_data = null)
    {
        try {
            if ($ma_data == null) {
                throw new \Exception(trans('admin.param_is_missing'));
            } else {
                $json = [
                  "Auth/VendorId"=> env('MYALLOCATOR_VENDOR_ID'),
                  "Auth/VendorPassword"=>env('MYALLOCATOR_VENDOR_PASSWORD'),
                  "Auth/UserToken"=> env('MYALLOCATOR_USER_TOKEN'),
                  "Auth/PropertyId"=>Helper::get_property(),
                  "RoomTypeId"=>$ma_data['RoomTypeId'],
                  "Label"=>$ma_data['Label'],
                  "IsIBEOnly"=>$ma_data['IsIBEOnly'],
                  "OccupancyMin"=>$ma_data['OccupancyMin'],
                  "OccupancyMax"=>$ma_data['OccupancyMax'],
                  "MinLOS"=>$ma_data['MinLOS'],
                  "MaxLOS"=>$ma_data['MaxLOS'],
                  "RateModel"=>$ma_data['RateModel']
                ];

                $response = Curl::to(env('MYALLOCATOR_API_URL')."RatePlanCreate")
              ->withData($json)
              ->asJson()
              ->returnResponseObject()
              ->post();

                return $response->content;
            }
        } catch (\Exception $e) {
            print $e->getMessage();
        }
    }


    public static function RatePlanRemove($ma_data = null)
    {
        try {
            if ($ma_data == null) {
                throw new \Exception(trans('admin.param_is_missing'));
            } else {
                $json = [
                  "Auth/VendorId"=> env('MYALLOCATOR_VENDOR_ID'),
                  "Auth/VendorPassword"=>env('MYALLOCATOR_VENDOR_PASSWORD'),
                  "Auth/UserToken"=> env('MYALLOCATOR_USER_TOKEN'),
                  "Auth/PropertyId"=>Helper::get_property(),
                  "RatePlanId"=>$ma_data['RatePlanId']
                ];

                $response = Curl::to(env('MYALLOCATOR_API_URL')."RatePlanRemove")
              ->withData($json)
              ->asJson()
              ->returnResponseObject()
              ->post();

                return $response->content;
            }
        } catch (\Exception $e) {
            print $e->getMessage();
        }
    }

    /*
    RoomList
    * returns a list of configured room types for a property
    */
    public static function RoomList()
    {
        $json = [
        "Auth/VendorId"=> env('MYALLOCATOR_VENDOR_ID'),
        "Auth/VendorPassword"=>env('MYALLOCATOR_VENDOR_PASSWORD'),
        "Auth/UserToken"=> env('MYALLOCATOR_USER_TOKEN'),
        "Auth/PropertyId"=>Helper::get_property()
      ];

        $response = Curl::to(env('MYALLOCATOR_API_URL')."RoomList")
        ->withData($json)
        ->asJson()
        ->returnResponseObject()
        ->post();
    }


    /*
    ARIUpdate
    An allocation sets the number of rooms or beds (depending on whether the room type is a private room or shared/dorm) available during any specific time frame. For more details see below.

      ** Excluding Channels ** prefix a channel id with a minus (-) to exclude it. Ex: a series of "all", "-loop" would update all configured channels except "loop"

      ** Handling Errors & Warnings **

      The response will always include the Success, Errors and Warnings tags. If Success is set to "true" the Errors tag will be empty. If Success is set to "partial" or "false" at least one Error tag is included.

      ** Sending Overlapping Ranges / Partial updates ** Overlapping ranges in an ARIUpdate are absolutely allowed. As long as the ranges are sent in the same request body they are applied in sequence as part of the same SQL transaction. Fields such as Units, MinStay, MaxStay, Price are optional so it is possible send different ranges for pricing and availability.

      How this works: we receive XML/JSON request body and decode it into a neutral format, then write availability directly to the internal MyAllocator database and provision an updateId for a date range plus room-type(s), then create one job per channel, finally we return the updateId to the response (assuming QueryForStatus==True).

      Each job contains ONLY the propertyid, updateId, and ota (no availability, or pricing). Jobs are picked up one of several auto-scaling "worker tier" servers, each server loads the latest availability from the db (which may have changed since the updateId was issued), and then applies any ARIRules, then transmit to OTA, and flag their portion of the updateId as completed. This ensures the latest availability will always be sent to the OTA.

      Once an UpdateId has been issued it falls within the scope of my monitoring domain, it should be considered reliable by a developer. The request is broken into a series of jobs (one for each channel). Each job is logged to disk and processed by a pool of servers. It would take a substantial level of failure to be lost. (Jobs are not quite as durable as bookings or configuration data, but it's still highly durable).

      ** Submitting to ALL channels **

      You can also submit to all available channels without specifying explicity which channels to update. Use the channel code "all" to do this. Channels that are not set up by the user will be skipped as indicated by a warning.

      It is also possible to submit to all channels while excluding certain channels. Add the attribute exclude="true" to skip a channel. See example below. Use the channel code "all" to do this.

      ** Running Jobs in the Background (ARIUpdateStatus) **

      You can also run the ARIUpdate update in the background and query for the updates using ARIUpdateStatus. This enables you to show the update progress to the user while it's still running. To enable this feature you need to add the node QueryForStatus (see example)

      If QueryForStatus is true then the ARIUpdateResponse will contain the additional parameter UpdateId, which is needed for ARIUpdateStatus

      QueryForStatus TRUE will be much more resilient than attempting to maintain a single persistent connection, and it will free up connection requests on our side. In the future QueryForStatus FALSE may be removed and/or additional timeouts will be added which may make it unusable. New integrations should avoid using QueryForStatus FALSE.
    */

    public static function ARIUpdate()
    {
        try {
            if (func_num_args()=== 0) {
                throw new \Exception(trans('admin.param_is_missing'));
            } else {
                $allocations = func_get_arg(0);

                $json = [
                  "Auth/VendorId"=> env('MYALLOCATOR_VENDOR_ID'),
                  "Auth/VendorPassword"=>env('MYALLOCATOR_VENDOR_PASSWORD'),
                  "Auth/UserToken"=> env('MYALLOCATOR_USER_TOKEN'),
                  "Auth/PropertyId"=> Helper::get_property(),
                  "Channels"=> ['all'],
                  "Allocations"=> $allocations,
                  "QueryForStatus" => true
                ];

                $response = Curl::to(env('MYALLOCATOR_API_URL') . "/ARIUpdate")
                    ->withData($json)
                    ->asJson()
                    ->returnResponseObject()
                    ->post();

                return $response->content;
            }
        } catch (\Exception $e) {
            print $e->getMessage();
        }
    }

    public static function VendorSet()
    {
        $json = [
        "Auth/VendorId"=> env('MYALLOCATOR_VENDOR_ID'),
        "Auth/VendorPassword"=> env('MYALLOCATOR_VENDOR_PASSWORD'),
        "Callback/URL"=> env('MYALLOCATOR_CALLBACK_URL'),
        "Callback/Password"=> env('MYALLOCATOR_CALLBACK_PASSWORD'),
        "Callback/NotifyBooking"=> env('MYALLOCATOR_CALLBACK_NOTIFY')
      ];

        $response = Curl::to(env('MYALLOCATOR_API_URL')."/VendorSet")
        ->withData($json)
        ->asJson()
        ->returnResponseObject()
        ->post();

        return $response->content;
    }

    /*
    ChannelList
    Dumps information about all known channels, their status, and supported fields.
    */

    public static function ChannelList()
    {
        $json = [
        "Auth/VendorId"=> env('MYALLOCATOR_VENDOR_ID'),
        "Auth/VendorPassword"=>env('MYALLOCATOR_VENDOR_PASSWORD')
      ];

        $response = Curl::to(env('MYALLOCATOR_API_URL')."/ChannelList")
        ->withData($json)
        ->asJson()
        ->returnResponseObject()
        ->post();

        if ($response->content->Success === true) {
            $channelslist = [];
            foreach ($response->content->Channels as $key => $value) {
                if ($value->isLive === 'true') {
                    array_push($channelslist, $key);
                }
            }
            return $channelslist;
        } else {
            return $response->content;
        }
    }

    /*
    RoomAvailabilityList

    Questa chiamata può essere utilizzata per richiedere tutti i dati che conserviamo per una proprietà specifica e un intervallo di date. L'intervallo di date può essere massimo di 31 giorni. È possibile eseguire una query più volte se è necessario un intervallo di date più lungo.

    Un'impostazione MaxStay di 0 significa "senza restrizioni". I 48 e 49 nell'esempio di risposta qui sotto si riferiscono all'ID stanza come restituito da GetRooms (XML API).

    ** Compatibilità: ** questo era precedentemente disponibile all'indirizzo: https://myallocator.com/it/api/data
    */

    public static function RoomAvailabilityList()
    {
        $json = [
          "Auth/VendorId"=> env('MYALLOCATOR_VENDOR_ID'),
          "Auth/VendorPassword"=>env('MYALLOCATOR_VENDOR_PASSWORD'),
          "Auth/UserToken"=> env('MYALLOCATOR_USER_TOKEN'),
          "Auth/PropertyId"=>Helper::get_property(),
          "StartDate"=> func_get_arg(0),
          "EndDate"=> func_get_arg(1)
        ];

        $response = Curl::to(env('MYALLOCATOR_API_URL') . "RoomAvailabilityList")
        ->withData($json)
        ->asJson()
        ->returnResponseObject()
        ->post();

        return $response->content;
    }

    /*
    Get MyAllocator Response
    */

    public static function GetMyAllocatorResponse($response)
    {
        if ($response) {
            $message = (object)[];
            if (isset($response->Success)) {
                if ($response->Success === true) {
                    $message->Success = true;
                } else {
                    $message->Success = false;
                }
                return $message;
            } elseif (isset($response->Errors)) {
                $message->Success = false;
                $message->Errors = $response->Errors;
                $errors = "";
                foreach ($response->Errors as $key => $value) {
                    $errors .= '<p>' .  $message->Errors[$key]->ErrorMsg . 'code('. $message->Errors[$key]->ErrorId .')' . '</p>';
                }
                $message->Errors = $errors;

                // send mail ticket to administrator
                $mail_data = [
                  'address' => Helper::get_setting('email')->address,
                  'subject' => trans('emails.channel_manage_errors'),
                  'name' => 'Novabeds',
                  'view' => 'emails.channel_manage_errors',
                  'with' => ['errors' => $errors]
                ];

                Mail::to(Helper::get_setting('hotel_data')->email)->send(new MailNotifications($mail_data));

                return $message;
            }
        } else {
            return false;
        }
    }
}
