<?php

namespace App\Helpers;

use App\Helpers\Helper;
use App\Reservations;
use App\RoomsBooked;
use App\Rooms;
use App\Availability;
use Request;

class ReservationHelper
{
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function put($data)
    {
        $errors = [];
        $data = (object)$data;
        $data->check_out = date('Y-m-d', strtotime('-1 day', strtotime($data->check_out)));

        $reservation = new Reservations;
        $reservation->customer_id = $data->customer->id;
        $reservation->pnr = $data->pnr;
        $reservation->note = $data->note;
        $reservation->origin = 1;
        $reservation->discount = $data->discount;
        $reservation->subtotal = $data->subtotal;
        $reservation->total = $data->grandtotal;
        $reservation->status = $data->status;
        $reservation->property_id = $data->property_id;
        $reservation->save();

        if ($data->rooms) {
            $rooms_type_ids = [];

            foreach ($data->rooms as $key => $room) {
                $rooms_type_ids[$key] = $room->rooms_type_id;
                $roomsfound = collect(Rooms::get_rooms_available_by_type($data->property_id, $data->check_in, $data->check_out, 1, $room->guest))->first();

                if ($roomsfound) {
                    $reservation_data = (object)[];
                    $reservation_data->Currency = Helper::get_setting('currency')->code;
                    $reservation_data->Occupancy = $room->guest;
                    $reservation_data->RateDesc = Helper::get_traslated_string($room->rate_name);
                    $reservation_data->RateId = $room->rate_id;
                    $reservation_data->StartDate = $data->check_in;
                    $reservation_data->EndDate = $data->check_out;
                    $reservation_data->Price = $room->rate_amount;
                    $reservation_data->RoomTypeId = $room->rooms_type_id;
                    $reservation_data->Property_id = $data->property_id;

                    $roomsbooked = new RoomsBooked();
                    $roomsbooked->room_id = $roomsfound->id;
                    $roomsbooked->property_id = $data->property_id;
                    $roomsbooked->rooms_type_id = $room->rooms_type_id;
                    $roomsbooked->reservation_id = $reservation->id;
                    $roomsbooked->check_in = $data->check_in;
                    $roomsbooked->check_out = $data->check_out;
                    $roomsbooked->guests = $room->guest;
                    $roomsbooked->price = $room->rate_amount;
                    $roomsbooked->rate_id = $room->rate_id;
                    $roomsbooked->reservation_data = serialize(json_encode($reservation_data));
                    $roomsbooked->status = 'CNF';
                    $roomsbooked->save();
                    /*
                    Scale Availability
                    */
                    Availability::scale($data->check_in, $data->check_out, $room->rooms_type_id);
                } else {
                    $errors[] = trans('admin.rooms_not_available');
                }
            }
        } else {
            $errors[] = trans('admin.rooms_field_is_empty');
        }

        if (isset($data->fees)) {
            foreach ($data->fees as $fee) {
                $reservation_fee = new \App\ReservationFees;
                $reservation_fee->reservation_id = $reservation->id;
                $reservation_fee->fee_id = $fee->id;
                $reservation_fee->fee_paid = $fee->value;
                $reservation_fee->percentage = $fee->percentage;
                $reservation_fee->save();
            }
        }

        if (isset($data->payment)) {
            $payments = new \App\ReservationPayments;
            $payments->reservation_id = $reservation->id;
            $payments->transaction_id = $data->payment['transaction_id'];
            $payments->causal = $data->payment['casual'];
            $payments->method = $data->payment['method'];
            $payments->payment_date =$data->payment['payment_date'];
            $payments->fee = $data->payment['fee'];
            $payments->total = $data->grandtotal;
            $payments->status = $data->payment['status'];
            $payments->save();
        } else {
            $errors[] = trans('admin.payment_details_missing');
        }

        if (isset($data->credit_card)) {
            $credit_card = new \App\ReservationCreditCard;
            $credit_card->reservation_id = $reservation->id;
            $credit_card->customer_id = $data->customer->id;
            $credit_card->data = encrypt($data->credit_card);
            $credit_card->save();
        }

        if (isset($data->offert)) {
            $offert = new \App\ReservationOffert;
            $offert->reservation_id = $reservation->id;
            $offert->code = $data->offert->code;
            $offert->save();
        }

        if (empty($errors)) {
            if (Helper::get_setting('my_allocator')->active) {
                $allocations = \App\Availability::get_availability_status($data->check_in, $data->check_out, $rooms_type_ids);
                $response = MyAllocator::GetMyAllocatorResponse(MyAllocator::ARIUpdate($allocations));
            }
            return true;
        } else {
            return $errors;
        }
    }

    public static function cancel($id)
    {
        $rooms_type_ids = [];
        $dates = [];
        $rooms = RoomsBooked::where('reservation_id', $id)->get();
        foreach ($rooms as $key => $room) {
            array_push($rooms_type_ids, $room->rooms_type_id);
            array_push($dates, $room->check_in);
            array_push($dates, $room->check_out);
            Availability::add($room->check_in, $room->check_out, $room->rooms_type_id);
        }
        RoomsBooked::where('reservation_id', '=', $id)->update(['status' => 'CAN']);
        $reservation = \App\Reservations::find($id);
        $reservation->status = 'CAN';
        $reservation->save();

        /*$check_in = min(array_map('strtotime', $dates));
        $check_out = max(array_map('strtotime', $dates));

        if (Helper::get_setting('my_allocator')->active) {
            $allocations = Availability::get_availability_status($check_in, $check_out, $rooms_type_ids);
            $response = MyAllocator::GetMyAllocatorResponse(MyAllocator::ARIUpdate($allocations));
            if ($response->Success == true) {
                $message = [
                'success'=>true,
                'type'=>'message_success',
                'text'=>'La tua prenotazione è stata eliminata ed è stata inviata una richiesta al servizio Channel Manager' . $reservation->MyAllocatorId
            ];
            } else {
                $message = [
                      'success'=>true,
                      'type'=>'message_warning',
                      'text'=> 'La tua prenotazione è stata eliminata ma si è verificato un errore nel servizio Channel Manager<br>' . $response->Errors
            ];
            }
        } else {
            $message = [
                'success'=>true,
                'type'=>'message_warning',
                'text'=> 'La tua prenotazione è stata eliminata ma non è stato aggiornato il servizio Channel Manager è possibile che non sia attivato'
               ];
        }*/

        return response()->json(['Success'=>true]);
    }

     public function get_reservations_from_myallocator(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
        [
          'start_date' => 'required|date',
          'end_date' => 'required|date'
        ],
        [
          'start_date.required' => trans('required.creation_start_date'),
          'end_date.required' => trans('required.creation_end_date'),
        ]
        );

        if ($validator->fails()) {
            return view('admin.dashboard')->withErrors($validator);
        } else {
            $response = MyAllocator::BookingList(['CreationStartDate'=> $request->start_date,'CreationEndDate'=> $request->end_date]);
            if ($response->Success == true) {
                if (isset($response->Bookings)) {
                    foreach ($response->Bookings as $key => $booking) {
                        Helper::save_reservation($booking);
                    }
                    return response()->json(['success'=>true , 'bookings'=>count($response->Bookings)]);
                }
            }
        }
    }
}
