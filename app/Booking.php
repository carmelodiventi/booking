<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Hashids\Hashids;
use Carbon\Carbon;
use App\Helpers\Helper;
use App\Helpers\MyAllocator;

class Booking extends Model
{

    public static function getRoomsDetails($check_in, $check_out, $rooms_type_id, $rates_type_id)
    {
        return collect(
            DB::select("SELECT
                        ra.rooms_type_id AS rooms_type_id,
                        ra.rates_type_id AS rate_id,
                        rot.type,
                        rot.description,
                        rot.ma_room_id,
                        (SELECT group_concat(url SEPARATOR ',') FROM rooms_photo WHERE rooms_photo.rooms_type_id = rot.id) AS photo,
                        rat.name AS rate_name,
                        rat.description AS rate_description,
                        rat.refundable,
                        rot.capacity,
                        SUM(ra.rate) AS rate_amount,
                        SUM(ra.single_rate) AS single_rate_amount
                        FROM rate_availability AS ra
                        LEFT JOIN rooms_type AS rot ON ra.rooms_type_id = rot.id
                        LEFT JOIN rates_type AS rat ON ra.rates_type_id = rat.id
                        WHERE ra.date BETWEEN '$check_in'
                        AND DATE_SUB('$check_out', INTERVAL 1 DAY)
                        AND ra.rate > 0
                        AND rot.isDisabled = 0
                        AND ra.rates_type_id = $rates_type_id
                        AND rooms_type_id = $rooms_type_id")
        )
            ->first();
    }

    public static function getDiscount()
    {
    }

    public static function getOffert($code)
    {
        $today = date('Y-m-d', strtotime('now'));
        $offert = Offerts::select('code', 'description', 'condition', 'value')
            ->where('start_date', '<=', $today)
            ->where('end_date', '>=',  $today)
            ->where('code', $code)
            ->first();

        return $offert;
    }

    public static function getFeesCost($total)
    {
        $fees = DB::select('SELECT SUM(fee_paid) as Fee
        FROM
            (SELECT CAST( (value * ' . $total . ') / 100 as decimal(10,2)) as fee_paid
            FROM fees
            WHERE status = \'CNF\' AND percentage = 1
            UNION
            SELECT CAST(value as decimal(10,2)) as fee_paid
            FROM fees
            WHERE status = \'CNF\' AND percentage = 0) AS f;');

        return $fees[0]->Fee;
    }

    public static function getFeesList()
    {
        $fees = DB::table('fees')->select('id', 'name', 'value', 'percentage')->where('status', '=', 'CNF')->get();
        return $fees;
    }

    public static function getCustomer($email)
    {
        $customer = DB::table('customers')->where('email', $email)->first();
        return $customer;
    }

    public static function putReservation($data)
    {
        $errors = [];
        $data = (object)$data;
        $data->check_out = date('Y-m-d', strtotime('-1 day', strtotime($data->check_out)));

        $reservation = new \App\Reservations;
        $reservation->customer_id = $data->customer->id;
        $reservation->pnr = $data->pnr;
        $reservation->note = $data->note;
        $reservation->origin = 1;
        $reservation->discount = $data->discount;
        $reservation->subtotal = $data->subtotal;
        $reservation->total = $data->grandtotal;
        $reservation->status = $data->status;
        $reservation->property_id = $data->property_id;
        $reservation->data = json_encode($data->rooms, true);
        $reservation->save();

        if ($data->rooms) {
            $rooms_type_ids = [];

            foreach ($data->rooms as $key => $room) {
                $rooms_type_ids[$key] = $room->rooms_type_id;
                $roomsfound = collect(\App\Rooms::get_rooms_available_by_type($data->property_id, $data->check_in, $data->check_out, 1, $room->guest))->first();

                if ($roomsfound) {

                    $roomsbooked = new \App\RoomsBooked();
                    $roomsbooked->room_id = $roomsfound->id;
                    $roomsbooked->property_id = $data->property_id;
                    $roomsbooked->rooms_type_id = $room->rooms_type_id;
                    $roomsbooked->reservation_id = $reservation->id;
                    $roomsbooked->check_in = $data->check_in;
                    $roomsbooked->check_out = $data->check_out;
                    $roomsbooked->guests = $room->guest;
                    $roomsbooked->price = $room->rate_amount;
                    $roomsbooked->rate_id = $room->rate_id;
                    $roomsbooked->status = 'CNF';
                    $roomsbooked->save();
                } else {
                    $errors[] = trans('booking.rooms_not_available');
                }
            }
        } else {
            $errors[] = trans('booking.rooms_field_is_empty');
        }

        if (isset($data->fees)) {
            foreach ($data->fees as $fee) {
                $reservation_fee = new \App\ReservationFees;
                $reservation_fee->reservation_id = $reservation->id;
                $reservation_fee->fee_id = $fee->id;
                $reservation_fee->fee_paid = $fee->value;
                $reservation_fee->percentage = $fee->percentage;
                $reservation_fee->save();
            }
        }

        if (isset($data->payment)) {
            $payments = new \App\ReservationPayments;
            $payments->reservation_id = $reservation->id;
            $payments->transaction_id = $data->payment['transaction_id'];
            $payments->causal = $data->payment['casual'];
            $payments->method = $data->payment['method'];
            $payments->payment_date = $data->payment['payment_date'];
            $payments->fee = $data->payment['fee'];
            $payments->total = $data->grandtotal;
            $payments->status = $data->payment['status'];
            $payments->save();
        }

        if (isset($data->credit_card)) {
            $credit_card = new \App\ReservationCreditCard;
            $credit_card->reservation_id = $reservation->id;
            $credit_card->customer_id = $data->customer->id;
            $credit_card->data = encrypt($data->credit_card);
            $credit_card->save();
        }

        if (isset($data->offert)) {
            $offert = new \App\ReservationOffert;
            $offert->reservation_id = $reservation->id;
            $offert->code = $data->offert->code;
            $offert->save();
        }

        if (empty($errors)) {

            $ma_response = '';
            /*
                Scale Availability if no errors
            */

            foreach ($data->rooms as $room) {
                Availability::scale($data->check_in, $data->check_out, $room->rooms_type_id);
            }

            if (Helper::get_setting('my_allocator')->active) {
                $allocations = \App\Availability::get_availability_status($data->check_in, $data->check_out, $rooms_type_ids);
                $ma_response = MyAllocator::GetMyAllocatorResponse(MyAllocator::ARIUpdate($allocations));
            }

            return (object)["Success" => true, 'Errors' => $errors, 'MAResponse' => $ma_response];
        } else {
            $reservation = \App\Reservations::find($reservation->id);
            $reservation->status = 'CAN';
            $reservation->save();

            RoomsBooked::where('reservation_id', '=', $reservation->id)->update(['status' => 'CAN']);

            return (object)["Success" => false, 'Errors' => $errors];
        }
    }
}
