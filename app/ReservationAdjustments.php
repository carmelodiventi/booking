<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationAdjustments extends Model
{
    //
    //protected $connection = 'property';
    protected $table = 'reservation_adjustments';

    public static function getSum($reservation_id)
    {
        ReservationAdjustments::where('reservation_id', $reservation_id)->sum('price');
    }
}
