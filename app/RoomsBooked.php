<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\RoomsBlocked;

class RoomsBooked extends Model
{
    //
    protected $table = 'rooms_booked';

    /**
    * getRoomsBooked
    * get rooms booked of all reservation
    *
    * Parameters:
    *     (keyword) - filter for reservation result list
    *     (reservation_ids) - array with id list from function Reservation::getReservations()
    */

    public static function getRoomsBooked($reservations_ids)
    {
        return RoomsBooked::select(
                'r.pnr',
                'rb.reservation_id',
                'r.created_at',
                'rm.name as room_name',
                'rt.type as room_type',
                'rb.guests',
                'rb.check_in as checkin',
                'rb.check_out as checkout',
                'c.name',
                'c.surname',
                'c.email',
                'c.phone'
        )
                ->from('rooms_booked as rb')
                ->leftJoin('reservations as r', 'r.id', '=', 'rb.reservation_id')
                ->leftJoin('customers as c', 'r.customer_id', '=', 'c.id')
                ->leftJoin('rooms as rm', 'rm.id', '=', 'rb.room_id')
                ->leftJoin('rooms_type as rt', 'rt.id', '=', 'rm.rooms_type_id')
                ->whereIn('reservation_id', $reservations_ids)->get();
    }

    /**
    * getRoomAvaibility
    * get room count avaiability by id with check-in and check-out date
    *
    * Parameters:
    *     (check_in) - check in date
    *     (check_out) - check out date
    *     (room_id) - room id to check avaibility
    *     (room_booked_id) - room booked id to exlude from select
    */

    public static function getRoomAvaibility($check_in, $check_out, $room_id, $room_booked_id)
    {
        return RoomsBooked::select(DB::raw('count(id) as count'))
        ->where('check_in', '<', $check_out)
        ->where('check_out', '>', $check_in)
        ->where('room_id', $room_id)
        ->where('id', '<>', $room_booked_id)
        ->where('status', 'CNF')
        ->first()->count;
    }

    /**
    * getSum
    * get sum of room price
    *
    * Parameters:
    *     (reservation_id) - reservation id
    */

    public static function getSum($reservation_id)
    {
        RoomsBooked::where('reservation_id', $reservation_id)->sum('price');
    }

    /**
    * getIDRoomsBookedToDate
    * get ID of rooms booked in determinate date
    *
    * Parameters:
    *     (check_in) - check in date
    *     (check_out) - check out date
    */

    public static function getIDRoomsBookedToDate($check_in, $check_out)
    {
        return RoomsBooked::where('check_in', '<', $check_out)
        ->where('check_out', '>', $check_in)
        ->get(['room_id']);
    }

    public static function loadReservations($start, $end, $room_id)
    {
        $reservations = RoomsBooked::select(
        'rooms_booked.reservation_id',
        'rooms_booked.id',
        'rooms_booked.rooms_type_id',
        'rooms_booked.check_in',
        DB::raw('DATE_ADD(rooms_booked.check_out, INTERVAL 1 DAY) as check_out'),
        DB::raw('DATEDIFF(DATE_ADD(rooms_booked.check_out, INTERVAL 1 DAY),rooms_booked.check_in) as nights'),
        'T3.name',
        'T3.surname',
        'T3.country',
        'T3.phone',
        'rooms_booked.price',
        'rooms_booked.guests',
        'T2.pnr',
        'T2.reservation_note',
        'T2.note',
        'T4.name as origin',
        'T5.transaction_code as transaction_code',
        DB::raw('(select COALESCE(sum(price), 0) as rooms_booked_sum from rooms_booked as rb
        where rb.reservation_id = rooms_booked.reservation_id) as rooms_booked_sum'),
        DB::raw('(select COALESCE(sum(price), 0) as adjustments_sum from reservation_adjustments as ra
        where ra.reservation_id = rooms_booked.reservation_id) as adjustments_sum'),
        DB::raw('(select COALESCE(sum(total), 0) as payments_sum from reservation_payments as rp
        where rp.reservation_id = rooms_booked.reservation_id) as payments_sum'),
        DB::raw('(select (rooms_booked_sum + adjustments_sum - payments_sum - T2.discount)) as topay'),
        'T2.total',
        'T2.review',
        'T2.status'
        )
          ->leftjoin('reservations AS T2', 'rooms_booked.reservation_id', '=', 'T2.id')
          ->leftjoin('customers AS T3', 'T3.id', '=', 'T2.customer_id')
          ->leftjoin('portals AS T4', 'T4.id', '=', 'T2.origin')
          ->leftjoin('portals_commission AS T5', 'T5.reservation_id', '=', 'T2.id')

          ->where('rooms_booked.room_id', '=', $room_id)
          ->where('rooms_booked.check_in', '>=', DB::raw('DATE_SUB("'.$start.'", INTERVAL 15 DAY)'))
          ->where('rooms_booked.check_out', '<=', DB::raw('DATE_ADD("'.$end.'", INTERVAL 15 DAY)'))
          ->where('rooms_booked.status', '=', 'CNF')
          ->orderBy('rooms_booked.check_in', 'asc')
          ->get();

        return response()->json($reservations);
    }

    /**
    * getRoomsBookedByReservationID
    * get rooms booked of a specifics reservation
    *
    * Parameters:
    *     (reservation_id) - Reservation ID
    */

    public static function getRoomsBookedByReservationID($reservations_id)
    {
        return RoomsBooked::join('rooms', 'rooms_booked.room_id', '=', 'rooms.id')
          ->join('rooms_type', function ($join) {
              $join->on('rooms_type.id', '=', 'rooms_booked.rooms_type_id');
          })
          ->leftJoin('rates_type', 'rates_type.id', '=', 'rooms_booked.rate_id')
          ->where('reservation_id', '=', $reservations_id)->get(['rooms_booked.id','rooms_booked.room_id','rooms.name', 'rooms_booked.check_in','rooms_booked.check_out','rooms_type.type','rates_type.name as rate_name','rooms_booked.rate_desc','rooms_booked.rate_id',
              'guests', 'price','reservation_data','rooms_booked.status']);
    }
}
