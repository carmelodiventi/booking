<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PortalsCommission extends Model
{
    protected $table = 'portals_commission';
}
