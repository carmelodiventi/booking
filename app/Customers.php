<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Scopes\PropertyScope;

class Customers extends Model
{
    //
    protected $table = 'customers';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new PropertyScope);
    }

    public function search($query, $keyword)
    {
        if ($keyword!='') {
            $query->where(function ($query) use ($keyword) {
                $query->where("name", "LIKE", "%$keyword%")
                        ->orWhere("surname", "LIKE", "%$keyword%")
                        ->orWhere("email", "LIKE", "%$keyword%")
                        ->orWhere("phone", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }
}
