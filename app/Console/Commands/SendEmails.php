<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendEmails extends Command
{
    protected $signature = 'emails:send';

    protected $description = 'Sending emails to the users.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $data = array(
            'name' => "Novabeds"
        );

        Mail::send('emails.test', $data, function ($message) {

            $message->from('info@novabeds.com');

            $message->to('lombardo.irene80@gmail.com ')->subject('test email');

        });
        
        $this->info('The emails are send successfully!');
    }
}