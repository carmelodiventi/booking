<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ConfigServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        config([
            'laravellocalization.supportedLocales' => [
              'it' => array( 'name' => 'Italiano', 'script' => 'Latn', 'native' => 'italiano' ),
              'fr'  => array( 'name' => 'Français', 'script' => 'Latn', 'native' => 'français' ),
              'en'  => array( 'name' => 'English', 'script' => 'Latn', 'native' => 'english' ),
            ],
            'laravellocalization.useAcceptLanguageHeader' => true,
            'laravellocalization.hideDefaultLocaleInURL' => true
          ]);
    }
}
