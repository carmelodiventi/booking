<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Cache\Factory;
use App\Settings;
use Schema;

class SettingsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Factory $cache, Settings $settings)
    {
        if (Schema::hasTable('settings')) {
            foreach (Settings::all() as $setting) {
                config()->set('settings.'.$setting->property_id . '.' . $setting->key, $setting->value);
            }
        }

        /*
        $settings = $cache->remember('settings', 60, function () use ($settings) {
            return $settings->pluck('value', 'key')->all();
        });
        config()->set('settings', $settings);

        */
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
