<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;
use LaravelLocalization;
use Carbon\Carbon;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        if (LaravelLocalization::getCurrentLocale() == 'it') {
            setlocale(LC_TIME, 'it_IT.UTF-8');
        } elseif (LaravelLocalization::getCurrentLocale() == 'fr') {
            setlocale(LC_TIME, 'fr_FR.UTF-8');
        } else {
            setlocale(LC_TIME, 'en_GB.UTF-8');
        }
        Carbon::setLocale(config('app.locale'));
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
