<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Auth;
use Request;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->viewForNotification();
        $this->viewForPropertyID();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }


    private function viewForNotification()
    {
        view()->composer('admin.includes.notifications', function ($view) {
            $user = Auth::user();
            $view->with([
              'notifications_count' => $user->unreadNotifications->count(),
              'notifications' => $user->unreadNotifications
            ]);
        });
    }

    private function viewForPropertyID()
    {
        //dd($property);
        view()->composer('main', function ($view) {
            $view->with([
            
          ]);
        });
    }
}
