var app = {
    init: function () {
        this.createDrop();
        this.getCart();
        this.sticky();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    },
    createDrop: function () {
        var droptarget = $('.drop-target');
        var droptcontent = $('.drop-content-inner');
        droptarget.each(function (index) {
            new Drop({
                target: droptarget[index],
                content: droptcontent[index],
                classes: 'drop-theme-hubspot-popovers',
                position: 'top right',
                openOn: 'click',
                remove: true
            });
        });
    },
    showRate: function ($this) {

        overlay = document.getElementsByClassName('overlay-modal-room')[0];
        $($this).closest("li.list").toggleClass('open');
        overlay.style.display = 'block';
        $('html, body').animate({
            scrollTop: $($this).closest("li.list").offset().top - 150
        }, 'slow');
        window.onclick = function (event) {
            if (event.target == overlay) {
                overlay.style.display = 'none';
                $($this).closest("li.list").toggleClass('open');
            }
        }
    },
    hideRate: function () {
        overlay = document.getElementsByClassName('overlay-modal-room')[0];
        overlay.style.display = 'none';
        $("li.list").removeClass('open');
    },
    showCondition: function ($this) {
        $($this).parent().next().toggleClass('visible');
    },
    showModal: function ($target) {
        var modals = document.getElementsByClassName('modal');
        var modal = document.getElementById($target);
        var span = document.getElementById($target).getElementsByClassName("modal-close")[0];

        for (var i = 0; i < modals.length; i++) {
            modals[i].style.display = "none";
        }

        modal.style.display = "block";
        span.onclick = function () {
            modal.style.display = "none";
        }
        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    },
    setLocale: function ($lang) {
        if ($lang.value) {
            window.location.href = base_url + '/' + $lang.value;
        }
    },
    reserve: function (item) {

        var id = $(item).data("id");
        var rateid = $(item).data("rate-id");
        var ratename = $(item).data("rate-name");
        var type = $(item).data("room-type");
        var type_id = $(item).data("room-type-id");
        var guests = $(item).data("guests");
        var price = $(item).data("room-price");
        var maxPreno = $(item).data("room-max-qty");
        var counterQty = 0;

        data = {
            room_id: id,
            rate_id: rateid,
            rate_name: ratename,
            room_type: type,
            room_price: price,
            room_type_id: type_id,
            guests: guests
        };

        $.ajax({
            url: base_url + '/cart/add',
            type: 'GET',
            data: data
        })
            .done(function (result) {
                if (result.status == 'success') {
                    app.updateStatus(item, result.item.id, result.item.__raw_id);
                    app.getCart();
                } else if (result.status == 'item_exits') {
                    $.noty.defaults.killer = true;
                    noty({
                        text: novabeds.warning_dispo,
                        layout: 'top',
                        timeout: 3000,
                        closeWith: ['click', 'hover'],
                        type: 'error'
                    });
                    return false;
                }

            });

    },
    getCart: function () {
        var totalroom = 0,
            totalprice = 0.00,
            currency = '&#8364;';
        $.ajax({
            url: base_url + '/cart/get',
            type: 'GET'
        })
            .done(function (results) {
                $('#cart ul#rooms-list').empty();
                item_count = $.map(results, function (n, i) {
                    return i;
                }).length;
                if (item_count > 0) {
                    $.each(results, function (i, el) {
                        $('#cart ul#rooms-list').append('<li item-name="' + el.name + '"><div class="details"><i class="ti-close color-red" onclick="app.removeCart(\'' + el.__raw_id + '\')"></i> <strong class="text-uc">' + el.name + '</strong> <br> <i>' + el.rate_name + '</i> <small class="price"><sup>€</sup>' + el.price + '</small></div><div class="clearfix"></div></li>');
                        totalroom++;
                        totalprice = app.getTotalPrice(totalprice, el.price);
                    });
                    $('.bt-cart-container').removeClass('empty');
                    $('[data-cart]').html($.map(results, function (n, i) {
                        return i;
                    }).length);
                    $('[data-total]').html('<sup>' + currency + '</sup>' + totalprice);
                } else {
                    $('.bt-cart-container').addClass('empty');
                    $('.overlay-cart').removeClass('visible');
                    $('[data-cart]').empty();
                    $('[data-total]').empty();
                }
            });
    },
    removeCart: function (rowId) {
        $.ajax({
            url: base_url + '/cart/remove',
            type: 'GET',
            data: {
                rowId: rowId
            }
        })
            .done(function (results) {
                item_id = $('div[data-row-id="' + rowId + '"]').data('id');
                $('button[data-row-id="' + rowId + '"]')
                    .text(novabeds.book)
                    .attr('data-row-id', '')
                    .prop("disabled", false);
                $('div[data-room-id="' + item_id + '"]')
                    .removeClass('is-added')
                    .data('row-id', '');
                app.getCart();
            });
    },
    updateStatus: function (item, id, row_id) {
        // put row_id in button
        $('button[data-id="' + id + '"]').attr("data-row-id", row_id);
        btn = $('button[data-row-id="' + row_id + '"]');
        // hide all rooms with same id
        btn.fadeOut();
        // put row_id in div
        btn.closest('div.room-line').attr('data-row-id', row_id);
        // put text booked and disable button
        btn.text(novabeds.booked).prop("disabled", true);
        // put css Class at button container
        $('div[data-room-id="' + id + '"]').addClass('is-added');
        // show btn
        $(item).fadeIn();
    },
    getTotalPrice: function (total, item_price) {
        total = parseFloat(total);
        item_price = parseFloat(item_price);
        total = (total + item_price);
        return total.toFixed(2);
    },
    showCart: function () {
        $(".overlay-cart").addClass('visible');
        $(".bt-cart-container").addClass('empty');
    },
    closeCart: function () {
        $(".overlay-cart").removeClass('visible');
        $(".bt-cart-container").removeClass('empty');
    },
    showGallery: function (src) {
        var pswpElement = document.querySelectorAll('.pswp')[0];

        var photos_room = src.split(',');
        var items = [];

        $(photos_room).each(function (index, item) {
            var element = {};
            element.src = item;
            element.w = 768;
            element.h = 768;
            items[index] = element;
        });

        // define options (if needed)
        var options = {
            history: false,
            focus: false,
            shareEl: false,
            showAnimationDuration: 0,
            hideAnimationDuration: 0
        };

        var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    },
    showtab: function ($this) {
        $target = $this.getAttribute('href');
        $('div.tab-pane > ul > li').removeClass('current');
        $('div.tab > .tab-content > div').hide().removeClass('visible');
        $('div.tab > .tab-content > div' + $target + '').fadeIn();
        $($this.parentNode).addClass('current');
    },
    sticky: function () {
        var sticky = document.querySelector('.sticky');
        if (sticky && sticky.length) {
            var origOffsetY = sticky.offsetTop;

            function onScroll(e) {
                window.scrollY >= origOffsetY ? sticky.classList.add('fixed') : sticky.classList.remove('fixed');
            }
            document.addEventListener('scroll', onScroll);
        }
    },
    showElement: function ($element) {
        $($element).slideDown();
    },
    hideElement: function ($element) {
        $($element).slideUp();
    },
    loginCustomer: function () {

        var email = $('#login-form input[name="email"]').val();
        var password = $('#login-form input[name="password"]').val();

        if (email == '' || email == undefined &&
            password == '' || password == undefined) {
            $.noty.defaults.killer = true;
            noty({
                text: novabeds.field_login_empty,
                layout: 'top',
                timeout: 3000,
                closeWith: ['click', 'hover'],
                type: 'error'
            });
            return false;

        } else {
            $.ajax({
                type: "post",
                url: 'customer/login',
                dataType: 'json',
                data: {
                    email: email,
                    password: password
                },
                success: function (response) {
                    if (response.status == 'success') {
                        swal({
                            title: response.title,
                            text: response.msg,
                            icon: response.status,
                            buttons: {
                                confirm: {
                                    text: 'OK',
                                    value: true,
                                    className: "btn-primary",
                                }
                            }
                        }).then((confirm) => {
                            location.reload();
                        });
                        return false;
                    } else {
                        var errors = response.msg;
                        var errorsHtml = '';
                        $.each(errors, function (key, value) {
                            errorsHtml += '<p>' + value[0] + '</p>';
                        });
                        $.noty.defaults.killer = true;
                        noty({
                            text: errorsHtml,
                            layout: 'top',
                            timeout: 1500,
                            closeWith: ['click', 'hover'],
                            type: 'error'
                        });
                        return false;
                    }
                }
            });

        }
    },
    registerCustomer: function () {
        var name = $('#register input[name="name"]').val();
        var surname = $('#register input[name="surname"]').val();
        var phone = $('#register input[name="phone"]').val();
        //var address = $('#register input[name="address"]').val();
        //var city = $('#register input[name="city"]').val();
        var country = $('#register select[name="country"]').val();
        //var province = $('#register input[name="province"]').val();
        var email = $('#register input[name="email"]').val();
        var password = $('#register input[name="password"]').val();

        if (email == '' || email == undefined &&
            password == '' || password == undefined &&
            name == '' || name == undefined &&
            surname == '' || surname == undefined &&
            phone == '' || phone == undefined &&
            country == '' || country == undefined) {

            $.noty.defaults.killer = true;
            noty({
                text: novabeds.field_empty,
                layout: 'top',
                timeout: 3000,
                closeWith: ['click', 'hover'],
                type: 'error'
            });

            return false;

        } else {

            var data = {
                email: email,
                password: password,
                name: name,
                surname: surname,
                phone: phone,
                country: country,
            };

            $.ajax({
                type: "POST",
                url: 'customer/register',
                data: data,
                success: function (response) {
                    if (response.status == 'success') {
                        swal({
                            title: response.title,
                            text: response.msg,
                            icon: response.status,
                            buttons: {
                                confirm: {
                                    text: 'OK',
                                    value: true,
                                    className: "btn-primary",
                                }
                            }
                        }).then((confirm) => {
                            location.reload();
                        });
                    } else {
                        var errors = response.msg;
                        var errorsHtml = '';
                        $.each(errors, function (key, value) {
                            errorsHtml += '<p>' + value[0] + '</p>';
                        });
                        $.noty.defaults.killer = true;
                        noty({
                            text: errorsHtml,
                            layout: 'top',
                            timeout: 3000,
                            closeWith: ['click', 'hover'],
                            type: 'error'
                        });
                    }
                },
                error: function (response) {
                    var errors = response.msg;
                    var errorsHtml = '';
                    $.each(errors, function (key, value) {
                        errorsHtml += '<p>' + value[0] + '</p>';
                    });
                    $.noty.defaults.killer = true;
                    noty({
                        text: errorsHtml,
                        layout: 'top',
                        timeout: 1500,
                        closeWith: ['click', 'hover'],
                        type: 'error'
                    });
                }
            });

        }
    },
    resetPasswordCustomer: function () {
        var email = $('#reset_password input[name="email"]').val();

        if (email == '' || email == undefined) {
            $.noty.defaults.killer = true;
            noty({
                text: novabeds.field_empty,
                layout: 'top',
                timeout: 1500,
                closeWith: ['click', 'hover'],
                type: 'error'
            });
            return false;
        } else {
            var data = {
                email: email
            };

            $.ajax({
                type: "POST",
                url: 'customer/password/renew',
                data: data,
                success: function (response) {
                    if (response.status == 'success') {
                        swal({
                            title: response.title,
                            text: response.msg,
                            icon: response.status,
                            buttons: {
                                confirm: {
                                    text: 'OK',
                                    value: true,
                                    className: "btn-primary",
                                }
                            }
                        }).then((confirm) => {
                            location.reload();
                        });
                    } else {
                        var errors = response.msg;
                        var errorsHtml = '';
                        $.each(errors, function (key, value) {
                            errorsHtml += '<p>' + value[0] + '</p>';
                        });
                        $.noty.defaults.killer = true;
                        noty({
                            text: errorsHtml,
                            layout: 'top',
                            timeout: 1500,
                            closeWith: ['click', 'hover'],
                            type: 'error'
                        });
                    }
                },
                error: function (response) {
                    var errors = response.msg;
                    var errorsHtml = '';
                    $.each(errors, function (key, value) {
                        errorsHtml += '<p>' + value[0] + '</p>';
                    });
                    $.noty.defaults.killer = true;
                    noty({
                        text: errorsHtml,
                        layout: 'top',
                        timeout: 1500,
                        closeWith: ['click', 'hover'],
                        type: 'error'
                    });
                }
            });
        }
    },
    validateOffertCode: function () {
        $code = $('input[name="offert_code"]');
        if ($code.val() != '' && $code.val() != undefined) {
            $.ajax({
                type: 'POST',
                url: property.id + '/validate-code',
                data: {
                    code: $code.val()
                },
                success: function (response) {
                    if (!$.isEmptyObject(response)) {
                        swal({
                            title: novabeds.code_correct, text: novabeds.code_correct_info + '<br>' + response.description
                        });
                        $code.addClass('success');
                    } else {
                        $.noty.defaults.killer = true;
                        noty({
                            text: novabeds.code_wrong_info,
                            layout: 'top',
                            timeout: 3000,
                            closeWith: ['click', 'hover'],
                            type: 'error'
                        });

                        $code.val('');
                        $code.addClass('danger');
                    }

                },
                error: function (response) {
                    console.log('Error:', response);
                }
            });
        }
    },
    switchPayment: function (payment) {

        if (payment.value == 1) {
            $('#credit_card').fadeIn();

            // Create an instance of Elements
            var elements = stripe.elements();

            // Custom styling can be passed to options when creating an Element.
            // (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
                base: {
                    color: '#32325d',
                    lineHeight: '18px',
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSmoothing: 'antialiased',
                    fontSize: '16px',
                    '::placeholder': {
                        color: '#aab7c4'
                    }
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };

            // Create an instance of the card Element
            card = elements.create('card', {
                style: style
            });

            // Add an instance of the card Element into the `card-element` <div>
            card.mount('#card-element');

            // Handle real-time validation errors from the card Element.
            card.addEventListener('change', function (event) {
                var displayError = document.getElementById('card-errors');
                if (event.error) {
                    displayError.textContent = event.error.message;
                } else {
                    displayError.textContent = '';
                }
            });

        } else {
            $('#credit_card').fadeOut();
        }

        if (payment.value == 2) {
            $('#paypal').fadeIn();
        } else {
            $('#paypal').fadeOut();
        }
    },
    validatePayment: function () {
        payment = $('select[name="payment"]');
        if (payment.val() != '' && payment.val() != 1) {
            document.payment.submit();
            $('.waiting-payment').css('visibility', 'visible');
            //document.getElementsByClassName('waiting-payment')[0].style.visibilty = 'visible';
        } else if (payment.val() == 1) {
            var token;
            stripe.createToken(card).then(function (result) {
                if (result.error) {
                    // Inform the user if there was an error
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    // Send the token to your server
                    app.stripeTokenHandler(result.token);
                }
            });
        }

    },
    stripeTokenHandler: function (token) {
        $('input[name="stripeToken"]').val(token.id);
        $('#confirm_and_pay').html($('#confirm_and_pay').data('loading')).prop('disabled', true);
        $('.waiting-payment').css('visibility', 'visible');
        //document.getElementsByClassName('waiting-payment')[0].style.visibilty = 'visible';
        document.payment.submit();
    },
    validateForm: function () {

        checkinDate = new Date($('input[name="check_in"]').val());
        checkoutDate = new Date($('input[name="check_out"]').val());

        if ($('input[name="check_in"]').val() == "" && $('input[name="check_out"]').val() == "") {
            $.noty.defaults.killer = true;
            noty({
                text: novabeds.field_empty_date,
                layout: 'top',
                timeout: 1500,
                closeWith: ['click', 'hover'],
                type: 'error'
            });

        } else if (checkoutDate < checkinDate) {
            $.noty.defaults.killer = true;
            noty({
                text: novabeds.checkoutDate,
                layout: 'top',
                timeout: 3000,
                closeWith: ['click', 'hover'],
                type: 'error'
            });
        } else {
            document.dashboard.submit();
        }

    }
};

app.init();