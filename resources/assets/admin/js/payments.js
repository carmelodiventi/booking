var $total_to_pay = parseFloat($('input[name="topay"]').val());

var payments = {
  init: function() {
    this.calculate_topay();
  },
  calculate_topay: function() {
    var typingTimer;                //timer identifier
    var doneTypingInterval = 500;  //time in ms (5 seconds)

    $('#amount').keyup(function(){
      clearTimeout(typingTimer);
      if ($('#amount').val()) {
          typingTimer = setTimeout(doneTyping, doneTypingInterval);
      }
    });
    function doneTyping () {
      $amount = parseFloat($('#amount').val());
      $difference = ($total_to_pay - $amount).toFixed(2);
      if ($difference >= 0) {
        $('input[name="topay"]').val($difference);
      } else {
        $('input[name="topay"]').val($total_to_pay);
        $('#amount').val('0.00');
        $.noty.closeAll();
        noty({
            text: novabeds.amount_exceeds,
            layout: 'top',
            type: 'error',
            timeout: 3000,
        })
        return false;
      }
    }
  }
}

payments.init();
