var changes = {};
var strDebug = '';
var units = 0;

var manage_rates = {
  init: function() {
    this.enableLongTermData();
    this.ajaxSetup();
    this.blur();
    $date_format = 'YYYY-MM-DD';

    $('form input[name="start_date"]').daterangepicker({
      autoUpdateInput: false,
      locale: {
        format: $date_format
      },
      allowInputToggle: true,
      minDate: new Date,
    });
    $('form input[name="end_date"]').daterangepicker({
      autoUpdateInput: false,
      locale: {
        format: $date_format
      },
      allowInputToggle: true,
      minDate: new Date,
    });
    $('form input[name="start_date"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format($date_format));
      $('input[name="end_date"]').val(picker.endDate.format($date_format));
    });
    $('form input[name="end_date"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.endDate.format($date_format));
      $('input[name="start_date"]').val(picker.startDate.format($date_format));
    });
  },
  ajaxSetup: function() {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
  },
  getAvailability: function(startDate,endDate){
    if(startDate && endDate){
      $.ajax({
        url: base_url + '/getAvailabilityFromDate',
        type: 'GET',
        data: {
          startDate: startDate,
          endDate: endDate
        }
      })
      .done(function($response) {
        if($response.Success == true){
            $().loadingTable('show');
          $.each($response.Availability, function(index,avail){
            var units_field = $( "input[type='number'][data-date='"+avail.date+"'][data-rid='"+avail.rates_type_id+"'][data-rtid='"+avail.rooms_type_id+"'][data-type='units']" );
            var rate_field = $( "input[type='number'][data-date='"+avail.date+"'][data-rid='"+avail.rates_type_id+"'][data-rtid='"+avail.rooms_type_id+"'][data-type='rate']" );
            var min_stay_field = $( "input[type='number'][data-date='"+avail.date+"'][data-rid='"+avail.rates_type_id+"'][data-rtid='"+avail.rooms_type_id+"'][data-type='min_stay']" ).val(avail.min_stay);
            var closed_field = $( "input[type='number'][data-date='"+avail.date+"'][data-rid='"+avail.rates_type_id+"'][data-rtid='"+avail.rooms_type_id+"'][data-type='closed']");
            units_field.val(avail.units);
            rate_field.val(avail.rate);
            min_stay_field.val(avail.min_stay);
            closed_field.val(avail.closed);
            if(closed_field.val() == 1){
              closed_field.closest('.input-group').addClass('has-error');
            }
          });

        }
        console.log($response);
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        $().loadingTable('hide');
        console.log("complete");
      });
    }
    else{
      $.noty({
        layout: 'top',
        type: 'error',
        text: novabeds.availability_error_in_loading
      });
    }

  },
  blur: function() {
    $('table#table-rates input.input-rates').blur(function(e) {

      var target = $(e.currentTarget);

      if (e.target.tagName != 'INPUT')
        return;
      if (e.target.disabled)
        return;

      var rid = target.attr('data-rid');
      var rtid = target.attr('data-rtid');
      var mrtid = target.attr('data-mrtid');
      var type = target.attr('data-type');
      var idx = parseInt(target.attr('data-idx'));
      var date = target.attr('data-date');;
      var val = target.val();

      if (val === 'âˆž')
        val = 0;
      if (val === '')
        val = 0;

      if (val != 0) {
        target.closest('.input-group').removeClass('has-error').addClass('has-success');
      } else if (val == 0) {
        target.closest('.input-group').removeClass('has-success').addClass('has-error');
      }

      if (type == 'max_stay') {
        var max = parseInt(target.attr('max'));

        if (parseInt(val) > max) {
          // Hide other ones
          if (lastTooltipElement)
            //lastTooltipElement.tooltip('destroy');
            target.tooltip('destroy');
            target.attr('title', 'You are trying to set more availability than the room is configured for. It will be reduced to ' + max.toString() + ' when saved.')
            target.tooltip({
            placement: 'bottom',
            trigger: 'hover'
          });
          target.tooltip('show');
          //lastTooltipElement = target;
        }
      }

      if (type == "closed" || type == 'min_stay' || type == 'max_stay') {
        val = parseInt(val);
      }

      manage_rates.addChange(rid,rtid,mrtid, date, date, type, val);
    });

  },
  addChange: function(rid,rtid,mrtid, start, end, type, val, wd) {



    if (!changes[rtid])
      changes[rtid] = {};
    if (!changes[rtid][type])
      changes[rtid][type] = [];

    if (!wd)
      wd = null;

    // Allow commas, but convert them to dots
    if (typeof(val) == 'string')
      val = val.replace(/,/g, '.');

    changes[rtid][type].push({
      s: start,
      e: end,
      v: val,
      wd: wd,
      r: rid,
      mrtid: mrtid
    });

    manage_rates.addDebug('Added change: ' + rtid + ',' + mrtid + ',' + rid + ',' + start + ',' + end + ',' + type + ',' + val + ',' + wd);
    manage_rates.enableSave(true);

    // Save changes to localStorage
    if (typeof(localStorage) !== "undefined")
      localStorage.setItem('changes', JSON.stringify(changes));

  },
  enableSave: function(check) {
    if (check) {
      $('#btn-save').prop('disabled', '');
    } else {
      $('#btn-save').prop('disabled', 'disabled');
    }
  },
  addDebug: function(txt) {
    console.log(strDebug += txt + '    -----    ');
  },
  editPrices: function(id) {
    $('[data-room-type-id="' + id + '"]').toggleClass('edit');
  },
  addPrices: function(target) {
    $(target).prev().show();
    $(target).hide();
  },
  changesToJSON: function() {
    var json = {};

    for (var rtid in changes) {
      json[rtid] = {};

      for (var type in changes[rtid]) {
        json[rtid][type] = [];

        changes[rtid][type].forEach(function(c) {
          var cCopy = {};

          for (var cKey in c) {
            if (typeof(c[cKey]) == 'object' && c[cKey])
              cCopy[cKey] = c[cKey];
            else
              cCopy[cKey] = c[cKey];
          }

          json[rtid][type].push(cCopy);
        });
      }
    }

    return JSON.stringify(json);
  },
  save: function() {

    var sendChanges = JSON.stringify(changes);

    if (sendChanges) {
      
      $('#btn-save').button('loading');

      $.ajax({
        type: "POST",
        url: base_url + '/storeAvailability',
        data: {
          changes: sendChanges,
        },
      }).done(function(result) {
        $('#btn-save').button('reset');
        if (result.status == "success") {
          $.noty.closeAll();
          noty({
            text: result.msg,
            layout: 'top',
            type: 'success',
            timeout: 3000,
          });
        } else {
          $.noty.closeAll();
          noty({
            text: result.msg,
            layout: 'top',
            type: 'error',
            timeout: 3000,
          });
        }

      });

    } else {
      return false;
    }
  },
  enableLongTermData: function() {
    var units = $('form#set_long_term_data select[name="rooms_type"] option:selected').attr('data-units');
    $('form#set_long_term_data input[name="value"]').attr('type', 'number').attr('max', units).val(0);

    $('form#set_long_term_data select[name="rooms_type"]').change(function(e) {
      var units = $('form#set_long_term_data select[name="rooms_type"] option:selected').attr('data-units');
      $('form#set_long_term_data input[name="value"]').attr('type', 'number').attr('max', units).val(0);
      $('form#set_long_term_data select[name="type"]').prop("selectedIndex", 0);
    });

    $('form#set_long_term_data select[name="type"]').change(function(e) {
      if (e.currentTarget.value == 'units') {
        units = $('form#set_long_term_data select[name="rooms_type"] option:selected').attr('data-units');
        $('form#set_long_term_data input[name="value"]').attr('type', 'number').attr('max', units).val(0);
      } else if (e.currentTarget.value == 'rate' || e.currentTarget.value == 'single_rate') {
        $('form#set_long_term_data input[name="value"]').attr('type', 'number').attr('max', '').val(0.00);
      } else if (e.currentTarget.value == 'min-stay' || e.currentTarget.value == 'min-stay') {
        $('form#set_long_term_data input[name="value"]').attr('type', 'number').attr('max', '').val(0);
      } else if (e.currentTarget.value == 'closed') {
        $('form#set_long_term_data input[name="value"]').attr('type', 'number').attr('max', '1').val(0);
      }
    });

    $('form#set_long_term_data select[name="rate_type"]').change(function(e) {
        $('form#set_long_term_data select[name="type"]').prop("selectedIndex", 0);
        if($(this).find(':selected').attr('data-master-rate') == 1){
          $('form#set_long_term_data select[name="type"] option').prop('disabled',false);
        }
        else{
            $('form#set_long_term_data select[name="type"] option').not('[data-type="rate"],[data-type="min_stay"],[data-type="max_stay"]').prop('disabled',true);
        }
    });

  },
  setLongTermData: function() {

    var rid = $('form#set_long_term_data select[name="rate_type"]').val();
    var rtid = $('form#set_long_term_data select[name="rooms_type"]').val();
    var mrtid = $('form#set_long_term_data select[name="rooms_type"] option:selected').data('mrtid');
    var start = $('form#set_long_term_data input[name="start_date"]').val();
    var end = $('form#set_long_term_data input[name="end_date"]').val();
    var wdays = $('form#set_long_term_data input[name="weekday"]');
    var type = $('form#set_long_term_data select[name="type"]').val();
    var val = $('form#set_long_term_data input[name="value"]').val();
    var start_date = start;
    var weekdays = [];
    var dates = [];
    var dates_w = [];

    if (val == 0 || val == undefined)
      return;

    wdays.each(function(i, wd) {
      var e = $(wd);
      if (e.prop('checked'))
        weekdays.push(parseInt(e.attr('data-weekday')));
    });


    while (start_date <= end) {
      dates.push(start_date);
      start_date = moment(start_date).add(1, 'days').format("YYYY-MM-DD");
    }

    $.each(dates, function(index, date) {
      $.each(weekdays, function(i, item) {
        if (moment(date).day() == item) {
          dates_w.push(date);
        }
      })
    });

    $(dates_w).each(function(index, date) {
      var target = $('input[data-rtid="' + rtid + '"][data-mrtid="' + mrtid + '"][data-rid="' + rid + '"][data-type="' + type + '"][data-type="' + type + '"][data-date="' + date + '"]');
      target.val(val).addClass('modified').removeClass('na');
    });

    manage_rates.addChange(rid,rtid,mrtid,start, end, type, val, weekdays);

    noty({
      layout:'top',
      type:'success',
      text:novabeds.set_long_term_data_done
    });

  },
  synchronizeAvailability: function() {

    swal({
      title: novabeds.are_you_sure,
      text: novabeds.price_and_availability_will_be_overwritten,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: novabeds.confirm
    }).then((result) => {
      if (result.value) {

        $('#synchronize_availability').modal('hide');
        var start = $('form#synchronize_availability input[name="start_date"]').val();
        var end = $('form#synchronize_availability input[name="end_date"]').val();
        var includePrice = false;
        if($('form#synchronize_availability input[name="price"]').is(':checked')){
            var includePrice = true;
        }

        $('button[data-target="#synchronize_availability"]').button('loading');

        if(start!== "" && end!== ""){
          $.ajax({
            type: "GET",
            url: base_url + '/myallocator/roomAvailabilityList',
            data: {
              startDate: start,
              endDate: end,
              includePrice: includePrice
            },
          })
          .done(function(result) {
            if(result.Success === true){
              $('button[data-target="#synchronize_availability"]').button('reset');
              swal(
                novabeds.synchronized,
                novabeds.availability_synchronized_successfully,
                'success'
              );
            }
          })
          .fail(function() {
            console.log("error");
          });
        }
        else{
          $.noty.closeAll();
          noty({
            text: novabeds.period_not_found,
            layout: 'top',
            type: 'error',
            timeout: 3000,
          });
        }

      }
    });

  }

};

manage_rates.init();
