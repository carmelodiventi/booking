var rooms_selected = [];
var reservation = {
    init: function() {
        this.validate();
        this.generateCalendar();
        this.manageRooms();
        this.getTotal();
        this.updateTotal();
        this.getNightsToStay();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('input[name="_token"]').val()
            }
        });
    },
    manageRooms: function() {
        var regex = /^(.+?)(\d+)$/i;
        var cloneIndex = $(".clonedInput").length;
        function clone() {
            var max_room = $('[data-room="0"] select[name="rooms[]"] option').length -1;
            if (cloneIndex < max_room) {
                var room_line = $('.clonedInput:first').clone();
                room_line.appendTo($("#rooms .col-md-9:last"))
                .attr('data-room', cloneIndex);
                reservation.resetFields($('[data-room="'+cloneIndex+'"]'));
                reservation.hideRooms();
                cloneIndex++;
            }
            else{
              $.noty.closeAll();
              noty({
                  text: novabeds.rooms_availablility_error,
                  layout: 'top',
                  type: 'error',
                  timeout: 3000,
              });
            }
        }
        function remove() {
            if (cloneIndex > 1) {
                $(".clonedInput:last").remove();
                reservation.getTotal();
                cloneIndex--;
                reservation.updateRooms();
            }
        }
        $("button.clone").on("click", clone);
        $("button.remove").on("click", remove);
    },
    updateRooms: function(item){
      rooms_selected = [];
      $('select[name="rooms[]"] option:selected').each(function(index, el) {
          rooms_selected.push($(el).val());
      });
    },
    hideRooms:function(){
      $('select[name="rooms[]"]:last option').each(function(index, item) {
          $(rooms_selected).each(function(index, el) {
              if($(item).val() == el){
                $('select[name="rooms[]"]:last option[value='+el+']').attr('disabled', 'disabled');
              }
            });
      })
    },
    generateCalendar: function(check_in = 'undefined', check_out = 'undefined') {

       $date_format = 'YYYY-MM-DD';

       if(check_in == 'undefined' && check_out == 'undefined'){
         today = new Date();
         check_in = today.setDate(today.getDate());
         check_out = today.setDate(today.getDate() + 1);
       }

        $('input[name="check_in"]').daterangepicker({
            autoUpdateInput: false,
            locale: {
              format: $date_format
            },
            allowInputToggle: true,
            startDate: check_in,
            endDate: check_out
        });

        $('input[name="check_out"]').daterangepicker({
            autoUpdateInput: false,
            locale: {
              format: $date_format
            },
            allowInputToggle: true,
            startDate: check_in,
            endDate: check_out
        });

        $('input[name="check_in"]').on('apply.daterangepicker', function(ev, picker) {
          $(this).val(picker.startDate.format($date_format));
          $('input[name="check_out"]').val(picker.endDate.format($date_format));
          reservation.getNightsToStay();
          reservation.getAvailability();
        });

        $('input[name="check_out"]').on('apply.daterangepicker', function(ev, picker) {
          $(this).val(picker.endDate.format($date_format));
          $('input[name="check_in"]').val(picker.startDate.format($date_format));
          reservation.getNightsToStay();
          reservation.getAvailability();
        });
    },
    getRate: function(rates) {
      var check_in = $('input[name="check_in"]').val();
      var check_out = $('input[name="check_out"]').val();
      var $container = $(rates).closest('.clonedInput');
      var rooms_type_id = $(rates).val();
      var room_id = $container.find('select[name="rooms[]"]').val();
      var room_index = $container.data('room');

      if (check_in == '' && check_out == '') {
          $.noty.closeAll();
          noty({
            text: novabeds.period_not_found,
            layout: 'top',
            closeWith: ['click', 'hover'],
            type: 'error',
            timeout: 3000,
          });
          reservation.resetSelect('div[data-room="' + room_index + '"] select[name="rates[]"]', novabeds.select_rate);
          $container.find('input[name="price[]"]').val('0.00');
          $container.find('input[name="guests[]"]').val(1);
          return false
      } else if (rooms_type_id == '') {
        $.noty.closeAll();
        noty({
          text: novabeds.fare_not_found,
          layout: 'top',
          closeWith: ['click', 'hover'],
          type: 'error',
          timeout: 3000,
        });
        reservation.resetSelect('div[data-room="' + room_index + '"] select[name="rates[]"]', novabeds.select_rate);
        $container.find('input[name="price[]"]').val('0.00');
        $container.find('input[name="guests[]"]').val(1);
        return false
      }

      $.ajax({
          type: "POST",
          url: base_url + '/api/v1/getRate',
          data: {
              check_in: check_in,
              check_out: check_out,
              id: rooms_type_id
          },
      }).done(function(results) {
        if (typeof results.data !== 'undefined' && results.data.length > 0) {
          reservation.resetSelect('div[data-room="'+room_index+'"] select[name="rates[]"]',novabeds.select_rate);
          $(results.data).each(function(index, el) {
            $('div[data-room="'+room_index+'"] select[name="rates[]"]').append($('<option>', {
                value: el.rates_type_id,
                text : JSON.parse(el.name)[locale] + ' ' + '('+ el.rate_amount +')',
                data: {
                  capacity: el.capacity,
                  amount: el.rate_amount,
                  index: room_index
                }
            }));
          });

        }
        else{
          reservation.resetSelect('div[data-room="'+room_index+'"] select[name="rates[]"]',novabeds.select_rate);
          $container.find('input[name="price[]"]').val('0.00');
          $container.find('input[name="guests[]"]').val(1);
          $.noty.closeAll();
          noty({
              text: novabeds.fare_not_found,
              layout: 'top',
              type: 'error',
              timeout: 3000,
          })
        }
      });

      reservation.getTotal();
    },
    selectRate: function(item){
      var room_index = $(item).find(':selected').data('index');
      var amount = $(item).find(':selected').data('amount');
      var capacity = $(item).find(':selected').data('capacity');
      $('[data-room="' + room_index + '"] input[name="price[]"]').val(parseFloat(amount).toFixed(2));
      $('[data-room="' + room_index + '"] input[name="guests[]"]').val(parseInt(capacity));
      $.noty.closeAll();
      noty({
          text: novabeds.price_updated,
          layout: 'top',
          type: 'success',
          timeout: 3000,
      });
      reservation.getTotal();
    },
    getAvailability: function() {
      var check_in = $('input[name="check_in"]').val();
      var check_out = $('input[name="check_out"]').val();
      var room_id = $('form#lodging').data('room-id');
      var rooms = [];
        if (check_in != '' && check_out != '') {
            reservation.resetEdit();
            $.ajax({
                type: "POST",
                url: base_url + '/api/v1/getAccomodationsAvailability',
                data: {
                    check_in: check_in,
                    check_out: check_out,
                    room_id: room_id
                },
            }).done(function(response) {
              if(response.status == 'success'){
                if (response.data.length == 0) {
                    $.noty.closeAll();
                    noty({
                        text: novabeds.rooms_availablility_error,
                        layout: 'top',
                        closeWith: ['click', 'hover'],
                        type: 'error',
                        timeout: 3000,
                    });

                }
                else{
                    rooms = response.data;
                    $(rooms).each(function(index, item) {
                      $('select[name="rooms[]"]').append($('<option>', {
                          value: item.id,
                          text : item.name
                      }));
                    });
                    $.noty.closeAll();
                    noty({
                        text: novabeds.availability_list_updated,
                        layout: 'top',
                        type: 'success',
                        timeout: 3000,
                    });
                }
              }
              else{
                $.noty.closeAll();
                noty({
                    text: novabeds.error,
                    layout: 'top',
                    type: 'error',
                    timeout: 3000,
                });
              }
            });
        }
    },
    getTotal: function() {
      var total = 0;
      $('input[name="price[]"]').each(function(index, el) {
          amount = parseFloat(el.value);
          total += amount;
      });
      $('input[name="total"]').val(parseFloat(total).toFixed(2));
    },
    getNightsToStay: function(){
      dt1 = new Date($('input[name="check_in"]').val());
      dt2 = new Date($('input[name="check_out"]').val());
      nights = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
      $('span[data-night]').html(nights);
    },
    updateTotal : function(){
        $('form').on('change keyup', 'input[name="price[]"]', function() {
            reservation.getTotal();
        });
    },
    validate : function(){
        $("#form-reservation").validate({
          ignore: ":not(:visible)",
          highlight:function(r){
            $(r).closest(".form-group").addClass("has-error")},unhighlight:function(r){
              $(r).closest(".form-group").removeClass("has-error")},
              errorElement:"span",errorClass:"help-block",
              errorPlacement:function(r,e){e.parent(".input-group").length?r.insertAfter(e.parent()):e.parent("label").length?r.insertBefore(e.parent()):r.insertAfter(e)}
            });
    },
    resetSelect : function(select,text){

      $(select).empty()
      .append($('<option>', {
          value: '',
          text : text
      }));

    },
    resetEdit : function(){
      var $container = $('[data-room="0"]');
      $container.nextAll('div[data-room]').remove();
      $container.find('select[name="rooms_type[]"]').val('');
      $container.find('input[name="price[]"]').val('0.00');
      $container.find('input[name="guests[]"]').val(1);
      $container.find('select[name="rooms[]"]').empty()
      .append($('<option>', {
          value: '',
          text : novabeds.select_room
      }));
      $container.find('select[name="rates[]"]').empty()
      .append($('<option>', {
          value: '',
          text : novabeds.select_rate
      }));
    },
    resetFields : function($container){
      $container.find('select[name="rooms[]"]').val('');
      $container.find('select[name="rooms_type[]"]').val('');
      $container.find('input[name="price[]"]').val('0.00');
      $container.find('input[name="guests[]"]').val(1);
      $container.find('select[name="rates[]"]').empty()
      .append($('<option>', {
          value: '',
          text : novabeds.select_rate
      }));
    }
};

reservation.init();
