$.fn.loadingTable = function(target,param) {
    var target = $(target);
    var html = " <div class='table-loading-overlay'>"
    + " <div class='table-loading-inner'>"
    + "<div class=\"col-xs-4 col-xs-offset-4\">"
    + "<div class=\"table-loading-msg\"><i class='fa fa-spinner fa-spin '></i> "+novabeds.availability_in_loading+"</div>"
    + " </div>"
    + " </div>"
    + " </div>";
    $(target).append(html);
    $('.table-loading-overlay').css('height', $(target).height() + 'px');
    $('.table-loading-inner').css('padding-top', ($(target).height() / 2) + 'px');

    if(param == 'show'){
      $('.table-loading-overlay').fadeIn('slow');
    }else if(param== 'hide') {
      $('.table-loading-overlay').fadeOut('slow');
    }
}
