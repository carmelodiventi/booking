var $ = require('jquery');
var Chart = require('chart.js');

$(document).ready(function () {
    if ($('#canvas-container').length) {
        $.ajax({
            type: "GET",
            url: base_url + '/getDashboardData',
            headers: {
                'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr("content")
            }
        }).done(function (results) {
            if (results.revenue) {

                var d = new Date();
                var year = d.getFullYear();
                var datasets = [];

                $(results.revenue).each(function (index, el) {

                    switch (el.name) {
                        case 'Direct':
                            datasets[index] = {
                                backgroundColor: "rgba(75,192,192,0.4)",
                                borderColor: "rgba(75,192,192,1)",
                            }
                            break;
                        case 'Booking.com':
                            datasets[index] = {
                                backgroundColor: "rgba(75,134,192,0.4)",
                                borderColor: "rgba(75,134,192,1)",
                            }
                            break;
                        case 'Expidia':
                            datasets[index] = {
                                backgroundColor: "rgba(147,182,217,0.4)",
                                borderColor: "rgba(147,182,217,1)",
                            }
                            break;
                        case 'Agriturismo.it':
                            datasets[index] = {
                                backgroundColor: "rgba(204,220,237,0.4)",
                                borderColor: "rgba(204,220,237,1)",
                            }
                            break;
                        case 'Gigi Travel':
                            datasets[index] = {
                                backgroundColor: "rgba(221,235,232,0.4)",
                                borderColor: "rgba(221,235,232,1)",
                            }
                            break;
                        case 'Hotel Italia':
                            datasets[index] = {
                                backgroundColor: "rgba(233,239,216,0.4)",
                                borderColor: "rgba(233,239,216,1)",
                            }
                            break;
                        case 'HRS':
                            datasets[index] = {
                                backgroundColor: "rgba(233,239,216,0.4)",
                                borderColor: "rgba(233,239,216,1)",
                            }
                            break;
                        case 'BookNow Version 2':
                            datasets[index] = {
                                backgroundColor: "rgba(75,134,195,0.4)",
                                borderColor: "rgba(75,134,195,1)",
                            }
                            break;
                        default:
                            datasets[index] = {
                                backgroundColor: "rgba(75,192,192,0.4)",
                                borderColor: "rgba(75,192,192,1)",
                            }

                    }

                    datasets[index]['label'] = el.name;
                    datasets[index]['data'] = [el.count];
                });

                var data = {
                    labels: [year],
                    datasets: datasets
                };

                var canvas = document.createElement("canvas");
                canvas.setAttribute("id", "reservations-chart");
                canvas.height = 610;

                $('#canvas-container').append(canvas);
                var ctx = $('#reservations-chart');
                new Chart(ctx, {
                    type: 'bar',
                    data: data,
                    options: {
                        height: 300,
                        responsive: true,
                        maintainAspectRatio: false,
                    }
                });

                $('span[data-total-reservation]').html(results.statistics.total);
                $('span[data-reservation-confirmed]').html(results.statistics.confirmed);
                $('span[data-reservation-cancelled]').html(results.statistics.cancelled);
                $('span[data-reservation-paid]').html(results.statistics.paid);
                $('span[data-outgoing-reservation-today]').html(results.statistics.outgoing_today);
                $('span[data-outgoing-reservation-tomorrow]').html(results.statistics.outgoing_tomorrow);
                $('span[data-outgoing-reservation]').html(results.statistics.outgoing);
                $('span[data-incoming-reservation-today]').html(results.statistics.incoming_tomorrow);
                $('span[data-incoming-reservation-tomorrow]').html(results.statistics.incoming_tomorrow);
                $('span[data-incoming-reservation]').html(results.statistics.incoming);
            }
        });
    }
});