(function($) {
    "use strict";
    var FC = $.fullCalendar;
    var BasicView = FC.BasicView;
    var SingleRowMonthView;
    SingleRowMonthView = BasicView.extend();

    FC.views.singleRowMonth = {
        class: SingleRowMonthView,
        type: 'basic',
        duration: {
            days: 31
        }
    };

    jQuery(window).load(function() {

        var $calendar = $('input[name="period"]');

        $(window).load(function() {
            $calendar.datepicker({
                format: 'yyyy-mm',
                minViewMode: 1,
                weekStart: 1,
                autoclose: true
            });
        });

        var manage_avaibility = {
            init: function() {
                this.GenListCalendar();
            },
            GetStatus: function(status) {
                switch (status) {
                    case 'CNF':
                        return '<label class="label label-success">' + novabeds.reservation_confirm + '</label>';
                        break;
                    case 'PND':
                        return '<label class="label label-warning">' + novabeds.reservation_pending + '</label>';
                        break;
                    case 'CNF':
                        return '<label class="label label-danger">' + novabeds.reservation_cancelled + '</label>';
                        break;
                    case 'SLD':
                        return '<label class="label success">' + novabeds.reservation_paid + '</label>';
                        break;
                    case 'CKO':
                        return '<label class="label info">'.novabeds.reservation_checkout + '</label>';
                        break;
                }

            },
            GenListCalendar: function() {
                var list = $(".rooms-month-manager").find('.manage-avb');
                list.each(function(i) {
                    var calender_id = list[i].getAttribute("id");
                    var room_id = list[i].getAttribute("data-id");
                    var room_name = list[i].getAttribute("data-name");

                    $("#" + calender_id).fullCalendar({
                        header: false,
                        ignoreTimezone: false,
                        handleWindowResize: true,
                        editable: true,
                        height: 'auto',
                        contentHeight: 'auto',
                        selectable: true,
                        defaultView: 'singleRowMonth',
                        defaultDate: date_current,
                        viewRender: function(view, element) {
                            view.dayNumbersVisible = false;
                            if (i == 0) {
                                var $days = element.find('.fc-day-header');
                                $.each($days, function(index, item) {
                                    var $date = item.getAttribute('data-date');
                                    item.innerHTML = item.innerHTML + '<br><strong>' + moment($date).date() + '</strong>';
                                });
                            } else {
                                $("#" + calender_id + ' .fc-widget-header').hide();
                            }
                        },
                        events: function(start, end, timezone, callback) {
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                                }
                            });
                            $.ajax({
                                type: "POST",
                                url: base_url + '/api/v1/loadReservations',
                                data: {
                                    start: date_start,
                                    end: date_end,
                                    room_id: room_id
                                },
                            }).done(function(result) {
                                var room_res = [];
                                if (result != '') {
                                    $.each(result, function(index, item) {

                                        if (result[index].name == null) {
                                            result[index].name = ''
                                        } else if (result[index].surname == null) {
                                            result[index].surname = ''
                                        }

                                        var description = '<strong>' + novabeds.check_in + ':</strong>&nbsp;' + result[index].check_in + '<br>' +
                                            '<strong>' + novabeds.check_out + ':</strong>&nbsp;' + result[index].check_out + '<br>' +
                                            '<strong>' + novabeds.customer + ':</strong>&nbsp;' + result[index].name + '&nbsp;' + result[index].surname + '&nbsp;(' + result[index].country + ')<br>' +
                                            '<strong>' + novabeds.phone + ':</strong>&nbsp;' + result[index].phone + '<br>' +
                                            '<strong>' + novabeds.room_price + ':</strong>&nbsp;' + result[index].price + '<br>' +
                                            '<strong>' + novabeds.guests + ':</strong>&nbsp;' + result[index].guests + '<br>' +
                                            '<strong>' + novabeds.advance + ':</strong>&nbsp;' + result[index].advance + '<br>' +
                                            '<strong>' + novabeds.topay + ':</strong>&nbsp;' + result[index].topay + '<br>' +
                                            '<strong>' + novabeds.total + ':</strong>&nbsp;' + result[index].total + '<br>';

                                        room_res[index] = {
                                            'resourceId': room_id,
                                            'start': result[index].check_in,
                                            'end': result[index].check_out,
                                            'title': result[index].name + ' ' + result[index].surname,
                                            'pnr': result[index].pnr,
                                            'description': description,
                                            'tooltip': result[index].pnr,
                                            'reservation_id': result[index].reservation_id
                                        };


                                        switch (result[index].status) {
                                            case 'CNF':
                                                room_res[index]['backgroundColor'] = '#f7d725';
                                                room_res[index]['textColor'] = '#000';
                                                room_res[index]['borderColor'] = '#f7d725';
                                                break;
                                            case 'SLD':
                                                room_res[index]['borderColor'] = '#85c807';
                                                room_res[index]['backgroundColor'] = '#85c807';
                                                break;
                                            case 'CKO':
                                                room_res[index]['borderColor'] = '#85c807';
                                                room_res[index]['backgroundColor'] = '#85c807';
                                                break;
                                            case 'OPT':
                                                room_res[index]['backgroundColor'] = '#0785c8';
                                                room_res[index]['textColor'] = '#000';
                                                room_res[index]['borderColor'] = '#0785c8';
                                                break;
                                            case 'ALT':
                                                room_res[index]['backgroundColor'] = '#cc3300';
                                                room_res[index]['borderColor'] = '#cc3300';
                                                break;
                                            case 'CAN':
                                                room_res[index]['backgroundColor'] = '#cc3300';
                                                room_res[index]['borderColor'] = '#cc3300';
                                                break;
                                        }
                                    });
                                }

                                callback(room_res);
                            });

                        },
                        eventClick: function(calEvent) {
                            var reservation_id = calEvent.reservation_id;
                            manage_avaibility.EditReservation(reservation_id);
                        },
                        eventDrop: function(event, delta, revertFunc) {
                            /*swal({
                                title: 'Scheduler',
                                text: novabeds.scheduler_reservation_move_msg,
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: novabeds.move_it
                            }).then(function() {
                                var data = {
                                    action: "moveReservation",
                                    start: event.start,
                                    end: event.end,
                                    pnr: event.pnr
                                };
                                $.post(ajaxurl, data, function(result) {
                                    if (result != '') {
                                        result = JSON.parse(result);

                                        if (result.status == 'success') {
                                            swal(
                                                'Deleted!',
                                                'Your file has been deleted.',
                                                'success'
                                            );
                                        }

                                    }
                                });
                            }}, function (dismiss) {
                              // dismiss can be 'cancel', 'overlay',
                              // 'close', and 'timer'
                              if (dismiss === 'cancel') {
                                  revertFunc();
                              }
                            }));
                            */
                        },
                        eventRender: function(event, el) {
                            el.find('.fc-time').remove();
                            //el.attr('title', event.tooltip);
                            el.qtip({
                                content: event.description,
                                style: {
                                    classes: 'qtip-tipsy'
                                },
                                position: {
                                    my: 'left top',
                                    at: 'top right',
                                }
                            });
                        },
                        eventAfterRender: function(event, element, view) {
                            $(".fc-day-header").css('font-size', '13px');
                            var width = element.parent().width()
                            var colspan = element.parent().get(0).colSpan;
                            var cell_width = width / colspan;
                            var half_cell_width = cell_width / 2;
                            element.css('margin-left', half_cell_width);
                            element.css('margin-right', half_cell_width * -1);
                            var width_event = element.children('.fc-content').width();
                            element.children().closest('.event-end').css('margin-left', width_event - 23);
                            if (element.hasClass('fc-not-end')) {
                                element.css('margin-right', 0);
                            }
                            if (element.hasClass('fc-not-start')) {
                                if (colspan == 1) {
                                    width_event = 0;
                                }
                                element.css('margin-left', 0);
                                element.children().closest('.event-end').css('margin-left', width_event);
                            }
                        },
                        select: function(startDate, endDate, jsEvent, view) {
                            var checkIn = moment(startDate).format('YYYY-MM-DD');
                            var checkOut = moment(endDate).format('YYYY-MM-DD');
                            window.location.href = 'reservations/create?check_in=' + checkIn + '&check_out=' + checkOut;

                        }
                    });
                });
            },
            EditReservation: function(reservation_id) {
                window.location.href = 'reservations/' + reservation_id + '/edit';
            }
        };

        manage_avaibility.init();

        var mange_pricing = {
            init: function() {
                this.GenListCalendar();
                this.OpenForm();
                this.UpdateAvailability();
                this.DeleteOrder();
            },
            data_init: {
                date_current: date_current,
            },
            OpenForm: function() {
                var x = false;
                $(".form-update-pricing-js").click(function(e) {
                    e.preventDefault();
                    x = !x;
                    if (x == true) {
                        $(".update-pricing").slideDown();
                    } else {
                        $(".update-pricing").slideUp();
                    }
                });
            },
            GenListCalendar: function() {
                var list = $(".rooms-month-manager").find('.manage-avb');
                list.each(function(i) {
                    var calender_id = list[i].getAttribute("id");
                    var room_id = list[i].getAttribute("data-id");
                    $("#" + calender_id).fullCalendar({
                        header: {
                            left: 'title',
                            center: '',
                            right: '',
                        },
                        ignoreTimezone: false,
                        handleWindowResize: true,
                        editable: false,
                        height: 120,
                        contentHeight: 120,
                        defaultView: 'singleRowMonth',
                        defaultDate: mange_pricing.data_init.date_current,
                        events: function(start, end, timezone, callback) {
                            var data = {
                                action: "loadEvents",
                                date: mange_pricing.data_init.date_current,
                                room_id: room_id
                            };
                            $.post(ajaxurl, data, function(result) {
                                callback(result);
                            });
                        },
                        eventClick: function(calEvent, jsEvent, view) {
                            if (calEvent.end === null) {
                                calEvent.end = calEvent.start;
                            }
                            var sd = calEvent.start.unix();
                            var ed = calEvent.end.unix();
                            $(".box-event-info-js").show();
                            mange_pricing.GetInfoEvent(sd, ed, room_id);
                        },
                        eventRender: function(event, el) {
                            el.find('.fc-time').remove();
                        },
                        eventAfterRender: function(event, element, view) {
                            $(".fc-day-header").css('font-size', '13px');
                            var width = element.parent().width()
                            var colspan = element.parent().get(0).colSpan;
                            var cell_width = width / colspan;
                            var half_cell_width = cell_width / 2;
                            element.css('margin-left', half_cell_width);
                            element.css('margin-right', half_cell_width * -1);
                            var width_event = element.children('.fc-content').width();
                            element.children().closest('.event-end').css('margin-left', width_event - 23);
                            if (element.hasClass('fc-not-end')) {
                                element.css('margin-right', 0);
                            }
                            if (element.hasClass('fc-not-start')) {
                                if (colspan == 1) {
                                    width_event = 0;
                                }
                                element.css('margin-left', 0);
                                element.children().closest('.event-end').css('margin-left', width_event);
                            }
                        }
                    });
                });
            },
            SecondsToDay: function() {
                var today = new Date();
                var mm = today.getMonth() + 1;
                if (mm < 10) {
                    mm = '0' + mm;
                }
                return today.getFullYear() + '-' + mm;
            },
            GetInfoEvent: function(from, to, room_id) {
                $(".content-event-js").html('');
                $(".apb-get-event").show();
                var data = {
                    action: "get_info_event",
                    from: from,
                    to: to,
                    room_id: room_id,
                    book_id: ''
                };
                $.post(ajaxurl, data, function(result) {
                    var data_result = result;
                    $(".content-event-js").html(data_result);
                    $(".apb-get-event").hide();
                });
            },
            DeleteOrder: function() {
                $(".awe-plugin").on('click', '.room-delete-order', function() {
                    var book_id = $(this).attr('data-id');
                    var data = {
                        action: "Controller_delete_booking",
                        book_id: book_id,
                    };
                    $.post(ajaxurl, data, function(result) {
                        var data_result = JSON.parse(result);
                        if (data_result == 'yes') {
                            $('.item-order-' + book_id).slideUp();
                        }
                    });
                });
            },
            UpdateAvailability: function() {
                $(".awe-avb-js").click(function() {
                    var rooms_start_date = $("input[name=rooms_start_date]").val();
                    var rooms_end_date = $("input[name=rooms_end_date]").val();
                    if (rooms_start_date == "") {
                        $(".date-start-js").focus();
                        return this;
                    }
                    if (rooms_end_date == "") {
                        $(".date-end-js").focus();
                        return this;
                    }
                    var action = $(this).attr('data-value');
                    if (action == 'yes') {
                        var rooms_start_date = $("input[name=rooms_start_date]").val();
                        var rooms_end_date = $("input[name=rooms_end_date]").val();
                        var unit_state = $("select[name=unit_state]").val();
                        var _room_id = new Array();
                        var day_option = $(".get_day_js:checked").map(function(_, el) {
                            return $(el).val();
                        }).get();
                        var room_id = $(".get_room_id_js:checked").map(function(_, el) {
                            _room_id.push($(el).val());
                        });
                        if (rooms_start_date == "") {
                            $("#bh_datepicker_start").focus();
                        } else {
                            $(".spinner").show();
                            var data = {
                                action: "add_availability_for_room",
                                rooms_start_date: rooms_start_date,
                                rooms_end_date: rooms_end_date,
                                unit_state: unit_state,
                                room_id: _room_id,
                                day_option: day_option,
                            };
                            $.post(ajaxurl, data, function(result) {
                                window.location = location.href = "";
                            });
                        }
                    } else {
                        $(".btn-update-status-js").slideUp();
                    }
                });
            }
        }

    });
})(jQuery);
