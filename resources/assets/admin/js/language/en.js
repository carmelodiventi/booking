novabeds = {
    loading: "Loading...",
    registration: "Registrazione in corso...",
    cancel_order: "Are sure to cancel Order?",
    scheduler_reservation_move_msg : "Do you want move the reservation?",
    move_it : 'Move',
    reservation_confirm: 'Confirmed',
    reservation_pending: 'Not Confirmed',
    reservation_cancelled: 'Deleted',
    reservation_paid: 'Paid',
    reservation_checkout: 'Check OUT',
    check_in:'Check IN',
    check_out:'Check OUT',
    note:'Note',
    customer_note : 'Customer Note',
    guests:'Guests',
    total:'Total',
    advance:'Advance',
    topay:'To Pay',
    phone:'Phone',
    customer:'Customer',
    origin:'Agency',
    transaction_code:'Transaction Code',
    room_price : 'Price per Room',
    rooms_availablility_error: 'There aren\'t rooms available',
    price_updated : 'Total price updated',
    fare_not_found : 'There aren\'t rates for the selected room',
    room_not_available: 'Room % is not available in the selected one',
    period_not_found: 'here is no selection of the Stay Period',
    delete:'Delete',
    remove:'Remove',
    select_room: 'Select room',
    availability_list_updated : 'List of available rooms updated',
    select_rate: 'Select an Rate',
    amount_exceeds: 'The amount typed exceeds the total cost of the reservation',
    reservation_not_moved : 'The reservation has not been moved, the room may be occupied or blocked',
    details : 'Details',
    availability_update: 'Availability updated successfully',
    availability_error: 'Availability not updated',
    select_item: 'Select an Item',
    units: 'Units',
    are_you_sure: 'Are you sure?',
    price_and_availability_will_be_overwritten: 'The price and availability will br overwritten!',
    confirm:'Confirm',
    synchronized:'Synchronized',
    availability_synchronized_successfully: 'Availability synchronized successfully',
    availability_error_in_loading: 'An error is occured in Availability\'s loading',
    availability_in_loading:'Update availability is working...',
    set_long_term_data_done:'Set Long data completed, if you has done please save!',
    available_rooms:'Available Rooms',
    confirm_move: 'Confirm Move',
    details_move: 'Details Move:',
    cancel: 'Cancel',
    room: 'Room',
    with: 'with',
    delete_reservation: 'Are you sure to delete the reservantion?'
};
