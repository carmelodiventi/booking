var planning = {
  init: function() {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr("content")
      }
    });
    this.getAvailability();
    this.getReservations();
  },
  dateToStr: function(date) {
    var year = date.getFullYear(),
      month = date.getMonth() + 1,
      day = date.getDate();
    day = day + '';
    if (day.length == 1) {
      day = '0' + day;
    }
    month = month + '';
    if (month.length == 1) {
      month = '0' + month;
    }
    var str = year + '-' + month + '-' + day;
    return str;
  },
  getDateDiff: function(dt1, dt2) {
    dateDiff = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
    return dateDiff;
  },
  getAvailability: function(){
    if(startDate && endDate){
      $.ajax({
        url: base_url + '/getAvailabilityFromDate',
        type: 'GET',
        data: {
          startDate: startDate,
          endDate: endDate,
          masterRate: true
        }
      })
      .done(function($response) {
        if($response.Success == true){
            $().loadingTable('show');
            $.each($response.Availability, function(index,avail){
              var rooms_type_td = $("td[data-date='"+avail.date+"'][data-rates-type-id='"+avail.rates_type_id+"'][data-room-type-id='"+avail.rooms_type_id+"']");
              var date_td = $("td.calendar-day[data-date='"+avail.date+"'][data-room-type-id='"+avail.rooms_type_id+"']");
              rooms_type_td.html('<span data-toggle="tooltip" data-html="true" title="'+novabeds.available_rooms + '&nbsp;' + '</strong>' + avail.units +'</strong>">'+avail.units+'</span>');
              if(avail.closed == 1){
                date_td.addClass('closed');
              }
            });
            $('[data-toggle="tooltip"]').tooltip();
        }
        console.log($response);
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        $().loadingTable('hide');
        console.log("complete");
      });
    }
    else{
      $.noty({
        layout: 'top',
        type: 'error',
        text: novabeds.availability_error_in_loading
      });
    }

  },
  getReservations: function() {
    $('table.planning tr.calendar-prices').each(function() {
      var room_id = $(this).attr('data-room-id');
      $.ajax({
        type: "POST",
        url: base_url + '/loadReservations',
        async: false,
        data: {
          room_id: room_id,
          start: startDate,
          end: endDate
        },
      }).done(function(results) {
        if (results) {
          var cellWidth = jQuery('tr.calendar-prices[data-room-id="' + room_id + '"] td.calendar-day:first').innerWidth();
          var res_tooltips = null;

          $(results).each(function(index, reservation) {

            var start_day = new Date(reservation.check_in);
            var last_day = new Date(reservation.check_out);


            var details = '<strong>' + novabeds.pnr + ':</strong>&nbsp;<strong>' + reservation.pnr + '</strong><br>' +
              '<strong>' + novabeds.check_in + ':</strong>&nbsp;' + reservation.check_in + '<br>' +
              '<strong>' + novabeds.check_out + ':</strong>&nbsp;' + reservation.check_out + '<br>' +
              '<strong>' + novabeds.nights_to_stay + ':</strong>&nbsp;' + reservation.nights + '&nbsp;' + novabeds.nights + '<br>' +
              '<strong>' + novabeds.origin + ':</strong>&nbsp;' + reservation.origin + '<br>' +
              '<strong>' + novabeds.transaction_code + ':</strong>&nbsp;' + reservation.transaction_code + '<br>' +
              '<strong>' + novabeds.customer + ':</strong>&nbsp;' + reservation.name + '&nbsp;' + reservation.surname + '&nbsp;(' + reservation.country + ')<br>' +
              '<strong>' + novabeds.phone + ':</strong>&nbsp;' + reservation.phone + '<br>' +
              '<strong>' + novabeds.room_price + ':</strong>&nbsp;' + reservation.price + '<br>' +
              '<strong>' + novabeds.guests + ':</strong>&nbsp;' + reservation.guests + '<br>' +
              '<strong>' + novabeds.note + ':</strong>&nbsp;' + reservation.reservation_note + '<br>' +
              '<strong>' + novabeds.customer_note + ':</strong>&nbsp;' + reservation.note + '<br>' +
              '<strong>' + novabeds.topay + ':</strong>&nbsp;' + reservation.topay + '<br>' +
              '<strong>' + novabeds.total + ':</strong>&nbsp;' + reservation.total + '<br>' +
              '<hr>' +
              '<a href="'+base_url + '/admin/reservation/details/' + reservation.reservation_id+'" class="btn btn-primary btn-block">' + novabeds.details + '</a>';

            res_tooltips = $('<div/>');
            res_tooltips.attr('data-toggle', 'popover')
              .attr('data-html', 'true')
              .attr('data-placement', 'top')
              .attr('data-pnr', reservation.pnr)
              .attr('data-room-booked-id', reservation.id)
              .attr('data-room-type-id', reservation.rooms_type_id)
              .attr('title', details)
              .attr('draggable', 'true')
              .attr('unselectable', 'on')
              .addClass('reservation res-details res-day-' + reservation.status)
              .append('<div><span>' + reservation.name + '&nbsp;' + reservation.surname + '</span></div>');

            if(reservation.review == 1){
              res_tooltips.addClass('res-to-review');
            }

            $('[data-room-id="' + room_id + '"]' + '[data-date="' + planning.dateToStr(start_day) + '"].calendar-day')
            .attr('data-reservation-id', reservation.reservation_id);

            while (start_day < last_day) {
              $('[data-room-id="' + room_id + '"]' + '[data-date="' + planning.dateToStr(start_day) + '"].calendar-day').attr('data-reservation-id', reservation.reservation_id);
              start_day.setDate(start_day.getDate() + 1);
            }

            last_day.setDate(last_day.getDate() - 1);
            var firstCell = $('[data-room-id="' + room_id + '"]' + '[data-reservation-id="'+reservation.reservation_id+'"]:first');
            var lastCell = $('[data-room-id="' + room_id + '"]' + '[data-reservation-id="'+reservation.reservation_id+'"]:last')
            firstCell.nextAll(".calendar-day:lt(" + (lastCell.index() - firstCell.index()) + ")").addClass('unavailable');
            $('[data-room-id="' + room_id + '"]' + '[data-date="' + planning.dateToStr(last_day) + '"].calendar-day').attr('data-reservation-id', reservation.reservation_id);

            //var res_days_start = jQuery('[data-room-id="' + room_id + '"]' + '[data-reservation-id="' + reservation.reservation_id + '"]');
            var res_days_lenght = planning.getDateDiff(new Date(reservation.check_in), new Date(reservation.check_out));
            var res_days = jQuery('[data-room-id="' + room_id + '"]' + '[data-reservation-id="' + reservation.reservation_id + '"]');

            if (res_days.length>0) {
                  var offset_start = res_days.first().position();
                  var offset_end = res_days.last().position();
                  var marginLeft = cellWidth /2;
                  //res_tooltips.css('top', offset_start.top + 'px').css('left', offset_start.left + 'px');
                  res_tooltips.css('top', 0 + 'px').css('left', 0 + 'px');
                  if (offset_start.left == 0 && res_days_lenght > 1) {
                    res_tooltips.css('margin-left', 0);
                    res_tooltips.css('width', (offset_end.left - offset_start.left) + cellWidth + marginLeft);
                  } else {
                    res_tooltips.css('margin-left', 0);
                    res_tooltips.css('width', offset_end.left - offset_start.left + cellWidth);
                  }


                  res_days.first().append(res_tooltips);
            }

          });
        }
        $("div[data-toggle='popover']").popover({ html : true, container: 'body'});
        $('div.reservation').on('click', function (e) {
          $('div.reservation').not(this).popover('hide');
        });
      });
    });

  },
  getRoomsTypeRate: function(rooms_type_id, start, end) {
    $.ajax({
      type: "POST",
      url: base_url + '/getRoomsTypeRate',
      data: {
        start: start,
        end: end,
        id: rooms_type_id
      },
    }).done(function(result) {
      result.rate_amount = parseFloat(result.rate_amount);
    });
  },
  enableDateRangeSelection: function($status) {
      planning.disableFuction('enableDateRangeSelection');
      var dateRange = [];
      var lodgingID = null;
      $('table.planning tr.calendar-prices').each(function(index, item) {
        var room_id = $(item).data('room-id');
        var calendarOverview = $('table.planning tr.calendar-prices[data-room-id="' + room_id + '"] > td');
          if($status == true){
            calendarOverview.click(function() {
          var cell = $(this);
          var selectedDate = $(this).data('date');
          var selectLodgingID = $(this).data('room-id');
          var highlightClass = 'highlighted';
          if (cell.hasClass(highlightClass) || cell.hasClass("unavailable")) {
            return false;
          } else if (dateRange.length == 2 || selectLodgingID !== lodgingID) {
            $('.calendar-day').removeClass(highlightClass);
            dateRange = [];
          }
          dateRange.push(selectedDate);
          cell.addClass(highlightClass);
          var firstCell = $(".calendar-day." + highlightClass + ":first");
          var lastCell = $(".calendar-day." + highlightClass + ":last");
          lodgingID = selectLodgingID;
          if (dateRange.length == 2) {
            var cellsToHighlight = firstCell.nextAll(".calendar-day:lt(" + (lastCell.index() - firstCell.index()) + ")");
            cellsToHighlight.addClass(highlightClass);
            $('button.btn-reserve')
              .attr('disabled', false)
              .attr('onclick', 'window.location.href=\'' + base_url + '/admin/reservations/create?check_in=' + dateRange[0] + '&check_out=' + dateRange[1] + '&lodgingID=' + lodgingID + '\'');
          }
        });
          }
          else{
            calendarOverview.unbind('click');
          }
      });

  },
  enableReservationsMove: function($status){
    planning.disableFuction('enableReservationsMove');
    if($status == true){
      var reservation = null;
      var pnr = null;
      var room_id = null;
      var room_type_id = null;
      var room_target_name = null;
      var room_target_type = null;
      var date = null;
      var room_booked_id = null;
      var room_booked_name = null;
      var room_booked_type = null;

      $('div.reservation').on("dragstart", function (event) {
          event.originalEvent.dataTransfer.setData('pnr', $(this).attr('data-pnr'));
          event.originalEvent.dataTransfer.effectAllowed = 'copy';
          event.target.style.opacity = .5;
          pnr = $(this).attr('data-pnr');
          room_booked_type_id = $(this).attr('data-room-type-id');
          room_booked_id = $(this).attr('data-room-booked-id');
          room_booked_name = $(this).parent().attr('data-room-name');
          room_booked_type = $(this).parent().attr('data-room-type');
          reservation = $(this);
      });

      $('table.planning td').on("dragenter dragover dragleave drop", function (event) {
          event.preventDefault();

          if (event.type === 'dragover') {
            event.preventDefault();
            $(this).addClass('drop-active');
          }
          if (event.type === 'dragleave') {
            event.preventDefault();
            $(this).removeClass('drop-active');
          }
          if (event.type === 'drop') {
            $(this).removeClass('drop-active');

            event.target.style.opacity = 1;
            date = $(this).attr('data-date');
            room_id = $(this).attr('data-room-id');
            room_type_id = $(this).attr('data-room-type-id');
            room_target_name = $(this).attr('data-room-name');
            room_target_type = $(this).attr('data-room-type');
            unavailable = $(this).hasClass('unavailable');
            is_reservation = $(this).hasClass('reservation');
            blocked = $(this).hasClass('blocked');
            $(this).removeClass('drop-active');

            var details = novabeds.room + " " + room_booked_name + " "  + novabeds.with  + " " +
              novabeds.room + " " + room_target_name;

            if(is_reservation || blocked){
                $.noty.closeAll();
                noty({
                  text: novabeds.reservation_not_moved,
                  layout: 'top',
                  type: 'error',
                  timeout: 3000,
                });
               return false;
            }

            swal({
              title : novabeds.confirm_move,
              text : novabeds.details_move + details,
              icon: 'warning',
              buttons :{
                cancel : novabeds.cancel,
                confirm : {
                  text : novabeds.confirm,
                  value: true,
                  className: "btn-primary",
                }
              }
            }).then((confirm) => {
              if (confirm) {
                $('#cancel').button('loading');
                $.ajax({
                  url: base_url + '/moveReservation',
                  type: 'POST',
                  data: {
                    'pnr': pnr,
                    'room_type_id':room_type_id,
                    'room_booked_type_id' : room_booked_type_id,
                    'room_booked_id' : room_booked_id,
                    'room_id':room_id,
                    'date':date
                  }
                })
                .done(function(response) {
                    $('#cancel').button('reset');
                    if(response.Success){
                        $.noty.closeAll();
                        noty({
                          text: response.Text,
                          layout: 'top',
                          type: 'success',
                          timeout: 3000,
                        });
                       $('table.planning tr.calendar-prices td.calendar-day')
                       .empty().removeAttr('data-reservation-id');
                       planning.getReservations();
                       planning.getAvailability();
                       planning.enableReservationsMove(false);
                       $('button:not(.btn-enable-function)').hide();
                       $('button.btn-enable-function').show();
                    }
                    else{
                      $.noty.closeAll();
                      noty({
                        text: response.Text,
                        layout: 'top',
                        type: 'error',
                        timeout: 3000,
                      });
                    }
                });
              }
              else{
                $('table.planning tr.calendar-prices td.calendar-day')
                .empty().removeAttr('data-reservation-id');
                planning.getReservations();
                planning.getAvailability();
                planning.enableReservationsMove(false);
                $('button:not(.btn-enable-function)').hide();
                $('button.btn-enable-function').show();
              }
            });

          };
      });
    }
    else{
      $('div.reservation').unbind('dragstart');
      $('table.planning td').unbind('dragenter dragover dragleave drop');
    }
  },
  disableFuction: function($case){
    switch ($case) {
      case 'enableDateRangeSelection':
        $('button:not(.btn-cancel)').hide();
        $('button.btn-cancel,button.btn-reserve').show();
        $('button.btn-cancel').on('click',function(){
            planning.enableDateRangeSelection(false);
            $('td.calendar-day').removeClass('highlighted');
            $('button:not(.btn-enable-function)').hide();
            $('button.btn-enable-function').show();
        });
        break;
        case 'enableReservationsMove':
          $('button:not(.btn-cancel)').hide();
          $('button.btn-cancel').show();
          $('button.btn-cancel').on('click',function(){
              planning.enableReservationsMove(false);
              $('button:not(.btn-enable-function)').hide();
              $('button.btn-enable-function').show();
          });
        break;
    }
  },
  blockRoom : function() {
    blockRoom = false;
    if (blockRoom == true) {
      $('table.planning tr.calendar-prices').click(function() {});
    }
  }
}

planning.init();
