<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Request input Language Lines
    |--------------------------------------------------------------------------
    */

    'customer_required' =>  'The customer is required',
    'check_in_required' => 'Check-IN Date Required',
    'check_out_required' => 'Check-Out Date Required',
    'rooms_required' => 'Selection Rooms Required  ',
    'rate_required' => 'Selection Rates Required',
    'guests_required'=> 'Selection Guests  Required',
    'price_required'=> 'Selection Price Required',
    'status_required' => 'Status Required',
    'extra_required'=> 'Extra Cost Required',
    'extra_price_required'=> 'Extra Price Required',
    'commission_percentage'=> 'The field Percentage Commission is required',
    'transaction_code'=> 'Also the Price field is required',
    'title_required'=> 'Title Required',
    'code_required'=> 'Also the Code field is required',
    'value_required'=> 'Value field is also required',
    'value_required_number'=> 'Value must be numeric',
    'condition_required'=> 'Conditions are required',
    'email_required' => 'Email Address Required',
    'email_wrong_format' => 'Invalid email address',
    'password_required' => 'Password is Obligatory',
    'name_required' => 'Name is required',
    'surname_required' => 'Last Name is Required',
    'phone_required' => 'Telephone number is required',
    'country_required' => 'Nation is Obligatory',
    'file_required' => 'You have to specify the file to upload',
    'password' => 'You must enter a Password',
    'password_lenght' => 'The password must be at least 8 characters long',
    'password_conf' => 'The password must match with the previously entered password',
    'email_exists' => 'Your email address has already been used, you can request the reset password and receive a new one in the email address used for registration.',
    'amount' => 'Amount is required'
];
