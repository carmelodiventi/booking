<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Emails Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'new_reservation_added' => 'Nuova Prenotazione Aggiunta',
    'gentle' => 'Gentile',
    'hi' => 'Ciao!',
    'new_reservation_added_message' => 'E\' stata appena aggiunta una nuova prenotazione sul Booking Engine.',
    'new_reservation_message' => 'Hai appena ricevuto una nuova prenotazione dal Booking Engine.',
    'see_new_details' => 'Vedi qui i nuovi dati della prenotazione',
    'greatings' => 'Cordiali Saluti',
    'greatings_message' => 'Grazie per aver prenotato presso la nostra struttura.',
    'new_reservation_info' => 'questa è una copia della sua prenotazione da esibire non appena arriverà in Hotel.',
    'new_reservation_admin_info'=> 'Questa è una notifica di prenotazione generata dal sistema.',
    'reservation_confirm' => 'Conferma Prenotazione',
    'new_reservation' => 'Nuova Prenotazione',
    'reservation_confirm_message' => 'La sua Prenotazione è stata effettuata correttamente.',
    'reservation_confirm_summary' => 'Riepilogo Prenotazione',
    'reservation_pnr' => 'Codice Prenotazione',
    'registration_confirm' => 'Conferma Registrazione',
    'login_data' => 'Dati di Accesso',
    'email' => 'Email',
    'password' => 'Password',
    'reset_password_confirmation' => 'Password Reset',
    'greatings_registration_message' => 'Grazie :name per esserti registrato sul nostro sistema di prenotazione.',
    'greatings_password_reset_message' => '<strong>Gentile :name</strong>, hai ricevuto questa e-mail perché hai inviato una richiesta di ripristino della password per il tuo account.',
    'greatings_password_renew_message' => '<strong>Gentile :name</strong>, hai ricevuto questa e-mail perché hai inviato una richiesta di rinnovo della password per il tuo account.',
    'no_reset_request' => 'Se non hai richiesto un ripristino della password, non è necessaria alcuna azione aggiuntiva.',
    'no_registration_request' => 'Se non hai richiesto un nuovo account, non è necessaria alcuna azione aggiuntiva.',
    'reset_password'=> 'Reset Password',
    'password_link_message' => 'Se hai problemi a fare clic sul pulsante "Reset password", copia e incolla l\'URL sotto nel tuo browser web:',

];
