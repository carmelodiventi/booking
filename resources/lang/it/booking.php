<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'hero_title' => 'Cerca, Prenota, Soggiorna',
    'hero_subtitle' => 'Scegli la camera Ti garantiamo la migliore tariffa',
    'select_date' => 'Seleziona Date',
    'select_stay_date' => 'Seleziona le date del Tuo soggiorno',
    'check_in' => 'Check-IN',
    'check_out' => 'Check-OUT',
    'select_person' => 'Seleziona Ospiti',
    'adult' => 'Adulti',
    'children' => 'Bambini',
    'guests' => 'Ospiti',
    'select_guests' => 'Seleziona Ospiti',
    'discount_code' => 'Codice Sconto',
    'insert_a_discount_code' => 'Inserisci il Codice Sconto',
    'search_avaibility' => 'Cerca Disponibilità',
    'date_error' => 'Le date indicate sono errate oppure sono mancanti',
    'room_guests' => ':guests Ospiti',
    'search_result' => 'La ricerca effettuata ha generato :result risultati per le date: Dal <strong>:check_in</strong> Al <strong>:check_out</strong> per <strong>:n_nights Notti</strong> di soggiorno',
    'room_not_available' => 'This room is not available for reservation',
    'reservation_summary' => 'Riepilogo Prenotazione',
    'data_summary' => 'Riepilogo Dati',
    'reserve' => 'Prenota Adesso',
    'continue' => 'Continua',
    'available_rooms' => 'Camere Disponibili',
    'room' => 'Camera',
    'book_now' => 'Prenota Adesso',
    'book' => 'Prenota Camera',
    'payment' => 'Pagamento',
    'there_are' => 'Hai selezionato',
    'items_in_cart_message' => 'Camera|Camere nella Tua prenotazione',
    'please_signin_or_signup' => 'Accedi o Registrati per continuare',
    'confirm_and_pay' => 'Conferma e Paga',
    'register_customer' => 'Cliente Registrato',
    'new_customer' => 'Nuovo Cliente',
    'email' => 'Email',
    'password' => 'Password',
    'login' => 'Accedi',
    'logout' => 'Esci',
    'register' => 'Registrati',
    'or' => 'oppure',
    'name' => 'Nome',
    'surname' => 'Cognome',
    'city' => 'Citta',
    'province' => 'Provincia',
    'country' => 'Nazione',
    'address' => 'Indirizzo',
    'phone' => 'Telefono',
    'password_confirm' => 'Conferma Password',
    'session_error' => 'Non è stato possibile caricare i dati di sessione torna alla home e ripeti la ricerca.',
    'total_nights'=> 'Totale [0,1] :count notte |[2,*] :count notti di soggiorno',
    'rooms'=> 'Camere',
    'rooms_n'=> 'Camera | Camere',
    'fees'=> 'Tasse',
    'price'=> 'Prezzo',
    'payment_summary'=> 'Riepilogo Pagamenti',
    'discount'=> 'Sconto',
    'advance'=> 'Anticipo',
    'total_tax'=> 'Totale Tasse',
    'sub_total'=> 'Sub Totale',
    'total'=> 'Totale',
    'back_to_home' => 'Torna alla Home',
    'confirm'=> 'Confermata',
    'alert'=> 'In Attesa di Conferma',
    'canceled' => 'Cancellata',
    'checkout' => 'Check-OUT',
    'paid' => 'Saldata',
    'note' => 'Note sulla Prenotazione',
    'no_rooms_available' => 'Siamo spiacenti, per le date richieste non
    ci sono camere disponibili. Visita anche l\'altra nostra struttura: il
    B&B Agriturismo Bannata.',
    'payment_cancel' => 'Il Pagamento della Prenotazione è stato
    annullato, la prenotazione non è andata a buon fine.',
    'repeat_search' => 'Ripeti Ricerca',
    'total_nights'=> 'Totale Notti',
    'nights'=> 'Notti',
    'for_night'=> 'Per Notte',
    'login_with_your_data' => 'Se sei un utente registrato inserisci le
    Tue credenziali d\'accesso (indirizzo e-mail e password) altrimenti Ti
    chiediamo di registrarti.',
    'register_new_account' => 'Registrati compilando il form qui sotto.',
    'hello' => 'Ciao',
    'thanks_for_login' => 'grazie per aver effettuato l\'accesso,
    seleziona un metodo di Pagamento',
    'add_special_request' => 'Aggiungi Richieste speciali',
    'special_request' => 'Richieste speciali :',
    'reservation_confirm' => 'Conferma la Tua Prenotazione',
    'select_payment_method'=>'Seleziona Medoto di Pagamento',
    'sub_total' => 'Sub Totale',
    'tax' => 'Tasse',
    'total' => 'Totale',
    'credit_card' => 'Carta di Credito',
    'credit_card_condition'=> 'Zero costi aggiuntivi! La carta di credito verrà utilizzata solo a garanzia della prenotazione. Non occorre anticipare alcun deposito. Pagamento del costo totale della prenotazione direttamente in struttura. Cancellazione gratuita entro i termini previsti.',
    'firstName_cc' => 'Nome',
    'lastName_cc' => 'Cognome',
    'number_cc' => 'Numero Carta di Credito',
    'month_exp_cc' => 'Mese es. 01',
    'year_exp_cc' => 'Anno es. 2017',
    'cvv_cc' => 'CVV',
    'privacy_check' => 'Registrandoti accetti <a href="#">Condizioni d\'uso</a> e <a href="#">Privacy Policy</a>',
    'paypal' => 'PayPal',
    'paypal_info' => 'Verrai indirizzato direttamente sul sito di PayPal
    Holdings, Inc. per effettuare il pagamento, subito dopo riceverai il
    voucher con il riepilogo della Tua prenotazione',
    'payment_method' => 'Metodo di Pagamento',
    'credit_card_payment_info' => 'Carta di Credito - <small>Zero costi
    aggiuntivi! La carta di credito verrà utilizzata solo a garanzia della
    prenotazione. Non occorre anticipare alcun deposito. Pagamento del costo
    totale della prenotazione direttamente in struttura. Cancellazione
    gratuita entro i termini previsti.</small>',
    'paypal_payment_info' => 'PayPal - <small>Troverai una ricevuta del
    pagamento da parte di Paypal, all\'indirizzo email inserito.</small>',
    'offert' => 'Offerta',
    'title_register_msg' => 'Registrazione Cliente',
    'title_login_msg' => 'Login Cliente',
    'register_success_msg' => 'Ciao :name, ti sei appena registrato sul
    nostro sistema di prenotazioni, i Tuoi dati verranno utilizzati come
    intestatario della prenotazione',
    'login_success_msg' => 'Ciao :name, hai appena effettuato l\'accesso
    al nostro sistema di prenotazioni, i Tuoi dati verranno utilizzati come
    intestatario della prenotazione',
    'login_error_msg'=>'Login fallito, Username o Password errati',
    'registration_error_msg'=> 'Si è verificato un errore in fase di
    registrazione',
    'forgot_password' => 'Password Dimenticata?',
    'reset_password' => 'Reset Password',
    'reset_your_password' => 'Se sei un utente registrato inserisci il Tuo indirizzo e-mail Ti invieremo la password di Accesso.',
    'reset' => 'Reset',
    'email_not_exist' => 'Questo indirizzo email non è Registrato',
    'title_login_data_sent_msg' => 'Dati di Accesso inviati',
    'login_data_sent_success_msg' => 'Gentile :name, i dati di accesso al
    sistema sono stati inviati all\'indirizzo email fornito.',
    'details_book' => 'Info & Prenota',
    'select_sooms' => 'Seleziona Camere',
    'rate_condition' => 'Condizioni Tariffa',
    'add' => 'Aggiungi',
    'last_room_available' => 'Ultima camera disponibile',
    'rooms_availability' => 'Ci sono :count camere disponibili',
    'starting_from' => 'a partire da',
    'price_for_nights' => 'Costo per :nights notti per :guests persone',
    'for_nights' => 'Per :nights notte|Per :nights notti',
    'continue_booking' => 'Ho altre camere da aggiungere alla prenotazione',
    'show_summary' => 'Mostra riepilogo',
    'rooms_count_of' => ':count di :totalcount',
    'your_details' => 'I tuoi dettagli',
    'is_a_member' => 'Sei già registrato? Accedi per favore.',
    'not_is_a_meber' => 'Non sei registrato? Inserisci i Tuoi dettagli qui sotto',
    'customer_not_login'=> 'Per procedere al pagamento devi effettuare il login, oppure registrarti',
    'receipt_of_reservation' => 'Ricevuta della Prenotazione',
    'receipt_info' => 'Gentile Ospite, lieti della Sua conferma, Le inviamo la ricevuta della Sua prenotazione che riceverà a breve anche tramite e-mail.',
    'print' => 'Stampa',
    'warning' => 'Attenzione',
    'before_login_msg'=>'<strong>Ciao :name</strong>, grazie per aver effettuato l\'accesso, di seguito trovi il riepilogo della prenotazione e le modalità di pagamento accettate.',
    'password_update' => 'Aggiornamento Password',
    'new_password' => 'Nuova Password',
    'repeat_password' => 'Ripeti Password',
    'update' => 'Aggiorna',
    'token_not_match' => 'Il token inviato non combacia o è scaduto, riprova a inviare una richiesta di reset password',
    'password_updated_successfully' => 'Password aggiornata correttamente'
];
