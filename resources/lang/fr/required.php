<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Request input Language Lines
    |--------------------------------------------------------------------------
    */

    'customer_required' =>  'Il cliente è richiesto',
    'check_in_required' => 'Data di Check-IN richiesta',
    'check_out_required' => 'Data di Check-OUT richiesta',
    'rooms_required' => 'Selezione Camere richiesta',
    'rate_required' => 'Selezione Tariffe richiesta',
    'guests_required'=> 'Selezione Ospiti richiesta',
    'price_required'=> 'Selezione Prezzo richiesta',
    'status_required' => 'Status richiesto',
    'extra_required'=> 'Extra Costo richiesto',
    'extra_price_required'=> 'Prezzo extra richiesto',
    'commission_percentage'=> 'Il campo Percentuale Commissione è richiesto',
    'transaction_code'=> 'Anche il campo Prezzo Costo',
    'title_required'=> 'Titolo richiesto',
    'code_required'=> 'Anche il campo Codice è richiesto',
    'value_required'=> 'Anche il campo Valore è richiesto',
    'value_required_number'=> 'Il Valore deve essere un numero',
    'condition_required'=> 'Le condizioni sono richieste',
    'email_required' => 'Indirizzo email richiesto',
    'email_wrong_format' => 'Indirizzo email non corretto',
    'password_required' => 'La Password è obbligatoria',
    'name_required' => 'Il Nome è richiesto',
    'surname_required' => 'Il Cognome è richiesto',
    'phone_required' => 'Il numero di Telefono è obbligatorio',
    'country_required' => 'La Nazione è Obbligatoria',
    'file_required' => 'Devi specificare il file da caricare',
    'password' => 'You must enter a Password',
    'password_lenght' => 'The password must be at least 8 characters long',
    'password_conf' => 'The password must match with the previously entered password',
    'email_exists' => 'Your email address has already been used, you can request the reset password and receive a new one in the email address used for registration.',
    'amount' => 'Amount is requred'
];
