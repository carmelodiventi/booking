@extends('main')

@section('content')
    @if(isset($error))
        <div class="notes danger">
            <p>{{$error}}</p>
        </div>
    @else

        @if($rooms)
            @php $index = 0 @endphp
            <form method="post" action="{{url('payment')}}">
                {!! csrf_field() !!}
                <div class="row">
                    <div class="column column-75">
                        <div class="block is-in-view is-no-margin">

                            @if (Session::has('customer'))
                                <div class="block-text">
                                    <div class="note">
                                        <p>
                                            {{trans('booking.hello')}} <strong> {{Session::get('customer')->name}}</strong> {{trans('booking.thanks_for_login')}}
                                        </p>
                                    </div>
                                    <div>
                                        <label>{{trans('booking.note_for_hotel')}}</label>
                                        <textarea name="note" rows="5"></textarea>
                                    </div>

                                    @if($payment_method)
                                      <div class="payment">
                                          <label>{!!trans('booking.select_payment_method')!!}</label>
                                          <ul style="list-style: none">
                                            @foreach ($payment_method as $method)

                                              @if($method->active === 'on')
                                              <li>
                                                  <label>
                                                      <input type="radio" name="payment" value="{{$method->name}}">
                                                      {{$method->name}}
                                                  </label>
                                              </li>
                                            @endif
                                            @endforeach
                                          </ul>
                                      </div>
                                    @endif
                                </div>
                            @else
                                <div class="tab">
                                    <div class="tab-pane">
                                        <ul>
                                            <li class="current"><a href="#existingCustomer"
                                                                   onclick="javascript:void(0); showtab(this)">{{trans('booking.register_customer')}}</a>
                                            </li>
                                            <li><a href="#newCustomer"
                                                   onclick="javascript:void(0); showtab(this)">{{trans('booking.new_customer')}}</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="tab-content">
                                        <div class="tab visible" id="existingCustomer">
                                            <div class="row">
                                                <div class="column">
                                                  <p>{{trans('booking.login_with_your_data')}}</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="column column-50">
                                                    <label>{{trans('booking.email')}}</label>
                                                    <div class="datepicker-wrap">
                                                        <input type="email" placeholder="{{trans('booking.email')}}" name="email" required>
                                                    </div>
                                                </div>
                                                <div class="column column-50">
                                                    <label>{{trans('booking.password')}}</label>
                                                    <div class="datepicker-wrap">
                                                        <input type="password"  placeholder="{{trans('booking.password')}}" name="password" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="column">
                                                    <hr>
                                                    <button type="button" class="button" onclick="app.loginCustomer()">{{trans('booking.login')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab" id="newCustomer">
                                            <div class="row">
                                                <div class="column">
                                                  <p>{!!trans('booking.register_new_account')!!}</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="column column-50">
                                                    <label>{{trans('booking.name')}}</label>
                                                    <div class="datepicker-wrap">
                                                        <input type="text" placeholder="{{trans('booking.name')}}"
                                                               name="name" required>
                                                    </div>
                                                </div>
                                                <div class="column column-50">
                                                    <label>{{trans('booking.surname')}}</label>
                                                    <div class="datepicker-wrap">
                                                        <input type="text" placeholder="{{trans('booking.surname')}}"
                                                               name="surname" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="column column-50">
                                                    <label>{{trans('booking.phone')}}</label>
                                                    <div class="datepicker-wrap">
                                                        <input type="tel" placeholder="{{trans('booking.phone')}}"
                                                               name="phone" required>
                                                    </div>
                                                </div>
                                                <div class="column column-50">
                                                    <label>{{trans('booking.address')}}</label>
                                                    <div class="datepicker-wrap">
                                                        <input type="text" placeholder="{{trans('booking.address')}}"
                                                               name="address" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="column column-50">
                                                    <label>{{trans('booking.city')}}</label>
                                                    <div class="datepicker-wrap">
                                                        <input type="text" placeholder="{{trans('booking.city')}}"
                                                               name="city" required>
                                                    </div>
                                                </div>
                                                <div class="column column-50">
                                                    <label>{{trans('booking.country')}}</label>
                                                    <div class="datepicker-wrap">
                                                        <input type="text" placeholder="{{trans('booking.country')}}"
                                                               name="country" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="column column-50">
                                                    <label>{{trans('booking.province')}}</label>
                                                    <div class="datepicker-wrap">
                                                        <input type="text" placeholder="{{trans('booking.province')}}"
                                                               name="province" required>
                                                    </div>
                                                </div>
                                                <div class="column column-50">
                                                    <label>{{trans('booking.email')}}</label>
                                                    <div class="datepicker-wrap">
                                                        <input type="email" placeholder="{{trans('booking.email')}}"
                                                               name="email" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="column column-50">
                                                    <label>{{trans('booking.password')}}</label>
                                                    <div class="datepicker-wrap">
                                                        <input type="password"
                                                               placeholder="{{trans('booking.password')}}"
                                                               name="password" required>
                                                    </div>
                                                </div>
                                                <div class="column column-50">
                                                    <label>{{trans('booking.password_confirm')}}</label>
                                                    <div class="datepicker-wrap">
                                                        <input type="password"
                                                               placeholder="{{trans('booking.password_confirm')}}"
                                                               name="password_confirm" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="column">
                                                    <hr>
                                                    <button type="button" class="button"
                                                            onclick="app.registerCustomer()">{{trans('booking.register')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                        </div>
                    </div>
                    <div class="column column-25">
                        <div class="row">
                            <div class="column">
                                <div class="block is-in-view is-no-margin" id="cart">
                                    <div class="block-text">
                                        <h5>{{trans('booking.reservation_summary')}}</h5>
                                        <div>
                                            <strong>Check-in : {{$check_in}}</strong>
                                        </div>
                                        <div>
                                            <strong>Check-out : {{$check_out}}</strong>
                                        </div>
                                        <div>
                                            <strong>{{ trans('booking.nights') }} : {{$nights or 0 }}</strong>
                                        </div>

                                          @if($rooms)
                                            <div>
                                              <ul id="room-list">
                                                  @foreach($rooms as $room)
                                                      <li item-name="{{$room->type}}">
                                                          <div class="thumbnail">
                                                              {!! Helper::get_single_photo($room->Foto) !!}
                                                          </div>
                                                          <div class="details">
                                                              {{$room->type}} <br>
                                                              <span class='price'>{!! Helper::get_money($room->Rate) !!}</span>
                                                          </div>

                                                      </li>
                                                  @endforeach
                                              </ul>
                                            </div>

                                          @endif


                                        @if ($fees)
                                            <div>
                                                <ul id="fees-list">
                                                    @foreach($fees as $fee)
                                                        <li item-name="{{$fee->name}}">
                                                            <div class="details">
                                                              @if($fee->percentage == 1)
                                                                {{$fee->name}} <sup>(%)</sup>
                                                              @else
                                                                {{$fee->name}}
                                                              @endif
                                                                <br>
                                                                <span class='price'> {!! Helper::get_money($fee->value) !!} </span>
                                                            </div>
                                                        </li>
                                                    @endforeach

                                                </ul>
                                            </div>
                                        @endif
                                        <div>
                                            <strong>{{trans('booking.sub_total')}} :
                                              <span id="subtotal"> {!!Helper::get_money($subtotal)!!}</span>
                                            </strong>
                                        </div>
                                        @if ($fees)
                                        <div>
                                            <strong>{{trans('booking.tax')}} :
                                              <span id="totaltax">{!!Helper::get_money($totaltax)!!}</span>
                                            </strong>
                                        </div>
                                        @endif
                                        <div>
                                            <strong>{{trans('booking.total')}} :
                                              <span id="grandtotal">
                                                {!!Helper::get_money($grandtotal)!!}
                                              </span>
                                            </strong>
                                        </div>
                                        @if (Session::has('customer'))
                                            <button class="button is-expanded" type="submit">
                                                {{trans('booking.confirm_and_pay')}}
                                            </button>
                                        @else
                                            <a href="#newCustomer" class="button is-expanded">
                                                {{trans('booking.login')}}
                                            </a>
                                        @endif
                                        <hr>
                                        <a href="{{url('/')}}"
                                           class="button button-clear is-expanded">
                                           {!!trans('booking.repeat_search')!!}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        @else
            <div class="row">
                <div class="column column-75">
                    <div class="notes danger">
                        <p>{{trans('booking.no_room_available')}}</p>
                    </div>
                </div>
                <div class="column column-25">
                    <div class="block is-no-margin is-in-view">
                        <div class="block-text">
                            <h5>Riepilogo Dati</h5>
                            <div>
                                <strong>Check-in : {{$check_in}}</strong>
                            </div>
                            <div>
                                <strong>Check-out : {{$check_out}}</strong>
                            </div>
                            <div>
                                <strong>Notti : {{$nights or 0 }}</strong>
                            </div>
                            <hr>
                            <div>
                                <a href="{{url('/')}}"
                                   class="button button-clear is-expanded">Ripeti
                                    Ricerca</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    @endif
@stop
