@extends('main')

@push('styles')
@endpush

@section('subheader')
  @include('includes.summary')
  <div class="container">
    @include('includes.dashboard')
  </div>
@stop

@section('content')
  @include('includes.cart')
  @include('includes.cart_bottom')
        @if($rooms_types)
            @php $index = 0 @endphp
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <h5 class="text-uc text-uc text-center">
                      {!!trans('booking.search_result',[
                        'result' => count($rooms_types),
                        'check_in' => date('d M Y', strtotime($check_in)),
                        'check_out'=> date('d M Y', strtotime($check_out)),
                        'n_nights' => $nights
                        ]) !!}
                    </h5>
                  </div>
                  <div class="col-md-12 col-xs-12">
                    {{Form::open(['url' => [LaravelLocalization::localizeURL($property_id . '/rooms-details')], 'id'=>'rooms-details'])}}
                            <ul class="list-room">
                              @foreach($rooms_types as $room_type)
                                <li class="list rooms" data-type-id="{{$room_type->rooms_type_id}}">
                                  <span class="close" onclick="app.hideRate()">
                                      <i class="ti-close"></i>
                                    </span>
                                  <div class="row">
                                    <figure class="col-md-3 col-xs-12" style="background-image:url({{Helper::get_src_photo($room_type->photo)}})">
                                      {!! Helper::get_photo_gallery($room_type->photo) !!}
                                    </figure>
                                    <div class="col-md-9 col-xs-12 details">
                                      <div>
                                        <div>
                                          <h4 class="text-uc">{!! Helper::get_traslated_string($room_type->type) !!}</h4>
                                        </div>
                                          <div>
                                            @if($room_type->units === 1)
                                              <small class="text-uc">
                                                <strong>{{trans('booking.last_room_available')}}</strong>
                                              </small>
                                            @else
                                              <small class="text-uc"><strong> {{trans('booking.rooms_availability',['count'=> $room_type->units ])}} </strong></small>
                                            @endif
                                          </div>
                                      </div>
                                      <div>
                                        <div class="description">
                                            {!! Helper::get_traslated_string($room_type->description) !!}
                                            <i class="ti-user"></i>&nbsp; {{trans('booking.room_guests',['guests'=> $room_type->capacity] )}}
                                        </div>
                                        <div>
                                            <span class="price">
                                              <small> {!!trans('booking.starting_from')!!} </small>
                                              {!! Helper::get_cheepest_price($room_type->rates, $guests) !!}
                                              <small> {!!trans('booking.total_nights')!!} {!!$nights!!} </small>
                                            </span>
                                              <button type="button" class="btn btn-primary btn-info" data-type-id="{{$room_type->rooms_type_id}}" onclick="app.showRate(this);"> {!! trans('booking.details_book')!!} </button>
                                          </div>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="rate-details">

                                        @foreach ($room_type->rates as $rate)
                                       
                                                  <div class="rate-container">
                                                    <div class="rate-result">
                                                      <strong>{!! Helper::get_traslated_string($rate->rate_name)!!}</strong>
                                                      <sup><a href="#" class="show-condition" onclick="app.showCondition(this); return false;"> {!! trans('booking.rate_condition') !!}</a></sup>
                                                      <div class="rate-description" data-rate-id="{{$rate->rate_id}}" data-room-type-id="{{$rate->rooms_type_id}}">
                                                        {!! Helper::get_traslated_string($rate->rate_description)!!}
                                                     </div>
                                                    </div>
                                                    <div class="rate-result">
                                                      <strong class="price">
                                                        @if($rate->single_rate_amount !== null && $guests <= 1)
                                                          {!! Helper::get_money($rate->single_rate_amount) !!}
                                                        @elseif ($rate->rate_amount !== null)
                                                          {!! Helper::get_money($rate->rate_amount) !!}
                                                        @endif
                                                      </strong><br>
                                                      @if($rate->single_rate_amount !== null && $guests <= 1)
                                                        <small> {{trans_choice('booking.price_for_nights_per_person', $nights)}} </small>
                                                      @elseif ($rate->rate_amount !== null && $guests > 1)
                                                        <small> {{trans('booking.price_for_nights',['nights' => $nights, 'guests' => $room_type->capacity])}} </small>
                                                      @endif
                                                    </div>

                                                    @if($rate->single_rate_amount !== null && $guests <= 1)
                                                      <div class="rate-result">{!! Helper::get_book_now($room_type->units, Helper::get_traslated_string($room_type->type), $rate->rooms_type_id,$rate->rate_id, Helper::get_traslated_string($rate->rate_name),$rate->single_rate_amount,$nights,$guests) !!}</div>
                                                    @elseif ($rate->rate_amount !== null)
                                                      <div class="rate-result">{!! Helper::get_book_now($room_type->units, Helper::get_traslated_string($room_type->type), $rate->rooms_type_id,$rate->rate_id, Helper::get_traslated_string($rate->rate_name),$rate->rate_amount,$nights,$guests) !!}</div>
                                                    @endif
                                                  </div>

                                      
                                        @endforeach
                                  </div>

                                </li>
                              @endforeach
                            </ul>
                        <hr>
                    {{Form::close()}}
                    </div>
                </div>
                  <!-- Overlay Modal Room. -->
                  <div class="overlay-modal-room"></div>
                  <!-- Root element of PhotoSwipe. Must have class pswp. -->
                  <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
                      <!-- Background of PhotoSwipe.
                           It's a separate element, as animating opacity is faster than rgba(). -->
                      <div class="pswp__bg"></div>

                      <!-- Slides wrapper with overflow:hidden. -->
                      <div class="pswp__scroll-wrap">

                          <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
                          <!-- don't modify these 3 pswp__item elements, data is added later on. -->
                          <div class="pswp__container">
                              <div class="pswp__item"></div>
                              <div class="pswp__item"></div>
                              <div class="pswp__item"></div>
                          </div>

                          <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
                          <div class="pswp__ui pswp__ui--hidden">

                              <div class="pswp__top-bar">

                                  <!--  Controls are self-explanatory. Order can be changed. -->

                                  <div class="pswp__counter"></div>

                                  <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                                  <button class="pswp__button pswp__button--share" title="Share"></button>

                                  <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                                  <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                                  <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                                  <!-- element will get class pswp__preloader-active when preloader is running -->
                                  <div class="pswp__preloader">
                                      <div class="pswp__preloader__icn">
                                          <div class="pswp__preloader__cut">
                                              <div class="pswp__preloader__donut"></div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                                  <div class="pswp__share-tooltip"></div>
                              </div>

                              <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                              </button>

                              <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                              </button>

                              <div class="pswp__caption">
                                  <div class="pswp__caption__center"></div>
                              </div>

                          </div>

                      </div>
                  </div>
        @else
            <div class="row">
                <div class="col-md-12 col-xs-12">
                  <h5 class="text-uc text-uc text-center">
                    {!!trans('booking.search_result',[
                      'result' =>  count($rooms_types),
                      'check_in' => date('d M Y', strtotime($check_in)),
                      'check_out'=> date('d M Y', strtotime($check_out)),
                      'n_nights' => $nights
                      ]) !!}
                  </h5>

                </div>
            </div>
        @endif
@stop
