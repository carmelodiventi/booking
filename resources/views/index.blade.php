<!DOCTYPE html>
<html style="height: 100%">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="Carmelo Di Venti">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="description" content="Bookingo is an online booking system for Small Hotels,Apartments and Bed & Breakfast, with Channel Manager and Booking Engine">
	<meta name="robots" content="index, follow">
	<meta name="keyword" content="Online Booking System, Simple Accomodation,Bed & Breakfast">
	<title>Booking – Online Booking System </title>
	<link rel="shortcut icon" type="image/png" href="{{asset('resources/assets/favicon.png')}}"/>
	<link rel="stylesheet" href="{{asset('resources/assets/front/css/flexboxgrid.min.css')}}" type="text/css">
	<link rel="stylesheet" href="{{asset('resources/assets/front/css/plugins.css')}}" type="text/css">
	<!-- Primary Style-->
	<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/admin/build/css/fourth-layout.css')}}">
	<link rel="stylesheet" href="{{asset('public/css/landingpage.css')}}" type="text/css">
	<!-- Ty-icons -->
	<link rel="stylesheet" href="{{ asset('resources/assets/front/themify-icons/themify-icons.css')}}">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,700,900">
	<!-- External Style-->
	@stack('styles')

	<!--[if lt IE 9]>
	<script src="docs-assets/assets/js/ie8-responsive-file-warning.js"></script>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

	<div style='text-align:center'><a
	        href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img
	        src="http://storage.ie6countdown.com/resources/assets/100/images/banners/warning_bar_0000_us.jpg" border="0"
	        height="42" width="820"
	        alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/></a>
	</div>
	<![endif]-->
</head>
<body style="background-image: url({{asset('resources/assets/admin/build/images/backgrounds/16.jpg')}})" class="body-bg-full">
	<div class="container page-container">
	    <div class="page-content landingpage">



				<div class="hero">
					<h1>{{trans('landingpage.hero_title')}}</h1>
					<h3>{{trans('landingpage.hero_subtitle')}}</h3>
					<p>{{trans('landingpage.hero_stuff')}}</p>
					<div>
						<span data-sumome-listbuilder-embed-id="929e4573717ddc74345dbfaf258714c1b9b0e15070c459d9fd59fc68f5230ef1"></span>
					</div>
				</div>

				
		</div>
	</div>
	<script src="{{asset('resources/assets/front/js/vendor/jquery.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			openModal = function(){
				$('#request_demo').fadeIn();
			}
			closeModal = function(){
				$('#request_demo').fadeOut();
			}
		});
	</script>
<script async>(function(s,u,m,o,j,v){j=u.createElement(m);v=u.getElementsByTagName(m)[0];j.async=1;j.src=o;j.dataset.sumoSiteId='5aa6d62100bc56f452a0e4a650163201384dfa8e9a5c6aa64985578f5b436555';v.parentNode.insertBefore(j,v)})(window,document,'script','//load.sumo.com/');</script>
</body>
</html>
