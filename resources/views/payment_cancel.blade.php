@extends('main')

@section('content')
    @if(isset($error))
        <div class="notes danger">
            <p>{{$error}}</p>
        </div>
    @else
            <div class="row">
                <div class="col-md-12 col-xs-12">
                  <h1 class="text-uc">{{trans('booking.payment')}}</h1>
                  <div class="alert danger">
                    <p>{{trans('booking.payment_cancel')}}</p>
                  </div>
                  <hr>
                  <div>
                      <a href="{{url($property_id . '/')}}" class="btn btn-primary">{{trans('booking.repeat_search')}}</a>
                  </div>
                </div>
            </div>
    @endif
@stop
