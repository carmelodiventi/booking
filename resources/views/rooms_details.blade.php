@extends('main')

@push('styles')
<style>
.waiting-payment{
  position: fixed;
    top: 0;
    left: 0;
    height:100%;
    width:100%;
    background: #fff;
    z-index: 10;
    display: flex;
    align-items: center;
    justify-content: center;
    visibility:hidden;
}
.waiting-payment h2{
  padding:2em;
  font-weight:200;
}
</style>
@endpush


@push('script')

@endpush

@section('subheader')
  @include('includes.summary')
@stop

@section('content')
        @if($rooms)
            @php $index = 0 @endphp
            @if (!Session::has('customer'))
              <div class="row">
                <div class="col-xs-12">
                  <h1 class="text-uc">{!! trans('booking.your_details') !!}</h1>
                </div>
              </div>
              @include('includes.customers.login')
              @include('includes.customers.register')
              <hr>
            @endif

            {{Form::open(['url' => [LaravelLocalization::localizeURL($property_id . '/payment')], 'method'=>'post', 'id'=>'payment-form', 'name'=> 'payment', 'onsubmit'=>'app.validatePayment(); return false;'])}}

                <div class="row">
                  <div class="col-xs-12">
                    <h1 class="text-uc">{!! trans('booking.payment') !!}</h1>
                    @if (Session::has('customer'))
                      <div class="color-primary"> {!! trans('booking.before_login_msg', ['name'=>Session::get('customer')->name]) !!} </div>
                    @else
                      <div class="text-uc color-light-grey"> {!!trans('booking.there_are')!!} <strong> {{$total_room}} </strong> @choice('booking.items_in_cart_message', $total_room) </div>
                    @endif
                  </div>
                </div>
                @if($rooms)
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="summary-items-wrapper">
                        <ul id="rooms-list">
                        @foreach($rooms as $room)
                            <li item-name="{{Helper::get_traslated_string($room->type)}}">
                                <div class="thumbnail">
                                  <div style="background-image:url({!! Helper::get_src_photo($room->photo) !!})"></div>
                                </div>
                                <div class="details">
                                  <strong class="text-uc">{{Helper::get_traslated_string($room->type)}}</strong>
                                  <sup class="text-uc">{{Helper::get_traslated_string($room->rate_name)}}</sup>
                                  <small class='price'>{!! Helper::get_money($room->rate_amount) !!}</small>
                                </div>
                                <div class="clearfix"></div>
                            </li>
                        @endforeach
                    </ul>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                    <div class="col-xs-12">
                      @if ($fees)
                          <div class="mb">
                              <ul id="fees-list">
                                  @foreach($fees as $fee)
                                      <li item-name="{{$fee->name}}">
                                          <div class="details">
                                            @if($fee->percentage == 1)
                                              {{$fee->name}} <sup>(%)</sup>
                                            @else
                                              {{$fee->name}}
                                            @endif
                                              <br>
                                              <span class='price'> {!! Helper::get_money($fee->value) !!} </span>
                                          </div>
                                      </li>
                                  @endforeach

                              </ul>
                          </div>
                      @endif
                      <div class="mb">
                          <strong class="other-price-title text-uc">{{trans('booking.sub_total')}}</strong>
                          <span class="other-price"> {!!Helper::get_money($subtotal)!!}</span>
                      </div>
                      @if ($fees)
                      <div class="mb">
                          <strong  class="other-price-title text-uc">{{trans('booking.tax')}}</strong>
                          <span class="other-price">{!!Helper::get_money($totaltax)!!}</span>
                      </div>
                      @endif
                      @if ($discount)
                      <div class="mb">
                          <strong  class="other-price-title text-uc">{{trans('booking.discount')}}</strong>
                          <span class="other-price">{!!Helper::get_money($discount)!!}</span>
                      </div>
                      @endif
                      <div class="mb">
                          <span class="text-uc total-price-title">{{trans('booking.total')}}</span> <span class="total-price">{!!Helper::get_money($grandtotal)!!}</span>
                      </div>
                    </div>
                </div>
                <hr>
                            @if (Session::has('customer'))
                              <div class="row">
                                  <div class="col-xs-12 col-md-12">
                                  <div class="form-group">
                                    <strong><a href="javascript::void(0)" onclick="$('.special-request').toggle();" class="text-un text-uc">{{trans('booking.add_special_request')}}</a></strong>
                                  </div>
                                  </div>
                              </div>
                            <hr>
                              <div class="row">
                                <div class="col-xs-12 col-md-12">
                                  <div class="form-group special-request">
                                    <textarea name="note" rows="5" class="form-field" placeholder="{{trans('booking.note_for_hotel')}}"></textarea>
                                  </div>
                                </div>
                              </div>
                              @if($payment_method)
                                 <div class="row">
                                  <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                          <select name="payment" onchange="app.switchPayment(this)" class="form-field text-uc">
                                              <option value="">{{trans('booking.select_payment_method')}}</option>

                                              @foreach ($payment_method as $payment)
                                                @if(isset($payment->active) AND isset($payment->code))
                                                  @if($payment->active === 'on')
                                                    <option value="{{$payment->code}}">{{$payment->name}}</option>
                                                  @endif
                                                @endif
                                              @endforeach
                                          </select>
                                        </div>
                                  </div>
                                  <div class="col-xs-12 col-md-8">
                                    <div id="paypal">
                                      <p>{{trans('booking.paypal_info')}}</p>
                                    </div>
                                    <div id="credit_card">
                                        <div class="form-row">
                                          <div id="card-element">
                                            <!-- a Stripe Element will be inserted here. -->
                                          </div>
                                          <!-- Used to display form errors -->
                                          <div id="card-errors" role="alert"></div>
                                          <input type="hidden" name="stripeToken" value="">
                                        </div>
                                    </div>
                                  </div>
                                </div>
                                 <div class="row">
                                  <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                      <hr>
                                      <button class="btn btn-primary"
                                        id="confirm_and_pay"
                                        type="submit"
                                        data-loading="<i class='ti-reload'></i> {{trans('booking.wait_please')}}">
                                        {{trans('booking.confirm_and_pay')}}
                                      </button>
                                    </div>
                                  </div>
                                </div>
                              @endif
                            @endif
                  {{Form::close()}}
                  <div class="waiting-payment">
                    <h2>{{trans('booking.wait_please_for_payment')}}</h2>
                  </div>
        @else
          <div class="row">
              <div class="col-md-12 col-xs-12">
                  <h3 class="text-uc">
                    {!! trans('booking.search_result',['result' =>  count($rooms), 'check_in' => $check_in, 'check_out'=> $check_out ])!!}
                  </h3>
              </div>
          </div>
        @endif
@stop
