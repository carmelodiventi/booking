<!DOCTYPE html>
<html lang="en" style="height: 100%">
  <head>
      @include('admin/includes.head')
  </head>
  <body style="background-image: url({{asset('resources/assets/admin/build/images/backgrounds/16.jpg')}})" class="body-bg-full">
    <div class="container page-container">
      <div class="page-content">
        <div class="logo"><img src="{{asset('resources/assets/admin/build/images/logo/logo-light.png')}}"></div>
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              @if(Session::has('status'))
                <div class="alert alert-success">
                    {{ Session::get('status') }}
                </div>
              @endif
                <div class="col-md-12">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-Mail Address" required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn-lg btn btn-primary btn-rounded btn-block">
                        Send Password Reset Link
                    </button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </body>
  <script src="{{asset('resources/assets/front/js/vendor/jquery.js') }}"></script>
  <script src="{{asset('resources/assets/front/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
  <script src="{{asset('resources/assets/front/plugins/noty-notifications/noty.min.js')}}"></script>

  @if (Session::has('message_success'))
      <script>
          $.noty.defaults.killer = true;
          noty({
              text: "{{Session::get('message_success')}}",
              layout: 'top',
              closeWith: ['click', 'hover'],
              type: 'success'
          });
      </script>
  @endif

  @if (Session::has('message_error'))
      <script>
          $.noty.defaults.killer = true;
          noty({
              text: "{{Session::get('message_error')}}",
              layout: 'top',
              closeWith: ['click', 'hover'],
              type: 'error'
          });
      </script>
  @endif

  @if (Session::has('message_warning'))
      <script>
          $.noty.defaults.killer = true;
          noty({
              text: "{!!Session::get('message_warning')!!}",
              layout: 'top',
              closeWith: ['click', 'hover'],
              type: 'warning'
          });
      </script>
  @endif

  @if(count($errors->all()) > 0)
      @foreach($errors->all() as $e)
          <script>
              $.noty.defaults.killer = true;
              noty({
                  text: "{{$e}}",
                  layout: 'top',
                  closeWith: ['click', 'hover'],
                  type: 'warning'
              });
          </script>
      @endforeach

  @endif
</html>
