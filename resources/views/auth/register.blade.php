<!DOCTYPE html>
<html lang="en" style="height: 100%">
  <head>
      @include('admin/includes.head')
  </head>
  <body style="background-image: url({{asset('resources/assets/admin/build/images/backgrounds/16.jpg')}})" class="body-bg-full">
    <div class="container page-container">
      <div class="page-content">
        <div class="logo"><img src="{{asset('resources/assets/admin/build/images/logo/logo-light.png')}}"></div>
        <form method="post" action="{{url('/register')}}" class="form-horizontal">
          {!! csrf_field() !!}
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              <input type="text" placeholder="Name" class="form-control" name="name"
                     value="{{ old('name') }}">
                     @if ($errors->has('name'))
                         <span class="help-block">
                             <strong>{{ $errors->first('name') }}</strong>
                         </span>
                     @endif
          </div>

          <div class="form-group">
              <div class="col-xs-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" placeholder="Email" class="form-control" name="email" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                    <p class="help-text">
                       <strong>{{ $errors->first('email') }}</strong>
                    </p>
                    @endif
              </div>
          </div>
          <div class="form-group">
              <div class="col-xs-12 {{ $errors->has('password') ? ' has-error' : '' }}">

                  <input type="password"
                         placeholder="Password"
                         class="form-control"
                         name="password"
                         id="password">
                         @if ($errors->has('password'))
                              <p class="help-text">
                                 <strong>{{ $errors->first('password') }}</strong>
                             </p>
                         @endif
              </div>
          </div>

          <div class="form-group">
              <div class="col-xs-12 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">

                  <input type="password"
                         placeholder="Password Confirmation"
                         class="form-control"
                         name="password_confirmation"
                         id="password_confirmation">
                         @if ($errors->has('password_confirmation'))
                              <p class="help-text">
                                 <strong>{{ $errors->first('password_confirmation') }}</strong>
                             </p>
                         @endif
              </div>
          </div>
          <div class="form-group">
            <div class="col-xs-12">
              <div class="checkbox-inline checkbox-custom pull-left">
                <input id="exampleCheckboxRemember" type="checkbox" name="remember" value="remember">
                <label for="exampleCheckboxRemember" class="checkbox-muted text-muted">Remember me</label>
              </div>
              <div class="pull-right"> <a href="{{url('login')}}">Sign in</a> </div>
            </div>
          </div>
          <button type="submit" class="btn-lg btn btn-primary btn-rounded btn-block">Sign up</button>
        </form>
      </div>
    </div>
  </body>
  <script src="{{asset('resources/assets/front/js/vendor/jquery.js') }}"></script>
  <script src="{{asset('resources/assets/front/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
  <script src="{{asset('resources/assets/front/plugins/noty-notifications/noty.min.js')}}"></script>

  @if (Session::has('message_success'))
      <script>
          $.noty.defaults.killer = true;
          noty({
              text: "{{Session::get('message_success')}}",
              layout: 'top',
              closeWith: ['click', 'hover'],
              type: 'success'
          });
      </script>
  @endif

  @if (Session::has('message_error'))
      <script>
          $.noty.defaults.killer = true;
          noty({
              text: "{{Session::get('message_error')}}",
              layout: 'top',
              closeWith: ['click', 'hover'],
              type: 'error'
          });
      </script>
  @endif

  @if (Session::has('message_warning'))
      <script>
          $.noty.defaults.killer = true;
          noty({
              text: "{!!Session::get('message_warning')!!}",
              layout: 'top',
              closeWith: ['click', 'hover'],
              type: 'warning'
          });
      </script>
  @endif

  @if(count($errors->all()) > 0)
      @foreach($errors->all() as $e)
          <script>
              $.noty.defaults.killer = true;
              noty({
                  text: "{{$e}}",
                  layout: 'top',
                  closeWith: ['click', 'hover'],
                  type: 'warning'
              });
          </script>
      @endforeach

  @endif
</html>
