<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body>
    @include('includes.header')
    @include('includes.customers.passwords.reset-modal')
    <!-- begin subheader -->
    @yield('subheader')
    <!-- end subheader -->
    <main id="main" class="site-main @if(isset($css_class)) home @endif" role="main">
        <div class="container-fluid">
            @yield('content')
        </div>
    </main>
    @include('includes.footer')
</body>
</html>
