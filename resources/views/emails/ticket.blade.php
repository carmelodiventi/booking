<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
<style>
.invoice-box {
    max-width: 800px;
    margin: 15px auto;
    padding: 30px;
    border: 1px solid #eee;
    background: #fff;
    box-shadow: 0 0 10px rgba(0, 0, 0, .15);
    font-size: 16px;
    line-height: 24px;
    font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    color: #555;
}

.invoice-box table {
    width: 100%;
    line-height: inherit;
    text-align: left;
}

.invoice-box table td {
    padding: 5px;
    vertical-align: top;
}

.invoice-box table tr td:nth-child(2) {
    text-align: right;
}

.invoice-box table tr.top table td {
    padding-bottom: 20px;
}

.invoice-box table tr.top table td.title {
    font-size: 45px;
    line-height: 45px;
    color: #333;
}

.invoice-box table tr.information table td {
    padding-bottom: 40px;
}

.invoice-box table tr.heading td {
    background: #eee;
    border-bottom: 1px solid #ddd;
    font-weight: bold;
}

.invoice-box table tr.details td {
    padding-bottom: 20px;
}

.invoice-box table tr.item td {
    border-bottom: 1px solid #eee;
}

.invoice-box table tr.item.last td {
    border-bottom: none;
}

.invoice-box table tr.total td:nth-child(2) {
    border-top: 2px solid #eee;
    font-weight: bold;
}

@media only screen and (max-width: 600px) {
    .invoice-box table tr.top table td {
        width: 100%;
        display: block;
        text-align: center;
    }

    .invoice-box table tr.information table td {
        width: 100%;
        display: block;
        text-align: center;
    }
}
</style>
</head>
<body style="width: 100% !important;min-width: 100%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100% !important;margin: 0;padding: 0;background-color: #FFFFFF">

    <table style="margin: 0 auto;">
      <tr><td class="invoice-box" style="max-width: 800px;margin: 15px auto;padding: 30px;border: 1px solid #eee;background: #fff;box-shadow: 0 0 10px rgba(0, 0, 0, .15);font-size: 16px;line-height: 24px;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;color: #555;">
      <table cellpadding="0" cellspacing="0" style="width: 100%;line-height: inherit;text-align: left;">
          <tr class="top">
              <td colspan="2" style="padding: 5px;vertical-align: top;">
                  <table style="width: 100%;line-height: inherit;text-align: left;">
                      <tr>
                          <td class="title" style="padding: 5px;vertical-align: top;padding-bottom: 20px;font-size: 45px;line-height: 45px;color: #333;">
                              <img src="{!!asset('resources/assets/front/img/logo_extended.png')!!}" style="max-width: 250px;">
                          </td>

                          <td style="padding: 5px;vertical-align: top;text-align: right;padding-bottom: 20px;">
                              PNR #: <strong>{!!$pnr!!}</strong><br>
                              Created: <strong>{!!$date!!}</strong><br>
                          </td>
                      </tr>
                  </table>
              </td>
          </tr>

          <tr class="information">
              <td colspan="2" style="padding: 5px;vertical-align: top;">
                  <table style="width: 100%;line-height: inherit;text-align: left;">
                      <tr>
                          <td style="padding: 5px;vertical-align: top;padding-bottom: 40px;">
                              @if($hotel)
                                  {!!$hotel->name!!}<br>
                                  {!! $hotel->address!!}<br>
                                  {!!$hotel->city!!}
                                  , {!!$hotel->country!!} {!! $hotel->postalcode!!}
                              @endif
                          </td>

                          <td style="padding: 5px;vertical-align: top;text-align: right;padding-bottom: 40px;">
                              {!!$customer->name . '&nbsp;' . $customer->surname!!}<br>
                              {!!$customer->email!!}
                          </td>
                      </tr>
                  </table>
              </td>
          </tr>

          <tr class="heading">
              <td style="padding: 5px;vertical-align: top;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;">
                  Check-IN
              </td>

              <td style="padding: 5px;vertical-align: top;text-align: right;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;">
                  Check-OUT
              </td>
          </tr>

          <tr class="details">
            <td style="padding: 5px;vertical-align: top;padding-bottom: 20px;">
                {{ Carbon::createFromFormat('Y-m-d', $check_in)->formatLocalized('%A %d %B %Y')}}
            </td>
            <td style="padding: 5px;vertical-align: top;text-align: right;padding-bottom: 20px;">
                {{ Carbon::createFromFormat('Y-m-d', $check_out)->formatLocalized('%A %d %B %Y')}}
            </td>
          </tr>

          <tr class="details">
              <td style="padding: 5px;vertical-align: top;padding-bottom: 20px;">

              </td>

              <td style="padding: 5px;vertical-align: top;text-align: right;padding-bottom: 20px;">
                  <strong>{!!trans('booking.total_nights')!!} {!!$nights!!} </strong>
              </td>
          </tr>

          @if ($rooms)
              <tr class="heading">
                  <td colspan="2" style="padding: 5px;vertical-align: top;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;">
                      {!!trans('booking.rooms')!!}
                  </td>
              </tr>

              @foreach ($rooms as $room)

                  <tr class="item">
                      <td style="padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;">
                          <strong>{{Helper::get_traslated_string($room->type)}}</strong>
                          <p>{{Helper::get_traslated_string($room->rate_name)}}</p>
                      </td>
                      <td style="padding: 5px;vertical-align: top;text-align: right;border-bottom: 1px solid #eee;">
                        @if ($room->rate_amount)
                          <strong>{!!Helper::get_money($room->rate_amount)!!}</strong>
                        @endif
                      </td>
                  </tr>

              @endforeach
          @endif
          <tr>
            <td colspan="2" style="padding: 5px;vertical-align: top;">
              <hr>
            </td>
          </tr>
          <tr class="heading">
              <td colspan="2" style="padding: 5px;vertical-align: top;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;">
                  {!!trans('booking.special_request')!!}
              </td>
          </tr>
          <tr class="details">
              <td colspan="2" style="padding: 5px;vertical-align: top;padding-bottom: 20px;">
                  {!!$note!!}
              </td>
          </tr>
          @if($payment_selected)
          <tr class="heading">
              <td colspan="2" style="padding: 5px;vertical-align: top;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;">
                  {!!trans('booking.payment_method')!!}
              </td>
          </tr>
          <tr class="details">
              <td colspan="2" style="padding: 5px;vertical-align: top;padding-bottom: 20px;">
                @php
                    switch ($payment_selected) {
                    case 1:
                      print trans('booking.credit_card_condition');
                    break;
                    case 2:
                      print trans('booking.paypal_payment_info');
                    break;
                    default:
                    //code to be executed
                    }
                @endphp
              </td>
          </tr>
          @endif
          @if ($offert)
          <tr class="heading">
              <td colspan="2" style="padding: 5px;vertical-align: top;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;">
                  {!!trans('booking.offert')!!}
              </td>
          </tr>
          <tr class="details">
              <td colspan="2" style="padding: 5px;vertical-align: top;padding-bottom: 20px;">
                  {!!$offert->description!!}
              </td>
          </tr>
          @endif
          @if ($fees)

              <tr class="heading">
                  <td style="padding: 5px;vertical-align: top;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;">
                      {!!trans('booking.fees')!!}
                  </td>
                  <td style="padding: 5px;vertical-align: top;text-align: right;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;"> {!!trans('booking.price')!!}</td>
              </tr>

              @foreach ($fees as $fee)

                  <tr class="item">
                      <td style="padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;">
                          {!!$fee->name!!}
                      </td>
                      <td style="padding: 5px;vertical-align: top;text-align: right;border-bottom: 1px solid #eee;"> {!!Helper::get_money($fee->value)!!} </td>
                  </tr>

              @endforeach
          @endif
          <tr class="heading">
              <td colspan="2" style="padding: 5px;vertical-align: top;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;">
                  {!!trans('booking.payment_summary')!!}
              </td>
          </tr>
          @if($discount)
              <tr class="item">
                  <td style="padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;">
                      {!!trans('booking.discount')!!}
                  </td>

                  <td style="padding: 5px;vertical-align: top;text-align: right;border-bottom: 1px solid #eee;">
                    {!!Helper::get_money($discount)!!}
                  </td>
              </tr>
          @endif
          <tr class="total">
              <td style="padding: 5px;vertical-align: top;"></td>

              <td style="padding: 5px;vertical-align: top;text-align: right;border-top: 2px solid #eee;font-weight: bold;">
                  {!!trans('booking.total_tax')!!}: {!!Helper::get_money($totaltax)!!}
              </td>
          </tr>
          <tr class="total">
              <td style="padding: 5px;vertical-align: top;"></td>

              <td style="padding: 5px;vertical-align: top;text-align: right;border-top: 2px solid #eee;font-weight: bold;">
                  {!!trans('booking.sub_total')!!}:  {!!Helper::get_money($subtotal)!!}

              </td>
          </tr>
          <tr class="total">
              <td style="padding: 5px;vertical-align: top;"></td>

              <td style="padding: 5px;vertical-align: top;text-align: right;border-top: 2px solid #eee;font-weight: bold;">
                  {!!trans('booking.total')!!}: {!!Helper::get_money($grandtotal)!!}
              </td>
          </tr>
      </table>
      </td>
      </tr>
    </table>
</body>
</html>
