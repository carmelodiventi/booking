<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
<meta charset="UTF-8">
<title>New Registration</title>
</head>
<body style="width: 100% !important;min-width: 100%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100% !important;margin: 0;padding: 0;background-color: #FFFFFF">
  <table width="100%" cellpadding="0" cellspacing="0">
        <tbody><tr>
            <td style="width:100%;margin:0;padding:0;background-color:#f2f4f6" align="center">
                <table width="100%" cellpadding="0" cellspacing="0">

                    <tbody><tr>
                        <td style="padding:25px 0;text-align:center">
                            <a style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;font-size:16px;font-weight:bold;color:#2f3133;text-decoration:none" href="https://novabeds.com" target="_blank">
                                {{$hotel->name}}
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:100%;margin:0;padding:0;border-top:1px solid #edeff2;border-bottom:1px solid #edeff2;background-color:#fff" width="100%">
                            <table style="width:auto;max-width:570px;margin:0 auto;padding:0" align="center" width="570" cellpadding="0" cellspacing="0">
                                <tbody><tr>
                                    <td style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;padding:35px">
                                      <span><h1 style="margin-top:0;color:#2f3133;font-size:19px;font-weight:bold;text-align:left">{{trans('emails.hi')}}</h1>
                                              <p style="margin-top:0;color:#74787e;font-size:16px;line-height:1.5em">{!! trans('emails.greatings_registration_message',['name'=> $name]) !!}</p>
                                              <table style="width:100%;margin:30px auto;padding:0;" width="100%" cellpadding="0" cellspacing="0">
                                                <tbody>
                                                  <tr>
                                                    <td>
                                                      <p style="margin-top:0;color:#74787e;font-size:16px;line-height:1.5em">{{trans('emails.login_data')}}</p>
                                                      <p style="margin-top:0;color:#74787e;font-size:16px;line-height:1.5em">{{trans('emails.email')}}: <strong>{{$email}}</strong></p>
                                                      <p style="margin-top:0;color:#74787e;font-size:16px;line-height:1.5em">{{trans('emails.password')}}: <strong>{{$password}}</strong></p>
                                                    </td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            <p style="margin-top:0;color:#74787e;font-size:16px;line-height:1.5em">Regards,<br> {{$hotel->name}} </p></span>
                                  </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>


                    <tr>
                        <td>
                            <table style="width:auto;max-width:570px;margin:0 auto;padding:0;text-align:center" align="center" width="570" cellpadding="0" cellspacing="0">
                                <tbody><tr>
                                    <td style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;color:#aeaeae;padding:35px;text-align:center">
                                        <p style="margin-top:0;color:#74787e;font-size:12px;line-height:1.5em"> © {{date("Y")}} <a style="color:#3869d4" href="https://novabeds.com" target="_blank">novabeds.com</a>. All rights reserved.</p>
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody></table>
</body>
</html>
