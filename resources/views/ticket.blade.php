@extends('main')

@push('styles')
  <style>
    @media print{

      body {
        font-size: 12px;
      }

      #masthead,
      a.btn,
      button.btn {
        display: none;
      }

      h1 {
        font-size: 24px;
      }

      strong.h2 {
        font-size: 20px;
      }

      strong.h4 {
          font-size: 14px;
      }

      body {transform: scale(.8);}
      table {page-break-inside: avoid;}

    }
  </style>
@endpush

@push('script')
<script>
function printElem(elem){
   window.print();
 }
</script>
@endpush

@section('content')
    @if(isset($error))
        <div class="notes danger">
            <p>{{$error}}</p>
        </div>
    @else
      <div class="row">
          <div class="col-xs-12">
              <h1 class="text-uc">{!! trans('booking.receipt_of_reservation') !!}</h1>
              <h5 class="color-primary">{{trans('booking.receipt_info')}}</h5>
          </div>
      </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="invoice-box" id="invoice">
                    <table cellpadding="0" cellspacing="0">
                        <tr class="top">
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td class="title">
                                          @if($hotel)
                                            <img src="{{$hotel->logo}}" style="max-width: 250px;">
                                          @endif
                                        </td>

                                        <td>
                                            PNR #: <strong>{{$pnr}}</strong><br>
                                            Created: <strong>{{$date}}</strong><br>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr class="information">
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            @if($hotel)
                                                {{$hotel->name}}<br>
                                                {{ $hotel->address}}<br>
                                                {{$hotel->city}}
                                                , {{$hotel->country}} {{ $hotel->postalcode}}
                                            @endif
                                        </td>

                                        <td>
                                            {{$customer->name . '&nbsp;' . $customer->surname}}<br>
                                            {{$customer->email}}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr class="heading">
                          <td>
                                <strong class="text-uc h4">{{trans('booking.check_in')}}</strong>
                          </td>
                          <td>
                                <strong class="text-uc h4">{{trans('booking.check_out')}}</strong>
                          </td>

                        </tr>
                        <tr class="details">
                          <td>
                              {{ Carbon::createFromFormat('Y-m-d', $check_in)->formatLocalized('%A %d %B %Y')}}
                          </td>
                          <td>
                              {{ Carbon::createFromFormat('Y-m-d', $check_out)->formatLocalized('%A %d %B %Y')}}
                          </td>

                        </tr>
                        <tr class="heading">
                          <td>
                                <strong class="text-uc h4">{{trans('booking.total_nights')}}</strong>
                          </td>
                          <td>
                                <strong class="text-uc h4">{{$nights}}</strong>
                          </td>

                        </tr>
                        @if ($rooms)
                            <tr class="heading">
                                <td colspan="2">
                                  <strong class="text-uc h4">{{trans('booking.rooms')}}</strong>
                                </td>
                            </tr>

                            @foreach ($rooms as $room)

                                <tr class="item">
                                    <td>
                                        <strong>{{Helper::get_traslated_string($room->type)}}</strong>
                                        <small>{{Helper::get_traslated_string($room->rate_name)}}</small>
                                    </td>
                                    <td>
                                        <strong>{!!Helper::get_money($room->rate_amount)!!}</strong>
                                    </td>
                                </tr>

                            @endforeach
                        @endif
                        <tr>
                          <td colspan="2">
                            <hr>
                          </td>
                        </tr>
                        <tr class="heading">
                            <td colspan="2">
                              <strong class="text-uc h4">{{trans('booking.special_request')}}</strong>
                            </td>
                        </tr>
                        <tr class="details">
                            <td colspan="2">
                                <blockquote>{{$note}}</blockquote>
                            </td>
                        </tr>
                        @if($payment_selected)
                        <tr class="heading">
                            <td colspan="2">
                                <strong class="text-uc h4">{{trans('booking.payment_method')}}</strong>
                            </td>
                        </tr>
                        <tr class="details">
                            <td colspan="2">
                              @php
                                  switch ($payment_selected) {
                                  case 1:
                                    print trans('booking.credit_card_condition');
                                  break;
                                  case 2:
                                    print trans('booking.paypal_payment_info');
                                  break;
                                  default:
                                  //code to be executed
                                  }
                              @endphp
                            </td>
                        </tr>
                        @endif
                        @if ($offert)
                        <tr class="heading">
                            <td colspan="2">
                              <strong class="text-uc h4">{{trans('booking.offert')}}</strong>
                            </td>
                        </tr>
                        <tr class="details">
                            <td colspan="2">
                                {!!$offert->description!!}
                            </td>
                        </tr>
                        @endif
                        @if (count($fees))
                            <tr class="heading">
                                <td>
                                    <strong class="text-uc h4">{{trans('booking.fees')}}</strong>
                                </td>
                                <td>
                                    <strong class="text-uc h4">{{trans('booking.price')}}</strong>
                                </td>
                            </tr>
                            @foreach ($fees as $fee)

                                <tr class="item">
                                    <td>
                                        {{$fee->name}}
                                    </td>
                                    <td> {!!Helper::get_money($fee->value)!!} </td>
                                </tr>
                            @endforeach
                        @endif
                        <tr>
                            <td>
                                <strong class="text-uc h2">{{trans('booking.payment_summary')}}</strong>
                            </td>
                            <td>
                              <strong class="text-uc h4">{{trans('booking.price')}}</strong>
                            </td>
                        </tr>
                        @if($discount)
                            <tr class="item">
                                <td>
                                  <strong class="text-uc h2">{{trans('booking.discount')}}</strong>
                                </td>

                                <td>
                                  <strong class="text-uc h2">{!!Helper::get_money($discount)!!}</strong>
                                </td>
                            </tr>
                        @endif
                        <tr class="total">
                            <td><strong class="text-uc h2">{{trans('booking.total_tax')}}</strong></td>
                            <td>
                              <strong class="text-uc h2">{!!Helper::get_money($totaltax)!!}</strong>
                            </td>
                        </tr>
                        <tr class="total">
                            <td><strong class="text-uc h2">{{trans('booking.sub_total')}}</strong></td>
                            <td><strong class="text-uc h2">{!! Helper::get_money($subtotal) !!}</strong></td>
                        </tr>
                        <tr class="total">
                            <td><strong class="text-uc h2">{{trans('booking.total')}}</strong></td>
                            <td><strong class="text-uc h2">{!!Helper::get_money($grandtotal)!!}</strong></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12" style="text-align: center">
                <button class="btn btn-default" onclick="printElem('invoice')">{{trans('booking.print')}}</button>
                <a href="{{ url($property_id . '/') }}" class="btn btn-primary">{{trans('booking.back_to_home')}}</a>
            </div>
        </div>
    @endif
@stop
