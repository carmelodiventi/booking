@extends('main')

@section('content')
  <div class="row">
    <div class="col-xs-12">
      <h1 class="text-uc">{!! trans('booking.password_update') !!}</h1>
    </div>
</div>
@if($customer)
  {{Form::open(['url'=> 'customer/password/update'.'/'.$token])}}
   <div class="row">
       <div class="col-xs-12 col-md-12">
         <div class="form-group">
           {{Form::password('password',[ 'class'=> 'form-field', 'placeholder'=>trans('booking.new_password')])}}
         </div>
       </div>
       <div class="col-xs-12 col-md-12">
         <div class="form-group">
           {{Form::password('password_confirmation',[ 'class'=> 'form-field', 'placeholder'=>trans('booking.repeat_password')])}}
         </div>
       </div>
       <div class="col-xs-12 col-md-12">
         <div class="form-group">
           {{Form::submit(trans('booking.update'),['class'=> 'btn btn-primary'])}}
         </div>
       </div>
   </div>
  {{Form::close()}}
@else
<div class="alert danger">
  <strong>{{trans('booking.warning')}}</strong>
  <p>{{trans('booking.token_not_match')}}</p>
</div>

<div class="row">
    <div class="col-xs-12 col-md-12">
      <div class="form-group">
        <a href="{{url('/')}}" class="btn btn-primary">{{trans('booking.back_to_home')}}</a>
      </div>
     </div>
     </div>
@endif


@stop
