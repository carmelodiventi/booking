<form method="get"
id="form-sidebar"
action="{{url(LaravelLocalization::getCurrentLocale().'/rooms-available')}}"
onsubmit="validateForm(); return false;">
    {!! csrf_field() !!}
          <div class="form-group">
            <input type="text" value="{{$check_in}}" name="check_in" placeholder="{!!trans('booking.check_in')!!}" class="form-field">
          </div>
          <div class="form-group">
            <input type="text" value="{{$check_out}}" name="check_out" placeholder="{!!trans('booking.check_out')!!}" class="form-field">
          </div>
          <div class="form-group">
          <select name="guests" class="form-field">
              <option value="">{{ trans('booking.guests') }}</option>
              <option value="1"> 1</option>
              <option value="2"> 2</option>
              <option value="3"> 3</option>
              <option value="4"> 4</option>
          </select>
          </div>
          <div class="form-group">
            <input type="text"
                   placeholder="{{ trans('booking.insert_a_discount_code') }}"
                   class="form-field"
                   name="offert_code"
                   value="{{$offert_code}}"
                   onchange="app.validateOffertCode()">
             <span for="offert_code" class="floating-label">{{ trans('booking.insert_a_discount_code') }}</span>
          </div>
          <hr>
          <div class="form-group">
            <button class="btn btn-primary btn-block">{!!trans('booking.repeat_search')!!} <span class="btn-item"> <i class="ion-ios-arrow-thin-right"></i> </span> </button>
          </div>
</form>

<div id="cart">
  <ul id="room-list"></ul>
  <strong>{{trans('booking.total')}} :  <span id="total">{!! Helper::get_money(0) !!}</span></strong>
</div>
