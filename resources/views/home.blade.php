@extends('main')

@push('styles')

@endpush

@section('content')
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div id="hero">
          <h1 class="hero-title">{{trans('booking.hero_title')}}</h1>
          <h3 class="hero-subtitle">{{trans('booking.hero_subtitle')}}</h2>
        </div>
        @include('includes.dashboard')
    </div>
    </div>
@stop
