<meta charset="utf-8">
<meta name="description" content="">
<meta name="author" content="Carmelo Di Venti">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>Nova Beds – {!! Helper::get_setting('hotel_data')->name !!} </title>
<link rel="shortcut icon" type="image/png" href="{{asset('resources/assets/favicon.png')}}"/>
<link rel="stylesheet" href="{{asset('resources/assets/front/css/normalize.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('resources/assets/front/css/flexboxgrid.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('resources/assets/front/css/tether.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('resources/assets/front/css/drop-theme-basic.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('resources/assets/front/css/drop-theme-hubspot-popovers.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('resources/assets/front/css/tooltip-theme-arrows.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('resources/assets/front/css/plugins.css')}}" type="text/css">
<link rel="stylesheet" href="{{ asset('resources/assets/front/plugins/noty-notifications/noty.css')}}" type="text/css">
<!-- Ty-icons -->
<link rel="stylesheet" href="{{ asset('resources/assets/front/themify-icons/themify-icons.css')}}">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,700,900">
<!-- External Style-->
@stack('styles')
<!-- App Style-->
@if(isset(Helper::get_setting('theme')->name))
  <link rel="stylesheet" href="{{asset('resources/assets/front/css/' . Helper::get_setting('theme')->name)}}" type="text/css">
@else
  <link rel="stylesheet" href="{{asset('resources/assets/front/css/app.css')}}" type="text/css">
@endif
<script>
  var property =  {
    id: '{{Helper::get_property()}}'
  }
</script>
<!--[if lt IE 9]>
<script src="docs-assets/assets/js/ie8-responsive-file-warning.js"></script>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

<div style='text-align:center'><a
        href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img
        src="http://storage.ie6countdown.com/resources/assets/100/images/banners/warning_bar_0000_us.jpg" border="0"
        height="42" width="820"
        alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/></a>
</div>
<![endif]-->
@if(Helper::get_setting('stripe')->active == 'on')
    <script src="https://js.stripe.com/v3/"></script>
    <script>
      var stripe = Stripe('{{Helper::get_setting('stripe')->publishable_key}}');
      var card;
    </script>
@endif