@push('script')
<!-- Moment-->
<script src="{{ asset('resources/assets/front/plugins/moment/min/moment.min.js')}}"></script>
<script src="{{asset('resources/assets/front/plugins/datedropper/datedropper.pro.min.js')}}"></script>
<script>
  $(document).ready(function() {
  $('input.roundtrip').dateDropper({
    theme: 'my-theme',
    roundtrip: true,
    format: 'Y-m-d',
    lang: '{{ LaravelLocalization::getCurrentLocale() }}',
    minDate: moment().format('MM/DD/YYYY')
  });
});
</script>
@endpush

<div id="dashboard">
  {{Form::open(['url' => [LaravelLocalization::localizeURL($property_id . '/rooms-availability')], 'method'=>'get', 'onsubmit'=>'app.validateForm(); return false;'])}}
  <div class="row">
    <div class="col-md-2 col-xs-12">
      <div class="form-group">
        <input type="text" 
          placeholder="{{ trans('booking.check_in') }}" 
          class="form-field roundtrip"
          name="check_in" 
          value="@if(Request::input('check_in')){{Request::input('check_in')}}@endif" 
          readonly 
          required/>

      </div>
    </div>
    <div class="col-md-2 col-xs-12">
      <div class="form-group">
        <input type="text" 
        placeholder="{{ trans('booking.check_out') }}" 
        class="form-field roundtrip"
        name="check_out" 
        value="@if(Request::input('check_out')){{Request::input('check_out')}}@endif" 
        readonly 
        required/>
      </div>
    </div>
    <div class="col-md-2 col-xs-12">
      <div class="form-group">
        {{Form::select('guests', [1 => 1, 2 => 2, 3 => 3, 4 => 4], Request::input('guests'), ['class'=>'form-field','placeholder'=> trans('booking.guests'), 'required' => true])}}
      </div>
    </div>
    <div class="col-md-3 col-xs-12">
      <div class="form-group">
        <input type="text" placeholder="{{ trans('booking.insert_a_discount_code') }}" class="form-field"
          name="offert_code" value="@if(Request::input('offert_code')) {{Request::input('offert_code')}} @endif"
          onchange="app.validateOffertCode()">
      </div>
    </div>
    <div class="col-md-3 col-xs-12">
      <div class="form-group">
        <button class="btn btn-primary btn-icon btn-block">{{ trans('booking.search_avaibility') }} <span
            class="btn-item"> <i class="ti-angle-right"></i> </span> </button>
      </div>
    </div>
  </div>
  {{Form::close()}}
</div>