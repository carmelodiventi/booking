<div class="bt-cart-container empty">
<div class="container">
<div class="row">
<div class="col-xs-12 col-md-12">
<!-- begin bottom cart-->
  <button type="submit"
  class="btn-continue"
  onclick="document.getElementById('rooms-details').submit();">{{trans('booking.confirm_and_pay')}} &nbsp; <span data-total></span> </button>
  <a href="javascript::void(0)" class="bt-cart-trigger" onclick="app.showCart()">
    {{trans('booking.show_summary')}}
    <span class="count" data-cart></span>
  </a>
<!-- end bottom cart-->
</div>
</div>
</div>
</div>
