
<script>
  var base_url = "{{url('/')}}";
  var locale = "{{LaravelLocalization::getCurrentLocale()}}";
</script>

<script src="{{asset('resources/assets/front/js/vendor/jquery.js') }}"></script>
<script src="{{asset('resources/assets/front/js/plugins.js') }}"></script>
<script src="{{asset('resources/assets/front/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{asset('resources/assets/front/plugins/noty-notifications/noty.min.js')}}"></script>
<script src="{{asset('resources/assets/front/js/language/'.LaravelLocalization::getCurrentLocale().'.js') }}"></script>
<script src="{{asset('resources/assets/front/js/vendor/tether.min.js') }}"></script>
<script src="{{asset('resources/assets/front/js/vendor/drop.min.js') }}"></script>
<script src="{{asset('resources/assets/front/js/vendor/tooltip.min.js') }}"></script>
<script src="{{asset('resources/assets/front/js/function.js') }}"></script>
@stack('script')

@if (Session::has('message_success'))
    <script>
        $.noty.defaults.killer = true;
        noty({
            text: "{{Session::get('message_success')}}",
            layout: 'top',
            closeWith: ['click', 'hover'],
            type: 'success'
        });
    </script>
@endif

@if (Session::has('message_error'))
    <script>
        $.noty.defaults.killer = true;
        noty({
            text: "{{Session::get('message_error')}}",
            layout: 'top',
            closeWith: ['click', 'hover'],
            type: 'error'
        });
    </script>
@endif

@if (Session::has('message_warning'))
    <script>
        $.noty.defaults.killer = true;
        noty({
            text: "{!!Session::get('message_warning')!!}",
            layout: 'top',
            closeWith: ['click', 'hover'],
            type: 'warning'
        });
    </script>
@endif

@if(count($errors->all()) > 0)
    @foreach($errors->all() as $e)
        <script>
            $.noty.defaults.killer = true;
            noty({
                text: "{{$e}}",
                layout: 'top',
                closeWith: ['click', 'hover'],
                type: 'warning'
            });
        </script>
    @endforeach

@endif
