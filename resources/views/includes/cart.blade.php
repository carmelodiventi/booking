<div class="overlay-cart">
  <div id="cart">
        <div class="cart-content">
          <i class="ti-close" onclick="app.closeCart()"></i>
          <h1 class="text-uc">{{trans('booking.reservation_summary')}}</h1>
          <ul id="rooms-list"></ul>
          <div class="payment-summary">
            <div class="mb">
              <span class="text-uc total-price-title">{{trans('booking.total')}}</span> <span class="total-price" data-total></span>
            </div>
            <div class="mb">
              <hr>
            </div>
            <div class="mb">
                <button type="submit" class="btn btn-primary btn-square" onclick="document.getElementById('rooms-details').submit();">{{trans('booking.confirm_and_pay')}}</button>
                <button type="button" class="btn btn-outline btn-square" onclick="app.closeCart()">{{trans('booking.continue_booking')}}</button>
            </div>
          </div>
        </div>
  </div>
</div>
