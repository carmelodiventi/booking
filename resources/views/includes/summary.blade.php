<div id="summary-bar">
  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-12 col-md-12">
          <ul>
            @if($check_in)
              <li><strong>{{trans('booking.check_in')}}</strong> : {{ Carbon::createFromFormat('Y-m-d', $check_in)->formatLocalized('%A %d %B %Y')}}</li>
            @endif
            @if($check_out)
              <li><strong>{{trans('booking.check_out')}}</strong> : {{Carbon::createFromFormat('Y-m-d', $check_out)->formatLocalized('%A %d %B %Y')}}</li>
            @endif
            @if($nights)
              <li><strong>{{trans('booking.nights')}}</strong> : {{$nights or 0 }}</li>
            @endif
            @if($guests)
              <li><strong>{{trans('booking.guests')}}</strong> : {{$guests or 0 }}</li>
            @endif
            @if($offert)
              <li>
                  <strong
                  data-tooltip="{!! $offert->description !!}"
                  data-tooltip-position="top center"
                  >{{trans('booking.discount_code')}} : <small>{{$offert->code}} <i class="ti-info-alt" onclick="$('#offert_description').toggle()"></i></small></strong>
              </li>
            @endif
          </ul>
      </div>
    </div>
</div>
</div>
