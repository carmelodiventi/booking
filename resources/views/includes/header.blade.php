<header id="masthead" role="banner">
  <div class="container-fluid">
  <div class="row">
    <div class="col-md-3 col-xs-3">
        <div class="site-branding">
            <h1 class="site-title" style="background-image:url({!! Helper::get_setting('hotel_data')->logo !!})">
              <a href="{{url('/'.$property_id)}}" rel="home">
                {!! Helper::get_setting('hotel_data')->name !!}
              </a>
            </h1>
        </div><!-- .site-branding -->
      </div>
      <div class="col-md-9 col-xs-9">
        <ul id="header-item">
            @if (Session::has('customer'))
                <li class="header-item">
                  {{trans('booking.hello')}} <strong> {{Session::get('customer')->name}}</strong> <a href="{{url('logout')}}">{{trans('booking.logout')}}</a>
              </li>
            @endif
            <li class="header-item lang">
              <div class="dropdown">
              <a href="javascript::void(0)" class="dropbtn">{{LaravelLocalization::getCurrentLocale()}}</a>
              <div class="dropdown-content">
                <ul>
                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                    <li>
                        <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                            {{ $properties['native'] }}
                        </a>
                    </li>
                @endforeach
                </ul>
              </div>
            </div>
          </li>
      </ul>
    </div>
    </div>
</header><!-- #masthead -->
