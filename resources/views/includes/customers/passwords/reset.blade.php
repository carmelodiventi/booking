<div id="reset_password" class="modal">
  <div class="modal-content">
    <span class="modal-close">&times;</span>
    <form id="reset-password-form" onsubmit="app.resetPasswordCustomer(); return false;" method="post">
    <div class="row">
      <div class="col-xs-12">
        <h1 class="text-uc">{!!trans('booking.reset_password')!!}</h1>
        <p>{!!trans('booking.reset_your_password')!!}</p>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="form-group">
        <input type="email" class="form-field" placeholder="{{trans('booking.email')}}" name="email" required>
        </div>
      </div>
      <div class="col-xs-12">
        <div class="form-group">
          <button class="btn btn-primary">{{trans('booking.reset')}}</button>
        </div>
      </div>
    </div>
    </form>
  </div>
</div>
