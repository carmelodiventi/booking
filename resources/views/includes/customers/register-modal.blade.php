<div id="register" class="modal">
  <div class="modal-content">
    <span class="modal-close">&times;</span>
      <form id="register-form" onsubmit="app.registerCustomer(); return false;" method="post">
        <div class="row">
            <div class="col-xs-12">
              <h1 class="text-uc">{!!trans('booking.register')!!}</h1>
              <p>{!!trans('booking.register_new_account')!!}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                    <input type="text" class="form-field" placeholder="{{trans('booking.name')}}" name="name" required>
              </div>
            </div>
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                    <input type="text" class="form-field" placeholder="{{trans('booking.surname')}}" name="surname" required>
             </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                    <input type="tel" class="form-field" placeholder="{{trans('booking.phone')}}" name="phone" required>
              </div>
            </div>
          <div class="col-xs-12 col-md-6">
            <div class="form-group">
                    <input type="text" class="form-field" placeholder="{{trans('booking.address')}}"  name="address" required>
            </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <input type="text" class="form-field" placeholder="{{trans('booking.city')}}" name="city" required>
              </div>
            </div>
            <div class="col-xs-12 col-md-2">
              <div class="form-group">
                <input type="text" class="form-field" placeholder="{{trans('booking.province')}}" name="province" required>
              </div>
            </div>
            <div class="col-xs-12 col-md-4">
              <div class="form-group">
                <input type="text" class="form-field" placeholder="{{trans('booking.country')}}" name="country" required>
              </div>
            </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-md-6">
            <div class="form-group">
                <input type="email" class="form-field" placeholder="{{trans('booking.email')}}" name="email" required>
            </div>
          </div>
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                  <input type="password" class="form-field" placeholder="{{trans('booking.password')}}" name="password" min="6" required>
              </div>
            </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <hr>
          </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-12">
              <div class="form-group">
                <p>{!!trans('booking.privacy_check')!!}</p>
              </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <hr>
                <button class="btn btn-primary">{{trans('booking.register')}}</button>
            </div>
        </div>
    </form>
  </div>
</div>
