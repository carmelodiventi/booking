<div id="login" class="modal">
  <div class="modal-content">
    <span class="modal-close">&times;</span>
    <form id="login-form" onsubmit="app.loginCustomer(); return false;" method="post">
    <div class="row">
      <div class="col-xs-12">
        <h1 class="text-uc">{!!trans('booking.login')!!}</h1>
        <p>{!!trans('booking.login_with_your_data')!!}</p>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="form-group">
        <input type="email" class="form-field" placeholder="{{trans('booking.email')}}" name="email" required>
        </div>
      </div>
      <div class="col-xs-12">
        <div class="form-group">
          <input type="password" class="form-field" placeholder="{{trans('booking.password')}}" name="password" required>
        </div>
      </div>
      <div class="col-xs-12">
        <div class="form-group">
          <a href="javascript::void(0)" onclick="app.showModal('reset_password')">{{ trans('booking.forgot_password')}}</a>
        </div>
      </div>
      <div class="col-xs-12">
        <div class="form-group">
          <button class="btn btn-primary">{{trans('booking.login')}}</button>
        </div>
      </div>
    </div>
    </form>
  </div>
</div>
