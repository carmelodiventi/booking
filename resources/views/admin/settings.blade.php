@extends('admin.main')

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Settings</h4>
      <p class="text-muted mb-0">Booking Engine</p>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
<ul role="tablist" class="nav nav-tabs mb-15">
  <li role="presentation" class="active"><a href="#general" aria-controls="general" role="tab" data-toggle="tab" aria-expanded="true">{!!trans('admin.hotel_settings')!!}</a></li>
  <li role="presentation" class=""><a href="#paypal" aria-controls="data" role="tab" data-toggle="tab" aria-expanded="false">{!!trans('admin.paypal')!!}</a></li>
  <li role="presentation" class=""><a href="#stripe" aria-controls="data" role="tab" data-toggle="tab" aria-expanded="false">{!!trans('admin.stripe')!!}</a></li>
  <li role="presentation" class=""><a href="#currency" aria-controls="data" role="tab" data-toggle="tab" aria-expanded="false">{!!trans('admin.currency')!!}</a></li>
  <li role="presentation" class=""><a href="#style" aria-controls="data" role="tab" data-toggle="tab" aria-expanded="false">{!!trans('admin.style')!!}</a></li>
  <li role="presentation" class=""><a href="#my_allocator" aria-controls="data" role="tab" data-toggle="tab" aria-expanded="false">{!!trans('admin.my_allocator')!!}</a></li>
  <li role="presentation" class=""><a href="#system" aria-controls="data" role="tab" data-toggle="tab" aria-expanded="false">{!!trans('admin.system_settings')!!}</a></li>
</ul>
<div class="tab-content">
  <!--begin tab-content-->
  <!--begin tab-->
<div id="general" role="tabpanel" class="tab-pane active">
{!! Form::model(Helper::get_setting('hotel_data'),['url' =>'admin/settings/update/hotel_data'])!!}
    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
        {!! Form::label('name', trans('admin.name')) !!}
        {!! Form::text('name', null, ['class' => 'form-control','placeholder'=> trans('admin.name')]) !!}
        </div>
      </div>
      <div class="col-md-4">
          <div class="form-group">
          {!! Form::label('email', trans('admin.email')) !!} <small>({!!trans('admin.email_notification')!!})</small>
          {!! Form::email('email', null, ['class' => 'form-control','placeholder'=> trans('admin.email')]) !!}
        </div>
       </div>
      <div class="col-md-4">
        <div class="form-group">
        {!! Form::label('phone', trans('admin.phone')) !!}
        {!! Form::text('phone', null, ['class' => 'form-control','placeholder'=> trans('admin.phone')]) !!}
        </div>
    </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          {!! Form::label('address', trans('admin.address')) !!}
          {!! Form::text('address', null, ['class' => 'form-control','placeholder'=> trans('admin.address')]) !!}
       </div>
     </div>
        <div class="col-md-4">
          <div class="form-group">
            {!! Form::label('city', trans('admin.city')) !!}
            {!! Form::text('city', null, ['class' => 'form-control','placeholder'=> trans('admin.city')]) !!}
          </div>
       </div>
       <div class="col-md-4">
         <div class="form-group">
           {!! Form::label('postalcode', trans('admin.postal_code')) !!}
           {!! Form::text('postalcode', null, ['class' => 'form-control','placeholder'=> trans('admin.postal_code')]) !!}
       </div>
      </div>
    </div>
    <hr>
    <div class="row">
       <div class="col-md-9">
         <div class="form-group">
         {!! Form::label('logo', trans('admin.logo')) !!}
         {!! Form::text('logo', null, ['class' => 'form-control','placeholder'=> trans('admin.logo')]) !!}
         </div>
      </div>
       <div class="col-md-3">
         <div class="form-group">
         {!! Form::label('vat', trans('admin.vat')) !!}
         {!! Form::text('vat', null, ['class' => 'form-control','placeholder'=> trans('admin.vat')]) !!}
         </div>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
        {!! Form::label('country', trans('admin.country')) !!}
        {!! Form::select('country',
          Config::get('countries.list'),
          null, ['class' => 'form-control']) !!}
        </div>
     </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            {!! Form::submit(trans('admin.update'), ['class' => 'btn btn-primary']) !!}
        </div>
    </div>
{!! Form::close() !!}
</div>
<!--end tab-->

<div id="style" role="tabpanel" class="tab-pane">
  {!! Form::model(Helper::get_setting('theme'),['url' =>'admin/settings/update/theme'])!!}
    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
        {!! Form::label('name', trans('admin.theme')) !!}
        {!! Form::text('name', null, ['class' => 'form-control','placeholder'=> trans('admin.theme')]) !!}
        </div>
      </div>
    </div>
  {!! Form::close() !!}
</div>

<!--begin tab-->
<div id="paypal" role="tabpanel" class="tab-pane">
    {!! Form::model(Helper::get_setting('paypal'), ['url' =>'admin/settings/update/paypal','class'=>'form-horizontal'])!!}

      <div class="row">
        <div class="col-md-2">
          {!! Form::label('name', trans('admin.name')) !!}
        </div>
        <div class="col-md-8">
          <div class="form-group">
          {!! Form::text('name', null, ['class' => 'form-control','placeholder'=> trans('admin.name')]) !!}
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-2">
            {!! Form::label('Api Username', trans('admin.name')) !!}
        </div>
        <div class="col-md-8">
          <div class="form-group">
            {!! Form::text('api_username', null, ['class' => 'form-control','placeholder'=> 'Api Username']) !!}
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-2">
            {!! Form::label('api_password', 'Api Password') !!}
        </div>
        <div class="col-md-8">
          <div class="form-group">
              {!! Form::password('api_password', ['class' => 'form-control','placeholder'=> 'Api Password']);!!}
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-2">
          {!! Form::label('api_signature', 'Api Signature') !!}

        </div>
        <div class="col-md-8">
          <div class="form-group">
            {!! Form::text('api_signature', null, ['class' => 'form-control','placeholder'=> 'Api Signature']) !!}
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-2">
              {!! Form::label('test', trans('admin.test_account')) !!}
        </div>
        <div class="col-md-8">
          <div class="form-group">
              {!! Form::checkbox('test', 'true') !!}
          </div>
        </div>
      </div>


      <div class="row">
        <div class="col-md-2">
              {!! Form::label('active', trans('admin.active')) !!}
        </div>
        <div class="col-md-8">
          <div class="form-group">
            {!! Form::checkbox('active', null) !!}
          </div>
        </div>
      </div>

    <div class="row">
        <div class="col-md-3">
            {!! Form::submit(trans('admin.update'), ['class' => 'btn btn-primary']) !!}
        </div>
    </div>
    {{Form::hidden('code', 2)}}
   {!! Form::close() !!}
</div>
<!--end tab-->

<!--begin tab-->
<div id="stripe" role="tabpanel" class="tab-pane">
    {!! Form::model(Helper::get_setting('stripe'),['url' =>'admin/settings/update/stripe', 'class'=>'form-horizontal'])!!}
      <div class="row">
        <div class="col-md-2">
          {!! Form::label('name', trans('admin.name')) !!}
        </div>
        <div class="col-md-8">
          <div class="form-group">
          {!! Form::text('name', null, ['class' => 'form-control','placeholder'=> trans('admin.name')]) !!}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-2">
            {!! Form::label('secret_key', trans('Secret key')) !!}
        </div>
        <div class="col-md-8">
          <div class="form-group">
            {!! Form::password('secret_key', ['class' => 'form-control','placeholder'=> 'Secret key']) !!}
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-2">
            {!! Form::label('publishable_key', 'Publishable key') !!}
        </div>
        <div class="col-md-8">
          <div class="form-group">
            {!! Form::password('publishable_key', ['class' => 'form-control','placeholder'=> 'Publishable key']);!!}
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-2">
              {!! Form::label('test', trans('admin.test_account')) !!}
        </div>
        <div class="col-md-8">
          <div class="form-group">
              {!! Form::checkbox('test', 'true') !!}
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-2">
              {!! Form::label('active', trans('admin.active')) !!}
        </div>
        <div class="col-md-8">
          <div class="form-group">
            {!! Form::checkbox('active', null) !!}
          </div>
        </div>
      </div>

    <div class="row">
        <div class="col-md-3">
            {!! Form::submit(trans('admin.update'), ['class' => 'btn btn-primary']) !!}
        </div>
    </div>

    {{Form::hidden('code', 1)}}
   {!! Form::close() !!}
</div>
<!--end tab-->

<!--begin tab-->
<div id="currency" role="tabpanel" class="tab-pane">
    {!! Form::model(Helper::get_setting('currency'),['url' =>'admin/settings/update/currency', 'class'=>'form-horizontal'])!!}

      <div class="row">
        <div class="col-md-2">
          {!! Form::label('currency', trans('admin.currency_code')) !!}
        </div>
        <div class="col-md-8">
          <div class="form-group">
          {!! Form::select('code', Config::get('countries.currency'),null, ['class' => 'form-control','placeholder'=> trans('admin.select_currency')]) !!}
          </div>
        </div>
      </div>


    <div class="row">
        <div class="col-md-3">
            {!! Form::submit(trans('admin.update'), ['class' => 'btn btn-primary']) !!}
        </div>
    </div>
   {!! Form::close() !!}
</div>
<!--end tab-->

<!--begin tab-->
<div id="my_allocator" role="tabpanel" class="tab-pane">
    {!! Form::model(Helper::get_setting('my_allocator'),['url' =>'admin/settings/update/my_allocator', 'class'=>'form-horizontal'])!!}

     

      <div class="row">
        <div class="col-md-4">
          {!! Form::label('property_id', trans('admin.property_id')) !!}
        </div>
        <div class="col-md-8">
          <div class="form-group">
              {!! Form::text('property_id', null, ['class' => 'form-control','placeholder'=> trans('admin.property_id')]) !!}
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-4">
          {!! Form::label('actived', trans('admin.actived')) !!}
        </div>
        <div class="col-md-8">
          <div class="form-group">
            {!! Form::select('active', ["0" => "false", "1" => "true"],null, ['class' => 'form-control']) !!} 
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-4">
          {!! Form::label('esclude_price', trans('admin.esclude_price_availability_store')) !!}
        </div>
        <div class="col-md-8">
          <div class="form-group">
            {!! Form::checkbox('esclude_price', true) !!}
          </div>
        </div>
      </div>

      <div class="row">
          <div class="col-md-3">
              {!! Form::submit(trans('admin.update'), ['class' => 'btn btn-primary']) !!}
          </div>
      </div>
   {!! Form::close() !!}
</div>
<!--end tab-->
<div id="system" role="tabpanel" class="tab-pane">
  <div class="row">
    <div class="col-md-2">{!! Form::label('clear_cache', trans('admin.clear_cache')) !!}</div>
    <div class="col-md-2"><a href="{{url('admin/clear-cache')}}" class="btn btn-primary">{{trans('admin.clear_cache')}}</a> </div>
  </div>
  <hr>
</div>
<!--end tab-content-->
</div>
@stop
