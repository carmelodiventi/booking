@extends('admin.main')

@push('styles')

@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Emails</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Emails
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')

    <a href="{{ url('admin/emails/create')}}" class="btn btn-primary">{{trans('admin.add')}}</a>
    <hr>
    <table id="emails-list" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans('admin.code')}}</th>
                <th>{{ trans('admin.subject')}}</th>
                <th>{{ trans('admin.language')}}</th>
                <th>{{ trans('admin.status')}}</th>
                <th>{{ trans('admin.options')}}</th>
            </tr>
            </thead>
            <tbody>
            @if($emails)
                @foreach ($emails as $email)
                    <tr>
                        <td>{{$email->code}}</td>
                        <td>{{$email->subject}}</td>
                        <td>{{$email->lang}}</td>
                        <td>{{$email->status}}</td>
                        <td>
                            <a href="{{ route('emails.edit', $email->id) }}" class="btn btn-success"><i
                                        class="fa fa-edit"></i></a>
                            {!! Form::open(['method' => 'DELETE','route' => ['emails.destroy', $email->id], 'class' => 'inline']) !!}
                            {!! Form::submit(trans('admin.delete'), ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>

    @push('script')

    @endpush

@stop
