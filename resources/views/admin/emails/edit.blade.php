@extends('admin.main')

@push('styles')
  <link rel="stylesheet" type="text/css" href="{{asset('resources/assets/admin/plugins/summernote/dist/summernote.css')}}">
@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Edit Email</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li><a href="{{url('/admin/emails')}}">Emails</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Edit Email
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
  <div class="widget">
    <div class="widget-heading">
                <h3 class="widget-title">Edit Emails Templates</h3>
    </div>
      <div class="widget-body">
        {!! Form::model($email,['method' => 'put', 'url' =>'admin/emails/'. $email['id']])!!}
        <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                {!! Form::label('code', trans('admin.code')) !!}
                {!! Form::select('code',
                  array(
                  'new_reservation'=> trans('admin.new_reservation'),
                  'added_reservation'=> trans('admin.added_reservation'),
                  'pending_reservation'=> trans('admin.pending_reservation'),
                  'deleted_reservation'=> trans('admin.deleted_reservation'),
                  'ticket'=> trans('admin.ticket'),
                  'new_customer'=> trans('admin.new_customer'),
                  )
                  , null, ['class' => 'form-control','placeholder' => trans('admin.select_element')]) !!}
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                {!! Form::label('subject', trans('admin.subject')) !!}
                {!! Form::text('subject', null, ['class' => 'form-control','placeholder'=> trans('admin.subject')]) !!}
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                {!! Form::label('language', trans('admin.language')) !!}
                {!! Form::select('language', LaravelLocalization::getSupportedLanguagesKeys(), null, ['class' => 'form-control','placeholder' => trans('admin.select_language')]) !!}
              </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-12">
              <div class="form-group">
              {!! Form::label('body', trans('admin.body')) !!}
              {!! Form::textarea('body', null, ['class' => 'form-control','placeholder'=> trans('admin.body')]) !!}
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-2">
            <div class="form-group">
              {!! Form::label('status', trans('admin.status')) !!}
              {!! Form::select('status', ['CNF' => 'Confermata', 'CAN' => 'Cancellata', 'PND' => 'In Attesa'], null, ['class' => 'form-control','placeholder' => 'Seleziona una stato']) !!}
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                {!! Form::submit(trans('admin.add'), ['class' => 'btn btn-primary']) !!}
            </div>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
    @push('script')
    <script type="text/javascript" src="{{asset('resources/assets/admin/plugins/summernote/dist/summernote.min.js')}}"></script>
      <script>
            $(document).ready(function(){
              $("#body").summernote();
            });
      </script>
    @endpush

@stop
