<div class="btn-group mt-5">
  <button type="button" class="btn btn-default btn-outline"><i class="flag-icon flag-icon-{{LaravelLocalization::getCurrentLocale()}} mr-5"></i> {{LaravelLocalization::getCurrentLocaleName()}}</button>
  <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default btn-outline dropdown-toggle"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>
  <ul class="dropdown-menu dropdown-menu-right animated fadeInDown">
    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
    <li>
      <a hreflang="{{$localeCode}}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }} "><i class="flag-icon flag-icon-{{$localeCode}} mr-5"></i> {{ $properties['native'] }}</a>
    </li>
   @endforeach
  </ul>

</div>
