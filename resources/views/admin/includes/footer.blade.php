<script>
  var base_url = "{{url('/')}}";
  var locale = "{{LaravelLocalization::getCurrentLocale()}}";
</script>
<!-- jQuery-->
<script type="text/javascript" src="{{ asset('resources/assets/admin/plugins/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap JavaScript-->
<script type="text/javascript" src="{{ asset('resources/assets/admin/plugins/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Noty-->
<script type="text/javascript" src="{{ asset('resources/assets/admin/plugins/noty-notifications/noty.js')}}"></script>
<!-- Chosen-->
<script type="text/javascript" src="{{ asset('resources/assets/admin/plugins/chosen/chosen.jquery.min.js')}}"></script>
<!-- Moment-->
<script type="text/javascript" src="{{ asset('resources/assets/admin/plugins/moment/min/moment.min.js')}}"></script>
<!-- Animo-->
<script type="text/javascript" src="{{ asset('resources/assets/admin/plugins/animo.js/animo.min.js')}}"></script>
<!-- Malihu-->
<script type="text/javascript" src="{{ asset('resources/assets/admin/plugins/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<!-- Swal -->
<script type="text/javascript" src="{{ asset('resources/assets/admin/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<!-- language -->
<script type="text/javascript" src="{{ asset('resources/assets/admin/js/language') . '/' . LaravelLocalization::getCurrentLocale() . '.js' }}"></script>
<!-- Custom JS-->
<script type="text/javascript" src="{{ asset('resources/assets/admin/build/js/fourth-layout/app.js')}}"></script>
<script type="text/javascript" src="{{ asset('resources/assets/admin/js/app.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/dist/admin.js')}}"></script>
<!-- Singolar JS-->
@stack('script')

@if (Session::has('message_success'))
    <script>
        $.noty.defaults.killer = true;
        noty({
            text: "{{Session::get('message_success')}}",
            layout: 'top',
            closeWith: ['click', 'hover'],
            timeout:3000,
            type: 'success'
        });
    </script>
@endif

@if (Session::has('message_error'))
    <script>
        $.noty.defaults.killer = true;
        noty({
            text: "{{Session::get('message_error')}}",
            layout: 'top',
            closeWith: ['click', 'hover'],
            timeout:3000,
            type: 'error'
        });
    </script>
@endif

@if (Session::has('message_warning'))
    <script>
        $.noty.defaults.killer = true;
        noty({
            text: "{!!Session::get('message_warning')!!}",
            layout: 'top',
            closeWith: ['click', 'hover'],
            type: 'warning'
        });
    </script>
@endif

@if(count($errors->all()) > 0)
    @foreach($errors->all() as $e)
        <script>
            $.noty.defaults.killer = true;
            noty({
                text: "{{$e}}",
                layout: 'top',
                closeWith: ['click', 'hover'],
                timeout:3000,
                type: 'warning'
            });
        </script>
    @endforeach

@endif
