@push('script')
  <script>

  countNotif = function(){
    $countNotif = $('li.notif').length;
    $('.countNotif').html($countNotif);
  }

  $('a[data-notif-id]').click(function () {

      var notif = $(this);
      var notif_id   = notif.data('notifId');

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

        $.ajax({
          url: base_url + '/admin/notifMarkAsRead',
          type: 'POST',
          data: {notif_id: notif_id}
        })
        .done(function(response) {
          if(response.status == 'success'){
            notif.parent().addClass('read').remove();
            countNotif();
          }
          else{
            $(this).addClass('unread');
          }
        });

        return false;
      });

    $(window).load(function() {
      countNotif();
    });
  </script>
@endpush

<li class="dropdown"><a id="dropdownMenu1" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle bubble header-icon"><i class="ti-world"></i><span class="badge bg-danger countNotif"></span></a>
          <div aria-labelledby="dropdownMenu1" class="dropdown-menu dropdown-menu-right dm-medium fs-12 animated fadeInDown">
            <h5 class="dropdown-header"> {!!trans('admin.have_notifications',['count'=> '<span class="countNotif"></span>'])!!} </h5>
            <ul data-mcs-theme="minimal-dark" class="media-list mCustomScrollbar">

                  @foreach ($notifications as $notification)

                    <li class="media notif">
                      <a href="#" data-notif-id="{{$notification->id}}">
                        <div class="media-body">
                          <h6 class="media-heading">{!!$notification->data['title']!!}</h6>
                          <p class="text-muted mb-0">{!!$notification->data['message']!!}</p>
                        </div>
                        <div class="media-right text-nowrap">
                          <time datetime="{{$notification->created_at}}" class="fs-11">{{$notification->created_at->diffForHumans()}}</time>
                        </div>
                      </a>
                    </li>

                  @endforeach
            </ul>
            <div class="dropdown-footer text-center p-10"><a href="javascript:;" class="fw-500 text-muted">{{trans('admin.see_all_notifications')}}</a></div>
          </div>
</li>
