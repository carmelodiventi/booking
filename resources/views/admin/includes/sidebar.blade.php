<!-- Main Sidebar start-->
<aside class="main-sidebar">
  <div class="user">
    <h4 class="fs-16 text-white mt-15 mb-5 fw-300">{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}</h4>
    @if(isset(Auth::user()->property_id))
      <p class="mb-0 text-muted">Property ID <strong>{{Auth::user()->property_id}}</strong></p>
    @endif
  </div>
    <ul class="list-unstyled navigation mb-0">
      <li class="sidebar-category">Main</li>
  @if (Auth::check())
      <li class="panel"><a href="{{url('admin')}}"><i class="ti-home"></i><span class="sidebar-title">Dashboard </span></a></li>
      <li class="panel"><a href="{{url('admin/availability')}}"><i class="ti-calendar"></i><span class="sidebar-title">Availability </span></a></li>
      <li class="panel"><a role="button" data-toggle="collapse" data-parent=".navigation" href="#collapse1" aria-expanded="false" aria-controls="collapse1" class="bubble collapsed"><i class="ti-layers-alt"></i><span class="sidebar-title">Planning</span></a>
        {!! Helper::planning_short_link() !!}
      </li>
      <li class="panel"><a role="button" data-toggle="collapse" data-parent=".navigation" href="#collapse3" aria-expanded="false" aria-controls="collapse3" class="collapsed"><i class="ti-ticket"></i><span class="sidebar-title">Reservations</span></a>
        <ul id="collapse3" class="list-unstyled collapse">
          <li><a href="{{ url('admin/reservations')}}">List</a></li>
          <li><a href="{{ url('admin/reservations/create')}}">{{trans('admin.add')}}</a></li>
          <li><a href="{{ url('admin/fees')}}">Fees</a></li>
          <li><a href="{{ url('admin/extra')}}">Extra</a></li>
        </ul>
      </li>
      <li class="panel"><a role="button" data-toggle="collapse" data-parent=".navigation" href="#collapse4" aria-expanded="false" aria-controls="collapse4" class="collapsed"><i class="ti-envelope"></i>
        <span class="sidebar-title">Rooms</span>
      </a>
        <ul id="collapse4" class="list-unstyled collapse">
          <li><a href="{{ url('admin/rooms')}}">Rooms</a></li>
          <li><a href="{{ url('admin/rooms-blocked')}}">Rooms Blocked</a></li>
          <li><a href="{{ url('admin/rooms-type')}}">Rooms Type</a></li>
        </ul>
      </li>
      <li class="panel">
        <a role="button" data-toggle="collapse" data-parent=".navigation" href="#collapse5" aria-expanded="false" aria-controls="collapse5" class="collapsed"><i class="ti-money"></i>
          <span class="sidebar-title">Rate</Rate>
        </a>
        <ul id="collapse5" class="list-unstyled collapse">
            <li><a href="{{ url('admin/rate-type')}}">Rate Type</a></li>
        </ul>
      </li>
      <li class="panel"><a href="{{url('admin/offerts')}}"><i class="ti-receipt"></i><span class="sidebar-title">Offerts</span></a></li>
      <li class="panel"><a href="{{url('admin/customers')}}"><i class="ti-user"></i><span class="sidebar-title">Customers </span></a></li>
      <li class="panel"><a href="{{url('admin/emails')}}"><i class="ti-email"></i><span class="sidebar-title">Emails</span></a></li>
      <li class="panel"><a href="{{url('admin/fragments')}}"><i class="ti-flag-alt"></i><span class="sidebar-title">{!!trans('admin.fragments')!!}</span></a></li>
      <li class="panel"><a href="{{url('admin/settings')}}"><i class="ti-settings"></i><span class="sidebar-title">Settings</span></a></li>
      @if(Auth::user()->hasRole('administrator'))

      @endif
  @endif
</ul>
</aside>
<!-- Main Sidebar end-->
