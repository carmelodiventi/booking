<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Carmelo Di Venti">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>Novabeds</title>
<!-- PACE-->
<link rel="stylesheet" type="text/css"
        href="{{ asset('resources/assets/admin/plugins/PACE/themes/blue/pace-theme-flash.css')}}">
<script type="text/javascript" src="{{ asset('resources/assets/admin/plugins/PACE/pace.min.js')}}"></script>
<!-- Bootstrap CSS-->
<link rel="stylesheet" type="text/css"
        href="{{ asset('resources/assets/admin/plugins/bootstrap/dist/css/bootstrap.min.css')}}">
<!-- Fonts-->
<link rel="stylesheet" type="text/css"
        href="{{ asset('resources/assets/admin/plugins/themify-icons/themify-icons.css')}}">
<!-- Flag Icons-->
<link rel="stylesheet" type="text/css"
        href="{{ asset('resources/assets/admin/plugins/flag-icon-css/css/flag-icon.min.css')}}">
<!-- Font Awesome-->
<link rel="stylesheet" type="text/css"
        href="{{ asset('resources/assets/admin/plugins/font-awesome/css/font-awesome.min.css')}}">
<!-- Noty-->
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/admin/plugins/noty-notifications/noty.css')}}">
<!-- Chosen-->
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/admin/plugins/chosen/chosen.min.css')}}">
<!-- Bootstrap Date Range Picker-->
<link rel="stylesheet" type="text/css"
        href="{{ asset('resources/assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.css')}}">
<!-- Animo -->
<link rel="stylesheet" type="text/css"
        href="{{ asset('resources/assets/admin/plugins/animo.js/animate-animo.min.css')}}">
<!-- Malihu -->
<link rel="stylesheet" type="text/css"
        href="{{ asset('resources/assets/admin/plugins/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css')}}">
<!-- Primary Style-->
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/admin/build/css/fourth-layout.css')}}">
<!-- Admin Style-->
<link type="text/css" rel="stylesheet" href="{{asset("/public/dist/admin.css")}}" />
<!-- External Style-->
@stack('styles')

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
<!-- WARNING: Respond.js doesn't work if you view the page via file://-->
<!--[if lt IE 9]>
<script type="text/javascript" src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script type="text/javascript" src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<div style='text-align:center'><a
        href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img
        src="http://storage.ie6countdown.com/resources/assets/admin/100/images/banners/warning_bar_0000_us.jpg"
        border="0"
        height="42" width="820"
        alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/></a>
</div>
<![endif]-->