@push('styles')
  <style>
    .clonedInput{
      display: flex;
      align-items: center;
      margin-bottom: 1em;
      border-radius: 10px;
      padding: 1em;
      box-shadow: 1px 2px 15px #f1f1f1;
    }

    .clonedInput .form-content:last hr{
      display: none
    }
  </style>
@endpush

<fieldset>
  <legend>{{ trans('admin.available_accomodation')}}</legend>
  <div id="rooms">
    <div class="row">
      @if($multiple == true)
        <div class="col-md-9">
      @else
        <div class="col-md-12">
      @endif

        <div class="row clonedInput" data-room="0">

            <div class="col-md-3">
              <div class="form-group">
                {!! Form::label('rooms_type', trans('admin.type')) !!}
                <select name="rooms_type[]" class="form-control" onchange="reservation.getRate(this)">
                  <option value="">{{trans('admin.rooms_type_select')}}</option>
                </select>
              </div>
            </div>

            <div class="col-md-2 options">
                <div class="form-group">
                  {!! Form::label('rooms', trans('admin.rooms')) !!}
                  <select name="rooms[]" class="form-control" onchange="reservation.getGuestsAndPrice(this)" data-rule-required="true">
                    <option value="0">{{trans('admin.room_select')}}</option>
                  </select>
              </div>
            </div>

            <div class="col-md-3 options">
              <div class="form-group">
                {!! Form::label('rate_id', trans('admin.rate')) !!}
                <select name="rates[]" class="form-control" onchange="reservation.selectRate(this)">
                  <option value="">{{trans('admin.rate_select')}}</option>
                </select>
              </div>
            </div>

          <div class="col-md-2 options guests">
            <div class="form-group">
              {!! Form::label('guests', trans('admin.guests')) !!}
                <div class="form-content"><!-- guests input --></div>
            </div>
          </div>
          <div class="col-md-2 options prices">
            <div class="form-group">
              {!! Form::label('price', trans('admin.price')) !!}
              <div class="form-content"><!-- prices input --></div>
            </div>
          </div>
          <!-- end rooms line -->
          </div>
    </div>
    @if($multiple == true)
          <div class="col-md-3 options text-right hidden">
              <div class="form-group">
              {!! Form::button( '<i class="fa fa-plus"></i> ' . trans('admin.add'), ['class' => 'btn btn-primary clone', 'style'=>'margin-top:25px']) !!}
              {!! Form::button( '<i class="fa fa-minus"></i> ' . trans('admin.remove'), ['class' => 'btn btn-primary remove', 'style'=>'margin-top:25px']) !!}
            </div>
          </div>
    @endif
</div>
</fieldset>
