@if(!$rooms->isEmpty())
<fieldset>
  <legend>{{ trans('admin.available_accomodation')}}</legend>
  <div id="rooms">
    <div class="row">
      @if($multiple == true)
        <div class="col-md-9">
      @else
        <div class="col-md-12">
      @endif

        <div class="row clonedInput" data-room="0">
              <div class="col-md-3">
                <div class="form-group">
                  {!! Form::label('rooms', trans('admin.rooms')) !!}
                  @if(isset($lodgingID))
                    {!! Form::select('rooms[]', $rooms, $lodgingID, ['class'=> 'form-control','placeholder'=> trans('admin.room_select'),'onchange'=> 'reservation.updateRooms(this)','data-rule-required'=>'true']) !!}
                  @else
                    {!! Form::select('rooms[]', $rooms, null, ['class'=> 'form-control','placeholder'=> trans('admin.room_select'),'onchange'=> 'reservation.updateRooms(this)','data-rule-required'=>'true']) !!}
                  @endif
              </div>
            </div>
          @if($rooms_type)
            <div class="col-md-3 options">
              <div class="form-group">
                {!! Form::label('rooms_type', trans('admin.type')) !!}
                <select name="rooms_type[]" class="form-control" onchange="reservation.getRate(this)">
                  <option value="">{{trans('admin.rooms_type_select')}}</option>
                  @foreach ($rooms_type as $key => $value)
                    <option value="{{$key}}">{{Helper::get_traslated_string($value)}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          @endif
            <div class="col-md-3 options">
              <div class="form-group">
                {!! Form::label('rate_id', trans('admin.rate')) !!}
                <select name="rates[]" class="form-control" onchange="reservation.selectRate(this)">
                  <option value="">{{trans('admin.rate_select')}}</option>
                </select>
              </div>
            </div>
          <div class="col-md-1 options">
            <div class="form-group">
              {!! Form::label('guests', trans('admin.guests')) !!}
              {!! Form::number('guests[]', '1', ['class'=> 'form-control','placeholder' => trans('admin.guests'), 'min'=> 1,'data-rule-required'=>'true']) !!}
          </div>
          </div>
          <div class="col-md-2 options">
            <div class="form-group">
              {!! Form::label('price', trans('admin.stay_cost_per_room')) !!}
              {!! Form::number('price[]', '0.00', ['class'=> 'form-control','placeholder' => trans('admin.stay_cost_per_room'),'step'=> 'any', 'min'=> 0, 'onchange'=>'reservation.getTotal()','data-rule-required'=>'true']) !!}
          </div>
          </div>
          <!-- end rooms line -->
          </div>
    </div>
    @if($multiple == true)
          <div class="col-md-3 options text-right">
              <div class="form-group">
              {!! Form::button( '<i class="fa fa-plus"></i> ' . trans('admin.add'), ['class' => 'btn btn-primary clone', 'style'=>'margin-top:25px']) !!}
              {!! Form::button( '<i class="fa fa-minus"></i> ' . trans('admin.remove'), ['class' => 'btn btn-primary remove', 'style'=>'margin-top:25px']) !!}
            </div>
          </div>
    @endif
</div>
</fieldset>
@endif
