<div class="row">
      <div class="col-md-4">
        <div class="form-group">
          {!! Form::label('origin', trans('admin.origin')) !!}
          {!! Form::select('origin', $agency, $reservation->origin,['class'=> 'form-control']) !!}
          </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          {!! Form::label('commission_percentage', trans('admin.origin_percentage')) !!}
          {!! Form::number('commission_percentage', null, ['class'=> 'form-control','placeholder' => trans('admin.origin_percentage'),'min'=>0,'step'=>'any']) !!}
          </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('transaction_code', trans('admin.origin_code')) !!}
          {!! Form::text('transaction_code', $reservation->transaction_code, ['class'=> 'form-control','placeholder' => trans('admin.origin_code')]) !!}
          </div>
      </div>
  </div>
<div class="row">
  <div class="col-md-12">
    {!! Form::submit(trans('admin.update'),['class'=>'btn btn-primary']) !!}
  </div>
</div>
