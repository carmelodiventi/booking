<header>
  <div class="search-bar closed">
    @yield('search-bar-mobile')
  </div><a href="{{url('/admin')}}" class="brand pull-left">
    <img src="{{ asset('resources/assets/admin/build/images/logo/logo.svg') }}" class="logo">
    <img src="{{ asset('resources/assets/admin/build/images/logo/logo-sm.svg') }}" class="logo-sm">
    </a>
    <a href="javascript:;" role="button" class="hamburger-menu pull-left"><span></span></a>
    @yield('search-bar')
    <ul class="notification-bar list-inline pull-right">
    <li class="visible-xs"><a href="javascript:;" role="button" class="header-icon search-bar-toggle"><i class="ti-search"></i></a></li>
    @include('admin.includes.notifications')
    <li class="dropdown"><a id="dropdownMenu2" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle header-icon lh-1 pt-15 pb-15">
        <div class="media mt-0">
          <div class="media-left avatar"><img src="{{ asset('resources/assets/admin/build/images/users/avatar.png')}}" alt="" class="media-object img-circle"><span class="status bg-success"></span></div>
          <div class="media-right media-middle pl-0 hidden-xs">
            <p class="fs-12 text-base mb-0">{{trans('admin.hello')}} {{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}</p>
          </div>
        </div>
      </a>
      <ul aria-labelledby="dropdownMenu2" class="dropdown-menu fs-12 animated fadeInDown">
        <li><a href="{{ url('admin/profile')}}"><i class="ti-user mr-5"></i> {{trans('admin.profile')}}</a></li>
        <li><a href="{{ url('admin/settings')}}"><i class="ti-settings mr-5"></i> {{trans('admin.settings')}}</a></li>
        <li><a href="{{ url('logout')}}"><i class="ti-power-off mr-5"></i> {{trans('admin.logout')}}</a></li>
      </ul>
    </li>
  </ul>
</header>
