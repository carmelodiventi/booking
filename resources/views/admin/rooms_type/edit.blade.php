@extends('admin.main')

@push('styles')
  <!-- Summernote-->
  <link rel="stylesheet" type="text/css" href="{{asset('resources/assets/admin/plugins/summernote/dist/summernote.css')}}">
@endpush

@section('page-header')
    <div class="row">
      <div class="col-sm-6">
        <h4 class="mt-0 mb-5">Rooms Type</h4>
        <ol class="breadcrumb mb-0">
          <li><a href="{{url('/admin')}}">Home</a></li>
          <li><a href="{{url('/admin/rooms-type')}}">Rooms Type</a></li>
          <li class="active">
              <span class="show-for-sr">Current: </span> Create Rooms Type
          </li>
        </ol>
      </div>
      <div class="col-sm-6">
        <div class="btn-group mt-5">
          <button type="button" class="btn btn-default btn-outline"><i class="flag-icon flag-icon-us mr-5"></i> English</button>
          <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default btn-outline dropdown-toggle"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>
          <ul class="dropdown-menu dropdown-menu-right animated fadeInDown">
            <li><a href="#"><i class="flag-icon flag-icon-de mr-5"></i> German</a></li>
            <li><a href="#"><i class="flag-icon flag-icon-fr mr-5"></i> French</a></li>
            <li><a href="#"><i class="flag-icon flag-icon-es mr-5"></i> Spanish</a></li>
            <li><a href="#"><i class="flag-icon flag-icon-it mr-5"></i> Italian</a></li>
            <li><a href="#"><i class="flag-icon flag-icon-jp mr-5"></i> Japanese</a></li>
          </ul>
        </div>
      </div>
    </div>
@stop

@section('page-content')

       {!! Form::model($room_type,['method' => 'put', 'url' =>'admin/rooms-type/'. $room_type['id']])!!}
        <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                {!! Form::label('type', trans('admin.type')) !!}
                {!! Form::text('type', null, ['class' => 'form-control','placeholder'=> trans('admin.type')]) !!}
            </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                {!! Form::label('capacity', trans('admin.capacity')) !!}
                {!! Form::selectRange('capacity', 1, 20, null, ['class'=>'form-control', 'placeholder' => 'Seleziona Capacità']) !!}
            </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                {!! Form::label('status', trans('admin.status')) !!}
                {!! Form::select('status', ['CNF' => 'Confermata', 'CAN' => 'Cancellato', 'PND' => 'In Attesa'], $room_type['status'], ['class'=>'form-control', 'placeholder' => 'Seleziona una stato']) !!}
            </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                {!! Form::label('description', trans('admin.description')) !!}
                {!! Form::textarea('description', null, ['class' => 'form-control','placeholder'=> trans('admin.description')]) !!}
            </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                {!! Form::submit(trans('admin.add'), ['class' => 'btn btn-primary']) !!}
            </div>
        </div>
        {!! Form::close() !!}

    @push('script')
      <script type="text/javascript" src="{{asset('resources/assets/admin/plugins/summernote/dist/summernote.min.js')}}"></script>
      <script>
      $(document).ready(function(){
        $("#description").summernote();
      });
      </script>
    @endpush

@stop
