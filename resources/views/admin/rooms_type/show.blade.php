@extends('admin.main')

@push('styles')

@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Rooms Type</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Rooms Type
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      <div class="btn-group mt-5">
        <button type="button" class="btn btn-default btn-outline"><i class="flag-icon flag-icon-us mr-5"></i> English</button>
        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default btn-outline dropdown-toggle"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>
        <ul class="dropdown-menu dropdown-menu-right animated fadeInDown">
          <li><a href="#"><i class="flag-icon flag-icon-de mr-5"></i> German</a></li>
          <li><a href="#"><i class="flag-icon flag-icon-fr mr-5"></i> French</a></li>
          <li><a href="#"><i class="flag-icon flag-icon-es mr-5"></i> Spanish</a></li>
          <li><a href="#"><i class="flag-icon flag-icon-it mr-5"></i> Italian</a></li>
          <li><a href="#"><i class="flag-icon flag-icon-jp mr-5"></i> Japanese</a></li>
        </ul>
      </div>
    </div>
  </div>
@stop

@section('page-content')

    <a href="{{ url('admin/rooms-type/create')}}" class="btn btn-primary">{{trans('admin.add')}}</a>
    <hr>
    <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans('admin.type')}}</th>
                <th>{{ trans('admin.capacity')}}</th>
                <th>{{ trans('admin.description')}}</th>
                <th>{{ trans('admin.status')}}</th>
                <th>{{ trans('admin.action')}}</th>
            </tr>
            </thead>
            <tbody>
            @if($rooms_type)
                @foreach ($rooms_type as $room_type)
                    <tr>
                        <td>{{$room_type->type}}</td>
                        <td>{{$room_type->capacity}}</td>
                        <td style="width: 50%"> {!! html_entity_decode($room_type->description) !!}</td>
                        <td>{!! html_entity_decode(Helper::getstatus($room_type->status)) !!}</td>
                        <td>
                            <a href="{{ url('admin/photo', $room_type->id) }}" class="btn btn-success"><i
                                        class="fa fa-photo"></i></a>
                            <a href="{{ route('rooms-type.edit', $room_type->id) }}" class="btn btn-success"><i
                                        class="fa fa-edit"></i></a>
                            {!! Form::open(['method' => 'DELETE','route' => ['rooms-type.destroy', $room_type->id], 'class' => 'inline']) !!}
                            {!! Form::submit(trans('admin.delete'), ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    @push('script')

    @endpush

@stop
