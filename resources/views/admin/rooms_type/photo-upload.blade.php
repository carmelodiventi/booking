@extends('admin.main')

@push('styles')
  <!-- DropzoneJS-->
     <link rel="stylesheet" type="text/css" href="{{asset('resources/assets/admin/plugins/dropzone/dist/min/dropzone.min.css')}}">
@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Rooms Type Photo</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li><a href="{{url('/admin/rooms-type')}}">Rooms Type</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Rooms Type Photo
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      <div class="btn-group mt-5">
        <button type="button" class="btn btn-default btn-outline"><i class="flag-icon flag-icon-us mr-5"></i> English</button>
        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default btn-outline dropdown-toggle"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>
        <ul class="dropdown-menu dropdown-menu-right animated fadeInDown">
          <li><a href="#"><i class="flag-icon flag-icon-de mr-5"></i> German</a></li>
          <li><a href="#"><i class="flag-icon flag-icon-fr mr-5"></i> French</a></li>
          <li><a href="#"><i class="flag-icon flag-icon-es mr-5"></i> Spanish</a></li>
          <li><a href="#"><i class="flag-icon flag-icon-it mr-5"></i> Italian</a></li>
          <li><a href="#"><i class="flag-icon flag-icon-jp mr-5"></i> Japanese</a></li>
        </ul>
      </div>
    </div>
  </div>
@stop

@section('page-content')

    <a href="{{ url('admin/rooms-type') }}" class="btn btn-primary">{{trans('admin.list')}}</a>
    <hr>
    <div class="row">
      <div class="col-md-12">
        <h4>{!! $room_type->type !!}</h4>
      </div>
    </div>
    @if($photo_list)
        <div class="row">
            @foreach($photo_list as $photo)
                <div class="col-md-2">
                    <a href="#" class="thumbnail">
                        <form id="deletephoto" action="{{url('admin/photodelete') .'/'.$photo->id  }}">
                            <img src="{{ url($photo->url)}}" style="margin-bottom: 15px" class="img-thumbnail"/>
                            <button class="btn btn-danger btn-block"><i
                                        class="fa fa-trash-o"></i> {{trans('admin.delete')}} </button>
                        </form>
                    </a>
                </div>
            @endforeach
        </div>
    @endif

    {!! Form::open([ 'url' => [ 'admin/photoupload/' . $room_type->id  ], 'files' => true, 'enctype' => 'multipart/form-data', 'class' => 'dropzone', 'id' => 'image-upload' ]) !!}
    {!! Form::close() !!}

    @push('script')
      <script type="text/javascript" src="{{asset('resources/assets/admin/plugins/dropzone/dist/min/dropzone.min.js')}}"></script>
    @endpush

@stop
