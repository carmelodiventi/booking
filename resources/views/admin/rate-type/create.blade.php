@extends('admin.main')

@push('styles')
<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/admin/plugins/summernote/dist/summernote.css')}}">
<style>
  .rate-conditions{display: none}
</style>
@endpush

@section('page-header')
    <div class="row">
      <div class="col-sm-6">
        <h4 class="mt-0 mb-5">Create a new Room Type Rate</h4>
        <ol class="breadcrumb mb-0">
          <li><a href="{{url('/admin')}}">Home</a></li>
            <li><a href="{{url('/admin/rate')}}">Rate</a></li>
          <li class="active">
              <span class="show-for-sr">Current: </span> Edit Rate
          </li>
        </ol>
      </div>
      <div class="col-sm-6">
        @include('admin.includes.language')
      </div>
    </div>
@stop

@section('page-content')
      <a href="{{ url('admin/rate-type')}}" class="btn btn-primary">{{trans('admin.list')}}</a>
      <hr>
        {!! Form::open(['route' => 'rate-type.store']) !!}

        <ul role="tablist" class="nav nav-tabs mb-15">
          @foreach (LaravelLocalization::getSupportedLanguagesKeys() as $key => $value)
            <li role="presentation" class="{{$loop->index==0 ? 'active' : ''}}"><a href="#{{$key}}" aria-controls="general" role="tab" data-toggle="tab" aria-expanded="true">{{$value}}</a></li>
          @endforeach
        </ul>

        <div class="tab-content">
        @foreach (LaravelLocalization::getSupportedLanguagesKeys() as $key => $value)
          <div id="{{$key}}" role="tabpanel" class="tab-pane {{$loop->index==0 ? 'active' : ''}}">
            <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                  {!! Form::label('name[]', trans('admin.name')) !!}
                  {!! Form::text('name[]', null, ['class' => 'form-control', 'placeholder'=> trans('admin.name')]) !!}
                  </div>
              </div>

            </div>
            <div class="row">
              <div class="col-md-12">
                {!! Form::label('description[]', trans('admin.description')) !!}
                {!! Form::textarea('description[]', null, ['class' => 'form-control','placeholder'=> trans('admin.description')]) !!}
              </div>
            </div>
          </div>
          {!! Form::text('language[]', $value, ['class' => 'hidden']) !!}
        @endforeach
        </div>
        <div class="row"><div class="col-md-12"><hr></div></div>
          <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                  <div class="radio">
                    <label>{!! Form::checkbox('refundable', 'refundable') !!} {{trans('admin.refundable')}} </label>
                </div>
              </div>
            </div>
            <div class="col-md-1 rate-conditions">
                <div class="form-group">
                    {!!  Form::select('increment_decrement', ['+' => '+', '-' => '-'],  '+', ['class' => 'form-control' ]) !!}
                </div>
            </div>
            <div class="col-md-2 rate-conditions">
                <div class="form-group">
                    {!! Form::text('total', 0, ['class' => 'form-control', 'placeholder'=> trans('admin.total')]) !!}
                </div>
            </div>
            <div class="col-md-1 rate-conditions">
                <div class="form-group">
                    {!!  Form::select('operator', ['€' => '€', '%' => '%'],  '%', ['class' => 'form-control' ]) !!}
                </div>
            </div>
          </div>
        <div class="row">
            <div class="col-md-3">
                {!! Form::submit(trans('admin.edit'), ['class' => 'btn btn-primary']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    @push('script')
    <script type="text/javascript" src="{{asset('resources/assets/admin/plugins/summernote/dist/summernote.min.js')}}"></script>
    <script>
        $(window).load(function () {
            $("#description").summernote();
        });
        $('input[name="refundable"]').click(function(event) {
          if($('input[name="refundable"]').is(':checked')){
            $('.rate-conditions').hide();
          }
          else{
            $('.rate-conditions').show();
          }

        });
    </script>
    @endpush

@stop
