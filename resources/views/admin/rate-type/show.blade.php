@extends('admin.main')

@push('styles')

@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Rate</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Rate
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')

        <a href="{{ url('admin/rate-type/create')}}" class="btn btn-primary">{{trans('admin.add')}}</a>
        <hr>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans('admin.type')}}</th>
                <th>{{ trans('admin.description')}}</th>
                <th>{{ trans('admin.refundable')}}</th>
                <th>{{ trans('admin.rules')}}</th>
                <th>{{ trans('admin.action')}}</th>
            </tr>
            </thead>
            <tbody>
            @if($rates_type)
                @foreach ($rates_type as $rates_type)
                    <tr>
                        <td>{!! Helper::get_traslated_string($rates_type->name) !!}</td>
                        <td>{!! Helper::get_traslated_string($rates_type->description)!!}</td>
                        <td>
                          @if($rates_type->refundable === 1)
                            {{trans('admin.refundable')}}
                          @endif
                          @if($rates_type->refundable === 0)
                            {{trans('admin.not_refundable')}}
                          @endif
                        </td>
                        <td>
                          @if($rates_type->rules)
                            @php
                              $rules = json_decode($rates_type->rules);
                            @endphp
                            {{$rules[0]}} {{$rules[1]}} {{$rules[2]}}
                          @endif
                        </td>
                        <td>
                          <a href="{{ route('rate-type.edit', $rates_type->id) }}"
                             class="btn btn-success">{!! trans('admin.edit') !!}
                          </a>
                          {!! Form::open(['method' => 'DELETE','route' => ['rate-type.destroy', $rates_type->id], 'class' => 'inline']) !!}
                          {!! Form::submit(trans('admin.delete'), ['class' => 'btn btn-danger']) !!}
                          {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>

    @push('script')

    @endpush

@stop
