@extends('admin.main')
@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">{{trans('admin.profile')}}</h4>
      <p class="text-muted mb-0">Booking Engine</p>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')


{!! Form::model($user,['url' =>'admin/profile'])!!}
    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
        {!! Form::label('name', trans('admin.name')) !!}
        {!! Form::text('name', null, ['class' => 'form-control','placeholder'=> trans('admin.name')]) !!}
        </div>
      </div>
      <div class="col-md-3">
          <div class="form-group">
            {!! Form::label('email', trans('admin.email')) !!}
            {!! Form::email('email', null, ['class' => 'form-control','placeholder'=> trans('admin.email')]) !!}
          </div>
       </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            {!! Form::submit(trans('admin.update'), ['class' => 'btn btn-primary']) !!}
        </div>
    </div>
{!! Form::close() !!}
@stop
