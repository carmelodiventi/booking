@extends('admin.main')

@push('styles')
<link rel="stylesheet"
  href="{!!asset('resources/assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.css')!!}" type="text/css"
  rel="stylesheet">
<style>
  .table-loading-overlay {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 9999;
    background: rgba(0, 0, 0, 0.5);
    display: none;
  }

  .table-loading-inner {
    width: 100%;
    height: 100%;
  }

  .table-loading-msg {
    text-align: center;
    color: #fff;
    font-size: 24px;
  }
</style>
@endpush

@section('page-header')
<div class="row">
  <div class="col-sm-6">
    <h4 class="mt-0 mb-5">Availability</h4>
    <ol class="breadcrumb mb-0">
      <li><a href="{{url('/admin')}}">Home</a></li>
      <li class="active">
        <span class="show-for-sr">Current: </span> Availability
      </li>
    </ol>
  </div>
  <div class="col-sm-6">
    @include('admin.includes.language')
  </div>
</div>
@stop

@section('page-content')
@if(!$rooms_type->isEmpty() && !$rates_type->isEmpty())
<div class="widget">
  <div class="widget-heading">
    <button class="btn btn-default" data-toggle="modal" data-target="#customize_calendar"
      data-loading-text="<i class='fa fa-spinner fa-spin '></i> {{trans('admin.update_calendar')}}">
      {{trans('admin.customize_calendar')}}
    </button>
    @if(Helper::get_setting('my_allocator')->active)
    <button class="btn btn-default" data-toggle="modal" data-target="#synchronize_availability"
      data-loading-text="<i class='fa fa-spinner fa-spin'></i> {{trans('admin.processing_availability')}}">
      {{trans('admin.synchronize_availability_with_myallocator')}}
    </button>
    @endif
    <div class="btn-group pull-right">
      <button id="btn-long-term" class="btn btn-primary btn-raised" data-toggle="modal"
        data-target="#set_long_term_data"> {{trans('admin.set_long_term_data')}}</button>
      <button id="btn-save" class="btn btn-danger btn-save btn-raised" onclick="availability.save()" disabled="disabled"
        data-loading-text="<i class='fa fa-spinner fa-spin '></i> {{trans('admin.saving_availability')}}"><i
          class="ti-save"></i> {{trans('admin.save')}}</button>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="widget-body">
    <div class="table-responsive">
      {!! Helper::availability_calendar($rooms_type,$rates_type,$start,$end)!!}
    </div>
  </div>
</div>

<div tabindex="-1" role="dialog" aria-labelledby="set_long_term_data" class="modal fade" id="set_long_term_data">
  <div role="document" class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span
            aria-hidden="true">×</span></button>
        <h4 id="modalWithForm" class="modal-title">{{trans('admin.set_long_term_data')}}</h4>
      </div>
      <div class="modal-body">
        <form id="set_long_term_data">
          <div class="form-group">
            <label for="rooms_type">{{trans('admin.rooms_type_select')}}</label>
            <select name="rooms_type" class="form-control">
              @foreach($rooms_type as $room_type)
              <option value="{{$room_type->id}}" data-units="{{$room_type->units}}"
                data-mrtid="{{$room_type->ma_room_id}}">{{ Helper::get_traslated_string( $room_type->type) }}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <label for="rate_type">{{trans('admin.rate')}}</label>
            <select name="rate_type" class="form-control">
              @foreach($rates_type as $rate_type)
              <option value="{{$rate_type->id}}" {{($rate_type->master_rate == 1) ? 'data-master-rate=1' : ''}}>
                {{ Helper::get_traslated_string( $rate_type->name) }}
                {{($rate_type->master_rate == 1) ? '(Master Rate)' : ''}}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group">

            <label for="start_date">{{trans('admin.from')}}</label>
            <div class='input-group date'>
              <input type="text" name="start_date" class="form-control">
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
              </span>
            </div>
          </div>

          <div class="form-group">
            <label for="end_date">{{trans('admin.to')}}</label>
            <div class='input-group date'>
              <input type="text" name="end_date" class="form-control">
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
              </span>
            </div>
          </div>

          <div class="form-group">
            <label>{{trans('admin.weekday')}}</label>
            <div>
              <div class="checkbox checkbox-inline"><label class="checkbox"><input type="checkbox" name="weekday"
                    data-weekday="1" checked="">Monday</label></div>
              <div class="checkbox checkbox-inline"><label class="checkbox"><input type="checkbox" name="weekday"
                    data-weekday="2" checked="">Tuesday</label></div>
              <div class="checkbox checkbox-inline"><label class="checkbox"><input type="checkbox" name="weekday"
                    data-weekday="3" checked="">Wednesday</label></div>
              <div class="checkbox checkbox-inline"><label class="checkbox"><input type="checkbox" name="weekday"
                    data-weekday="4" checked="">Thursday</label></div>
              <div class="checkbox checkbox-inline"><label class="checkbox"><input type="checkbox" name="weekday"
                    data-weekday="5" checked="">Friday</label></div>
              <div class="checkbox checkbox-inline"><label class="checkbox"><input type="checkbox" name="weekday"
                    data-weekday="6" checked="">Saturday</label></div>
              <div class="checkbox checkbox-inline"><label class="checkbox"><input type="checkbox" name="weekday"
                    data-weekday="0" checked="">Sunday</label></div>
            </div>
          </div>
          <div class="form-group">
            <label for="type">{{trans('admin.type')}}</label>
          </div>
          <div class="form-group row">
            <div class="col-md-6">
              <select name="type" class="form-control">
                <option value="">{!!trans('admin.select_type')!!}</option>
                <option value="units" data-type="units">{!!trans('admin.units')!!}</option>
                <option value="rate" data-type="rate">{!!trans('admin.rate')!!}</option>
                <option value="single_rate" data-type="single_rate">{!!trans('admin.single_rate')!!}</option>
                <option value="min_stay" data-type="min_stay">{!!trans('admin.min_stay')!!}</option>
                <option value="max_stay" data-type="max_stay">{!!trans('admin.max_stay')!!}</option>
                <option value="closed" data-type="closed">{!!trans('admin.close')!!}</option>
              </select>
            </div>
            <div class="col-md-6">
              <input type="number" name="value" min="0" class="form-control">
            </div>
          </div>


        </form>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-raised btn-default">{{trans('admin.close')}}</button>
        <button type="button" class="btn btn-raised btn-black"
          onclick="availability.setLongTermData()">{{trans('admin.set')}}</button>
      </div>
    </div>
  </div>
</div>

<div tabindex="-1" role="dialog" aria-labelledby="synchronize_availability" class="modal fade"
  id="synchronize_availability">
  <div role="document" class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span
            aria-hidden="true">×</span></button>
        <h4 id="modalWithForm" class="modal-title">{{trans('admin.synchronize_availability')}}</h4>
      </div>
      <div class="modal-body">
        <form id="synchronize_availability">
          <div class="form-group">
            <label for="start_date">{{trans('admin.from')}}</label>
            <div class='input-group date'>
              <input type="text" name="start_date" class="form-control">
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
              </span>
            </div>
          </div>
          <div class="form-group">
            <label for="end_date">{{trans('admin.to')}}</label>
            <div class='input-group date'>
              <input type="text" name="end_date" class="form-control">
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
              </span>
            </div>
          </div>
          <div class="form-group">
            <label for="price">{{trans('admin.include_price')}}</label>
            <input type="checkbox" name="price" value="true" class="form-control">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-raised btn-default">{{trans('admin.close')}}</button>
        <button type="button" class="btn btn-raised btn-black"
          onclick="availability.synchronizeAvailability()">{{trans('admin.synchronize')}}</button>
      </div>
    </div>
  </div>
</div>

<div tabindex="-1" role="dialog" aria-labelledby="customize_calendar" class="modal fade" id="customize_calendar">
  <div role="document" class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span
            aria-hidden="true">×</span></button>
        <h4 id="modalWithForm" class="modal-title">{{trans('admin.customize_calendar')}}</h4>
      </div>
      <div class="modal-body">
        <form id="form_customize_calendar" method="get" action="">
          <div class="form-group">
            <label for="start_date">{{trans('admin.from')}}</label>
            <div class='input-group date'>
              <input type="text" name="start_date" value="{{Request::get('start_date')}}" class="form-control">
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
              </span>
            </div>
          </div>
          <div class="form-group">
            <label for="end_date">{{trans('admin.to')}}</label>
            <div class='input-group date'>
              <input type="text" name="end_date" value="{{Request::get('end_date')}}" class="form-control">
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
              </span>
            </div>
          </div>
          <div class="form-group">
            <label for="rooms_type[]">{{trans('admin.rooms_type')}}</label>
            @foreach ($rooms_type as $room_type)
            <div class="form-check">
              <input class="form-check-input" type="checkbox" name="rooms_type[]" value="{{$room_type->id}}"
                id="{{$room_type->id}}">
              <label class="form-check-label" for="{{$room_type->id}}">
                {{ Helper::get_traslated_string($room_type->type)}}
              </label>
            </div>

            @endforeach
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-raised btn-default">{{trans('admin.close')}}</button>
        <button type="button" class="btn btn-raised btn-black"
          onclick="$('#form_customize_calendar').submit()">{{trans('admin.customize')}}</button>
      </div>
    </div>
  </div>
</div>

@else
<div class="alert alert-warning">
  <p>{{trans('admin.no_rates_rooms_type_available')}}</p>
</div>
@endif
@push('script')
<script>
  var startDate = '{{$start}}';
        var endDate = '{{$end}}';
</script>
<script type="text/javascript"
  src="{!!asset('resources/assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.min.js')!!}"></script>
<script type="text/javascript" src="{{asset('resources/assets/admin/js/availability.js')}}"></script>
<script>
  availability.getAvailability(startDate,endDate);
</script>
@endpush

@stop