@extends('admin.main')

@push('styles')
<link rel="stylesheet" href="{{asset('resources/assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" type="text/css" rel="stylesheet">
<!-- Summernote-->
<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/admin/plugins/summernote/dist/summernote.css')}}">
@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Create new Offert</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li><a href="{{url('/admin/offerts')}}">Offerts</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> New Offerts
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
  <div class="widget">
    <div class="widget-heading">
                <h3 class="widget-title">New Offert</h3>
    </div>
      <div class="widget-body">
        {!! Form::open(['route' => 'offerts.store']) !!}
        <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                {!! Form::label('title', trans('admin.title')) !!}
                {!! Form::text('title', null, ['class' => 'form-control','placeholder'=> trans('admin.title')]) !!}
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  {!! Form::label('start_date', trans('admin.start')) !!}
              <div class='input-group date' id='start'>
              {!! Form::text('start_date', null, ['class' => 'form-control', 'placeholder'=> trans('admin.start')]) !!}
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  {!! Form::label('end_date', trans('admin.end')) !!}
              <div class='input-group date' id='end'>
              {!! Form::text('end_date', null, ['class' => 'form-control', 'placeholder'=> trans('admin.end')]) !!}
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                {!! Form::label('code', trans('admin.code')) !!}
                {!! Form::text('code', null, ['class' => 'form-control','placeholder'=> trans('admin.code')]) !!}
              </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-12">
              <div class="form-group">
              {!! Form::label('description', trans('admin.description')) !!}
              {!! Form::textarea('description', null, ['class' => 'form-control','placeholder'=> trans('admin.description')]) !!}
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-2">
              <div class="form-group">
              {!! Form::label('condition', trans('admin.condition')) !!}
              {!! Form::select('condition', ['percentage_value' => 'Percentuale', 'total_value' => 'Totale Valore'], null, ['class' => 'form-control','placeholder' => 'Seleziona una Condizione']) !!}
              </div>
          </div>
          <div class="col-md-2">
              <div class="form-group">
                {!! Form::label('value', trans('admin.value')) !!}
                {!! Form::number('value', '0.00', ['class' => 'form-control','placeholder'=> trans('admin.value'),'min'=>0 ,'step'=>'any']) !!}
              </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              {!! Form::label('status', trans('admin.status')) !!}
              {!! Form::select('status', ['CNF' => 'Confermata', 'CAN' => 'Cancellata', 'PND' => 'In Attesa'], null, ['class' => 'form-control','placeholder' => 'Seleziona una stato']) !!}
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                {!! Form::submit(trans('admin.add'), ['class' => 'btn btn-primary']) !!}
            </div>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
    @push('script')
      <script type="text/javascript" src="{{asset('resources/assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('resources/assets/admin/plugins/summernote/dist/summernote.min.js')}}"></script>
      <script>
          $(window).load(function () {

              $date_format = 'YYYY-MM-DD';
               $('input[name="start_date"]').daterangepicker({
                   autoUpdateInput: false,
                   locale: {
                     format: $date_format
                   },
                   allowInputToggle: true
               });
               $('input[name="end_date"]').daterangepicker({
                   autoUpdateInput: false,
                   locale: {
                     format: $date_format
                   },
                   allowInputToggle: true
               });

               $('input[name="start_date"]').on('apply.daterangepicker', function(ev, picker) {
                 $(this).val(picker.startDate.format($date_format));
                 $('input[name="end_date"]').val(picker.endDate.format($date_format));
               });
               $('input[name="end_date"]').on('apply.daterangepicker', function(ev, picker) {
                 $(this).val(picker.endDate.format($date_format));
                 $('input[name="start_date"]').val(picker.startDate.format($date_format));
               });

            $("#description").summernote();

          });
      </script>
    @endpush

@stop
