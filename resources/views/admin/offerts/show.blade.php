@extends('admin.main')

@push('styles')

@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Offerts</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Offerts
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')

    <a href="{{ url('admin/offerts/create')}}" class="btn btn-primary">{{trans('admin.add')}}</a>
    <hr>
    <table id="offert-list" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans('admin.title')}}</th>
                <th>{{ trans('admin.description')}}</th>
                <th>{{ trans('admin.code')}}</th>
                <th>{{ trans('admin.condition')}}</th>
                <th>{{ trans('admin.value')}}</th>
                <th>{{ trans('admin.start')}}</th>
                <th>{{ trans('admin.end')}}</th>
                <th>{{ trans('admin.status')}}</th>
                <th>{{ trans('admin.action')}}</th>
            </tr>
            </thead>
            <tbody>
            @if($offerts)
                @foreach ($offerts as $offert)
                    <tr>
                        <td>{{$offert->title}}</td>
                        <td style="width:20%">{!! $offert->description !!}</td>
                        <td>{{$offert->code}}</td>
                        <td>{{$offert->condition}}</td>
                        <td>{{$offert->value}}</td>
                        <td>{{$offert->start_date}}</td>
                        <td>{{$offert->end_date}}</td>
                        <td>{{$offert->status}}</td>
                        <td>
                            <a href="{{ route('offerts.edit', $offert->id) }}" class="btn btn-success"><i
                                        class="fa fa-edit"></i></a>
                            {!! Form::open(['method' => 'DELETE','route' => ['offerts.destroy', $offert->id], 'class' => 'inline']) !!}
                            {!! Form::submit(trans('admin.delete'), ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>

    @push('script')

    @endpush

@stop
