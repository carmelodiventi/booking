@extends('admin.main')

@push('styles')

@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Fees</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Fees
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop


@section('page-content')

      <a href="{{ url('admin/fees/create')}}" class="btn btn-primary">{{trans('admin.add')}}</a>
      <hr/>


        <table id="fees-list" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans('admin.name')}}</th>
                <th>{{ trans('admin.value')}}</th>
                <th>{{ trans('admin.percentage')}}</th>
                <th>{{ trans('admin.status')}}</th>
                <th>{{ trans('admin.action')}}</th>
            </tr>
            </thead>
            <tbody>
            @if($fees)
                @foreach ($fees as $fee)
                    <tr>
                        <td>{{$fee->name}}</td>
                        <td>{{$fee->value}}</td>
                        <td>{{$fee->percentage}}</td>
                        <td>{!! html_entity_decode(Helper::getStatus($fee->status)) !!}</td>
                        <td>
                            <a href="{{ route('fees.edit', $fee->id) }}" class="btn btn-success"><i
                                        class="fa fa-edit"></i></a>
                            {!! Form::open(['method' => 'DELETE','route' => ['fees.destroy', $fee->id], 'class' => 'inline']) !!}
                            {!! Form::submit(trans('admin.delete'), ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>

    @push('script')

    @endpush

@stop
