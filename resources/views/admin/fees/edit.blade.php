@extends('admin.main')

@push('styles')

@endpush


@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Edit a Fee</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li><a href="{{url('/admin/fees')}}">Fees</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Edit a Fees
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
    <div class="callout">
        {!! Form::model($fee,['method' => 'put', 'url' =>'admin/fees/'. $fee['id']])!!}
        <div class="row">

            <div class="col-md-4">
              <div class="form-group">
                {!! Form::label('name', trans('admin.name')) !!}
                {!! Form::text('name', null, ['class' => 'form-control','placeholder'=> trans('admin.name')]) !!}
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                {!! Form::label('value', trans('admin.value')) !!}
                {!! Form::number('value', null, ['class' => 'form-control','min'=>'0', 'step'=>'any','placeholder'=> trans('admin.value')]) !!}
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                {!! Form::label('', trans('admin.percentage')) !!}
                <div class="checkbox-custom">
                  {!! Form::checkbox('percentage', null, null,['id'=>'percentage']) !!}
                  {!! Form::label('percentage', trans('admin.is_percentage')) !!}
                </div>
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                {!! Form::label('status', trans('admin.status')) !!}
                {!! Form::select('status', ['CNF' => 'Confermata', 'CAN' => 'Chiusa', 'PND' => 'In Attesa'], null, ['placeholder' => 'Seleziona una stato','class' => 'form-control']) !!}
              </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                {!! Form::submit(trans('admin.edit'), ['class' => 'btn btn-primary']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    @push('script')

    @endpush

@stop
