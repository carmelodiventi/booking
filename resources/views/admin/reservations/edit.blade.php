@extends('admin.main')

@push('styles')
<link rel="stylesheet" href="{{asset('resources/assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" type="text/css" rel="stylesheet">
<style>
    .button-group input {
        display: none;
    }

    .extra {
        display: none;
    }
</style>
@endpush

@section('page-header')
    <div class="row">
      <div class="col-sm-6">
        <h4 class="mt-0 mb-5">Edit a Reservation {{$reservations['pnr']}}</h4>
        <ol class="breadcrumb mb-0">
          <li><a href="{{url('/admin')}}">Home</a></li>
          <li><a href="{{url('/admin/reservations')}}">Reservations</a></li>
          <li class="active">
              <span class="show-for-sr">Current: </span> Edit a Reservation
          </li>
        </ol>
      </div>
      <div class="col-sm-6">
        @include('admin.includes.language')
      </div>
    </div>
@stop

@section('page-content')

    {!! Form::model($reservations,['method' => 'put', 'url' =>'admin/reservations/'. $reservations['id'],'id'=>'form-reservation','novalidate'=>'novalidate'])!!}
    <fieldset>
      <legend>{{ trans('admin.customer')}}</legend>
      @if($customers)
          <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  {!! Form::label('customer', trans('admin.customer')) !!}
                  {!! Form::select('customer', $customers, $reservations['customer_id'], ['class'=> 'form-control','placeholder' => trans('admin.customer_select'),'data-rule-required'=>'true']) !!}
                </div>
              </div>
          </div>
          <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                  <a href="javascript:void(0)" onclick="reservation.getCustomers()" class="btn btn-primary">{!!trans('admin.update_list')!!} <img src="{{asset('resources/assets/admin/img/loader.gif')}}" id="loader" style="display:none"/ ></a>
                  <a href="{{url('admin/customers/create')}}" class="btn btn-success" target="_blank">{!!trans('admin.add_new')!!}</a>
                  </div>
                </div>
            </div>
      @endif
    </fieldset>
    <hr>
      <fieldset>
        <legend>{{ trans('admin.stay')}}</legend>
      <div class="row">
        <div class="col-md-5">
            <div class="form-group">
              {!! Form::label('check_in', trans('admin.check_in')) !!}
              <div class='input-group date' id='check_in_date'>
              {!! Form::text('check_in', null, ['class'=> 'form-control','placeholder' => trans('admin.check_in'),'data-rule-required'=>'true','data-rule-date'=>'true']) !!}
                  <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                  </span>
              </div>
          </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
              {!! Form::label('check_out', trans('admin.check_out')) !!}
              <div class='input-group date' id='check_out_date'>
              {!! Form::text('check_out', null, ['class'=> 'form-control','placeholder' => trans('admin.check_out'),'data-rule-required'=>'true','data-rule-date'=>'true']) !!}
                  <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                  </span>
              </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                  {!! Form::label('nights', trans('admin.stay')) !!}
                  <div class="label label-info block p-10 fs-14">
                    <span data-night></span> {{trans('admin.nights')}}
                  </div>
            </div>
        </div>
      </div>
      </fieldset>
      <hr>
      <fieldset>
        <legend>{{ trans('admin.rooms')}}</legend>
            <div id="rooms">
              @if(!$rooms_booked->isEmpty())
                @php $i = 0; @endphp
                @foreach ($rooms_booked as $room_booked)
                  <div class="row">
                    <div class="col-md-9">
                      <div class="row clonedInput" data-room="{{$i}}">
                        @if($rooms)
                        <div class="col-md-3">
                          <div class="form-group">
                            {!! Form::label('rooms', trans('admin.rooms')) !!}
                            {!! Form::select('rooms[]', $rooms, $room_booked->room_id, ['class'=> 'form-control','placeholder'=> trans('admin.room_select'),'data-rule-required'=>'true']) !!}
                        </div>
                        </div>
                        @endif
                        @if($rooms_type)
                        <div class="col-md-3">
                          <div class="form-group">
                          {!! Form::label('rooms_type', trans('admin.rate')) !!}
                          <select name="rooms_type[]" class="form-control" data-rule-required="true">
                            <option value="">{{trans('admin.rooms_type_select')}}</option>
                            @foreach ($rooms_type as $key => $value)
                              <option value="{{$key}}" {{$key == $room_booked->rate_id ? 'selected' : ''}}>{{Helper::get_traslated_string($value)}}</option>
                            @endforeach
                          </select>
                        </div>
                        </div>
                        @endif
                        <div class="col-md-2">
                          <div class="form-group">
                            {!! Form::label('guests', trans('admin.guests')) !!}
                            {!! Form::number('guests[]', $room_booked->guests, ['class'=> 'form-control','placeholder' => trans('admin.guests'), 'min'=> 1,'data-rule-required'=>'true']) !!}
                        </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                          {!! Form::label('price', trans('admin.stay_cost_per_room')) !!}
                          {!! Form::number('price[]', $room_booked->price, ['class'=> 'form-control','placeholder' => trans('admin.stay_cost_per_room'), 'min'=> 0,'data-rule-required'=>'true']) !!}
                        </div>
                        </div>
                    </div>
                    </div>
                    @if($i == 0)
                      <div class="col-md-3 text-right">
                            <div class="form-group">
                            {!! Form::button( '<i class="fa fa-plus"></i> ' . trans('admin.add'), ['class' => 'btn btn-primary clone', 'style'=>'margin-top:25px']) !!}
                            {!! Form::button( '<i class="fa fa-minus"></i> ' . trans('admin.remove'), ['class' => 'btn btn-primary remove', 'style'=>'margin-top:25px']) !!}
                          </div>
                       </div>
                    @endif

                   </div>
                    @php $i++; @endphp
                @endforeach
                @else
                      <div class="row">
                      <div class="col-md-9">
                          <div class="row clonedInput" data-room="0">
                            <!-- rooms line -->
                            @if($rooms)
                                <div class="col-md-3">
                                  <div class="form-group">
                                    {!! Form::label('rooms', trans('admin.rooms')) !!}
                                    {!! Form::select('rooms[]', $rooms, null, ['class'=> 'form-control','placeholder'=> trans('admin.room_select'),'data-rule-required'=>'true']) !!}
                                </div>
                              </div>
                            @endif
                            @if($rooms_type)
                              <div class="col-md-3 options">
                                <div class="form-group">
                                  {!! Form::label('rooms_type', trans('admin.rate')) !!}
                                  {!! Form::select('rooms_type[]', $rooms_type, null, ['class'=> 'form-control','placeholder'=> trans('admin.rooms_type_select')]) !!}
                                </div>
                              </div>
                            @endif
                            <div class="col-md-2 options">
                              <div class="form-group">
                                {!! Form::label('guests', trans('admin.guests')) !!}
                                {!! Form::number('guests[]', '1', ['class'=> 'form-control','placeholder' => trans('admin.guests'), 'min'=> 1,'data-rule-required'=>'true']) !!}
                            </div>
                            </div>
                            <div class="col-md-4 options">
                              <div class="form-group">
                                {!! Form::label('price', trans('admin.stay_cost_per_room')) !!}
                                {!! Form::number('price[]', '0.00', ['class'=> 'form-control','placeholder' => trans('admin.stay_cost_per_room'),'min'=> 0, 'onchange'=>'reservation.getTotal()','data-rule-required'=>'true']) !!}
                            </div>
                            </div>
                            <!-- end rooms line -->
                            </div>
                      </div>
                      <div class="col-md-3 options text-right">
                        <div class="form-group">
                        {!! Form::button( '<i class="fa fa-plus"></i> ' . trans('admin.add'), ['class' => 'btn btn-primary clone', 'style'=>'margin-top:25px']) !!}
                        {!! Form::button( '<i class="fa fa-minus"></i> ' . trans('admin.remove'), ['class' => 'btn btn-primary remove', 'style'=>'margin-top:25px']) !!}
                      </div>
                      </div>
                  </div>
              @endif
            </div>
      </fieldset>
          <hr>
      <fieldset>
        <legend>{{ trans('admin.origin')}}</legend>
            <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                {!! Form::label('origin', trans('admin.origin')) !!}
                {!! Form::select('origin', $portals, $portals_commission['portal_id'],['class'=> 'form-control','placeholder' => trans('admin.origin')]) !!}
                </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                {!! Form::label('commission_percentage', trans('admin.origin_percentage')) !!}
                {!! Form::number('commission_percentage', $portals_commission['percentage'], ['class'=> 'form-control','placeholder' => trans('admin.origin_percentage'),'min'=> 0]) !!}
                </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                {!! Form::label('transaction_code', trans('admin.origin_code')) !!}
                {!! Form::text('transaction_code', $portals_commission['transaction_code'], ['class'=> 'form-control','placeholder' => trans('admin.origin_code')]) !!}
                </div>
            </div>
        </div>
      </fieldset>
          <hr>
       <fieldset>
          <legend>{{ trans('admin.note')}}</legend>
            <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    {!! Form::label('note', trans('admin.note')) !!}
                    {!! Form::text('note', $reservations['reservation_note'], ['class'=> 'form-control','placeholder' => trans('admin.note'),'row'=> 5 ]) !!}
                </div>
                </div>
            </div>
        </fieldset>
          <hr>
          <fieldset>
              <legend>{{ trans('admin.adjustments')}}</legend>
                <div class="row">
                <div class="col-md-2">
                    <div class="button-group">
                      <div class="form-group">
                        <button type="button"
                        data-toggle="modal" data-target="#modal-adjustments"
                        class="btn btn-success">
                         <i class="ti-plus"></i>  {{trans('admin.add')}}
                        </button>
                    </div>
                    </div>
                </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <table class="table table-bordered">
                      <thead><tr><th>{{trans('admin.description')}}</th> <th>{{trans('admin.price')}}</th></tr></thead>
                      <tbody id="adjustaments-list"></tbody>
                    </table>
                  </div>
                </div>
          </fieldset>
          <hr>
      <fieldset>
        <legend>{{ trans('admin.payment_situation')}}</legend>
          <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                  {!! Form::label('advance', trans('admin.advance')) !!}
                  {!! Form::number('advance', $reservations['advance'], ['class'=> 'form-control','placeholder' => trans('admin.advance'), 'onblur' => 'reservation.getTotal()','data-rule-required'=>'true']) !!}
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  {!! Form::label('topay', trans('admin.topay')) !!}
                  {!! Form::number('topay', $reservations['topay'], ['class'=> 'form-control','placeholder' => trans('admin.topay'),'readonly'=>'readonly','data-rule-required'=>'true']) !!}
                </div>
             </div>
              <div class="col-md-2">
                <div class="form-group">
                  {!! Form::label('discount', trans('admin.discount')) !!}
                  {!! Form::number('discount', $reservations['discount'], ['class'=> 'form-control','placeholder' => trans('admin.discount'), 'onblur' => 'reservation.getTotal()','data-rule-required'=>'true']) !!}
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  {!! Form::label('subtotal', trans('admin.sub_total')) !!}
                  {!! Form::number('subtotal', $reservations['sub_total'], ['class'=> 'form-control','placeholder' => trans('admin.total'),'onchange' => 'reservation.getTotal()','data-rule-required'=>'true']) !!}
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  {!! Form::label('total', trans('admin.total')) !!}
                  {!! Form::number('total', $reservations['total'], ['class'=> 'form-control','placeholder' => trans('admin.total'),'readonly'=>'readonly','data-rule-required'=>'true']) !!}
                </div>
              </div>
          </div>
        </fieldset>
          <hr>
        <fieldset>
            <legend>{{ trans('admin.status')}}</legend>
          <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  {!! Form::select('status', ['CNF' => 'Confermata ma non pagata o Pagata parzialmente', 'SLD'=> 'Prenotazione Saldata', 'CKO' => 'Checkout', 'OPN' => 'Opzione', 'ALT'=> 'Prenotazioni alert'], null, ['class'=> 'form-control','placeholder' => 'Seleziona una stato','data-rule-required'=>'true']) !!}
        </div>
              </div>
          </div>
        </fieldset>
    <hr>
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          {!! Form::submit(trans('admin.edit'), ['class' => 'btn btn-primary']) !!}
        </div>
      </div>
    </div>
      {!! Form::close() !!}

      <div class="modal fade" id="modal-rates" tabindex="-1" role="dialog" aria-labelledby="modal-rates">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">{{trans('admin.rates')}}</h4>
            </div>
            <div class="modal-body">
                <table class="table table-hover">
                  <thead>
                    <tr><th>{{trans('admin.rate')}}</th><th>{{trans('admin.cost')}}</th></tr>
                  </thead>
                  <tbody id="rates">
                  </tbody>
                </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
              <button type="button" class="btn btn-primary">{{trans('admin.save')}}</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modal-adjustments" tabindex="-1" role="dialog" aria-labelledby="modal-adjustments">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">{{trans('admin.adjustments')}}</h4>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    {!! Form::label('adjustment', trans('admin.description')) !!}
                    {!! Form::text('adjustment', null, ['class'=> 'form-control','placeholder' => trans('admin.description'),'data-rule-required'=>'true']) !!}
                  </div>
                </div>
                </div>
                <div class="row">
                  <div class="col-md-5">
                    <div class="form-group">
                      {!! Form::label('application_modality', trans('admin.application_modality')) !!}
                      <div class="radio">
                        <label>
                          {{ Form::radio('adjustment[type]',0,true)}}
                          {{trans('admin.fixed')}}
                        </label>
                      </div>
                      <div class="radio">
                      <label>
                        {{ Form::radio('adjustment[type]',1) }}
                        {{trans('admin.percentage')}}
                      </label>
                      </div>
                    </div>
                  </div>
                <div class="col-md-7">
                  <div class="form-group">
                    {!! Form::label('price', trans('admin.price')) !!}
                    {!! Form::number('adjustment[amount]', null, ['class'=> 'form-control','placeholder' => trans('admin.price'),'step'=>'0.1']) !!}
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
              <button type="button" class="btn btn-primary" onclick="reservation.addAdjustments()">{{trans('admin.add')}}</button>
            </div>
          </div>
        </div>
      </div>

    @push('script')
    <script type="text/javascript" src="{{asset('resources/assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{url('resources/assets/admin/plugins/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script type="text/javascript" src="{{url('resources/assets/admin/plugins/jquery-validation/dist/additional-methods.min.js')}}"></script>
    <script src="{{url('resources/assets/admin/js/reservation.js')}}"></script>
    @endpush

@stop
