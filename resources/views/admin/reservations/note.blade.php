@extends('admin.main')

@push('styles')

@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Reservation  <a href="{{url('admin/reservation/details') . '/' . $reservation->id}}"> {{$reservation->pnr}}</a></h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li><a href="{{url('/admin/reservation')}}">Reservation</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Edit Note
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
    <div class="widget">
      <div class="widget-heading">
                  <h3 class="widget-title">Edit Note</h3>
      </div>
        <div class="widget-body">
            {!! Form::open(['url' => LaravelLocalization::getLocalizedURL()])!!}
            <div class="row">
              <div class="col-md-12">
            <div class="form-group">
              {!! Form::textarea('note',$reservation->reservation_note,['class'=>'form-control'])!!}
            </div>
              </div>
            </div>
                <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                {!!Form::submit(trans('admin.add'),['class'=>'btn btn-primary'])!!}
              </div>
                </div>
                  </div>
            {!! Form::close() !!}
        </div>
    </div>
    @push('script')

    @endpush

@stop
