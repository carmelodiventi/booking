@extends('admin.main')

@push('styles')

@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Reservation  <a href="{{url('admin/reservation/details') . '/' . $reservation->id}}"> {{$reservation->pnr}}</a></h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li><a href="{{url('/admin/reservation')}}">Reservation</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Edit Agency
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
    <div class="widget">
      <div class="widget-heading">
                  <h3 class="widget-title">Edit Agency</h3>
      </div>
        <div class="widget-body">
            {!! Form::open(['url' =>LaravelLocalization::getLocalizedURL()])!!}
                @include('admin.includes.form.agency',['text_btn'=> trans('admin.edit')])
            {!! Form::close() !!}
        </div>
    </div>
    @push('script')
    <script>
        $('select[name="country"]').chosen();
    </script>
    @endpush

@stop
