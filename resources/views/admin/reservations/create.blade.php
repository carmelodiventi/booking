@extends('admin.main')

@push('styles')
<link rel="stylesheet" href="{{asset('resources/assets/admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="{{asset('resources/assets/admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="{{asset('resources/assets/admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.standalone.min.css')}}" type="text/css" rel="stylesheet">
<style>
    .button-group input {
        display: none;
    }

    #credit_card,
    #payments,
    .extra {
        display: none;
    }

    .prices hr:last-child,
    .guests hr:last-child{
      display: none;
    }
</style>
@endpush

@push('script')
<script type="text/javascript" src="{{asset('resources/assets/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{asset('resources/assets/admin/plugins/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{asset('resources/assets/admin/plugins/jquery-validation/dist/additional-methods.min.js')}}"></script>
<script src="{{asset('resources/assets/admin/js/reservation.js')}}"></script>
@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Create a new Reservation</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li><a href="{{url('/admin/reservations')}}">Reservations</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Add Reservation
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
{!! Form::open(['route' => 'reservations.store','id'=>'form-reservation','novalidate'=>'novalidate']) !!}

  <ul role="tablist" class="nav nav-tabs mb-15">
                    <li role="presentation" class="active"><a href="#reservation-data" aria-controls="general" role="tab" data-toggle="tab" aria-expanded="true">{{trans('admin.data_of_stay')}}</a></li>
                    <li role="presentation" class=""><a href="#select-rooms" aria-controls="general" role="tab" data-toggle="tab" aria-expanded="false">{{trans('admin.select_rooms')}}</a></li>
                    <li role="presentation" class=""><a href="#extra-costs" aria-controls="general" role="tab" data-toggle="tab" aria-expanded="false">{{trans('admin.cost_and_extras')}}</a></li>
                </ul>
                <div class="tab-content">
                    <div id="reservation-data" role="tabpanel" class="tab-pane active">
                      <fieldset>
                        <legend>{{ trans('admin.customer')}}</legend>
                        @if($customers)
                            <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    {!! Form::label('customer', trans('admin.customer')) !!}
                                    {!! Form::select('customer', $customers, old('customer'), ['class'=> 'form-control','placeholder' => trans('admin.customer_select'),'data-rule-required'=>'true']) !!}
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                    <a href="javascript:void(0)" onclick="reservation.getCustomers()" class="btn btn-primary">{!!trans('admin.update_list')!!} <img src="{{asset('resources/assets/admin/img/loader.gif')}}" id="loader" style="display:none"/ ></a>
                                    <a href="{{url('admin/customers/create')}}" class="btn btn-success" target="_blank">{!!trans('admin.add_new')!!}</a>
                                    </div>
                                  </div>
                              </div>
                        @endif
                      </fieldset>
                      <hr>
                      <fieldset>
                        <legend>{{ trans('admin.stay')}}</legend>
                        <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                              {!! Form::label('check_in', trans('admin.check_in')) !!}
                              <div class='input-group date'>
                              {!! Form::text('check_in', $stay['check_in'], ['class'=> 'form-control','placeholder' => trans('admin.check_in'),'data-rule-required'=>'true','data-rule-date'=>'true']) !!}
                                  <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                              </div>
                          </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                              {!! Form::label('check_out', trans('admin.check_out')) !!}
                              <div class='input-group date'>
                              {!! Form::text('check_out', $stay['check_out'], ['class'=> 'form-control','placeholder' => trans('admin.check_out'),'data-rule-required'=>'true','data-rule-date'=>'true']) !!}
                                  <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                  {!! Form::label('nights', trans('admin.stay')) !!}
                                  <div class="label label-info block p-10 fs-14">
                                    <span data-night></span> {{trans('admin.nights')}}
                                  </div>
                            </div>
                        </div>
                        </div>
                      </fieldset>
                  </div>
                    <div id="select-rooms" role="tabpanel" class="tab-pane">
                      <!-- Rooms Form -->
                        @include('admin.includes.form.rooms_type',['multiple' => true])
                      <!-- // Rooms Form -->
                    </div>
                   <div id="extra-costs" role="tabpanel" class="tab-pane">
                            <fieldset>
                              <legend>{{ trans('admin.origin')}}</legend>
                                  <div class="row">
                                  <div class="col-md-4">
                                    <div class="form-group">
                                      {!! Form::label('origin', trans('admin.origin')) !!}
                                      {!! Form::select('origin', $portals, old('origin'),['class'=> 'form-control']) !!}
                                      </div>
                                  </div>
                                  <div class="col-md-2">
                                    <div class="form-group">
                                      {!! Form::label('commission_percentage', trans('admin.origin_percentage')) !!}
                                      {!! Form::number('commission_percentage', old('commission_percentage'), ['class'=> 'form-control','placeholder' => trans('admin.origin_percentage'),'min'=> 0]) !!}
                                      </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      {!! Form::label('transaction_code', trans('admin.origin_code')) !!}
                                      {!! Form::text('transaction_code', old('transaction_code'), ['class'=> 'form-control','placeholder' => trans('admin.origin_code')]) !!}
                                      </div>
                                  </div>
                              </div>
                            </fieldset>
                                <hr>
                             <fieldset>
                                <legend>{{ trans('admin.note')}}</legend>
                                  <div class="row">
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          {!! Form::label('note', trans('admin.note')) !!}
                                          {!! Form::text('note', null, ['class'=> 'form-control','placeholder' => trans('admin.note'),'row'=> 5 ]) !!}
                                      </div>
                                      </div>
                                  </div>
                              </fieldset>
                                <hr>
                            <fieldset>
                                <legend>{{ trans('admin.adjustments')}}</legend>
                                  <div class="row">
                                  <div class="col-md-2">
                                      <div class="button-group">
                                        <div class="form-group">
                                          <button type="button"
                                          data-toggle="modal" data-target="#modal-adjustments"
                                          class="btn btn-success">
                                           <i class="ti-plus"></i>  {{trans('admin.add')}}
                                          </button>
                                      </div>
                                      </div>
                                  </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-12">
                                      <table class="table table-bordered">
                                        <thead><tr><th>{{trans('admin.description')}}</th> <th>{{trans('admin.price')}}</th><th></th></tr></thead>
                                        <tbody id="adjustaments-list"></tbody>
                                      </table>
                                    </div>
                                  </div>
                            </fieldset>
                                <hr>
                            <fieldset>
                              <legend>{{ trans('admin.payment_situation')}}</legend>
                                <div class="row">
                                  <div class="col-md-3">
                                    <div class="checkbox">
                                      <label>
                                        {!! Form::checkbox('register_deposit', null, false,['onclick'=>'reservation.showPayments();']) !!}
                                        {{trans('admin.register_deposit')}}</label>
                                    </div>
                                  </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12"><hr></div>
                                    </div>
                                    <div id="payments">
                                    <div class="row">
                                  @if($payments_list)
                                      <div class="col-md-3">
                                        {!! Form::label('topay', trans('admin.payment_method')) !!}
                                          <div class="form-group">
                                            {!! Form::select('payment_method', $payments_list,null, ['class'=> 'form-control','placeholder' => trans('admin.payment_method'),'data-rule-required'=>'true', 'onchange'=>'reservation.switchPayment(this)']) !!}
                                        </div>
                                      </div>
                                  @endif
                                  <div class="col-md-2">
                                    <div class="form-group">
                                      {!! Form::label('amount', trans('admin.amount')) !!}
                                      {!! Form::number('amount', '0.00', ['class'=> 'form-control','placeholder' => trans('admin.amount'), 'onblur' => 'reservation.getTotal()','data-rule-required'=>'true','min'=>0,'step'=>'any']) !!}
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="checkbox">
                                        <label>
                                          {!! Form::checkbox('received', null, true) !!}
                                          {{trans('admin.record_as_received')}}
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                </div>
                                <div id="credit_card">
                                <label>{!!trans('booking.credit_card')!!}</label>
                                  <div class="row">
                                    <div class="col-xs-12 col-md-4">
                                      <div class="form-group">
                                        <input type="text" class="form-control" name="firstName_cc" placeholder="{{trans('booking.firstName_cc')}}">
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                      <div class="form-group">
                                        <input type="text" class="form-control" name="lastName_cc" placeholder="{{trans('booking.lastName_cc')}}">
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                      <div class="form-group">
                                        <select name="type_cc" class="form-control">
                                          <option value="visa">Visa</option>
                                          <option value="mastercard">Mastercard</option>
                                          <option value="amex">Amex</option>
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-xs-12 col-md-8">
                                        <div class="form-group">
                                          <input type="text" class="form-control" name="number_cc" placeholder="{{trans('booking.number_cc')}}">
                                        </div>
                                      </div>
                                      <div class="col-xs-12 col-md-4">
                                      <div class="row">
                                        <div class="col-xs-6 col-md-6">
                                          <div class="form-group">
                                            <input type="text" class="form-control" name="month_exp_cc" placeholder="{{trans('booking.month_exp_cc')}}">
                                          </div>
                                        </div>
                                        <div class="col-xs-6 col-md-6">
                                          <div class="form-group">
                                            <input type="text" class="form-control" name="year_exp_cc" placeholder="{{trans('booking.year_exp_cc')}}">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-xs-10">
                                        <small class="form-text">{{trans('booking.credit_card_condition')}}</small>
                                    </div>
                                    <div class="col-xs-2">
                                      <span class="form-text">
                                        <i class="ion-locked"></i>
                                        <strong>Secure area</strong><br><small>Your data 100% protected</small>
                                        <img src="{{asset('resources/assets/front/img/comodo_secure_seal.png')}}">
                                      </span>
                                    </div>
                                  </div>
                                </div>


                                <div class="row">
                                  <div class="col-md-12"><hr></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                      <div class="form-group">
                                        {!! Form::label('topay', trans('admin.topay')) !!}
                                        {!! Form::number('topay', '0.00', ['class'=> 'form-control','placeholder' => trans('admin.topay'),'readonly'=>'readonly','data-rule-required'=>'true','min'=>0,'step'=>'any']) !!}
                                      </div>
                                   </div>
                                    <div class="col-md-2">
                                      <div class="form-group">
                                        {!! Form::label('discount', trans('admin.discount')) !!}
                                        {!! Form::number('discount', '0.00', ['class'=> 'form-control','placeholder' => trans('admin.discount'), 'onblur' => 'reservation.getTotal()','data-rule-required'=>'true','min'=>0,'step'=>'any']) !!}
                                      </div>
                                    </div>
                                    <div class="col-md-2">
                                      <div class="form-group">
                                        {!! Form::label('subtotal', trans('admin.sub_total')) !!}
                                        {!! Form::number('subtotal', '0.00', ['class'=> 'form-control','placeholder' => trans('admin.total'),'onchange' => 'reservation.getTotal()','data-rule-required'=>'true','min'=>0,'step'=>'any']) !!}
                                      </div>
                                    </div>
                                    <div class="col-md-2">
                                      <div class="form-group">
                                        {!! Form::label('total', trans('admin.total')) !!}
                                        {!! Form::number('total', '0.00', ['class'=> 'form-control','placeholder' => trans('admin.total'),'readonly'=>'readonly','data-rule-required'=>'true','min'=>0,'step'=>'any']) !!}
                                      </div>
                                    </div>
                                </div>
                              </fieldset>
                                <hr>
                              <fieldset>
                                  <legend>{{ trans('admin.status')}}</legend>
                                <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                          <div class="checkbox">
                                            <label>
                                              {{Form::checkbox('mark_to_review',1,false)}}
                                            {{trans('admin.mark_to_review')}}
                                          </label>
                                          </div>
                                      </div>
                                    </div>
                                </div>
                              </fieldset>
                          <hr>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                {!! Form::submit(trans('admin.add'), ['class' => 'btn btn-primary']) !!}
                              </div>
                            </div>
                          </div>
                    </div>
                </div>
    {!! Form::close() !!}
    <div class="modal fade" id="modal-adjustments" tabindex="-1" role="dialog" aria-labelledby="modal-adjustments">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">{{trans('admin.adjustments')}}</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  {!! Form::label('adjustment', trans('admin.description')) !!}
                  {!! Form::text('adjustment', null, ['class'=> 'form-control','placeholder' => trans('admin.description'),'data-rule-required'=>'true']) !!}
                </div>
              </div>
              </div>
              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    {!! Form::label('application_modality', trans('admin.application_modality')) !!}
                    <div class="radio">
                      <label>
                        {{ Form::radio('adjustment[type]',0,true)}}
                        {{trans('admin.fixed')}}
                      </label>
                    </div>
                    <div class="radio">
                    <label>
                      {{ Form::radio('adjustment[type]',1) }}
                      {{trans('admin.percentage')}}
                    </label>
                    </div>
                  </div>
                </div>
              <div class="col-md-7">
                <div class="form-group">
                  {!! Form::label('price', trans('admin.price')) !!}
                  {!! Form::number('adjustment[amount]', null, ['class'=> 'form-control','placeholder' => trans('admin.price'),'min'=>0,'step'=>'any']) !!}
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
            <button type="button" class="btn btn-primary" onclick="reservation.addAdjustments()">{{trans('admin.add')}}</button>
          </div>
        </div>
      </div>
    </div>


@stop
