@extends('admin.main')

@push('styles')
@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Reservation  <a href="{{url('admin/reservation/details') . '/' . $reservation->id}}"> {{$reservation->pnr}}</a></h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span>  Reservations Adjustments
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
  <div class="widget">
    <div class="widget-body">
      {{Form::open(['url' => LaravelLocalization::getLocalizedURL()])}}
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            {!! Form::label('description', trans('admin.description')) !!}
            {!! Form::text('description', null, ['class'=> 'form-control','placeholder' => trans('admin.description'),'data-rule-required'=>'true']) !!}
          </div>
        </div>
        </div>
        <div class="row">
          <div class="col-md-5">
            <div class="form-group">
              {!! Form::label('application_modality', trans('admin.application_modality')) !!}
              <div class="radio">
                <label>
                  {{ Form::radio('adjustaments[type]', '0')}}
                  {{trans('admin.fixed')}}
                </label>
              </div>
              <div class="radio">
              <label>
                {{ Form::radio('adjustaments[type]', '1') }}
                {{trans('admin.percentage')}}
              </label>
              </div>
            </div>
          </div>
        <div class="col-md-5">
          <div class="form-group">
            {!! Form::label('price', trans('admin.price')) !!}
            {!! Form::text('amount', null, ['class'=> 'form-control','placeholder' => trans('admin.price'),'data-rule-required'=>'true', 'min'=>0,'step'=>'any']) !!}
          </div>
        </div>
      </div>
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              {!! Form::submit(trans('admin.add'),['class'=> 'btn btn-primary','placeholder' => trans('admin.price'),'data-rule-required'=>'true']) !!}
            </div>
          </div>
          </div>
      {{Form::close()}}
    </div>
  </div>
    @push('script')
    @endpush
@stop
