@extends('admin.main')

@push('styles')
@endpush

@push('script')
  <script type="text/javascript" src="{{asset('resources/assets/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('resources/assets/admin/plugins/jquery-validation/dist/jquery.validate.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('resources/assets/admin/plugins/jquery-validation/dist/additional-methods.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('resources/assets/admin/js/reservation.js')}}"></script>
@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Reservation {{$reservation->pnr}}</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span>  Reservations
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
  <div class="widget">
    <div class="widget-body">
      <div class="row">
        <div class="col-sm-3">
              <address>
                <strong>{!! $reservation->name !!} {!! $reservation->surname !!}</strong>
                <br>
                <abbr title="Phone">P:</abbr> {!! $reservation->phone !!} <a href="mailto:{!! $reservation->email !!}">{!! $reservation->email !!}</a>
                <br>
                {!! $reservation->country !!}
             </address>
             <a href="{{url('admin/reservation/customer') .'/'. $reservation->id}}" class="btn btn-primary btn-outline">{{trans('admin.edit')}}</a>
             <a href="{{url('admin/reservation/email') .'/'. $reservation->id}}" class="btn btn-default btn-outline">{{trans('admin.send_confirm_email')}}</a>
        </div>
        <div class="col-sm-3">
          <ul class="list-unstyled">
            <li>{!! trans('admin.portal') !!}:<strong>{!! $reservation->portal_name !!}</strong> </li>
            <li>{!! trans('admin.transaction_code') !!}:<strong> {!! $reservation->transaction_code !!}</strong> </li>
            <li>{!! trans('admin.commission_cost') !!}:<strong> {!! Helper::get_money($reservation->commission_cost) !!}</strong> </li>
          </ul>
          <div role="group" class="btn-group">
            <a href="{{url('admin/reservation/agency') .'/'. $reservation->id}}" class="btn btn-primary btn-outline">{{trans('admin.edit')}}</a>
          </div>
          @if($reservation->offert_code)
            <ul class="list-unstyled">
              <li>
                {!! trans('admin.offert_code') !!}: <strong>{!!$reservation->offert_code!!}</strong>
                <br>
                <small>{!! trans('admin.offert_condition') !!}</small>
              </li>
            </ul>
          @endif
        </div>
        <div class="col-sm-3">
          @if($reservation->payment_method)
            <ul class="list-unstyled">
              <li>
                {!! trans('admin.payment_method') !!} :
                @if($reservation->payment_method == 'credit_card')
                  <strong>{!!trans('admin.credit_card')!!}</strong>
                  <button class="btn btn-sm btn-raised btn-success"
                  data-toggle="popover"
                  data-placement="top"
                  data-html="true"
                  title="{{trans('admin.credit_card')}}"
                  data-content="{!! Helper::get_card($reservation->credit_card_data) !!}">
                    {{trans('admin.show_credit_card')}}
                  </button>
                @endif
                @if($reservation->payment_method == 'expresscheckout')
                  <strong>{!!trans('admin.paypal')!!}</strong>
                  <br>
                  <small>{{trans('admin.transaction_id')}} : {{$reservation->transaction_id}}</small>
                @endif
              </li>
            </ul>
          @elseif($reservation->credit_card_data)
            <ul class="list-unstyled">
              <li>
                {!! trans('admin.payment_method') !!} :
                  <strong>{!!trans('admin.credit_card')!!}</strong>
                  <button class="btn btn-sm btn-raised btn-success"
                  data-toggle="popover"
                  data-placement="top"
                  data-html="true"
                  title="{{trans('admin.credit_card')}}"
                  data-content="{!! Helper::get_card($reservation->credit_card_data) !!}">
                    {{trans('admin.show_credit_card')}}
                  </button>
              </li>
            </ul>
          @endif
        </div>

        <div class="col-sm-3">
          <ul class="list-unstyled">
            <li>PNR: <strong>{!! $reservation->pnr !!}</strong></li>
            @if($reservation->myallocatorId)
              <li>MyAllocatorId: <strong><small>{!! $reservation->myallocatorId !!}</small></strong></li>
            @endif
            <li>{!! trans('admin.created_at')!!}: {!! Carbon::createFromFormat('Y-m-d H:s:i', $reservation->created_at)->formatLocalized('%A %d %B %Y %H:%M:%S')  !!}</li>
            <li>{!! trans('admin.updated_at')!!}: {!! Carbon::createFromFormat('Y-m-d H:s:i', $reservation->updated_at)->formatLocalized('%A %d %B %Y %H:%M:%S')  !!}</li>
            <li>{!! trans('admin.status') !!}:
              {!! Helper::getstatus($reservation->status,$reservation->review) !!}
            </li>
            @if($reservation_history->isNotEmpty())
              <li>
                {!! trans('admin.reservation_history')!!}:
                @foreach ($reservation_history as $key => $value)
                  @php
                    $resHistoryData = json_decode(unserialize($value->data));
                  @endphp
                  <a href="#{{$key}}-{{$value->reservation_id}}" data-toggle="modal" data-target="#{{$key}}-{{$value->reservation_id}}">{{$key}}-{{$value->reservation_id}}</a>
                  <div class="modal fade" id="{{$key}}-{{$value->reservation_id}}">
                  <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title">{{trans('admin.reservation_history')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <table class="table">
                          <thead>
                            <tr>
                              <th colspan="3">{{trans('admin.rooms')}}</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td colspan="3">
                                <table class="table">
                                  <thead>
                                    <tr>
                                      <th>{{trans('admin.name')}}</th>
                                      <th>{{trans('admin.check_in')}}</th>
                                      <th>{{trans('admin.check_out')}}</th>
                                    </tr>
                                  </thead>
                                  @foreach ($resHistoryData->rooms as $key => $room)
                                    <tr>
                                      <td>{{$room->name}}</td>
                                      <td>{{Carbon::createFromFormat('Y-m-d', $room->check_in)->formatLocalized('%A %d %B %Y')}}</td>
                                      <td>{{Carbon::createFromFormat('Y-m-d', $room->check_out)->addDays(1)->formatLocalized('%A %d %B %Y')}}</td>
                                    </tr>
                                  @endforeach
                                </table>
                              </td>
                              </tr>
                              <tr>
                              <th>{{trans('admin.note')}}</th>
                              <th>{{trans('admin.total')}}</th>
                              <th>{{trans('admin.status')}}</th>
                              </tr>
                              <tr>
                              <td>
                                {!! $resHistoryData->reservations->note !!}
                                <hr>
                                {!! $resHistoryData->reservations->reservation_note!!}
                              </td>
                              <td><strong>{!! Helper::get_money($resHistoryData->reservations->total) !!} </strong></td>
                              <td>{!! Helper::getstatus($resHistoryData->reservations->status,$resHistoryData->reservations->review)!!}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                      </div>
                    </div>
                  </div>
                </div>
                @endforeach
              </li>
            @endif
          </ul>

          <div class="btn-group mr-10">
                    <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-primary dropdown-toggle">{{trans('admin.edit')}}
                      <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                      <li>
                        <a href="{{url('admin/reservation/accomodations') .'/'. $reservation->id}}">{{trans('admin.edit_lodging_and_date')}}</a>
                      </li>
                      @if($reservation->review === 0)
                        <li><a href="{{url('admin/reservation/mark-to-review') .'/'. $reservation->id}}">{{trans('admin.mark_to_review')}}</a></li>
                      @elseif ($reservation->review === 1)
                        <li><a href="{{url('admin/reservation/mark-to-complete') .'/'. $reservation->id}}">{{trans('admin.mark_to_complete')}}</a></li>
                      @endif
                      @if($reservation->status !== 'CAN')
                        <li><a href="#" data-toggle="modal" data-target="#cancellationReasonModal"> {{trans('admin.cancel')}}</a></li>
                      @endif

                    </ul>
          </div>

        </div>
      </div>
      <hr>

      @if($rooms_booked)
          <div class="table-responsive">
              <table class="table table-striped">
                  <thead>
                  <tr>
                      <th></th>
                      <th>{{trans('admin.name')}}</th>
                      <th>{{trans('admin.check_in')}}</th>
                      <th>{{trans('admin.check_out')}}</th>
                      <th>{{trans('admin.rate')}}</th>
                      <th>{{trans('admin.guests')}}</th>
                      <th>{{trans('admin.price')}}</th>
                      <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($rooms_booked as $room)
                    @if(isset($room->reservation_data))
                      @php
                        $reservation_data = json_decode($room->reservation_data);
                        //is_array($reservation_data) ? $reservation_data = (object)$reservation_data : '';

                      @endphp
                    @endif
                      <tr>
                        <td>
                          @if(isset($room->reservation_data))

                            <button class="btn btn-primary" data-toggle="modal" data-target="#{{$room->id}}"><i class="ti-comment-alt"></i></button>
                            <div class="modal fade" id="{{$room->id}}" tabindex="-1" role="dialog" aria-labelledby="{!! preg_replace('/\s+/', '-', $room->name) !!}" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="{{$room->id}}-title">{{trans('admin.info')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <div class="form-horizontal">
                                      @if(isset($reservation_data->OccupantLName))
                                        <div class="form-group">
                                          <label class="col-sm-4 control-label"> {{trans('admin.occupant_name')}} </label>
                                          <div class="col-sm-8">
                                            {{$reservation_data->OccupantLName}}
                                          </div>
                                        </div>
                                      @endif
                                      @if(isset($reservation_data->OccupantSmoker))
                                        <div class="form-group">
                                          <label class="col-sm-4 control-label"> {{trans('admin.occupant_smoker')}} </label>
                                            <div class="col-sm-8">
                                              {{$reservation_data->OccupantSmoker}}
                                          </div>
                                        </div>
                                      @endif
                                      @if(isset($reservation_data->Policy))
                                          <div class="form-group">
                                            <label class="col-sm-4 control-label"> {{trans('admin.policy')}} </label>
                                            <div class="col-sm-8">
                                              {{$reservation_data->Policy}}
                                            </div>
                                        </div>
                                      @endif
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            </div>
                          @endif
                        </td>
                        <td>{{Helper::get_traslated_string($room->type)}} <small>({{$room->name}})</small></td>
                        <td>{{Carbon::createFromFormat('Y-m-d', $room->check_in)->formatLocalized('%A %d %B %Y')}}</td>
                        <td>{{Carbon::createFromFormat('Y-m-d', $room->check_out)->addDays(1)->formatLocalized('%A %d %B %Y')}} </td>
                        <td>
                          @if(isset($room->rate_name))
                              {{Helper::get_traslated_string($room->rate_name)}} 
                          @else(isset($room->rate_desc))    
                              <i class="fa fa-info" data-toggle="tooltip" data-placement="left" title="Rate Channel Name"></i> {{$room->rate_desc}} 
                          @endif
                        </td>
                        <td>
                          @if(isset($room->guests))
                              {{$room->guests}}
                          @endif
                        </td>
                        <td>
                          @if(isset($room->price))
                              {!!Helper::get_money($room->price)!!}
                          @endif
                        </td>
                        <td>
                          <div class="btn-group mr-10">
                                    <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-primary dropdown-toggle">{{trans('admin.edit')}}
                                      <span class="caret"></span></button>
                                      <ul class="dropdown-menu">

                                        <li><a href="{{url('admin/reservation/guests') .'/'. $reservation->id .'/'. $room->id}}">{{trans('admin.edit_guest')}}</a></li>
                                        @if($loop->count > 1 && $room->status == 'CNF')
                                          <li><a href="{{url('admin/reservation/accomodations/remove') .'/'. $reservation->id .'/'. $room->id}}">{{trans('admin.remove')}}</a></li>
                                        @endif
                                      </ul>
                          </div>
                        </td>
                      </tr>
                  @endforeach
                  </tbody>
              </table>
          </div>
      @endif

      <div class="row">
        <div class="col-md-12">
          <h5>{{trans('admin.note')}}</h5>
          <blockquote><p>{!! $reservation->note !!}</p></blockquote>
          <hr>
          <h5>{{trans('admin.reservation_note')}}</h5>
          <blockquote><p>{!! $reservation->reservation_note !!}</p></blockquote>
          <a href="{{url('admin/reservation/note').'/' . $reservation->id }}" class="btn btn-primary btn-sm btn-outline">{{trans('admin.edit')}}</a>
        </div>
      </div>

      <hr>

      <div class="row">
        <div class="col-md-8">
          <h5>{!! trans('admin.payments') !!}</h5>
          <p></p>
          <hr>

          @if(!$reservation_payments->isEmpty())
          <table class="table table-borderd">
            <tbody>

                  <tr>
                    <td>{{trans('admin.type')}}</td>
                    <td>{{trans('admin.amount')}}</td>
                    <td>{{trans('admin.payment_method')}}</td>
                    <td>{{trans('admin.payment_register')}}</td>
                    <td>{{trans('admin.payment_status')}}</td>
                    <td></td>
                  </tr>

                    @foreach ($reservation_payments as $payments)
                      <tr>
                      <td>
                        @if($payments->payment_type == 'deposit')
                          {{trans('admin.advance')}}
                        @else
                          {{trans('admin.payment')}}
                        @endif
                      </td>
                      <td>{!! Helper::get_money($payments->total)!!}</td>
                      <td>{{ Helper::get_payments_method($payments->method)}}</td>
                      <td>{{$payments->payment_date}}</td>
                      <td>
                        @if($payments->status == 'Welded')
                          <label class="label label-success">{{trans('admin.welded')}}</label>
                        @else
                          <label class="label label-warning">{{trans('admin.on_hold')}}</label>
                        @endif
                      </td>
                      <td>
                        <div class="btn-group btn-raised mr-10">
                          @if($payments->status == 'On Hold')
                          <a href="{{url('admin/payments/confirm').'/'.$payments->id . '/' . $reservation->id }}" class="btn btn-success btn-raised btn-sm">{{trans('admin.confirm_payment')}}</a>
                          @endif
                          <a href="{{url('admin/payments/delete').'/'.$payments->id . '/' . $reservation->id }}" class="btn btn-danger btn-raised btn-sm">{{trans('admin.cancel_payment')}}</a>
                      </div>
                      </td>
                      </tr>
                    @endforeach
            </tbody>
          </table>
        @else
          {{trans('admin.no_payments_found')}}
                @endif
        </div>
        <div class="col-md-4">
          <h5>{!! trans('admin.adjustments') !!}</h5>
          <p></p>
          <hr>
          <table class="table">
            <tbody>
              @if($reservation_adjustments)
                  <tr>
                        <td colspan="2">
                          <table class="table">
                            @foreach ($reservation_adjustments as $adjustments)
                                      <tr>
                                        <td style="padding-left:0; {{$loop->index == 0 ? 'border:0' : ''}}">{!! $adjustments->name !!}</td>
                                        <td style="padding-left:0; {{$loop->index == 0 ? 'border:0' : ''}}">{!!Helper::get_money($adjustments->price)!!}  <a href="{{url('admin/adjustments/delete').'/'.$adjustments->id . '/' . $reservation->id }}" class="btn btn-danger btn-raised btn-sm pull-right">{{trans('admin.delete')}}</a> </td>
                                      </tr>
                            @endforeach
                      </table>
                  </td>
                </tr>
              @endif

              <tr>
                  <td class="left">{!! trans('admin.topay') !!}:</td> <td class="right">{!! Helper::get_money($topay)!!}</td>
              </tr>
              <tr>
                  <td class="left">{!! trans('admin.discount') !!}:</td> <td class="right">{!! Helper::get_money($reservation->discount) !!} </td>
              </tr>
              <tr>
                  <td class="left"><strong>{!! trans('admin.total') !!}</strong></td> <td class="right"> <strong>{!! Helper::get_money($reservation->total) !!} </strong></td>
              </tr>
            </tbody>
          </table>
          @if($topay !== '0.00')
            <div class="btn-group mr-10">
            <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-primary btn-raised dropdown-toggle">{{trans('admin.payments')}}
            <span class="caret"></span></button>
              <ul class="dropdown-menu">
                <li><a href="{{url('admin/reservation/register-deposit') .'/'. $reservation->id}}">{{trans('admin.register_deposit')}}</a></li>
                <li><a href="{{url('admin/reservation/register-payment') .'/'. $reservation->id}}">{{trans('admin.register_payment')}}</a></li>
              </ul>
            </div>
          @endif
          <div class="btn-group mr-10">
          <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-success btn-raised dropdown-toggle">{{trans('admin.adjustments')}}
          <span class="caret"></span></button>
            <ul class="dropdown-menu">
              <li><a href="{{url('admin/reservation/adjustments') .'/'. $reservation->id}}">{{trans('admin.adjustments')}}</a></li>
              <li><a href="{{url('admin/reservation/discount') .'/'. $reservation->id}}">{{trans('admin.discount')}}</a></li>
            </ul>
          </div>

          <hr>
          <button class="btn btn-danger btn-raised" data-id="{{$reservation->id}}" onclick="reservation.deleteBooking(this)">{{trans('admin.delete')}}</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="cancellationReasonModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{trans('admin.cancellation_reason')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <textarea class="form-control" id="cancellationReason" placeholder="{{trans('admin.please_indicate_cancellation_reason')}}"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-id="{{$reservation->id}}" onclick="reservation.cancelBooking(this)">{{trans('admin.confirm')}}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
      </div>
    </div>
  </div>
</div>
@stop
