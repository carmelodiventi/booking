@extends('admin.main')

@push('styles')
@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Reservation  <a href="{{url('admin/reservation/details') . '/' . $reservation->id}}"> {{$reservation->pnr}}</a></h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span>  Reservations Discount
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
  <div class="widget">
    <div class="widget-body">
      {{Form::open(['url' => LaravelLocalization::getLocalizedURL()])}}

        <div class="row">
        <div class="col-md-5">
          <div class="form-group">
            {!! Form::label('price', trans('admin.price')) !!}
            {!! Form::text('amount', $reservation->discount, ['class'=> 'form-control', 'step'=>'any','min'=>'0','max'=> $topay, 'placeholder' => trans('admin.price'),'data-rule-required'=>'true']) !!}
          </div>
        </div>
      </div>
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              {!! Form::submit(trans('admin.add'),['class'=> 'btn btn-primary','placeholder' => trans('admin.price'),'data-rule-required'=>'true']) !!}
            </div>
          </div>
          </div>
      {{Form::close()}}
    </div>
  </div>
    @push('script')
    @endpush
@stop
