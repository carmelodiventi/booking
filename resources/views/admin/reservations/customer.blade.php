@extends('admin.main')

@push('styles')

@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Reservation  <a href="{{url('admin/reservation/details') . '/' . $reservation->id}}"> {{$reservation->pnr}}</a></h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li><a href="{{url('/admin/reservation')}}">Reservation</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Edit Customer
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
    <div class="widget">
      <div class="widget-heading">
                  <h3 class="widget-title">Edit customer</h3>
      </div>
        <div class="widget-body">
            {!! Form::model($customer,['url' => LaravelLocalization::getLocalizedURL()])!!}
                <div class="form-group">
                  <label>{{trans('admin.customer')}}</label>
                  {{Form::select('customer',$customer,$reservation->customer_id,['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                  {{Form::button(trans('admin.edit'),['class'=>'btn btn-primary','type'=>'submit'])}}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    @push('script')
    <script>
        $('select[name="customer"]').chosen();
    </script>
    @endpush

@stop
