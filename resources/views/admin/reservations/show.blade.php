@extends('admin.main')

@push('styles')
<link rel="stylesheet" href="{!!asset('resources/assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.css')!!}" type="text/css" rel="stylesheet">
<style>
@media print{

  body {
    font-size: 14px;
  }

  aside.main-sidebar,
  div.page-header,
  form#reservations-filter,
  header,
  button.btn {
    display: none;
  }

  a:link:after, a:visited:after {
    content: "";
  }

  .reservations-list{
    page-break-inside: avoid;
  }

  @page {size: landscape}

}
</style>
@endpush

@section('search-bar')
  {!! Form::open(['url' => 'admin/reservations/','method'=>'get','class'=>'mt-15 mb-15 pull-left hidden-xs']) !!}
    <div class="form-group has-feedback mb-0">
      <input type="text" aria-describedby="inputSearchFor" placeholder="{!!trans('admin.search_for')!!}" name="keyword" style="width: 200px" class="form-control rounded"><span aria-hidden="true" class="ti-search form-control-feedback"></span><span id="inputSearchFor" class="sr-only">(default)</span>
    </div>
  {!! Form::close() !!}
@endsection

@section('search-bar-mobile')
  {!! Form::open(['url' => 'admin/reservations/','method'=>'get']) !!}
    <div class="input-group input-group-lg">
      <input type="text" aria-describedby="inputSearchFor" placeholder="{!!trans('admin.search_for')!!}" name="keyword" class="form-control">
      <span class="input-group-btn">
        <button type="button" class="btn btn-default search-bar-toggle"><i class="ti-close"></i></button></span>
    </div>
  </form>
  {!! Form::close() !!}
@endsection

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Reservation List</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{!!url('/admin')!!}">Home</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span>  Reservations
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')

      {!! Form::open(['url' => 'admin/reservations', 'method' => 'get','class'=>'collapse','id'=>'reservations-filter']) !!}
      <div class="row">
        <div class="col-md-12"><h4>{!!trans('admin.search_function')!!}<h4></div>
      </div>
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
          <div class='input-group date'>
            <span class="input-group-addon">
              <strong>
                {!! trans('admin.from') !!}
              </strong>
            </span>
            {!! Form::text('from', null, ['class' => 'form-control', 'placeholder'=> trans('admin.start')]) !!}
            <span class="input-group-addon">
              <span class="glyphicon glyphicon-calendar"></span>
            </span>
          </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
          <div class='input-group date'>
            <span class="input-group-addon">
              <strong>
                {!! trans('admin.to') !!}
              </strong>
            </span>
          {!! Form::text('to', null, ['class' => 'form-control', 'placeholder'=> trans('admin.end')]) !!}
            <span class="input-group-addon">
              <span class="glyphicon glyphicon-calendar"></span>
            </span>
          </div>
          </div>
        </div>
        <div class="col-md-5">
          {!! Form::select('status[]',[ 'cnf' => trans('admin.confirmed'), 'sld' =>trans('admin.paid'),  'can' => trans('admin.canceled')], old('status'), ['class'=> 'form-control','data-placeholder' => trans('admin.status'), 'multiple','data-rule-required'=>'true']) !!}
        </div>
        <div class="col-md-1">
            <button class="btn btn-primary btn-block">{!!trans('admin.filter')!!}</button>
        </div>
      </div>
      <hr>
      {!! Form::close() !!}

    <div class="row">
      <div class="col-md-8">
        <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
          <div class="btn-group mr-2 hidden" role="group" aria-label="First group">
            <a href="{{url('admin/reservations?passed=true')}}" class="btn btn-default btn-outline"> {{trans('admin.passed')}} </a>
            <a href="{{url('admin/reservations')}}" class="btn btn-default btn-outline"> {{trans('admin.current')}} </a>
          </div>
          <div class="btn-group mr-2" role="group" aria-label="Second group">
            <a href="{{url('admin/reservations?status=cnf')}}" class="btn btn-warning btn-outline {{ (Request::input('status') == 'cnf') ? 'active' : ''  }}"> {{trans('admin.confirmed')}} </a>
            <a href="{{url('admin/reservations?status=sld')}}" class="btn btn-success btn-outline {{ (Request::input('status') == 'sld') ? 'active' : ''  }}"> {{trans('admin.paid')}} </a>
            <a href="{{url('admin/reservations?status=can')}}" class="btn btn-danger btn-outline {{ (Request::input('status') == 'can') ? 'active' : ''  }}"> {{trans('admin.canceled')}} </a>
            <a href="{{url('admin/reservations')}}" class="btn btn-primary btn-outline {{ (Request::input('status') == '') ? 'active' : ''  }}"> {{trans('admin.all')}} </a>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="btn-group pull-right">
          @if(Request::input('type') != 'compact')
          <a class="btn btn-default" href="{{url('admin/reservations?type=compact')}}">
            <i class="fa fa-th-list"></i> {{trans('admin.compact')}}
          </a>
          @else
            <a class="btn btn-default" href="{{url('admin/reservations')}}">
              <i class="fa fa-bars"></i> {{trans('admin.extended')}}
            </a>
          @endif
          <button class="btn btn-default" type="button" data-toggle="collapse" data-target="#reservations-filter" aria-expanded="false" aria-controls="reservations-filter">
            <i class="fa fa-filter"></i> {{trans('admin.filter')}}
          </button>
          <button class="btn btn-default" onclick="printElem('reservations-list')"> <i class="fa fa-print"></i> {!!trans('booking.print')!!}</button>
        </div>
      </div>
    </div>

    @if (Request::get('from') && Request::get('from'))
      <hr>
      <h5>{!! trans('admin.search_result') !!} {!!trans('admin.from')!!} <strong>{{ Carbon::createFromFormat('Y-m-d', Request::get('from'))->formatLocalized('%A %d %B %Y')}}</strong> {!!trans('admin.to')!!} <strong>{{ Carbon::createFromFormat('Y-m-d', Request::get('to'))->formatLocalized('%A %d %B %Y')}}</strong> </h5>
    @endif
    <hr>
        @if($reservations)
       
          @if(Request::input('type') == 'compact')
            <table class="table">
              <thead>
                <tr>
                  <th>{!! trans('admin.pnr')!!}</th>
                  <th>{!! trans('admin.customer')!!}</th>
                  <th>{!! trans('admin.origin')!!}</th>
                  <th>{!! trans('admin.status')!!}</th>
                </tr>
              </thead>
                <tbody>
                @foreach ($reservations as $reservation)
                  <tr>
                    <td><a href='{!!url('admin/reservation/details/' .$reservation->id) !!}'>{!!$reservation->pnr!!}</a></td>
                    <td>{!!$reservation->name!!} {!!$reservation->surname!!}</td>
                    <td>{!!$reservation->origin!!}</td>
                    <td>{!! html_entity_decode(Helper::getstatus($reservation->status)) !!}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
        @else
          @foreach ($reservations as $reservation)
            <div class="reservations-list" data-reservation-id="{{$reservation->id}}">
              <div class="table-responsive">
                <table class="table table-striped table-bordered">
                  <thead>
                  <tr>
                      <th>{!! trans('admin.pnr')!!}</th>
                      <th>{!! trans('admin.created_at')!!}</th>
                      <th colspan="3">{!! trans('admin.customer')!!}</th>
                  </tr>
                  </thead>
                  <tbody>
                    <tr>
                        <td>
                          <a href='{!!url('admin/reservation/details/' .$reservation->id) !!}'>{!!$reservation->pnr!!}</a>
                        </td>
                        <td>{!!date("d/m/Y", strtotime($reservation->created_at))!!}</td>
                        <td colspan="3">
                            {!!$reservation->name!!} {!!$reservation->surname!!} <br>
                            @if ($reservation->email)
                              <small>{!!$reservation->email!!}</small><br>
                            @endif
                            <small>{!!$reservation->phone!!}</small>
                        </td>
                    </tr>
                    <tr>
                      <th>
                        {!! trans('admin.check_in')!!}
                      </th>
                      <th>
                        {!! trans('admin.check_out')!!}
                      </th>
                      <th colspan="2">
                        {!! trans('admin.room')!!} ({!! trans('admin.type')!!})
                      </th>
                      <th>
                        {!! trans('admin.guests')!!}
                      </th>
                    </tr>
                @if($reservation->rooms_booked)

                    @php 
                      $rooms_booked = '[' . $reservation->rooms_booked . ']';
                      $rooms_booked = json_decode($rooms_booked);

                    @endphp
                    @if($rooms_booked)
                    
                    @foreach ($rooms_booked as $room_booked)
                          <tr>
                              <td style="white-space: nowrap;">
                                {{Carbon::createFromFormat('Y-m-d', $room_booked->check_in)->formatLocalized('%A %d %B %Y')}}
                              </td>
                              <td style="white-space: nowrap;">
                                {{Carbon::createFromFormat('Y-m-d', $room_booked->check_out)->addDays(1)->formatLocalized('%A %d %B %Y')}}
                              </td>
                              <td colspan="2">
                                {!!$room_booked->name!!}
                                <small>({!! Helper::get_traslated_string($room_booked->room_type)!!})</small>
                              </td>
                              <td>
                                  {!!$room_booked->guests!!}
                              </td>
                          </tr>
                    @endforeach
                    @endif
                @endif
                <tr>
                    <th>{!! trans('admin.origin')!!}</th>
                    <th colspan="2">{!! trans('admin.note')!!}</th>
                    <th>{!! trans('admin.payment_situation')!!}</th>
                    <th>{!! trans('admin.status')!!}</th>
                </tr>
                <tr>
                  <td>{!!$reservation->origin!!}</td>
                  <td colspan="2">
                    {!!$reservation->note!!}
                    <hr>
                    {!!$reservation->reservation_note!!}
                  </td>
                  <td>
                    <ul class="list-unstyled" style="white-space: nowrap;">

                      <li>{!! trans('admin.topay')!!} : <strong>{!! Helper::get_money(Helper::get_topay($reservation->rooms_booked_sum, $reservation->adjustments_sum,$reservation->discount,$reservation->payments_sum))!!}</strong></li>
                      <li>{!! trans('admin.discount')!!} : <strong>{!!Helper::get_money($reservation->discount)!!}</strong></li>
                      @if($reservation->adjustments_sum)
                        <li>{!! trans('admin.adjustments')!!} : <strong>{!!Helper::get_money($reservation->adjustments_sum)!!}</strong></li>
                      @endif
                      <li>{!! trans('admin.total')!!} : <strong>{!!Helper::get_money($reservation->total)!!}</strong></li>

                    </ul>
                  </td>
                  <td>{!! html_entity_decode(Helper::getstatus($reservation->status)) !!}</td>
                </tr>
                </tbody>
                </table>
              </div>
            </div>
          @endforeach
        @endif

        @endif
        <hr>
    <div>
    {!! $reservations->links() !!}
    </div>
    @push('script')
    <script type="text/javascript" src="{!!asset('resources/assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.min.js')!!}"></script>
    <script>
              $(window).load(function () {

                $date_format = 'YYYY-MM-DD';
                 $('input[name="from"]').daterangepicker({
                     autoUpdateInput: false,
                     locale: {
                       format: $date_format
                     },
                     allowInputToggle: true
                 });
                 $('input[name="to"]').daterangepicker({
                     autoUpdateInput: false,
                     locale: {
                       format: $date_format
                     },
                     allowInputToggle: true
                 });

                 $('input[name="from"]').on('apply.daterangepicker', function(ev, picker) {
                   $(this).val(picker.startDate.format($date_format));
                   $('input[name="to"]').val(picker.endDate.format($date_format));
                 });
                 $('input[name="to"]').on('apply.daterangepicker', function(ev, picker) {
                   $(this).val(picker.endDate.format($date_format));
                   $('input[name="from"]').val(picker.startDate.format($date_format));
                 });

                 $('select').chosen('destroy');
                 $('select[name="status[]"]').chosen({
                   width: "100%"
                 });

              });
    </script>
    <script>
    function printElem(elem){
       window.print();
     }
    </script>
    @endpush

@stop
