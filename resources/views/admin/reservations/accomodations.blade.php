@extends('admin.main')

@push('styles')
<link rel="stylesheet" href="{{asset('resources/assets/admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="{{asset('resources/assets/admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="{{asset('resources/assets/admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.standalone.min.css')}}" type="text/css" rel="stylesheet">
<style>
    .button-group input {
        display: none;
    }

    #credit_card,
    #payments,
    .extra {
        display: none;
    }

    .prices hr:last-child,
    .guests hr:last-child{
      display: none;
    }
</style>
@endpush

@push('script')
<script type="text/javascript" src="{{asset('resources/assets/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{asset('resources/assets/admin/plugins/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{asset('resources/assets/admin/plugins/jquery-validation/dist/additional-methods.min.js')}}"></script>
<script src="{{asset('resources/assets/admin/js/reservation.js')}}"></script>
@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Reservation  <a href="{{url('admin/reservation/details') . '/' . $reservation->id}}"> {{$reservation->pnr}}</a></h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Accomodations and Date
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
  <div class="widget">
    <div class="widget-body">
       <fieldset>
            <legend>{{ trans('admin.initial_booking')}}</legend>
             <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>{{trans('admin.name')}}</th>
                        <th>{{trans('admin.check_in')}}</th>
                        <th>{{trans('admin.check_out')}}</th>
                        <th>{{trans('admin.rate')}}</th>
                        <th>{{trans('admin.guests')}}</th>
                        <th>{{trans('admin.price')}}</th>
                    </tr>
                    </thead>
                    <tbody>
      @if($rooms_booked)
        @foreach($rooms_booked as $room)
         <tr>
            <td>{{Helper::get_traslated_string($room->type)}} <small>({{$room->name}})</small></td>
            <td>{{Carbon::createFromFormat('Y-m-d', $room->check_in)->formatLocalized('%A %d %B %Y')}}</td>
            <td>{{Carbon::createFromFormat('Y-m-d', $room->check_out)->addDays(1)->formatLocalized('%A %d %B %Y')}} </td>
            <td>{{Helper::get_traslated_string($room->rate_name)}}</td>
            <td>{{$room->guests}}</td>
            <td>{!!Helper::get_money($room->price)!!}</td>
          </tr>
        @endforeach
      @endif
       </tbody>
                </table>
            </div>
          </fieldset>
</div>
</div>
 <hr>
 {{Form::open(['url' => LaravelLocalization::getLocalizedURL(),'id' => 'lodging', 'data-reservation-id'=> $reservation->id])}}
  {{Form::hidden('edit_rooms')}}
   <div class="widget">
    <div class="widget-body">
      <fieldset>
            <legend>{{ trans('admin.edit_rooms')}}</legend>
             <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>{{trans('admin.name')}}</th>
                        <th>{{trans('admin.check_in')}}</th>
                        <th>{{trans('admin.check_out')}}</th>
                        <th>{{trans('admin.rate')}}</th>
                        <th>{{trans('admin.guests')}}</th>
                        <th>{{trans('admin.price')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                      @if($rooms_booked)
                        @foreach($rooms_booked as $room)
                         <tr>
                            <td>
                              {!! Form::select('rooms[]', $rooms, $room->room_id,['class'=> 'form-control']) !!}
                              {{Form::hidden('rooms_booked[]',$room->id)}}
                            </td>
                            <td>
                              <div class='input-group date'>
                                  {!! Form::text('check_in[]', $room->check_in, ['class'=> 'form-control','placeholder' => trans('admin.check_in'),'data-rule-required'=>'true','data-rule-date'=>'true']) !!}
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                              </div>
                            </td>
                            <td>
                              <div class='input-group date'>
                                  {!! Form::text('check_out[]', date('Y-m-d', strtotime('+1 day', strtotime($room->check_out))), ['class'=> 'form-control','placeholder' => trans('admin.check_out'),'data-rule-required'=>'true','data-rule-date'=>'true']) !!}
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                              </div>
                            </td>
                            <td>
                              <select class="form-control" name="rates_type[]">
                                @foreach($rates_type as $rate_type)
                                  <option value="{{$rate_type['id']}}" 
                                   {{$rate_type['id'] == $room->rate_id ? 'selected' : ''}}>
                                    {{Helper::get_traslated_string($rate_type['name'])}}
                                  </option>
                                @endforeach
                              </select>
                            </td>
                            <td>
                              {!! Form::number('guests[]', $room->guests, ['class'=> 'form-control','placeholder' => trans('admin.guests'),'data-rule-required'=>'true','min' => 0,'step'=> '1']) !!}
                            </td>
                            <td>
                              {!! Form::number('prices[]', $room->price, ['class'=> 'form-control','placeholder' => trans('admin.price'),'data-rule-required'=>'true','min' => 0, 'step'=> 'any']) !!}
                            </td>
                          </tr>
                        @endforeach
                      @endif
                  </tbody>
                </table>
            </div>
            <div>

            <div>
              <small>
                {{trans('admin.be_carefoul_availability_status')}}
              </small>
            </div>
            <hr>
       
          {{Form::submit(trans('admin.update'),['class'=>'btn btn-primary'])}}
     
      </div>
          </fieldset>
    </div>
   </div>
  {{Form::close()}}
 <hr>
  {{Form::open(['url' => LaravelLocalization::getLocalizedURL(),'id' => 'lodging', 'data-reservation-id'=> $reservation->id])}}
  {{Form::hidden('add_rooms')}}
 <div class="widget">
    <div class="widget-body">
     
      <fieldset>
        <legend>{{ trans('admin.add_rooms')}}</legend>
        <div class="row">
        <div class="col-md-5">
            <div class="form-group">
              {!! Form::label('check_in', trans('admin.check_in')) !!}
              <div class='input-group date'>
              {!! Form::text('check_in', null, ['class'=> 'form-control','placeholder' => trans('admin.check_in'),'data-rule-required'=>'true','data-rule-date'=>'true']) !!}
                  <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                  </span>
              </div>
          </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
              {!! Form::label('check_out', trans('admin.check_out')) !!}
              <div class='input-group date'>
              {!! Form::text('check_out', null, ['class'=> 'form-control','placeholder' => trans('admin.check_out'),'data-rule-required'=>'true','data-rule-date'=>'true']) !!}
                  <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                  </span>
              </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                  {!! Form::label('nights', trans('admin.stay')) !!}
                  <div class="label label-info block p-10 fs-14">
                    <span data-night></span> {{trans('admin.nights')}}
                  </div>
            </div>
        </div>
        </div>
 
      <hr>

      <!-- Rooms Form -->
        @include('admin.includes.form.rooms_type', ['multiple' => true])
      <!-- // Rooms Form -->

      
      <div class="row">
        <div class="col-md-2">
          <div class="form-group">
            {!! Form::label('total', trans('admin.total')) !!}
            {!! Form::number('total', '0.00', ['class'=> 'form-control','placeholder' => trans('admin.total'),'readonly'=>'readonly','data-rule-required'=>'true','min'=>0,'step'=>'any']) !!}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3">
          {{Form::submit(trans('admin.add'),['class'=>'btn btn-primary'])}}
        </div>
      </div>
   
      </fieldset>
    </div>
  </div>
  {{Form::close()}}

@stop
