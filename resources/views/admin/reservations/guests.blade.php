@extends('admin.main')

@push('styles')
  
@endpush

 @push('script')

@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Reservation  <a href="{{url('admin/reservation/details') . '/' . $reservation->id}}"> {{$reservation->pnr}}</a></h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Edit number Guests
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
  <div class="widget">
    <div class="widget-body">

      @if($room_booked)
        {{Form::open(['url' => LaravelLocalization::getLocalizedURL(),'data-room-id' => $room_booked->room_id])}}
        <fieldset>
          <legend>{{ trans('admin.initial_booking')}}</legend>
          <div class="table-responsive">
              <table class="table table-striped">
                  <thead>
                  <tr>
                      <th>{{trans('admin.name')}}</th>
                      <th>{{trans('admin.check_in')}}</th>
                      <th>{{trans('admin.check_out')}}</th>
                      <th>{{trans('admin.guests')}}</th>
                      <th>{{trans('admin.price')}}</th>
                  </tr>
                  </thead>
                  <tbody>
                      <tr>
                        <td>{{Helper::get_traslated_string($room_booked->type)}} <small>({{$room_booked->name}})</small></td>
                        <td>
                          {{Carbon::createFromFormat('Y-m-d', $room_booked->check_in)->formatLocalized('%A %d %B %Y')}}
                        </td>
                        <td>
                          {{Carbon::createFromFormat('Y-m-d', $room_booked->check_out)->formatLocalized('%A %d %B %Y')}} 
                        </td>
                        <td>
                          @if(isset($room_booked->guests))
                            {{Form::text('guests',$room_booked->guests,['class'=>'form-control'])}}
                          @endif
                        </td>
                        <td>
                          @if(isset($room_booked->price))
                              {!!Helper::get_money($room_booked->price)!!}
                          @endif
                        </td>
                      </tr>
                  </tbody>
              </table>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-3">
              {{Form::submit(trans('admin.update'),['class'=>'btn btn-primary'])}}
            </div>
          </div>
        </fieldset>

        {{Form::close()}}
      @endif
    </div>
  </div>

   

@stop
