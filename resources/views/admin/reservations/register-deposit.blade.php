@extends('admin.main')

@push('styles')
@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Reservation  <a href="{{url('admin/reservation/details') . '/' . $reservation->id}}"> {{$reservation->pnr}}</a></h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span>  Register Deposit
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
  <div class="widget">
    <div class="widget-body">
      {{Form::open(['url' => LaravelLocalization::getLocalizedURL()])}}
      <div class="alert alert-info">
        {{trans('admin.register_deposit_info')}}
      </div>
        <div class="row">
      @if($payments_list)

          <div class="col-md-3">
            {!! Form::label('topay', trans('admin.payment_method')) !!}
              <div class="form-group">
                {!! Form::select('payment_method', $payments_list,null, ['class'=> 'form-control','placeholder' => trans('admin.payment_method'),'data-rule-required'=>'true']) !!}
            </div>
          </div>

      @endif
          <div class="col-md-3">
          <div class="form-group">
            {!! Form::label('topay', trans('admin.to_pay_to_soldered')) !!}
            {!! Form::number('topay', $topay , ['class'=> 'form-control','placeholder' => trans('admin.to_pay_to_soldered'),'readonly'=>'readonly']) !!}
          </div>
        </div>
          <div class="col-md-2">
          <div class="form-group">
            {!! Form::label('amount', trans('admin.amount')) !!}
            {!! Form::number('amount', '0.00' , ['class'=> 'form-control','placeholder' => trans('admin.amount'),'step'=>'any','min'=>'0']) !!}
          </div>
        </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <div class="checkbox">
                <label>
                  {!! Form::checkbox('received', null, true) !!}
                  {{trans('admin.record_as_received')}}
                </label>
              </div>
            </div>
          </div>
          </div>
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              {!! Form::submit(trans('admin.add'),['class'=> 'btn btn-primary','placeholder' => trans('admin.price'),'data-rule-required'=>'true']) !!}
            </div>
          </div>
          </div>
      {{Form::close()}}
    </div>
  </div>
    @push('script')
      <script src="{{asset('resources/assets/admin/js/payments.js')}}"></script>
    @endpush
@stop
