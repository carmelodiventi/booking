@extends('admin.main')

@push('styles')

@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Create a new Room</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li><a href="{{url('/admin/rooms')}}">Rooms</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Add Room
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
    <div class="widget">
        <div class="widget-heading">
            <h3 class="widget-title">Data Room</h3>
        </div>
        <div class="widget-body">
        {!! Form::open(['route' => 'rooms.store']) !!}
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                {!! Form::label('name', trans('admin.name')) !!}
                {!! Form::text('name', null, ['class' => 'form-control','placeholder'=> trans('admin.name')]) !!}
            </div>
          </div>
        </div>
        @if($rooms_type)
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                    {!! Form::label('type', trans('admin.type')) !!}
                    <select name="type" class="form-control">
                      <option value="">{{trans('admin.select_type')}}</option>
                      @foreach ($rooms_type as $key => $value)
                        <option value="{{$key}}">{{Helper::get_traslated_string($value)}}</option>
                      @endforeach
                    </select>
                </div>
              </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                {!! Form::label('ordinal', trans('admin.ordinal')) !!}
                {!! Form::number('ordinal', null, ['class' => 'form-control','placeholder'=> trans('admin.ordinal')]) !!}
            </div>  </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                {!! Form::label('status', trans('admin.status')) !!}
                {!! Form::select('status', ['CNF' => 'Confermata', 'LKC' => 'Chiusa', 'PND' => 'In Attesa'], null, ['class' => 'form-control','placeholder' => 'Seleziona una stato']) !!}
            </div>  </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                {!! Form::submit(trans('admin.add'), ['class' => 'btn btn-primary']) !!}
            </div>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
    @push('script')

    @endpush

@stop
