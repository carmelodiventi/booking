@extends('admin.main')

@push('styles')

@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Rooms</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Rooms
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
        <a href="{{ url('admin/rooms/create')}}" class="btn btn-primary">{{trans('admin.add')}}</a>
        <hr>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans('admin.name')}}</th>
                <th>{{ trans('admin.type')}}</th>
                <th>{{ trans('admin.capacity')}}</th>
                <th>{{ trans('admin.status')}}</th>
                <th>{{ trans('admin.action')}}</th>
            </tr>
            </thead>
            <tbody>
            @if($rooms)
                @foreach ($rooms as $room)
                    <tr>
                        <td>{{$room->name}}</td>
                        <td>{{json_decode($room->type,true)[LaravelLocalization::getCurrentLocale()]}}</td>
                        <td>{{$room->capacity}}</td>
                        <td>{!! html_entity_decode(Helper::getstatus($room->status)) !!}</td>
                        <td>
                            <a href="{{ route('rooms.edit', $room->id) }}"
                               class="btn btn-success"><i
                                        class="fa fa-edit"></i></a>
                                {!! Form::open(['method' => 'DELETE','route' => ['rooms.destroy', $room->id], 'class' => 'inline']) !!}
                                {!! Form::submit(trans('admin.delete'), ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}

                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    @push('script')

    @endpush

@stop
