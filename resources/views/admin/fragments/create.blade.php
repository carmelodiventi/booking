@extends('admin.main')
@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">{{trans('admin.fragments')}}</h4>
      <p class="text-muted mb-0">Booking Engine</p>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
  <ul role="tablist" class="nav nav-tabs mb-15">
    @foreach (LaravelLocalization::getSupportedLanguagesKeys() as $key => $value)
      <li role="presentation" class="{{$loop->index==0 ? 'active' : ''}}"><a href="#{{$key}}" aria-controls="general" role="tab" data-toggle="tab" aria-expanded="true">{{$value}}</a></li>
    @endforeach
  </ul>

  <div class="tab-content">
    {!! Form::open(['route' => 'fragments.store']) !!}
    <div class="row">
      <div class="col-md-4">
          <div class="form-group">
          {!! Form::label('group', trans('admin.group')) !!}
          {!! Form::select('group', $groups, 0 ,['class' => 'form-control', 'placeholder'=> trans('admin.group')]) !!}
          </div>
      </div>
      <div class="col-md-4">
          <div class="form-group">
          {!! Form::label('key', trans('admin.key')) !!}
          {!! Form::text('key', null, ['class' => 'form-control', 'placeholder'=> trans('admin.key')]) !!}
          </div>
      </div>
    </div>
      @foreach (LaravelLocalization::getSupportedLanguagesKeys() as $key => $value)
      <div id="{{$key}}" role="tabpanel" class="tab-pane {{$loop->index==0 ? 'active' : ''}}">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                {!! Form::label('text[]', trans('admin.text')) !!}
                {!! Form::text('text[]', null, ['class' => 'form-control','placeholder'=> trans('admin.text')]) !!}
              </div>
            </div>
          </div>
      </div>
      {!! Form::text('language[]', $value, ['class' => 'hidden']) !!}
      @endforeach
      <div class="row">
        <div class="col-md-4">
            <div class="form-group">
            {!! Form::submit(trans('admin.save'), ['class' => 'btn btn-primary']) !!}
            </div>
        </div>
      </div>
    {!! Form::close() !!}
  </div>
@stop
