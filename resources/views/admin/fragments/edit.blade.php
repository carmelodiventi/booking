@extends('admin.main')
@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">{{trans('admin.fragments')}}</h4>
      <p class="text-muted mb-0">Booking Engine</p>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')

  <div class="tab-content">
    {!! Form::open(['route' => 'fragments.store']) !!}
    @foreach ($fragments as $fragment)
      <div class="row">
        <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('key[]', trans('admin.key')) !!}
            {!! Form::text('key[]', $fragment->key, ['class' => 'form-control', 'placeholder'=> trans('admin.key')]) !!}
            </div>
        </div>
        <div class="col-md-8">
          <div class="form-group">
            {!! Form::label('text[]', trans('admin.text')) !!}
            {!! Form::text('text[]', $fragment->text, ['class' => 'form-control','placeholder'=> trans('admin.text')]) !!}
          </div>
        </div>
      </div>
    @endforeach
      <div class="row">
        <div class="col-md-4">
            <div class="form-group">
            {!! Form::submit(trans('admin.save'), ['class' => 'btn btn-primary']) !!}
            </div>
        </div>
      </div>

    {!! Form::close() !!}
  </div>
@stop
