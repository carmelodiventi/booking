@extends('admin.main')

@push('style')
<style>
  .btn-edit{
    visibility: hidden;
  }
</style>
@endpush

@push('script')
  <script type="text/javascript">
            $(document).ready(function(){

              $('textarea').on('keyup', function() {
                  parent = this.closest('tr');
                  $id = $(parent).data('id');
                  $("tr[data-id='"+$id+"'] .btn-edit").attr('disabled', false).css('visibility','visible');
              });

              updateFragment = function($id){
                var text = [];
                var language = [];
                var key = $("tr[data-id='"+$id+"'] input[name='key']").val();

                $("tr[data-id='"+$id+"'] textarea[name='text[]']").each(function() {
                  text.push($(this).val());
                });
                $("tr[data-id='"+$id+"'] input[name='language[]']").each(function() {
                  language.push($(this).val());
                });

                $.ajax({
                  url: base_url + '/admin/fragments/update/' + $id,
                  type: "post",
                  headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },
                  data: {'key':key,'text[]':text,'language[]':language},
                  success: function(response){
                    if(response.status == 'success'){
                    noty({
                      text: response.msg,
                      layout: 'top',
                      type: 'success',
                      timeout: 3000,
                    });
                    $("tr[data-id='"+$id+"'] .btn-edit").attr('disabled','disabled');
                    }
                  }
                });
              };
              deleteFragment = function($id){
                $.ajax({
                  url: base_url + '/admin/fragments/delete/' + $id,
                  type: "post",
                  headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },
                  success: function(response){
                    if(response.status == 'success'){
                    noty({
                      text: response.msg,
                      layout: 'top',
                      type: 'success',
                      timeout: 3000,
                    });
                    }
                  }
                });
              };
            });</script>
@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">{{trans('admin.fragments')}}</h4>
      <p class="text-muted mb-0">Booking Engine</p>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
      <div class="row">
        <div class="col-md-4">
            <div class="form-group">
              <a href="{{url('admin/fragments/create')}}" class="btn btn-primary"> {{trans('admin.add')}} </a>
            </div>
        </div>
      </div>

      <hr>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>{!! trans('admin.key') !!}</th>
            <th>{!! trans('admin.text') !!}</th>
            <th colspan="2"></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($fragments as $fragment)
          <tr data-id="{{$fragment->id}}">
            <td>{!! Form::text('key', $fragment->key, ['class' => 'form-control', 'placeholder'=> trans('admin.key')]) !!}</td>
            @php
              $strings = json_decode($fragment->text);
            @endphp
            @foreach ($strings as $key => $value)
              <td>{!! Form::textarea('text[]', $value, ['class' => 'form-control','rows' => '1','placeholder'=> trans('admin.text')]) !!}{!! Form::hidden('language[]', $key) !!}</td>
            @endforeach
            <td colspan="2">
              {!! Form::button('<i class="fa fa-floppy-o"></i>', ['class' => 'btn btn-success btn-edit', 'disabled'=>'disabled', 'onclick'=>'updateFragment('.$fragment->id.')',]) !!}
              {!! Form::button('<i class="fa fa-trash"></i>', ['class' => 'btn btn-danger btn-delete','onclick'=>'deleteFragment('.$fragment->id.')',]) !!}
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
@stop
