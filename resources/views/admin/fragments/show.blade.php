@extends('admin.main')
@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">{{trans('admin.fragments')}}</h4>
      <p class="text-muted mb-0">Booking Engine</p>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
      <div class="row">
        <div class="col-md-4">
            <div class="form-group">
              <a href="{{url('admin/fragments/create')}}" class="btn btn-primary"> {{trans('admin.add')}} </a>
            </div>
        </div>
      </div>

      <hr>
        <table class="table table-over">
          <thead>
            <tr><th>{{trans('admin.group')}}</th> <th></th></tr>
          </thead>
          <tbody>
              @foreach ($groups as $group)
              <tr><td>{{$group}}</td> <td> <a href="{{url('admin/fragments/list') . '/' . $group}}" class="btn btn-success"> {{trans('admin.show')}} </a></td></tr>
              @endforeach
          </tbody>
        </table>
@stop
