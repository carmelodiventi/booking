@extends('admin.main')

@push('styles')

@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Rooms</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Rooms
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
        <a href="{{ url('admin/rooms-blocked/create')}}" class="btn btn-primary">{{trans('admin.add')}}</a>
        <hr>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans('admin.name')}}</th>
                <th>{{ trans('admin.start')}}</th>
                <th>{{ trans('admin.end')}}</th>
                <th>{{ trans('admin.status')}}</th>
                <th>{{ trans('admin.action')}}</th>
            </tr>
            </thead>
            <tbody>
            @if($blocked_periods)
                @foreach ($blocked_periods as $period)
                    <tr>
                        <td>{{$period->room}}</td>
                        <td>{{date('d M Y', strtotime($period->start))}}</td>
                        <td>{{date('d M Y', strtotime($period->end))}}</td>
                        <td>{!! html_entity_decode(Helper::getstatus($period->status)) !!}</td>
                        <td>
                          <a href="{{ route('rooms-blocked.edit', $period->id) }}"
                             class="btn btn-success"><i
                                      class="fa fa-edit"></i></a>
                          {!! Form::open(['method' => 'DELETE','route' => ['rooms-blocked.destroy', $period->id], 'class' => 'inline']) !!}
                          {!! Form::submit(trans('admin.delete'), ['class' => 'btn btn-danger']) !!}
                          {!! Form::close() !!}
                        </a>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    @push('script')

    @endpush

@stop
