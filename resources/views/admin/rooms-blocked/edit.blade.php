@extends('admin.main')

@push('styles')
<link rel="stylesheet" href="{{asset('resources/assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" type="text/css" rel="stylesheet">
@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Create a new block period for Room</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li><a href="{{url('/admin/rooms')}}">Rooms</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Add Block Period
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
    <div class="widget">
        <div class="widget-heading">
            <h3 class="widget-title">Data Blocked Period</h3>
        </div>
        <div class="widget-body">
          {!! Form::model($blocked_period,['method' => 'put', 'url' =>'admin/rooms-blocked/'. $blocked_period['id']])!!}
        <div class="row">
          @if($rooms)
              <div class="col-md-3">
                <div class="form-group">
                  {!! Form::label('room_id', trans('admin.rooms')) !!}
                  {!! Form::select('room_id', $rooms, null, ['class'=> 'form-control','placeholder'=> trans('admin.room_select'),'data-rule-required'=>'true']) !!}
              </div>
            </div>
          @endif
          <div class="col-md-3">
              <div class="form-group">
                  {!! Form::label('start', trans('admin.start')) !!}
              <div class='input-group date'>
                  {!! Form::text('start', null, ['class' => 'form-control', 'placeholder'=> trans('admin.start')]) !!}
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
              </div>
              </div>
            </div>
          <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('end', trans('admin.end')) !!}
            <div class='input-group date'>
                {!! Form::text('end', null, ['class' => 'form-control', 'placeholder'=> trans('admin.end')]) !!}
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              {!! Form::label('status', trans('admin.status')) !!}
              {!! Form::select('status', ['CNF' => 'Confermato', 'CAN' => 'Cancellato', 'PND' => 'In Attesa'], null, ['class' => 'form-control','placeholder' => 'Seleziona una stato']) !!}
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                {!! Form::submit(trans('admin.update'), ['class' => 'btn btn-primary']) !!}
            </div>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
    @push('script')
      <script type="text/javascript" src="{{asset('resources/assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}"></script>
      <script>
          $(window).load(function () {
            $date_format = 'YYYY-MM-DD';
            $('input[name="start"]').daterangepicker({
                autoUpdateInput: false,
                locale: {
                  format: $date_format
                },
                allowInputToggle: true
            });
            $('input[name="end"]').daterangepicker({
                autoUpdateInput: false,
                locale: {
                  format: $date_format
                },
                allowInputToggle: true
            });

            $('input[name="start"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format($date_format));
              $('input[name="end"]').val(picker.endDate.format($date_format));
            });
            $('input[name="end"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.endDate.format($date_format));
              $('input[name="start"]').val(picker.startDate.format($date_format));
            });
          });
      </script>
    @endpush

@stop
