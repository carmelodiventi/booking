@extends('admin.main')

@push('styles')
  <link href="{!! asset('resources/assets/admin/css/calendar-prices.css') !!}" media="all" rel="stylesheet" type="text/css"/>
  <style>
  .tooltip {
      position: fixed;
  }
  </style>
@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Room Details</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li><a href="{{url('/admin/rooms')}}">Rooms</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Details
            @foreach ($rooms as $room)
              {{$room->name}}
            @endforeach
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
    @php
      $period_st = strtotime($period);
      $start = date('Y-m-1', $period_st);
      $end = date('Y-m-t', $period_st);
      $period = explode('-', $period);
    @endphp
      <div class="table-responsive">
          {!! Helper::draw_calendar($rooms, $period[0], $period[1])!!}
      </div>

@stop

@push('script')
  <script>
    var date_start = '{{$start}}';
    var date_end = '{{$end}}';
  </script>
  <script src="{{asset('resources/assets/admin/plugins/knockout/knockout-3.4.1.js')}}"></script>
  <script src="{{asset('resources/assets/admin/js/calendar-prices.js')}}"></script>
@endpush
