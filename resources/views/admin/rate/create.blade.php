@extends('admin.main')

@push('styles')
<link rel="stylesheet" href="{{asset('resources/assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" type="text/css" rel="stylesheet">
@endpush

@section('page-header')
    <div class="row">
      <div class="col-sm-6">
        <h4 class="mt-0 mb-5">Create a new Room Type Rate</h4>
        <ol class="breadcrumb mb-0">
          <li><a href="{{url('/admin')}}">Home</a></li>
            <li><a href="{{url('/admin/rate')}}">Rate</a></li>
          <li class="active">
              <span class="show-for-sr">Current: </span> New Rate
          </li>
        </ol>
      </div>
      <div class="col-sm-6">
        @include('admin.includes.language')
      </div>
    </div>
@stop

@section('page-content')

        {!! Form::open(['route' => 'rate.store']) !!}
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('start', trans('admin.start')) !!}
                <div class='input-group date'>
                {!! Form::text('start', null, ['class' => 'form-control', 'placeholder'=> trans('admin.start')]) !!}
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('end', trans('admin.end')) !!}
              <div class='input-group date'>
              {!! Form::text('end', null, ['class' => 'form-control', 'placeholder'=> trans('admin.end')]) !!}
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
          </div>
          @if($rooms_type)
              <div class="col-md-3">
                  <div class="form-group">
                  {!! Form::label('type', trans('admin.type')) !!}
                  <select name="type" class="form-control">
                    <option value="">{{trans('admin.select_type')}}</option>
                    @foreach ($rooms_type as $key => $value)
                      <option value="{{$key}}">{{Helper::get_traslated_string($value)}}</option>
                    @endforeach
                  </select>
                  </div>
              </div>
          @endif


          </div>

          <div class="row">
            @if($rates_type)
                <div class="col-md-3">
                    <div class="form-group">
                    {!! Form::label('rates_type', trans('admin.rate')) !!}
                    <select name="rates_type" class="form-control">
                      <option value="">{{trans('admin.select_type')}}</option>
                      @foreach ($rates_type as $key => $value)
                        <option value="{{$key}}">{{Helper::get_traslated_string($value)}}</option>
                      @endforeach
                    </select>
                    </div>
                </div>
            @endif
            <div class="col-md-2">
              <div class="form-group">
              {!! Form::label('price', trans('admin.price')) !!}
              {!! Form::number('price', null, ['class' => 'form-control','min' => 0, 'step'=>'any', 'placeholder'=> trans('admin.price')]) !!}
              </div>
          </div>
            <div class="col-md-3">
            <div class="form-group">
              {!! Form::label('status', trans('admin.status')) !!}
              {!! Form::select('status', ['CNF' => 'Confermata', 'CAN' => 'Cancellato', 'PND' => 'In Attesa'], null, ['class' => 'form-control','placeholder' => 'Seleziona una stato']) !!}
            </div>
          </div>
          </div>
      <div class="row">
          <div class="col-md-3">
              {!! Form::submit(trans('admin.add'), ['class' => 'btn btn-primary']) !!}
          </div>
      </div>
      {!! Form::close() !!}
    @push('script')
    <script type="text/javascript" src="{{asset('resources/assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}"></script>
    <script>
      $(window).load(function () {
        $date_format = 'YYYY-MM-DD';

       $('input[name="start"]').daterangepicker({
           autoUpdateInput: false,
           locale: {
             format: $date_format
           },
           allowInputToggle: true
       });

       $('input[name="end"]').daterangepicker({
           autoUpdateInput: false,
           locale: {
             format: $date_format
           },
           allowInputToggle: true
       });

       $('input[name="start"]').on('apply.daterangepicker', function(ev, picker) {
         $(this).val(picker.startDate.format($date_format));
         $('input[name="end"]').val(picker.endDate.format($date_format));
       });

       $('input[name="end"]').on('apply.daterangepicker', function(ev, picker) {
         $(this).val(picker.endDate.format($date_format));
         $('input[name="start"]').val(picker.startDate.format($date_format));
       });

      });
    </script>
    @endpush

    @stop
