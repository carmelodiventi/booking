@extends('admin.main')

@push('styles')
  <link href="{!! asset('resources/assets/admin/css/calendar-rates.css') !!}" media="all" rel="stylesheet" type="text/css"/>
@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Rate</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Rate
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')

      @php
      $start_date = date("Y-m-d", $interval);
      $end_date = date("Y-m-d", strtotime('+45 days', strtotime($start_date)));
      $next_month = date('Y-m', strtotime(' +1 month', $interval));
      $prev_month = date('Y-m', strtotime(' -1 month', $interval));
      $next_year = date('Y-m', strtotime(' +1 year', $interval));
      $prev_year = date('Y-m', strtotime(' -1 year', $interval));
      @endphp
      <div class="widget">
        <div class="widget-heading">
          <div class="btn-group">
            <a href="{!! url()->current() !!}?interval={{$prev_year}}" alt="{{trans('admin.prev_year')}}"
              data-toggle="tooltip" title="{{trans('admin.prev_year')}}"
              class="btn btn-primary">
            <i class="ti-angle-double-left"></i>
            </a>
            <a href="{!! url()->current() !!}?interval={{$prev_month}}" alt="{{trans('admin.prev_month')}}"
              data-toggle="tooltip" title="{{trans('admin.prev_month')}}"
              class="btn btn-primary">
            <i class="ti-angle-left"></i>
            </a>
            <a href="{!! url('admin/planning') !!}" class="btn btn-primary" data-toggle="tooltip" title="{{trans('admin.current_month')}}"> <i class="ti-target"></i></a>
            <a href="{!! url()->current() !!}?interval={{$next_month}}"
              data-toggle="tooltip" title="{{trans('admin.next_month')}}"
              alt="{{trans('admin.next_month')}}" class="btn btn-primary">
              <i class="ti-angle-right"></i>
            </a>
            <a href="{!! url()->current() !!}?interval={{$next_year}}"
              data-toggle="tooltip" title="{{trans('admin.next_year')}}"
              alt="{{trans('admin.next_year')}}" class="btn btn-primary">
              <i class="ti-angle-double-right"></i>
            </a>
          </div>
          <div class="btn-group pull-right">
            <button class="btn btn-danger btn-save"><i class="ti-save"></i> {{trans('admin.save')}}</button>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="widget-body">

          @if($rooms_type)
              @foreach ($rooms_type as $room_type)
                {!! Form::open(['method' => 'put', 'url' =>'admin/rate/'. $room_type->id])!!}
                  <div class="panel panel-default">
                    <div class="panel-heading">{{json_decode($room_type->type,true)[LaravelLocalization::getCurrentLocale()]}}
                      <div class="btn-group pull-right" data-room-type-id="{{$room_type->id}}">
                        <button type="button" class="btn btn-success btn-sm btn-no-edit" onclick="manage_rates.editPrices({{$room_type->id}})"> {{trans('admin.edit_prices')}}</button>
                        <button type="button" class="btn btn-default btn-sm btn-edit" onclick="javascript:location.reload()"> {{trans('admin.cancel')}}</button>
                        <button type="submit" class="btn btn-success btn-sm btn-edit"> {{trans('admin.save_prices')}}</button>
                        </div>
                    </div>
                    <div class="panel-body">
                      <div class="table-responsive">
                          {!! Helper::rates_calendar($rates,$room_type->units,$prices,$room_type->id, $interval)!!}
                      </div>
                    </div>
                </div>
                {!! Form::close() !!}
              @endforeach
          @endif
        </div>
      </div>



    @push('script')
      <script type="text/javascript" src="{{asset('resources/assets/admin/js/manage-rates.js')}}"></script>
    @endpush

@stop
