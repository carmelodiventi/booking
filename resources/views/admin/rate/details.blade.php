@extends('admin.main')

@push('styles')

@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Rate</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Rate
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      <div class="btn-group mt-5">
        <button type="button" class="btn btn-default btn-outline"><i class="flag-icon flag-icon-us mr-5"></i> English</button>
        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default btn-outline dropdown-toggle"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>
        <ul class="dropdown-menu dropdown-menu-right animated fadeInDown">
          <li><a href="#"><i class="flag-icon flag-icon-de mr-5"></i> German</a></li>
          <li><a href="#"><i class="flag-icon flag-icon-fr mr-5"></i> French</a></li>
          <li><a href="#"><i class="flag-icon flag-icon-es mr-5"></i> Spanish</a></li>
          <li><a href="#"><i class="flag-icon flag-icon-it mr-5"></i> Italian</a></li>
          <li><a href="#"><i class="flag-icon flag-icon-jp mr-5"></i> Japanese</a></li>
        </ul>
      </div>
    </div>
  </div>
@stop

@section('page-content')

        <a href="{{ url('admin/rate')}}" class="btn btn-primary">{{trans('admin.list')}}</a>
          <hr>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>{{ trans('admin.season')}}</th>
                <th>{{ trans('admin.price')}}</th>
                <th>{{ trans('admin.start')}}</th>
                <th>{{ trans('admin.end')}}</th>
                <th>{{ trans('admin.type')}}</th>
                <th>{{ trans('admin.action')}}</th>
            </tr>
            </thead>
            <tbody>
            @if($rate)
                @foreach ($rate as $single_rate)
                    <tr>
                        <td>{{$single_rate->season}}</td>
                        <td>{{$single_rate->price}}</td>
                        <td>{{date('d M Y', strtotime($single_rate->start))}}</td>
                        <td>{{date('d M Y', strtotime($single_rate->end))}}</td>
                        <td>{{$single_rate->rate_name}}</td>
                        <td>
                          <a href="{{ route('rate.edit', $single_rate->rate_id) }}" class="btn btn-success"><i class="fa fa-edit"></i></a>
                            {!! Form::open(['method' => 'DELETE','route' => ['rate.destroy', $single_rate->rate_id], 'class' => 'inline']) !!}
                            {!! Form::submit(trans('admin.delete'), ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>

    @push('script')

    @endpush

@stop
