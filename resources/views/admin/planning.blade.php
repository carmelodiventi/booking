@extends('admin.main')
@push('styles')
<style>
  .btn-cancel,
  .btn-reserve {
    display: none;
  }
</style>
@endpush

@section('page-header')
<div class="row">
  <div class="col-sm-6">
    <h4 class="mt-0 mb-5">Planning</h4>
    <ol class="breadcrumb mb-0">
      <li><a href="{{url('/admin')}}">Home</a></li>
      <li class="active">
        <span class="show-for-sr">Current: </span> Planning
      </li>
    </ol>
  </div>
  <div class="col-sm-6">
    @include('admin.includes.language')
  </div>
</div>
@stop

@section('page-content')

@php
$start_date = date("Y-m-d", $interval);
$end_date = date("Y-m-d", strtotime('+45 days', strtotime($start_date)));
$next_month = date('Y-m', strtotime(' +1 month', $interval));
$prev_month = date('Y-m', strtotime(' -1 month', $interval));
$next_year = date('Y-m', strtotime(' +1 year', $interval));
$prev_year = date('Y-m', strtotime(' -1 year', $interval));
@endphp

@if(!$rooms_type->isEmpty() && !$rooms->isEmpty() && $master_rate !== null)

<div class="widget">
  <div class="widget-heading">
    <div class="btn-group">
      <a href="{!! url()->current() !!}?interval={{$prev_year}}" alt="{{trans('admin.prev_year')}}"
        data-toggle="tooltip" title="{{trans('admin.prev_year')}}" class="btn btn-primary">
        <i class="ti-angle-double-left"></i>
      </a>
      <a href="{!! url()->current() !!}?interval={{$prev_month}}" alt="{{trans('admin.prev_month')}}"
        data-toggle="tooltip" title="{{trans('admin.prev_month')}}" class="btn btn-primary">
        <i class="ti-angle-left"></i>
      </a>
      <a href="{!! url('admin/planning') !!}" class="btn btn-primary" data-toggle="tooltip"
        title="{{trans('admin.current_month')}}"> <i class="ti-target"></i></a>
      <a href="{!! url()->current() !!}?interval={{$next_month}}" data-toggle="tooltip"
        title="{{trans('admin.next_month')}}" alt="{{trans('admin.next_month')}}" class="btn btn-primary">
        <i class="ti-angle-right"></i>
      </a>
      <a href="{!! url()->current() !!}?interval={{$next_year}}" data-toggle="tooltip"
        title="{{trans('admin.next_year')}}" alt="{{trans('admin.next_year')}}" class="btn btn-primary">
        <i class="ti-angle-double-right"></i>
      </a>
    </div>
    <div class="btn-group pull-right">
      <button id="move" class="btn btn-primary btn-enable-function" onclick="planning.enableReservationsMove(true)"><i
          class="ti-move"></i> {{trans('admin.move')}}</button>
      <button class="btn btn-success btn-reserve"><i class="ti-save"></i> {{trans('admin.save')}}</button>
      <button id="cancel" class="btn btn-danger btn-cancel"
        data-loading-text="<i class='fa fa-spinner fa-spin'></i> {{trans('admin.waiting_please')}}"><i
          class="ti-na"></i> {{trans('admin.cancel')}}</button>
      <button id="reserve" class="btn btn-success  btn-enable-function"
        onclick="planning.enableDateRangeSelection(true)"><i class="ti-save"></i> {{trans('admin.reserve')}}</button>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="widget-body">
    <div class="table-responsive">
      {!! Helper::planning_calendar($rooms_type, $rooms, $master_rate, $interval)!!}
    </div>
  </div>
</div>

<div class="modal" id="new_reservation">
  <div class="modal-content">
    <div class="modal-header">
      <h3> {{trans('admin.new_reservation')}}</h3>
    </div>
    <div class="modal-body">

    </div>
  </div>
</div>

@else
<div class="alert alert-warning">
  <p>{{trans('admin.no_rooms_rooms_type_available')}}</p>
</div>
@endif
@stop

@push('script')
<script>
  var startDate = '{{$start_date}}';
    var endDate = '{{$end_date}}';
</script>
<script src="{{asset('resources/assets/admin/js/planning.js')}}"></script>
@endpush