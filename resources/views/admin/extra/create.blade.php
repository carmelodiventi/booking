@extends('admin.main')

@push('styles')
@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">New Extra</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li><a href="{{url('/admin/emails')}}">Extra</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> New Extra
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
  <div class="widget">
    <div class="widget-heading">
                <h3 class="widget-title">Create Extra</h3>
    </div>
      <div class="widget-body">
        {!! Form::open(['route' =>'extra.store'])!!}
        <ul role="tablist" class="nav nav-tabs mb-15">
          @foreach (LaravelLocalization::getSupportedLanguagesKeys() as $key => $value)
            <li role="presentation" class="{{$loop->index==0 ? 'active' : ''}}"><a href="#{{$key}}" aria-controls="general" role="tab" data-toggle="tab" aria-expanded="true">{{$value}}</a></li>
          @endforeach
        </ul>
        <div class="tab-content">
        @foreach (LaravelLocalization::getSupportedLanguagesKeys() as $key => $value)
          <div id="{{$key}}" role="tabpanel" class="tab-pane {{$loop->index==0 ? 'active' : ''}}">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  {!! Form::label('name', trans('admin.name')) !!}
                  {!! Form::text('name[]', null, ['class' => 'form-control','placeholder'=> trans('admin.name')]) !!}
                </div>
              </div>
              {!! Form::text('language[]', $value, ['class' => 'hidden']) !!}
            </div>
          </div>
        @endforeach
        </div>
        <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            {!! Form::label('price', trans('admin.price')) !!}
            {!! Form::number('price', null, ['class' => 'form-control', 'min'=>'0', 'step'=>'any','placeholder'=> trans('admin.price')]) !!}
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            {!! Form::label('status', trans('admin.status')) !!}
            {!! Form::select('status[]', ['CNF' => 'Confermato', 'CAN' => 'Cancellato', 'PND' => 'In Attesa'], null, ['class' => 'form-control','placeholder' => 'Seleziona una stato']) !!}
          </div>
        </div>
        </div>
        <div class="row">
          <div class="col-md-3">
              {!! Form::submit(trans('admin.save'), ['class' => 'btn btn-primary']) !!}
          </div>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
    @push('script')
    @endpush

@stop
