@extends('admin.main')

@push('styles')

@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Extra</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Extra
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')

    <a href="{{ url('admin/extra/create')}}" class="btn btn-primary">{{trans('admin.add')}}</a>
    <hr>
    <table id="extras-list" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans('admin.name')}}</th>
                <th>{{ trans('admin.price')}}</th>
                <th>{{ trans('admin.status')}}</th>
                <th>{{ trans('admin.options')}}</th>
            </tr>
            </thead>
            <tbody>
            @if($extras)
                @foreach ($extras as $extra)
                    <tr>
                        <td>{{ Helper::get_traslated_string($extra->name)}}</td>
                        <td>{{$extra->price}}</td>
                        <td>{{$extra->status}}</td>
                        <td>
                            <a href="{{ route('extra.edit', $extra->id) }}" class="btn btn-success"><i
                                        class="fa fa-edit"></i></a>
                            {!! Form::open(['method' => 'DELETE','route' => ['extra.destroy', $extra->id], 'class' => 'inline']) !!}
                            {!! Form::submit(trans('admin.delete'), ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>

    @push('script')

    @endpush

@stop
