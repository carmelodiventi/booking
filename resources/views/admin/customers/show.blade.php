@extends('admin.main')

@push('styles')

@endpush

@section('search-bar')
  {!! Form::open(['url' => 'admin/customers/','method'=>'get','class'=>'mt-15 mb-15 pull-left hidden-xs']) !!}
    <div class="form-group has-feedback mb-0">
      <input type="text" aria-describedby="inputSearchFor" placeholder="{{trans('admin.search_for')}}" name="keyword" style="width: 200px" class="form-control rounded"><span aria-hidden="true" class="ti-search form-control-feedback"></span><span id="inputSearchFor" class="sr-only">(default)</span>
    </div>
  {!! Form::close() !!}
@endsection

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Customers</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> List
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')

        <a href="{{ url('admin/customers/create')}}" class="btn btn-primary">{{trans('admin.add')}}</a>
        <hr>
        <table id="customers-list" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans('admin.name')}}</th>
                <th>{{ trans('admin.surname')}}</th>
                <th>{{ trans('admin.phone')}}</th>
                <th>{{ trans('admin.email')}}</th>
                <th>{{ trans('admin.country')}}</th>
                <th>{{ trans('admin.action')}}</th>
            </tr>
            </thead>
            <tbody>
            @if($customers)
                @foreach ($customers as $customer)
                    <tr>
                        <td>{{$customer->name}}</td>
                        <td>{{$customer->surname}}</td>
                        <td>
                            {{$customer->phone}}<br>
                            {{$customer->phone_2}}<br>
                            {{$customer->phone_3}}
                        </td>
                        <td>{{$customer->email}}</td>
                        <td>{{$customer->country}}</td>
                        <td>
                            <a href="{{route('customers.edit', $customer->id)}}" class="btn btn-success"><i class="fa fa-edit"></i></a>
                            {!! Form::open(['method' => 'DELETE','route' => ['customers.destroy', $customer->id], 'class' => 'inline']) !!}
                            {!! Form::submit(trans('admin.delete'), ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        <div>
          {{ $customers->links() }}
        </div>

    @push('script')

    <script>
        $(window).load(function () {
            $('#customers-list').DataTable({
                responsive : true,
                paging: false,
                "bInfo" : false
            });
        });
    </script>
    @endpush

@stop
