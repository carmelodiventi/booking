@extends('admin.main')

@push('styles')

@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Create a new Customer</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li><a href="{{url('/admin/customers')}}">Customers</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> New
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')
    <div class="widget">
      <div class="widget-heading">
                  <h3 class="widget-title">Insert new Customer</h3>
      </div>
        <div class="widget-body">
            {!! Form::open(['route' => 'customers.store']) !!}
              @include('admin.includes.form.customer',['text_btn'=> trans('admin.add')])
            {!! Form::close() !!}
        </div>
    </div>
    @push('script')
    <script>
        $('select[name="country"]').chosen();
    </script>
    @endpush

@stop
