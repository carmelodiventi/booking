@extends('admin.main')

@push('styles')

@endpush


@section('page-header')
<div class="row">
  <div class="col-sm-6">
    <h4 class="mt-0 mb-5">Dashboard</h4>
    <p class="text-muted mb-0">Booking Engine</p>
  </div>
  <div class="col-sm-6">
    @include('admin.includes.language')
  </div>
</div>
@stop

@section('page-content')
<div class="row">
  <div class="col-lg-9">
    <div id="canvas-container"></div>
    <div class="clearfix"></div>
  </div>
  <div class="col-lg-3">
    <div class="row">
      <div class="col-lg-12 col-md-4">
        <div class="mb-30">
          <div class="media">
            <div class="media-body">
              <h5 class="media-heading">{!! trans('admin.total_reservations') !!} </h5>
              <div class="fs-36 fw-600">
                <span class="counter" data-total-reservation></span>
              </div>
            </div>
            <div class="media-right"><i class="fs-30 ti-receipt"></i></div>
            <ul class="list-unstyled">
              <li>
                <div class="block clearfix mb-5"><span class="pull-left fs-12">{{trans('admin.deleted')}}</span><span
                    class="pull-right label label-danger" data-reservation-cancelled></span></div>
              </li>
              <li>
                <hr>
              </li>
              <li>
                <div class="block clearfix mb-5"><span class="pull-left fs-12">{{trans('admin.confirmed')}}</span><span
                    class="pull-right label label-warning" data-reservation-confirmed></span></div>
              </li>
              <li>
                <hr>
              </li>
              <li>
                <div class="block clearfix mb-5"><span class="pull-left fs-12">{{trans('admin.paid')}}</span><span
                    class="pull-right label label-success" data-reservation-paid></span></div>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div class="col-lg-12 col-md-4">
        <div class="mb-30">
          <div class="media">
            <div class="media-body">
              <h5 class="media-heading">{!! trans('admin.incoming_reservations') !!} </h5>
              <div class="fs-36 fw-600">
                <span class="counter" data-incoming-reservation></span>
              </div>
            </div>
            <div class="media-right"><i class="fs-30 ti-arrow-left"></i></div>
            <ul class="list-unstyled">
              <li>
                <div class="block clearfix mb-5"><span class="pull-left fs-12">{{trans('admin.today')}}</span><span
                    class="pull-right label label-outline label-primary" data-incoming-reservation-today></span></div>
              </li>
              <li>
                <hr>
              </li>
              <li>
                <div class="block clearfix mb-5"><span class="pull-left fs-12">{{trans('admin.tomorrow')}}</span><span
                    class="pull-right label label-outline label-primary" data-incoming-reservation-tomorrow></span>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-12 col-md-4">
        <div class="mb-30">
          <div class="media">
            <div class="media-body">
              <h5 class="media-heading">{!! trans('admin.outgoing_reservations') !!}</h5>
              <div class="fs-36 fw-600">
                <span class="counter" data-outgoing-reservation></span>
              </div>
            </div>
            <div class="media-right"><i class="fs-30 ti-arrow-right"></i></div>
          </div>
          <ul class="list-unstyled">
            <li>
              <div class="block clearfix mb-5"><span class="pull-left fs-12">{{trans('admin.today')}}</span><span
                  class="pull-right label label-outline label-primary" data-outgoing-reservation-today></span></div>
            </li>
            <li>
              <hr>
            </li>
            <li>
              <div class="block clearfix mb-5"><span class="pull-left fs-12">{{trans('admin.tomorrow')}}</span><span
                  class="pull-right label label-outline label-success" data-outgoing-reservation-tomorrow></span></div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
@stop