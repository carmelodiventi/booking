@extends('admin.main')

@push('styles')
  <!-- DropzoneJS-->
     <link rel="stylesheet" type="text/css" href="{{asset('resources/assets/admin/plugins/dropzone/dist/min/dropzone.min.css')}}">
@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Rooms Type Photo</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li><a href="{{url('/admin/rooms-type')}}">Rooms Type</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Rooms Type Photo
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')

    <a href="{{ url('admin/rooms-type') }}" class="btn btn-primary">{{trans('admin.list')}}</a>
    <hr>
    <div class="row">
      <div class="col-md-12">
        <h4>{!! Helper::get_traslated_string($room_type->type) !!}</h4>
      </div>
    </div>
    @if($photo_list)
        <div class="row">
            @foreach($photo_list as $photo)
                <div class="col-md-2">
                    <a href="#" class="thumbnail">
                        <form id="deletephoto" action="{{url('admin/photodelete') .'/'.$photo->id  }}">
                            <img src="{{ url($photo->url)}}" style="margin-bottom: 15px" class="img-thumbnail"/>
                            <button class="btn btn-danger btn-block"><i
                                        class="fa fa-trash-o"></i> {{trans('admin.delete')}} </button>
                        </form>
                    </a>
                </div>
            @endforeach
        </div>
    @endif

    {!! Form::open([ 'url' => [ 'admin/photoupload/' . $room_type->id  ], 'files' => true, 'enctype' => 'multipart/form-data', 'class' => 'dropzone', 'id' => 'image-upload' ]) !!}
    {!! Form::close() !!}

    @push('script')
      <script type="text/javascript" src="{{asset('resources/assets/admin/plugins/dropzone/dist/min/dropzone.min.js')}}"></script>
    @endpush

@stop
