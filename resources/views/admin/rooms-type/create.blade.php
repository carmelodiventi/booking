@extends('admin.main')

@push('styles')
  <!-- Summernote-->
  <link rel="stylesheet" type="text/css" href="{{asset('resources/assets/admin/plugins/summernote/dist/summernote.css')}}">
@endpush

@section('page-header')
    <div class="row">
      <div class="col-sm-6">
        <h4 class="mt-0 mb-5">Rooms Type</h4>
        <ol class="breadcrumb mb-0">
          <li><a href="{{url('/admin')}}">Home</a></li>
          <li><a href="{{url('/admin/rooms-type')}}">Rooms Type</a></li>
          <li class="active">
              <span class="show-for-sr">Current: </span> Create Rooms Type
          </li>
        </ol>
      </div>
      <div class="col-sm-6">
        @include('admin.includes.language')
      </div>
    </div>
@stop

@section('page-content')

        {!! Form::open(['route' => 'rooms-type.store']) !!}

        <ul role="tablist" class="nav nav-tabs mb-15">
          @foreach (LaravelLocalization::getSupportedLanguagesKeys() as $key => $value)
            <li role="presentation" class="{{$loop->index==0 ? 'active' : ''}}"><a href="#{{$key}}" aria-controls="general" role="tab" data-toggle="tab" aria-expanded="true">{{$value}}</a></li>
          @endforeach
        </ul>

        <div class="tab-content">
        @foreach (LaravelLocalization::getSupportedLanguagesKeys() as $key => $value)
          <div id="{{$key}}" role="tabpanel" class="tab-pane {{$loop->index==0 ? 'active' : ''}}">
            <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    {!! Form::label('type[]', trans('admin.type')) !!}
                    {!! Form::text('type[]', null, ['class' => 'form-control','placeholder'=> trans('admin.type')]) !!}
                </div>
            </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    {!! Form::label('description[]', trans('admin.description')) !!}
                    {!! Form::textarea('description[]', null, ['class' => 'form-control','placeholder'=> trans('admin.description')]) !!}
                </div>
            </div>
            </div>
          </div>
          {!! Form::text('language[]', $value, ['class' => 'hidden']) !!}
        @endforeach
        </div>

        <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                {!! Form::label('capacity', trans('admin.capacity')) !!}
                {!! Form::selectRange('capacity', 1, 20, null, ['class'=>'form-control', 'placeholder' => 'Seleziona Capacità']) !!}
            </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                {!! Form::label('units', trans('admin.units')) !!}
                {!! Form::selectRange('units', 1, 20, null, ['class'=>'form-control', 'placeholder' => 'Seleziona Unità']) !!}
            </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                   {!! Form::label('gender', trans('admin.gender')) !!}
                  <div class="radio">
                      <label class="radio-inline"> {!! Form::radio('gender', 'MI') !!} {{trans('admin.mixed')}}</label>
                      <label class="radio-inline"> {!! Form::radio('gender', 'MA') !!} {{trans('admin.males')}}</label>
                      <label class="radio-inline"> {!! Form::radio('gender', 'FE') !!} {{trans('admin.females')}}</label>
                  </div>

            </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                {!! Form::label('status', trans('admin.status')) !!}
                <p>
                  <label class="radio-inline"> {!! Form::checkbox('isDisabled', 1) !!} {{trans('admin.disabled')}}</label>
                </p>
            </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                {!! Form::submit(trans('admin.add'), ['class' => 'btn btn-primary']) !!}
            </div>
        </div>
        {!! Form::close() !!}

    @push('script')
      <script type="text/javascript" src="{{asset('resources/assets/admin/plugins/summernote/dist/summernote.min.js')}}"></script>
      <script>
      $(document).ready(function(){
        $('textarea[name="description[]"]').summernote();
      });
      </script>
    @endpush

@stop
