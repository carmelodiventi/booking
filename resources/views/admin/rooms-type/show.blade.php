@extends('admin.main')

@push('styles')

@endpush

@section('page-header')
  <div class="row">
    <div class="col-sm-6">
      <h4 class="mt-0 mb-5">Rooms Type</h4>
      <ol class="breadcrumb mb-0">
        <li><a href="{{url('/admin')}}">Home</a></li>
        <li class="active">
            <span class="show-for-sr">Current: </span> Rooms Type
        </li>
      </ol>
    </div>
    <div class="col-sm-6">
      @include('admin.includes.language')
    </div>
  </div>
@stop

@section('page-content')

    <a href="{{ url('admin/rooms-type/create')}}" class="btn btn-primary">{{trans('admin.add')}}</a>
    <hr>
    <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans('admin.type')}}</th>
                <th>{{ trans('admin.capacity')}}</th>
                <th>{{ trans('admin.units')}}</th>
                <th>{{ trans('admin.description')}}</th>
                <th>{{ trans('admin.status')}}</th>
                <th>{{ trans('admin.action')}}</th>
            </tr>
            </thead>
            <tbody>
            @if($rooms_type)
                @php $check = false; @endphp
                @foreach ($rooms_type as $room_type)
                    @if(env('MYALLOCATOR_ACTIVED') === true)
                      @foreach($ma_RoomsType->RoomTypes as $ma_room_type)
                          @if($ma_room_type->Units == $room_type->units && $ma_room_type->RoomId == $room_type->ma_room_id)
                            @php $check = true; @endphp
                          @else
                            @php $check = false; @endphp
                          @endif
                      @endforeach
                    @endif
                      <tr>
                          <td>{{Helper::get_traslated_string($room_type->type)}}</td>
                          <td>{{$room_type->capacity}}</td>
                          <td>{{$room_type->units}}</td>
                          <td style="width: 50%"> {!! html_entity_decode(Helper::get_traslated_string($room_type->description)) !!}</td>
                          <td>
                            {!! $room_type->isDisabled == 0 ? '<span class="label label-success">' . trans('admin.enabled') . '</span>' : '<span class="label label-danger">' . trans('admin.disabled') . '</span>' !!}
                        
                            @if($check)
                              <label data-popover="" class="label label-success"><i class="fa fa-check"></i></label>
                            @endif
                          </td>
                          <td>
                              <a href="{{ url('admin/photo', $room_type->id) }}" class="btn btn-success"><i
                                          class="fa fa-photo"></i></a>
                              <a href="{{ route('rooms-type.edit', $room_type->id) }}" class="btn btn-success"><i
                                          class="fa fa-edit"></i></a>
                              {!! Form::open(['method' => 'DELETE','route' => ['rooms-type.destroy', $room_type->id], 'class' => 'inline']) !!}
                              {!! Form::submit(trans('admin.delete'), ['class' => 'btn btn-danger']) !!}
                              {!! Form::close() !!}
                          </td>
                      </tr>


                @endforeach
            @endif
            </tbody>
        </table>
    @push('script')

    @endpush

@stop
