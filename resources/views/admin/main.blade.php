<!DOCTYPE html>
<html lang="en">
<head>
    @include('admin/includes.head')
</head>
<body>
<!-- Header start-->
@include('admin/includes.header')
<!-- Header end-->
<div class="main-container">
  <!-- Main Sidebar start-->
  @include('admin/includes.sidebar')
  <!-- Main Sidebar end-->
  <div class="page-container">
    <div class="page-header container-fluid">
        @yield('page-header')
    </div>
    <div class="page-content container-fluid">
        @yield('page-content')
    </div>
    <div class="footer">2019 &copy;  <a href="http://novabeds.com/">Novabeds.com</a></div>
  </div>
</div>
  @include('admin/includes.footer')
</body>
</html>
