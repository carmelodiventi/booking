/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/admin/js/reservation.js":
/*!**************************************************!*\
  !*** ./resources/assets/admin/js/reservation.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var rooms_selected;
var reservation = {
  init: function init() {
    this.validate();
    this.generateCalendar();
    this.selectChosen();
    this.manageExtra();
    this.showExtra();
    this.manageRooms();
    this.updateRooms();
    this.getNightsToStay();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
  },
  manageRooms: function manageRooms() {
    var regex = /^(.+?)(\d+)$/i;
    var cloneIndex = $(".clonedInput").length;

    function clone() {
      var max_room = $('[data-room="0"] select[name="rooms_type[]"] option').not(':first').length;

      if (cloneIndex < max_room && $('select[name="rooms_type[]"]').val() != "") {
        var room_line = $('.clonedInput:first').clone();
        room_line.appendTo($("#rooms .col-md-9:last")).attr('data-room', cloneIndex);
        reservation.resetFields($('[data-room="' + cloneIndex + '"]'));
        reservation.updateRooms();
        reservation.hideRooms();
        cloneIndex++;
      } else {
        $.noty.closeAll();
        noty({
          text: novabeds.rooms_availablility_error,
          layout: 'top',
          type: 'error',
          timeout: 3000
        });
      }
    }

    function remove() {
      if (cloneIndex > 1) {
        $(".clonedInput:last").remove();
        reservation.getTotal();
        reservation.updateRooms();
        cloneIndex--;
      }
    }

    $("button.clone").on("click", clone);
    $("button.remove").on("click", remove);
  },
  updateRooms: function updateRooms(item) {
    rooms_selected = [];
    $('select[name="rooms_type[]"] option:selected').each(function (index, el) {
      if ($(el).val() !== "") {
        rooms_selected.push($(el).val());
      }
    });
  },
  hideRooms: function hideRooms() {
    if (rooms_selected.length) {
      $('select[name="rooms_type[]"]:last option').each(function (index, item) {
        $(rooms_selected).each(function (index, el) {
          if ($(item).val() == el) {
            $('select[name="rooms_type[]"]:last option[value=' + el + ']').attr('disabled', 'disabled');
          }
        });
      });
    }
  },
  manageExtra: function manageExtra() {
    $('input[name="add_extra"]').click(function () {
      reservation.showExtra();
    });
  },
  showExtra: function showExtra() {
    if ($('input[name="add_extra"]').is(':checked')) {
      $('.extra').fadeIn();
      return;
    } else {
      $('.extra').fadeOut();
    }
  },
  addAdjustments: function addAdjustments() {
    var adjustment = $('input[name="adjustment"]');
    var adjustment_type = $('input[name="adjustment[type]"]:checked');
    var adjustment_amount = $('input[name="adjustment[amount]"]');
    var adjustment_list = '<tr class="adjustment">';

    if (adjustment.val() != '' && adjustment_type.val() != '' && adjustment_amount.val() != '') {
      adjustment_list += '<td>' + adjustment.val() + '<input type="hidden" name="adjustments[name][]" value="' + adjustment.val() + '"></td>';

      if (adjustment_type.val() == "1") {
        adjustment_list += '<td>' + adjustment_amount.val() + ' % <input type="hidden" name="adjustments[amount][]" value="' + adjustment_amount.val() + '"></td>';
      } else {
        adjustment_list += '<td>' + parseFloat(adjustment_amount.val()).toFixed(2) + '<input type="hidden" name="adjustments[amount][]" value="' + adjustment_amount.val() + '"> <input type="hidden" name="adjustments[type][]" value="' + adjustment_type.val() + '"></td>';
      }

      adjustment_list += '<td><button type="button" class="btn btn-danger btn-xs" onclick="reservation.removeAdjustments(this)">' + novabeds.remove + '</button></td>';
      adjustment_list += '</tr>';
      $('table#adjustaments').show();
      $('tbody#adjustaments-list').append(adjustment_list);
      $('#modal-adjustments').modal('hide');
      reservation.getTotal();
    } else {
      return false;
    }
  },
  removeAdjustments: function removeAdjustments($this) {
    $this.closest('tr.adjustment').remove();
  },
  generateCalendar: function generateCalendar() {
    $date_format = 'yyyy-mm-dd';
    $('input[name="check_in"],input[name="check_in[]"]').datepicker({
      calendarWeeks: true,
      autoclose: true,
      todayHighlight: true,
      //startDate: new Date(),
      format: $date_format
    }).on('changeDate', function (e) {
      var d = new Date(e.date);
      d.setUTCDate(d.getUTCDate() + 1);
      $('input[name="check_out"],input[name="check_out[]"]').datepicker('setStartDate', new Date(d));
    });
    $('input[name="check_out"],input[name="check_out[]"]').datepicker({
      calendarWeeks: true,
      autoclose: true,
      todayHighlight: true,
      //startDate: new Date,
      format: $date_format
    });
    $('input[name="check_out"],input[name="check_out[]"]').on('changeDate', function (e) {
      reservation.getNightsToStay();
      reservation.getAvailability();
    });
  },
  getRate: function getRate(rooms_type) {
    var check_in = $('input[name="check_in"]').val();
    var check_out = $('input[name="check_out"]').val();
    var $container = $(rooms_type).closest('.clonedInput');
    var rooms_type_id = $(rooms_type).val();
    var n_units = $(rooms_type).find('option:selected').data('units');
    var room_index = $container.data('room');
    var units = new Array();
    var count = 1;

    while (count <= n_units) {
      units.push(count++);
    }

    if (check_in == '' && check_out == '') {
      $.noty.closeAll();
      noty({
        text: novabeds.period_not_found,
        layout: 'top',
        closeWith: ['click', 'hover'],
        type: 'error',
        timeout: 3000
      });
      $('div[data-room="' + room_index + '"] select[name="rates[]"]').find('option').not(':first').remove();
      $('div[data-room="' + room_index + '"] div.guests > .form-group > .form-content').children().remove();
      $('div[data-room="' + room_index + '"] div.prices > .form-group > .form-content').children().remove();
      return false;
    } else if (rooms_type_id == '') {
      $.noty.closeAll();
      noty({
        text: novabeds.fare_not_found,
        layout: 'top',
        closeWith: ['click', 'hover'],
        type: 'error',
        timeout: 3000
      });
      $('div[data-room="' + room_index + '"] select[name="rates[]"]').find('option').not(':first').remove();
      $('div[data-room="' + room_index + '"] div.guests > .form-group > .form-content').children().remove();
      $('div[data-room="' + room_index + '"] div.prices > .form-group > .form-content').children().remove();
      return false;
    }

    $.ajax({
      type: "POST",
      url: base_url + '/getRate',
      data: {
        check_in: check_in,
        check_out: check_out,
        id: rooms_type_id
      }
    }).done(function (results) {
      if (typeof results.data !== 'undefined' && results.data.length > 0) {
        $('div[data-room="' + room_index + '"] select[name="rates[]"]').find('option').not(':first').remove();
        $('div[data-room="' + room_index + '"] select[name="rooms[]"]').find('option').not(':first').remove();
        $('div[data-room="' + room_index + '"] div.guests > .form-group > .form-content').children().remove();
        $('div[data-room="' + room_index + '"] div.prices > .form-group > .form-content').children().remove();
        $(results.data).each(function (index, el) {
          $('div[data-room="' + room_index + '"] select[name="rates[]"]').append($('<option>', {
            value: el.rates_type_id + "|" + JSON.parse(el.name)[locale],
            text: JSON.parse(el.name)[locale] + ' ' + '(' + el.rate_amount + ')',
            data: {
              capacity: el.capacity,
              amount: el.rate_amount,
              index: room_index
            }
          }));
        });
        $.each(units, function (index, value) {
          $('div[data-room="' + room_index + '"] select[name="rooms[]"]').append($('<option>', {
            value: value,
            text: value,
            data: {
              index: room_index
            }
          }));
        });
      } else {
        $('div[data-room="' + room_index + '"] select[name="rates[]"]').find('option').not(':first').remove();
        $('div[data-room="' + room_index + '"] select[name="rooms[]"]').find('option').not(':first').remove();
        $('div[data-room="' + room_index + '"] div.guests > .form-group > .form-content').children().remove();
        $('div[data-room="' + room_index + '"] div.prices > .form-group > .form-content').children().remove();
        $.noty.closeAll();
        noty({
          text: novabeds.fare_not_found,
          layout: 'top',
          type: 'error',
          timeout: 3000
        });
      }
    });
    reservation.getTotal();
  },
  getRooms: function getRooms(rooms_type_id) {
    $('div[data-room="' + room_index + '"] select[name="rooms[]"]').find('option').not(':first').remove();
  },
  selectRate: function selectRate(item) {
    var room_index = $(item).find(':selected').data('index');
    var amount = $(item).find(':selected').data('amount');
    var capacity = $(item).find(':selected').data('capacity');
    $('[data-room="' + room_index + '"] input[name*="price"]').val(parseFloat(amount).toFixed(2));
    $.noty.closeAll();
    noty({
      text: novabeds.price_updated,
      layout: 'top',
      type: 'success',
      timeout: 3000
    });
    reservation.getTotal();
  },
  checkAvailability: function checkAvailability(check_in, check_out, room_id) {
    if (check_in != '' && check_out != '' && room_id != '') {
      $.ajax({
        type: "POST",
        url: base_url + '/checkAvailabilityRoom',
        data: {
          check_in: check_in,
          check_out: check_out,
          room_id: room_id
        }
      }).done(function (result) {
        $.noty.closeAll();

        if (result.count != 0) {
          noty({
            text: novabeds.room_not_available.replace(/%/g, result.name),
            layout: 'top',
            closeWith: ['click', 'hover'],
            type: 'error',
            timeout: 3000
          });
        }
      });
    }
  },
  getAvailability: function getAvailability() {
    var check_in = $('input[name="check_in"]').val();
    var check_out = $('input[name="check_out"]').val();
    var rooms_type = [];

    if (check_in != '' && check_out != '') {
      reservation.resetEdit();
      $.ajax({
        type: "POST",
        url: base_url + '/getAvailability',
        data: {
          check_in: check_in,
          check_out: check_out
        }
      }).done(function (response) {
        if (response.status == 'success') {
          if (response.data.length == 0) {
            $.noty.closeAll();
            noty({
              text: novabeds.rooms_availablility_error,
              layout: 'top',
              closeWith: ['click', 'hover'],
              type: 'error',
              timeout: 3000
            });
          } else {
            rooms_type = response.data;
            $('select[name="rooms_type[]"]').find('option').not(':first').remove();
            $(rooms_type).each(function (index, item) {
              var type = JSON.parse(response.data[index].type)[locale];
              $('select[name="rooms_type[]"]').append($('<option>', {
                value: item.rooms_type_id,
                text: type + " (" + item.units + " " + novabeds.units + ")"
              }).attr("data-units", item.units).attr("data-capacity", item.capacity));
            });

            if (rooms_type.length > 1) {
              $('.options').removeClass('hidden');
            }

            $.noty.closeAll();
            noty({
              text: novabeds.availability_list_updated,
              layout: 'top',
              type: 'success',
              timeout: 3000
            });
          }
        } else {
          $.noty.closeAll();
          noty({
            text: novabeds.error,
            layout: 'top',
            type: 'error',
            timeout: 3000
          });
        }
      });
    }
  },
  getGuestsAndPrice: function getGuestsAndPrice(item) {
    var room_index = $(item).find(':selected').data('index');
    $('div[data-room="' + room_index + '"] div.guests > .form-group > .form-content').children().remove();
    $('div[data-room="' + room_index + '"] div.prices > .form-group > .form-content').children().remove();
    $('div[data-room="' + room_index + '"] select[name="rates[]"]').find('option:first').prop('selected', 'selected');
    var rooms_type_id = $('div[data-room="' + room_index + '"] select[name="rooms_type[]"]').find('option:selected').val();
    var n_room = $('div[data-room="' + room_index + '"] select[name="rooms[]"]').find('option:selected').val();
    var capacity = $('div[data-room="' + room_index + '"] select[name="rooms_type[]"]').find('option:selected').attr('data-capacity');
    var count = 1;

    while (count <= n_room) {
      // Guests Inputs
      var guests_html = $('<div class="input-group"><span class="input-group-addon"><i class="ti-user"></i></span><input type="number" name="guests[' + rooms_type_id + '][]" min="0" value="' + capacity + '" max="' + capacity + '" step="1" class="form-control" placeholder="Guest"></div><hr>');
      $('div[data-room="' + room_index + '"] div.guests > .form-group > .form-content').append(guests_html); // Prices Inputs

      var prices_html = $('<div class="input-group"><span class="input-group-addon"><i class="ti-money"></i></span><input type="number" name="price[' + rooms_type_id + '][]" min="0" value="0.00" step="0.00" class="form-control" placeholder="Prices" onchange="reservation.getTotal()"></div><hr>');
      $('div[data-room="' + room_index + '"] div.prices > .form-group > .form-content').append(prices_html); // increment count

      count++;
    }
  },
  selectChosen: function selectChosen() {
    $('select').chosen('destroy');
    $('select[name="customer"]').chosen({
      width: "100%"
    });
    $('select[name="origin"]').chosen({
      width: "100%"
    });
  },
  getsubTotal: function getsubTotal() {
    var subtotal = 0;
    $('input[name*="price"]').each(function (index, el) {
      //amount = parseFloat(el.value) * parseFloat($('select[name="rooms[]"]')[index].value);
      amount = parseFloat(el.value);
      subtotal += amount;
    });
    return subtotal;
  },
  getTotal: function getTotal() {
    var extra_amount = 0.00;
    var adjustments_amount = 0.00;
    var subtotal = reservation.getsubTotal();
    var amount = parseFloat($('input[name="amount"]').val());
    var discount = parseFloat($('input[name="discount"]').val());

    if ($('input[name="adjustments[amount][]"]').length) {
      $('input[name="adjustments[amount][]"]').each(function (index, el) {
        if ($('input[name="adjustments[type][]"]')[index] == 1) {
          adjustments_amount = subtotal * 100 / el.value;
        } else {
          adjustments_amount = adjustments_amount + parseFloat(el.value);
        }
      });
    }

    var topayval = (subtotal + extra_amount + adjustments_amount - amount - discount).toFixed(2);
    var totalval = (subtotal + extra_amount + adjustments_amount).toFixed(2);

    if (amount > totalval) {
      $('input[name="amount"]').val('0.00');
      $.noty.closeAll();
      noty({
        text: novabeds.amount_exceeds,
        layout: 'top',
        type: 'error',
        timeout: 3000
      });
      return false;
    } else {
      $('input[name="subtotal"]').val(subtotal.toFixed(2));
      $('input[name="topay"]').val(topayval);
      $('input[name="total"]').val(totalval);
    }
  },
  getCustomers: function getCustomers() {
    $('#loader').fadeIn();
    $.ajax({
      type: "GET",
      url: base_url + '/getCustomers'
    }).done(function (data) {
      $.each(data, function (i, val) {
        $('select[name="customer"]').append($('<option>', {
          value: i,
          text: val
        }));
      });
      $('select[name="customer"]').trigger("chosen:updated");
      $('#loader').fadeOut();
    });
  },
  getNightsToStay: function getNightsToStay() {
    dt1 = new Date($('input[name="check_in"]').val());
    dt2 = new Date($('input[name="check_out"]').val());
    nights = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
    $('span[data-night]').html(nights);
  },
  updateTotal: function updateTotal() {
    $('form').on('change keyup', 'input[name="price[]"]', function () {
      reservation.getTotal();
    });
  },
  validate: function validate() {
    $("#form-reservation").validate({
      ignore: ":not(:visible)",
      highlight: function highlight(r) {
        $(r).closest(".form-group").addClass("has-error");
      },
      unhighlight: function unhighlight(r) {
        $(r).closest(".form-group").removeClass("has-error");
      },
      errorElement: "span",
      errorClass: "help-block",
      errorPlacement: function errorPlacement(r, e) {
        e.parent(".input-group").length ? r.insertAfter(e.parent()) : e.parent("label").length ? r.insertBefore(e.parent()) : r.insertAfter(e);
      }
    });
  },
  resetSelect: function resetSelect(select, text) {
    $(select).empty().append($('<option>', {
      value: '',
      text: text
    }));
  },
  resetEdit: function resetEdit() {
    var $container = $('[data-room="0"]');
    $container.nextAll('div[data-room]').remove();
    $container.find('select[name="rooms[]"]').find('option:first').prop('selected', 'selected');
    $container.find('select[name="rates[]"]').find('option:first').prop('selected', 'selected');
    $container.find('select[name="rooms_type[]"]').find('option:first').prop('selected', 'selected');
    $container.find('div.guests > .form-group > .form-content').children().remove();
    $container.find('div.prices > .form-group > .form-content').children().remove();
  },
  resetFields: function resetFields($container) {
    $container.find('select[name="rooms[]"]').find('option:first').prop('selected', 'selected');
    $container.find('select[name="rates[]"]').find('option:first').prop('selected', 'selected');
    $container.find('select[name="rooms_type[]"]').find('option:first').prop('selected', 'selected');
    $container.find('div.guests > .form-group > .form-content').children().remove();
    $container.find('div.prices > .form-group > .form-content').children().remove();
  },
  showPayments: function showPayments(item) {
    if ($('input[name="register_deposit"]').is(':checked')) {
      $('#payments').fadeIn();
      return;
    } else {
      $('#payments').fadeOut();
    }
  },
  switchPayment: function switchPayment(item) {
    if ($(item).val() == 'credit_card') {
      $('#credit_card').fadeIn();
    } else {
      $('#credit_card').fadeOut();
    }
  },
  cancelBooking: function cancelBooking(el) {
    var id = $(el).attr('data-id');
    var cancellation_reason = $('textarea#cancellationReason').val();

    if (id !== undefined || id !== '') {
      $.ajax({
        url: base_url + '/admin/reservation/cancel',
        type: 'POST',
        data: {
          id: id,
          cancellationReason: cancellation_reason
        }
      }).done(function (response) {
        if (response.success) {
          $('#cancellationReasonModal').modal('hide');
          new noty({
            text: response.text,
            layout: 'top',
            type: 'success',
            timeout: 3000,
            callback: {
              onClose: function onClose() {
                location.reload();
              }
            }
          });
        } else {
          $('#cancellationReasonModal').modal('hide');
          new noty({
            text: response.text,
            layout: 'top',
            type: 'error',
            timeout: 3000
          });
        }
      }).fail(function (response) {
        console.log("error");
      }).always(function (response) {
        console.log("complete");
      });
    }
  },
  deleteBooking: function deleteBooking(el) {
    swal({
      title: novabeds.are_you_sure,
      text: novabeds.delete_reservation,
      type: 'warning',
      buttons: {
        cancel: novabeds.cancel,
        confirm: {
          text: novabeds.confirm,
          value: true,
          className: "btn-primary"
        }
      }
    }).then(function (confirm) {
      if (confirm) {
        var id = $(el).attr('data-id');

        if (id !== undefined || id !== '') {
          $.ajax({
            url: base_url + '/admin/reservation/delete',
            type: 'POST',
            data: {
              id: id
            }
          }).done(function (response) {
            if (response.success) {
              new noty({
                text: response.text,
                layout: 'top',
                type: 'success',
                timeout: 3000,
                callback: {
                  onClose: function onClose() {
                    window.history.back();
                  }
                }
              });
            } else {
              $('#cancellationReasonModal').modal('hide');
              new noty({
                text: response.text,
                layout: 'top',
                type: 'error',
                timeout: 3000
              });
            }
          }).fail(function () {
            console.log("error");
          }).always(function () {
            console.log("complete");
          });
        }
      }
    });
  }
};
reservation.init();

/***/ }),

/***/ 1:
/*!********************************************************!*\
  !*** multi ./resources/assets/admin/js/reservation.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/carmelodiventi/Development/booking-novabeds/resources/assets/admin/js/reservation.js */"./resources/assets/admin/js/reservation.js");


/***/ })

/******/ });