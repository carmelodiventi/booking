<?php

use Illuminate\Http\Request;
use App\BookingEngine;
use App\Http\Controllers\MyAllocatorController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::group(['prefix' => 'v1'], function () {
    //MyAllocatorController
    Route::any('/myallocator/sort_request', 'MyAllocatorController@SortRequest');
    Route::any('/myallocator/callback', 'MyAllocatorController@Callback');
    Route::get('/myallocator/bookinglist', 'MyAllocatorController@BookingList');
});

Route::group(['prefix' => 'v1', 'middleware' => 'auth:api'], function () {
    
});