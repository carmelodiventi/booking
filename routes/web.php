<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect']], function () {
    Route::get('/', 'AdminController@dashboard');
    // Autenticazione
    Auth::routes();
    Route::get('logout', 'Auth\LoginController@logout');
});

Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'auth', 'property']], function () {
    Route::get('/', 'AdminController@dashboard');
    Route::any('/loadReservations', 'ApiController@loadReservations');
    Route::post('/getRate', 'ApiController@getRate');
    Route::post('/checkAvailability', 'ApiController@checkAvailability');
    Route::post('/checkAvailabilityRoom', 'ApiController@checkAvailabilityRoom');
    Route::post('/getDetails', 'ApiController@getDetails');
    Route::get('/getCustomers', 'ApiController@getCustomers');
    Route::get('/getDashboardData', 'ApiController@getDashboardData');
    Route::post('/moveReservation', 'ApiController@moveReservation');
    // Availability
    Route::post('/getAccomodationsAvailability', 'ApiController@getAccomodationsAvailability');
    Route::post('/getAvailability', 'ApiController@getAvailability');
    Route::post('/storeAvailability', 'ApiController@storeAvailability');
    Route::get('/getAvailabilityFromDate', 'ApiController@getAvailabilityFromDate');
    // MyAllocator
    Route::get('/myallocator/roomAvailabilityList', 'MyAllocatorController@RoomAvailabilityList');
    Route::get('/myallocator/BookingList', 'MyAllocatorController@BookingList');
    // Admin
    Route::group(['prefix' => 'admin'], function () {
        Route::get('/', 'AdminController@dashboard');
        Route::get('profile', 'AdminController@profile');
        Route::get('dashboard', 'AdminController@dashboard');
        Route::get('planning', 'AdminController@planning');
        Route::get('settings', 'SettingsController@index');
        Route::post('settings/update/{key}', 'SettingsController@update');
        Route::post('settings/hotel', 'SettingsController@hotel');
        Route::post('settings/payment/paypal', 'SettingsController@paypal');
        Route::post('settings/payment/stripe', 'SettingsController@stripe');
        Route::post('settings/currency', 'SettingsController@currency');
        Route::post('settings/credit_card', 'SettingsController@credit_card');
        Route::post('settings/my_allocator', 'SettingsController@my_allocator');
        Route::get('photo/{id}', 'RoomsTypeController@photo');
        Route::post('photoupload/{id}', 'RoomsTypeController@photoupload');
        Route::get('photodelete/{id}', 'RoomsTypeController@photodelete');
        Route::get('reservation/details/{id}', 'ReservationsController@details');
        Route::post('reservation/cancel', 'ReservationsController@cancel');
        Route::post('reservation/delete', 'ReservationsController@destroy');
        Route::get('reservation/fix', 'ReservationsController@fix');
        Route::get('reservation/mark-to-review/{id}', 'ReservationsController@mark_to_review');
        Route::get('reservation/mark-to-complete/{id}', 'ReservationsController@mark_to_complete');
        Route::get('reservation/agency/{id}', 'ReservationsController@agency');
        Route::post('reservation/agency/{id}', 'ReservationsController@agency');
        Route::get('reservation/customer/{id}', 'ReservationsController@customer');
        Route::post('reservation/customer/{id}', 'ReservationsController@customer');
        Route::get('reservation/note/{id}', 'ReservationsController@note');
        Route::post('reservation/note/{id}', 'ReservationsController@note');
        Route::get('reservation/discount/{id}', 'ReservationsController@discount');
        Route::post('reservation/discount/{id}', 'ReservationsController@discount');
        Route::post('reservation/adjustments/{id}', 'ReservationsController@adjustments');
        Route::get('reservation/adjustments/{id}', 'ReservationsController@adjustments');
        Route::get('reservation/guests/{reservation_id}/{id}', 'ReservationsController@guests');
        Route::post('reservation/guests/{reservation_id}/{id}', 'ReservationsController@guests');
        Route::any('reservation/accomodations/{reservation_id}', 'ReservationsController@accomodations');
        Route::get('reservation/accomodations/remove/{reservation_id}/{id}', 'ReservationsController@accomodations_remove');
        Route::get('reservation/register-deposit/{id}', 'ReservationsController@register_deposit');
        Route::post('reservation/register-deposit/{id}', 'ReservationsController@register_deposit');
        Route::get('reservation/register-payment/{id}', 'ReservationsController@register_payment');
        Route::post('reservation/register-payment/{id}', 'ReservationsController@register_payment');
        Route::get('reservations/get-reservations-from-myallocator', 'ReservationsController@get_reservations_from_myallocator');
        Route::get('reservation/email/{id}', 'ReservationsController@email');
        Route::get('payments/confirm/{id}/{reservation_id}', 'ReservationPaymentsController@confirm');
        Route::get('payments/delete/{id}/{reservation_id}', 'ReservationPaymentsController@delete');
        Route::get('adjustments/delete/{id}/{reservation_id}', 'AdjustmentsController@delete');
        Route::get('fragments/put/', 'FragmentController@put');
        Route::get('fragments/list/{group}', 'FragmentController@list');
        Route::post('fragments/update/{id}', 'FragmentController@update');
        Route::post('fragments/delete/{id}', 'FragmentController@delete');
        Route::resource('reservations', 'ReservationsController');
        Route::resource('rooms', 'RoomsController');
        Route::resource('rooms-type', 'RoomsTypeController');
        Route::resource('fees', 'FeesController');
        Route::resource('rate-type', 'RatesTypeController');
        Route::resource('customers', 'CustomersController');
        Route::resource('offerts', 'OffertsController');
        Route::resource('emails', 'EmailsController');
        Route::resource('extra', 'ExtraController');
        Route::resource('fragments', 'FragmentController');
        Route::resource('availability', 'AvailabilityController');
        Route::post('availability/store', 'AvailabilityController@store');
        //Route::get('availability', 'AvailabilityController@index');
        Route::post('/notifMarkAsRead', 'NotificationsController@MarkAsRead');

        Route::get('/clear-cache', function () {
            $exitCode = Artisan::call('cache:clear');
            return back()->with('message_success', 'Cache cleared');
        });

        //Reoptimized class loader:
        Route::get('/optimize', function () {
            $exitCode = Artisan::call('optimize');
            return back()->with('message_success', 'Reoptimized class loader');
        });

        //Clear Route cache:
        Route::get('/route-cache', function () {
            $exitCode = Artisan::call('route:cache');
            return back()->with('message_success', 'Route cache cleared');
        });

        //Clear View cache:
        Route::get('/view-clear', function () {
            $exitCode = Artisan::call('view:clear');
            return back()->with('message_success', 'View cache cleared');
        });

        //Clear Config cache:
        Route::get('/config-cache', function () {
            $exitCode = Artisan::call('config:cache');
            return back()->with('message_success', 'Clear Config cleared');
        });
    });
});

Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'propertyWeb']], function () {
    Route::get('{property}/', 'BookingController@index');
    Route::any('{property}/rooms-availability', 'BookingController@roomsAvailability');
    Route::any('{property}/rooms-details/', 'BookingController@roomsDetails');
    Route::any('{property}/validate-code', ['as' => 'validate-code', 'uses' => 'OffertsController@validateCode']);
    Route::any('{property}/payment', ['as' => 'payment', 'uses' => 'BookingController@payment']);
    Route::any('{property}/ticket', ['as' => 'ticket', 'uses' => 'BookingController@ticket']);
    // Auth Cliente
    Route::post('{property}/customer/login', 'CustomersController@login');
    Route::post('{property}/customer/register', 'CustomersController@register');
    Route::post('{property}/customer/password/reset', 'CustomersController@resetPassword');
    Route::post('{property}/customer/password/renew', 'CustomersController@renewPassword');
    Route::any('{property}/customer/password/update/{token}', 'CustomersController@updatePassword');
});

/* CART */
Route::group(['middleware' => ['web']], function () {

    Route::get('cart/get', 'ReservationCartController@get');
    Route::get('cart/add', 'ReservationCartController@add');
    Route::get('cart/update', 'ReservationCartController@update');
    Route::get('cart/remove', 'ReservationCartController@remove');
    Route::get('cart/destroy', 'ReservationCartController@destroy');
});

Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'web', 'propertyWeb']], function () {
    Route::get('{property}/paypal', ['as' => 'paypal', 'uses' => 'PaymentsController@paypal']);
    Route::get('{property}/payment-complete', ['as' => 'paymentComplete', 'uses' => 'PaymentsController@paymentComplete']);
    Route::get('{property}/payment-cancel', ['as' => 'paymentCancel', 'uses' => 'PaymentsController@paymentCancel']);
    Route::get('{property}/credit-card', ['as' => 'creditCard', 'uses' => 'PaymentsController@creditCard']);
    Route::get('{property}/stripe', ['as' => 'stripe', 'uses' => 'PaymentsController@stripe']);
});
