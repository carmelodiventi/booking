<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReservationPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reservation_payments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('reservation_id')->index('fk_reservation');
			$table->string('transaction_id', 50)->default('');
			$table->string('causal', 100)->nullable();
			$table->string('method', 20)->nullable();
			$table->string('payment_type', 20)->nullable();
			$table->dateTime('payment_date')->nullable();
			$table->decimal('fee')->nullable();
			$table->decimal('advance', 10)->default(0.00);
			$table->decimal('total', 10);
			$table->timestamps();
			$table->string('status', 20)->default('');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reservation_payments');
	}

}
