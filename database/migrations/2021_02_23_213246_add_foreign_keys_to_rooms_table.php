<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRoomsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rooms', function(Blueprint $table)
		{
			$table->foreign('rooms_type_id', 'fk_rooms_type')->references('id')->on('rooms_type')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('property_id', 'rooms_ibfk_1')->references('id')->on('properties')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rooms', function(Blueprint $table)
		{
			$table->dropForeign('fk_rooms_type');
			$table->dropForeign('rooms_ibfk_1');
		});
	}

}
