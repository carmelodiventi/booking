<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoomsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rooms', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('property_id')->nullable()->index('rooms_ibfk_1');
			$table->integer('rooms_type_id')->index('fk_rooms_type');
			$table->string('name', 20)->default('');
			$table->timestamps();
			$table->integer('ordinal')->nullable();
			$table->char('status', 3)->default('CNF');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rooms');
	}

}
