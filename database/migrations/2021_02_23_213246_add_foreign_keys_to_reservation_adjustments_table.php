<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToReservationAdjustmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reservation_adjustments', function(Blueprint $table)
		{
			$table->foreign('reservation_id', 'fk_reservation_adjustments')->references('id')->on('reservations')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reservation_adjustments', function(Blueprint $table)
		{
			$table->dropForeign('fk_reservation_adjustments');
		});
	}

}
