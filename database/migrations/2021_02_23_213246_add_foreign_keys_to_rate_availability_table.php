<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRateAvailabilityTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rate_availability', function(Blueprint $table)
		{
			$table->foreign('rates_type_id', 'fk_availability_rate_types')->references('id')->on('rates_type')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('rooms_type_id', 'fk_availability_rt')->references('id')->on('rooms_type')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rate_availability', function(Blueprint $table)
		{
			$table->dropForeign('fk_availability_rate_types');
			$table->dropForeign('fk_availability_rt');
		});
	}

}
