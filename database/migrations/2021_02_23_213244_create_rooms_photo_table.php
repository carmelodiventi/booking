<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoomsPhotoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rooms_photo', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('rooms_type_id')->index('idroomtipo_idx');
			$table->string('url', 100)->nullable();
			$table->timestamps();
			$table->char('status', 3)->nullable()->default('CNF');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rooms_photo');
	}

}
