<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRateAvailabilityTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rate_availability', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('rates_type_id')->nullable()->index('fk_availability_rate_types_idx');
			$table->integer('rooms_type_id')->nullable()->index('fk_availability_idx');
			$table->date('date')->nullable();
			$table->decimal('units', 3, 0)->nullable();
			$table->decimal('rate', 10)->nullable();
			$table->decimal('single_rate', 10, 0)->nullable();
			$table->integer('min_stay')->nullable();
			$table->integer('max_stay')->nullable();
			$table->boolean('closed', 1)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rate_availability');
	}

}
