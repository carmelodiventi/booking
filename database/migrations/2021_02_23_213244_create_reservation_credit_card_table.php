<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReservationCreditCardTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reservation_credit_card', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('customer_id')->nullable();
			$table->integer('reservation_id')->nullable()->index('fk_reservation_credit_card');
			$table->text('data', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reservation_credit_card');
	}

}
