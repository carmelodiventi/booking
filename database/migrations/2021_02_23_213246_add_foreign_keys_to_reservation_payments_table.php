<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToReservationPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reservation_payments', function(Blueprint $table)
		{
			$table->foreign('reservation_id', 'fk_reservation_payment')->references('id')->on('reservations')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reservation_payments', function(Blueprint $table)
		{
			$table->dropForeign('fk_reservation_payment');
		});
	}

}
