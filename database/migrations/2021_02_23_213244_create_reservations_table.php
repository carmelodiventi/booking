<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReservationsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reservations', function (Blueprint $table) {
			$table->integer('id', true);
			$table->integer('property_id')->nullable()->index('reservations_ibfk_1');
			$table->string('myallocatorId', 45)->nullable();
			$table->integer('customer_id');
			$table->string('pnr', 8)->default('');
			$table->text('note', 65535)->nullable();
			$table->text('reservation_note', 65535)->nullable();
			$table->text('data', 65535)->nullable();
			$table->integer('origin');
			$table->decimal('discount')->default(0.00);
			$table->decimal('advance')->default(0.00);
			$table->decimal('subtotal')->default(0.00);
			$table->decimal('total')->default(0.00);
			$table->timestamps();
			$table->char('status', 3)->default('CNF');
			$table->boolean('review', 1)->default('0');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reservations');
	}
}
