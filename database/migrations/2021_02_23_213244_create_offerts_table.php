<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOffertsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offerts', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('property_id')->nullable()->index('property_id');
			$table->string('title', 100)->nullable();
			$table->text('description', 65535)->nullable();
			$table->char('code', 8)->nullable();
			$table->string('value', 20)->nullable();
			$table->string('condition', 20)->nullable();
			$table->date('start_date')->nullable();
			$table->date('end_date')->nullable();
			$table->timestamps();
			$table->char('status', 3)->default('CNF');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('offerts');
	}
}
