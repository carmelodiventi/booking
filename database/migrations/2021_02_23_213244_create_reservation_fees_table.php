<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReservationFeesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reservation_fees', function(Blueprint $table)
		{
			$table->integer('reservation_id')->nullable()->index('fk_reservationFees');
			$table->integer('fee_id')->nullable();
			$table->decimal('fee_paid', 10)->nullable();
			$table->boolean('percentage', 1)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reservation_fees');
	}

}
