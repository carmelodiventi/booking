<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePortalsCommissionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('portals_commission', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('reservation_id')->unsigned()->nullable()->index('fk_portal_commission_idx');
			$table->integer('portal_id')->nullable();
			$table->string('order_source_id', 100)->nullable();
			$table->decimal('cost', 10)->nullable();
			$table->decimal('percentage', 10, 1)->nullable();
			$table->string('transaction_code', 50)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('portals_commission');
	}

}
