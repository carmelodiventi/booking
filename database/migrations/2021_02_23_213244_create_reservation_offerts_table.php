<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReservationOffertsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reservation_offerts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('reservation_id')->nullable()->index('fk_reservation_offert');
			$table->string('code', 20)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reservation_offerts');
	}

}
