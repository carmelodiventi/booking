<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFeesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fees', function(Blueprint $table)
		{
			$table->foreign('property_id', 'fees_ibfk_1')->references('id')->on('properties')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fees', function(Blueprint $table)
		{
			$table->dropForeign('fees_ibfk_1');
		});
	}

}
