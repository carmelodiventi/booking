<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToReservationFeesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reservation_fees', function(Blueprint $table)
		{
			$table->foreign('reservation_id', 'fk_reservation_fee')->references('id')->on('reservations')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reservation_fees', function(Blueprint $table)
		{
			$table->dropForeign('fk_reservation_fee');
		});
	}

}
