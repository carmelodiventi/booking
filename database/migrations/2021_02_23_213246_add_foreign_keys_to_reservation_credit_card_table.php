<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToReservationCreditCardTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reservation_credit_card', function(Blueprint $table)
		{
			$table->foreign('reservation_id', 'fk_reservation_credit_card')->references('id')->on('reservations')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reservation_credit_card', function(Blueprint $table)
		{
			$table->dropForeign('fk_reservation_credit_card');
		});
	}

}
