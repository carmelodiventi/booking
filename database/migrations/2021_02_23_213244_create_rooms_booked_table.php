<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoomsBookedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rooms_booked', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('property_id')->index('fk_rooms_booked_properties_idx');
			$table->integer('room_id');
			$table->integer('reservation_id')->index('fk_rooms_booked_idx');
			$table->text('reservation_data', 65535)->nullable();
			$table->integer('rooms_type_id');
			$table->date('check_in');
			$table->date('check_out');
			$table->integer('guests')->nullable();
			$table->decimal('price')->default(0.00);
			$table->string('rate_desc', 200)->nullable();
			$table->integer('rate_id')->nullable();
			$table->timestamps();
			$table->char('status', 3)->default('CNF');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rooms_booked');
	}

}
