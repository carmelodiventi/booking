<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRoomsBookedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rooms_booked', function(Blueprint $table)
		{
			$table->foreign('property_id', 'fk_rooms_booked_properties')->references('id')->on('properties')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('reservation_id', 'fk_rooms_booked_reservations')->references('id')->on('reservations')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rooms_booked', function(Blueprint $table)
		{
			$table->dropForeign('fk_rooms_booked_properties');
			$table->dropForeign('fk_rooms_booked_reservations');
		});
	}

}
