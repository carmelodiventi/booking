<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRoomsTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rooms_type', function(Blueprint $table)
		{
			$table->foreign('property_id', 'rooms_type_ibfk_1')->references('id')->on('properties')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rooms_type', function(Blueprint $table)
		{
			$table->dropForeign('rooms_type_ibfk_1');
		});
	}

}
