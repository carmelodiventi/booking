<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('property_id')->index('fk_customers_idx');
			$table->string('name', 50)->nullable();
			$table->string('surname', 50)->nullable();
			$table->string('phone', 50)->nullable();
			$table->string('phone_2', 50)->nullable();
			$table->string('phone_3', 50)->nullable();
			$table->string('email', 100)->nullable();
			$table->string('password')->nullable();
			$table->string('reset_token', 100)->nullable();
			$table->string('address', 100)->nullable();
			$table->string('country', 50)->nullable();
			$table->string('city', 50)->nullable();
			$table->string('province', 50)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers');
	}

}
