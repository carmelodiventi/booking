<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReservationAdjustmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reservation_adjustments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('reservation_id')->nullable()->index('fk_reservation_REC');
			$table->string('name', 100)->nullable();
			$table->decimal('price', 10)->nullable();
			$table->timestamps();
			$table->char('status', 3)->nullable()->default('CNF');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reservation_adjustments');
	}

}
