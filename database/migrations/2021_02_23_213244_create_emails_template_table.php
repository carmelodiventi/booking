<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmailsTemplateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('emails_template', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('property_id')->nullable();
			$table->string('code', 32)->nullable();
			$table->string('subject', 128)->nullable();
			$table->text('body', 65535)->nullable();
			$table->char('lang', 2)->nullable();
			$table->char('status', 3)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('emails_template');
	}

}
