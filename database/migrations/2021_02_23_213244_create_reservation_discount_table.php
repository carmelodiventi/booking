<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReservationDiscountTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reservation_discount', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('reservation_id')->nullable()->index('fk_reservation_REC');
			$table->decimal('amount', 10)->nullable();
			$table->timestamps();
			$table->char('status', 3)->nullable()->default('CNF');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reservation_discount');
	}

}
