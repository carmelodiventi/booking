<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOffertsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('offerts', function(Blueprint $table)
		{
			$table->foreign('property_id', 'offerts_ibfk_1')->references('id')->on('properties')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('offerts', function(Blueprint $table)
		{
			$table->dropForeign('offerts_ibfk_1');
		});
	}

}
