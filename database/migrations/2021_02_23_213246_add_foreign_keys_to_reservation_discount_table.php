<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToReservationDiscountTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reservation_discount', function(Blueprint $table)
		{
			$table->foreign('reservation_id', 'reservation_discount_ibfk_1')->references('id')->on('reservations')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reservation_discount', function(Blueprint $table)
		{
			$table->dropForeign('reservation_discount_ibfk_1');
		});
	}

}
