<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRatesTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rates_type', function(Blueprint $table)
		{
			$table->foreign('property_id', 'fk_rate_types')->references('id')->on('properties')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rates_type', function(Blueprint $table)
		{
			$table->dropForeign('fk_rate_types');
		});
	}

}
