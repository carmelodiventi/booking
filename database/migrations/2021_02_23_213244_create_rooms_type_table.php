<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoomsTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rooms_type', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('type');
			$table->decimal('units', 2, 0)->nullable();
			$table->decimal('capacity', 2, 0);
			$table->text('description', 65535);
			$table->char('gender', 2)->nullable();
			$table->char('private', 5)->nullable();
			$table->boolean('isDisabled')->default(0);
			$table->integer('ma_room_id');
			$table->integer('property_id')->nullable()->index('property_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rooms_type');
	}

}
