<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fees', function (Blueprint $table) {
			$table->integer('id', true);
			$table->integer('property_id')->nullable()->index('property_id');
			$table->string('name', 45)->nullable();
			$table->decimal('value', 10)->nullable();
			$table->boolean('percentage', 1)->default('0');
			$table->timestamps();
			$table->char('status', 3)->nullable()->default('CNF');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fees');
	}
}
